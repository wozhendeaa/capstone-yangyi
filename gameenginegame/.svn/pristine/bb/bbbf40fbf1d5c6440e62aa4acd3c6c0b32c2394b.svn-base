#include "KinectColor.h"
#include <KinectModule\KinectDepth.h>
#include <MemoryDebug.h>

KinectColor::KinectColor(void)
{
	colorPool = new RGBQUAD[colorWidth * colorHeight];	
	colorDepth = new byte[KinectDepth::depthWidth * KinectDepth::depthHeight * 3];
	depthPoints = new ColorSpacePoint[KinectDepth::depthWidth * KinectDepth::depthHeight];
}

void KinectColor::updateColor()
{
	auto colorFrameReader = KinectSensor::GetSensor()->getColorFrameReader();

	if(colorFrameReader != nullptr )
	{
		IColorFrame* colorFrame = nullptr;
		long long dt = 0;
		auto hr = colorFrameReader->AcquireLatestFrame(&colorFrame);

		if(SUCCEEDED(hr))
		{
			IFrameDescription* description = nullptr;
			ColorImageFormat imageFormat = ColorImageFormat_None;
			unsigned bufferSize = 0;
			RGBQUAD* cBuffer = nullptr;
			int width = 0;
			int height = 0;

			hr = colorFrame->get_RelativeTime(&dt);

			if (coordinateMapper == nullptr)
			{
				KinectSensor::GetSensor()->
					GetKinectDevice()->
					get_CoordinateMapper(&coordinateMapper);
			}

			if(SUCCEEDED(hr))
			{
				hr = colorFrame->get_FrameDescription(&description);
			}

			if(SUCCEEDED(hr))
			{
				hr = description->get_Width(&width);
			}

			if(SUCCEEDED(hr))
			{
				hr = description->get_Height(&height);
			}

			if(SUCCEEDED(hr))
			{
				hr = colorFrame->get_RawColorImageFormat(&imageFormat);
			}

			if(SUCCEEDED(hr))
			{
				if(imageFormat == ColorImageFormat_Bgra)
				{
					hr = colorFrame->AccessRawUnderlyingBuffer(&bufferSize, 
						reinterpret_cast<byte**>(cBuffer));
				}
				else if ( colorPool != nullptr)
				{
					cBuffer = colorPool;
					bufferSize = width * height * sizeof(RGBQUAD);
					hr = colorFrame->CopyConvertedFrameDataToArray(bufferSize, 
						reinterpret_cast<byte*>(cBuffer), ColorImageFormat_Bgra);
				}
			}

			SafeRelease(description);

		}

		SafeRelease(colorFrame);
	}
}

byte* KinectColor::getColorData() const
{ 
	return reinterpret_cast<byte*>(colorPool);
}


byte* KinectColor::GetDepthColorData(UINT16* depth)
{

	if (depth != nullptr && coordinateMapper != nullptr)
	{
		coordinateMapper->MapDepthFrameToColorSpace(KinectDepth::depthWidth * KinectDepth::depthHeight,
			depth,
			KinectDepth::depthWidth * KinectDepth::depthHeight,
			depthPoints);
	
		for (size_t i = 0; i < KinectDepth::depthWidth * KinectDepth::depthHeight; i++)
		{
			if (depth[i] < 4500 && depth[i] > 500)
			{	
				//DepthSpacePoint p;
				//p.X = static_cast<int>(i % KinectDepth::depthWidth + .5f);
				//p.Y = static_cast<int>((i - p.X) / KinectDepth::depthWidth + .5f);
				//auto index = static_cast<int>(p.X + p.Y * KinectDepth::depthWidth);
				//auto p = depthPoints[i];				

				//ColorSpacePoint c;
				//coordinateMapper->MapDepthPointToColorSpace(p, depth[i], &c);
				//c.X = c.X * KinectDepth::depthWidth / colorWidth;
				//c.Y = c.Y * KinectDepth::depthHeight / colorHeight ;

			//auto depthValue = depth[i] ;

			//if (c.X >= 0 && c.X < colorWidth && c.Y > 0 && c.Y < colorHeight)
			//if (depthValue > 400 && depthValue < 4000)
			//{
				auto c = depthPoints[i];
				auto x = std::floorf(c.X - .5f);
				auto y = std::floorf(c.Y - .5f);
				if (x >= 0 && x < colorWidth && y >= 0  && y < colorHeight)
				{
					auto colorIndex = static_cast<int>(x + y * colorWidth);

					colorDepth[i * 3 + 0] = colorPool[colorIndex].rgbRed;
					colorDepth[i * 3 + 1] = colorPool[colorIndex].rgbGreen;
					colorDepth[i * 3 + 2] = colorPool[colorIndex].rgbBlue;
				}
			}
		}
	}

	return colorDepth;
}




void KinectColor::shutDown()
{
	if (colorPool != nullptr)
	{
		delete[] colorPool;
		colorPool = nullptr;
	}


	if (colorDepth != nullptr)
	{
		delete[] colorDepth;
		colorDepth = nullptr;
	}

	if (depthPoints != nullptr)
	{
		delete[] depthPoints;
		depthPoints = nullptr;
	}

}


KinectColor::~KinectColor(void)
{
	shutDown();
}
