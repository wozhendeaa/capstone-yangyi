#include <GUI\AppWindow.h>
static const int SLIDER_HEIGHT = 100;
static const int SLIDER_WIDTH = 300;


AppWindow::AppWindow(void)
{
	setMinimumSize(1920, 1080);
	debug_menu = new DebugMenu;	
	show();
}

void AppWindow::addGLWindow(GLWindow* window)
{
	windowLayout = new QVBoxLayout(this);
	windowLayout->setDirection(QBoxLayout::Direction::TopToBottom);
	setLayout(windowLayout);
	windowLayout->addWidget(debug_menu);
	windowLayout->addWidget(window);
}

void AppWindow::keyPressEvent(QKeyEvent* e)
{
	auto is_menu_on = debug_menu->isVisible();
	
	if(e->key() == 0x60)
	{
		debug_menu->setVisible(!is_menu_on);
	} 
}

void AppWindow::mouseMoveEvent(QMouseEvent* e)
{
	e;
}

void AppWindow::setupGUI()
{

}

AppWindow::~AppWindow(void)
{
	
	delete debug_menu;
	debug_menu = nullptr;

}
