#include "GLWindow.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <ObjLoader\ObjLoader.h>
#include <MemoryDebug.h>
#include <Random.h>
#include <QtGui\qfiledialog.h>
#include <memory>

using Neumont::ShapeData;
using Neumont::ShapeGenerator;
using Neumont::Vertex;

#define FPS_MODE_ON
#define HEADER_SIZE sizeof(unsigned int) * 2
#define VARIABLE_MISSING 0xfdfdfdfd
#define VARIABLE_UNINITIALIZED 0xfeeefeee
char* astarOffset = 0;


GLWindow::GLWindow()
{
	renderer = new Renderer;	
	setMinimumHeight(900);
	setMinimumWidth(1600);
	isReady = false;
}


void GLWindow::mousePressEvent(QMouseEvent* e)
{ 
	renderer->current_cam->MouseUpdate(vec2(e->x(), e->y()));
}

void GLWindow::mouseMoveEvent(QMouseEvent* e)
{
#ifdef FPS_MODE_ON
	renderer->current_cam->MouseUpdate(vec2(e->x(), e->y()));
#endif
}

bool GLWindow::rayIntersectSphere(const vec3& ray, const vec3& target_position, const float& radius)
{
	bool intersect = false;

	auto l0 = renderer->current_cam->position;
	auto c_vec = target_position - l0;
	auto e = glm::dot(c_vec, ray) * ray;
	
	auto c_squared = std::powf(glm::length(c_vec), 2);
	auto e_squared =  std::powf(glm::length(e), 2);
	auto r_squared = radius * radius;

	intersect = c_squared - e_squared <= r_squared;

	return intersect;
}


vec3 GLWindow::castRay(const float& x, const float& y)
{
	vec2 screen_center(width() / 2.0f, height() / 2.0f);
	vec2 screen_clicked(x, y);

	vec4 result = vec4(screen_clicked - screen_center, -1.0f, 1.0f);
	result.x = result.x * 2 / width();
	result.y = -result.y * 2 / height();
	
	auto cam = renderer->cams[0];
	auto proj = renderer->Matrices.projection;
	auto ray_eye = glm::inverse(proj) * result;
	ray_eye = vec4(ray_eye.x, ray_eye.y, -1.0f, 0.0f);

	auto l = glm::normalize(vec3(glm::inverse(cam->GetWorldToViewMat()) * ray_eye));

	return l;
}

float GLWindow::RayIntersectPlaneDistance(const vec3& n, const vec3& l, const vec3& p0, vec3& l0)
{
	float denom = glm::dot(n, l);
	float d = -1.0f;

    if (denom < 0) {
        vec3 ray = p0 - l0;
        d = glm::dot(ray, n) / denom; 
    }

    return d;
}

bool GLWindow::IsReady()
{
	return isReady;
}

void GLWindow::initializeGL()
{
	auto result = glewInit();

	glEnable(GL_DEPTH_TEST);

	if(result != GLEW_OK)
	{
		fprintf(stderr,"Errir:s", glewGetString(GLEW_OK));
	}
	isReady = true;

}

void GLWindow::loadLevel(const char* path)
{
	std::ifstream in_stream(path, std::ios::in | std::ios::binary);
	if(in_stream.good())
	{
		//read header
		auto size = HEADER_SIZE;
		auto header_data = new char[size];

		in_stream.seekg(0, std::ios::beg);
		in_stream.read(header_data, size);

		auto geo_num = *TO_TYPE(uint*, header_data);
		auto offset = header_data + sizeof(uint);

		auto geo_size = *TO_TYPE(uint*, offset);
	    offset += sizeof(uint);

		num_game_nodes = *TO_TYPE(uint*, offset);
		offset += sizeof(uint);

		auto num_connection = *TO_TYPE(uint*, offset);
		delete [] header_data;

		//read geometry
		char* geo_data = new char[geo_size];
		in_stream.read(geo_data, geo_size);
		offset = renderer->deserializeRenderer(geo_data, geo_num, geo_size);
		delete [] geo_data;

		//read astar
		auto astar_size = num_game_nodes * sizeof(GameNode) + num_connection * sizeof(GameConnection);
		auto all_data = new char[astar_size];
		in_stream.read(all_data, astar_size);
		in_stream.close();

		readAstars(all_data, num_game_nodes);


	}
}

void GLWindow::readAstars(const char* file_address, const uint& num_node)
{
	game_nodes = (GameNode*) file_address;
	//auto connections = (GameConnection*) (file_address + num_node * sizeof(GameNode));

	for (uint i = 0; i < num_node; i++)
	{
		auto& node = game_nodes[i];
		auto connection_offset = (unsigned int)node.connections;
		node.connections = (GameConnection*)(file_address + connection_offset);
		
		for (uint j = 0; j < node.num_connections; j++)
		{
			auto& con = node.connections[j];
			auto node_offset = (unsigned int)con.target;
			con.target = (GameNode*)( file_address + node_offset);
		}
	}
}

void GLWindow::PressKey()
{
	if(GetAsyncKeyState(0x57))
	{
		renderer->current_cam->moveForward();
	}

	if(GetAsyncKeyState(0x53))
	{
		renderer->current_cam->moveBackForward();
	}

	if(GetAsyncKeyState(0x41) )
	{
		renderer->current_cam->moveLeft();
	}

	if(GetAsyncKeyState(0x44))
	{
		renderer->current_cam->moveRight();
	}

	if(GetAsyncKeyState( Qt::Key::Key_R))
	{
		renderer->current_cam->moveUp();
	}

	if(GetAsyncKeyState(Qt::Key::Key_F))
	{
		renderer->current_cam->moveDown();
	}
	
}

void GLWindow::DefaultDraw()
{
	glViewport(0, 0, width(), height());
	//glClearColor(23,213,23, 255);
	
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	
}

void GLWindow::paintGL()
{
	////render scene
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glViewport(0, 0, width(), height());
	//glClearColor(255, 0,0,1);
	//glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	//renderer->Draw();
}


GLWindow::~GLWindow()
{
	if(renderer != nullptr)
	{
		renderer->shutdown();
		delete renderer;
		renderer = nullptr;
	}
#ifdef DEBUG_ON
	DebugShapesManager::getInstance()->shutDown();
#endif

}
