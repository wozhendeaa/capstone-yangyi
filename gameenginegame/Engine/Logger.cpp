//#include "Logger.h"
//
//
//vector <std::string> Logger::logList;
//vector <SEVERITY> Logger::severityList;
//
//Logger::Logger(void){}
//Logger::~Logger(void){}
//
//void Logger::StringReplace(string& str, const string& from, const string& to)
//{
//	size_t pos = 0;
//	do
//	{
//		pos = str.find(from, pos);
//		if( pos != string::npos )
//		{
//			str.replace( pos, from.length(), to);
//			pos += to.length();
//		}
//	} while (pos != string::npos);
//}
//
//string Logger::Sanitize(string str)
//{
//	StringReplace( str, "I" , "Info" );
//	StringReplace( str, "W" , "Warning" );
//	StringReplace( str, "E" , "Error" );
//	StringReplace( str, "S" , "Severe" );
//	StringReplace( str, ">" , "&lt" );
//	StringReplace( str, "<" , "&gt" );
//
//	return str;
//}
//
//void Logger::Log( SEVERITY severity, const char* message,  const char * logFile,int logLine)
//{
//	message;
//	std::stringstream ss;
//	ss << logFile << "(" << logLine << ") - " << Sanitize("message");
//	std::string logEntry;
//	logEntry = ss.str();
//
//	logList.push_back(logEntry);
//	severityList.push_back(severity);
//}
//void Logger::shutDown()
//{
//	WriteFile();
//}
//void Logger::WriteFile()
//{
//	std::ofstream myFile("Logging.html",ios::trunc);
//	//myFile.open("Logging.html",ios::trunc);
//	myFile << "<!DOCTYPE html>" << std::endl << "<html>"
//		<< std::endl << "<head>" << std::endl << "<title>Log File</title>" << std::endl << "</head>"
//		<< std::endl << "<body bgcolor = '#FFFFFF' >" << std::endl << "<h2>Log File</h2>";
//		//<< std::endl << THE_LIST;
//	for(unsigned int i = 0; i < logList.size(); i ++)
//	{
//		switch (severityList[i])
//		{
//		case _INFO_:
//				myFile <<  "<font color=\"#FFFFFF\">";
//				break;
//
//			case _WARNING_:
//				myFile <<  "<font color=\"#FFFFFF\">";
//				break;
//
//			case _ERROR_:
//				myFile <<  "<font color=\"#FFFFFF\">";
//				break;
//
//			case _SEVERE_:
//				myFile <<  "<font color=\"#FFFFFF\">";
//				break;
//		}
//		myFile << logList[i].c_str() << "</font><br>";
//	}
//	myFile << "</body>" << std::endl << "</html>";
//	myFile.close();
//}
//

#include "Logger.h" 

//#define LOG_PRINT(f) \

Logger::Logger(void)
{
}

Logger::~Logger(void)
{
}
#if LOGGING_ON
int Logger::VDebugPrintF(const char * format, va_list argList)
{
	const unsigned int MAX_CHARS = 1023;
	static char s_buffer[MAX_CHARS +1];
	int charsWritten = vsnprintf_s(s_buffer,MAX_CHARS, format, argList);
	s_buffer[MAX_CHARS] = '\0';

	OutputDebugStringA(s_buffer);
	return charsWritten;
}
int  Logger::DebugPrintF(const char * format, ...)
{
	va_list argList;
	va_start (argList,format);
	int charsWritten = VDebugPrintF(format,argList);
	va_end(argList);

	return charsWritten;
}

void Logger:: VerboseDebugPrintF(SEVERITY severity,const char * format,int lineNumber, const char * fileName ...)
{
	stringstream ss;
	ss<<lineNumber;
	string fullFormat;
	fullFormat.append(Severity::toString(severity)).append(fileName).append("(").append(ss.str().c_str()).append(")").append(format).append("\n");
	if(severity >= SEVERITY::_INFO_)
	{
		logInfo.push_back(fullFormat);
		va_list argList;
		va_start (argList,format);
		VDebugPrintF(fullFormat.c_str(),argList);
		va_end(argList);
	}
}

 void Logger::createFile()
{
	ofstream outStream(LOG_FILE_NAME, ios::trunc);
	stringstream ss;
	for(unsigned int i = 0; i < logInfo.size();i++)
	{
		string color;
		std::size_t found;
		if((found = logInfo[i].find(Severity::toString(SEVERITY::_INFO_))) != string::npos)
			color = "#000000";
		if((found = logInfo[i].find(Severity::toString(SEVERITY::_WARNING_))) != string::npos)
			color = "#FFFF00";
		if((found = logInfo[i].find(Severity::toString(SEVERITY::_ERROR_))) != string::npos || (found = logInfo[i].find(Severity::toString(SEVERITY::_SEVERE_))) != string::npos)
			color = "#FF0000";

		ss<< "<p " << "style= \"color:"<< color <<"\" > " <<Sanitize(logInfo[i]) << "</p>" ;
	}
	outStream.write(ss.str().c_str(),ss.str().size());
}

string Logger::Sanitize(string str)
{
	StringReplace( str, "<" , "&lt" );
	StringReplace( str, ">" , "&gt" );

	return str;
}

void Logger::StringReplace(string& str, const string& from, const string& to)
{
	size_t pos = 0;
	do
	{
		pos = str.find(from, pos);
		if( pos != string::npos )
		{
			str.replace( pos, from.length(), to);
			pos += to.length();
		}
	} while (pos != string::npos);
}

const char * Logger::LOG_FILE_NAME = "LogFile.html";
#endif