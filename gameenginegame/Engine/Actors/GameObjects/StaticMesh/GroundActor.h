#pragma once
#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <Actors\Components\MeshComponents\MeshRenderer.h>
class ENGINE_SHARED GroundActor
	: public Actor
{
	void Init() override;
public:
	GroundActor(void);
	~GroundActor(void);

	void Destroy() override;
	//void Update(float dt) override;
};

