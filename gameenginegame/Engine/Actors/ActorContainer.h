#pragma once

#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <MemoryDebug.h>
#include <RenderComponents\Renderer.h>
class ENGINE_SHARED ActorContainer
{
	typedef std::map<std::string, Actor*> Actors;
	Actors actorContainer;
	static uint actorCount;
public:
	ActorContainer(void);
	~ActorContainer(void);

	void AddActor(Actor* actor);
	void RemoveActor(Actor* actor);
	
	void Update(float dt);
	void Draw(BatEngine::Renderer* r, GLuint passIndex = 0);

	void Destroy();

	int size();

	Actor* Get(std::string);

	std::map<std::string, Actor*>& GetContainer();


	template<class ActorType>
	std::weak_ptr<ActorType>
		GetActor(Actor_id id)
	{
		auto actor = actorContainer.find(id);

		if(actor != actorContainer.end())
		{
			Actor base(actor->second);
			auto subType = std::tr1::static_pointer_cast<ActorType>(base);
			return std::weak_ptr<ActorType>(subType);
		}

		return std::weak_ptr<ActorType>();
	}


};

