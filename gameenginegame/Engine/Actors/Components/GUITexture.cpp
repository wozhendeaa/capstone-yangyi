#include "GUITexture.h"
GUITexture::GUITexture(
	BatEngine::Renderer* r, 
	unsigned shaderIndex,
	const FREE_IMAGE_FORMAT& fmt,
	const char* filePath, 
    float x,
    float y,
    float width  ,
    float height ,
	bool isNearPlane   ,
	const char* uniformName,
	bool flipHorizontal,
	bool flipVertical)
{
	TextureInfo gui;
	gui.addColorTexture(filePath, 
	fmt,
	r->shaders[shaderIndex]->program_id);

	guiTexture = r->GenGuiTexture(shaderIndex,
		gui,
		x,
		y,
		uniformName,
		width, 
		height,
		isNearPlane,
		flipHorizontal,
		flipVertical);
}

void GUITexture::Draw(BatEngine::Renderer* r, GLuint passIndex)
{
	r->Draw(guiTexture, passIndex);
}


Component_id GUITexture::GetComponentId() const
{
	static Component_id id = 
		typeid(GUITexture).name();
	return id;
}

void GUITexture::Destroy()
{
	delete guiTexture;
	guiTexture = nullptr;
}

GUITexture::~GUITexture(void)
{
	Destroy();
}
