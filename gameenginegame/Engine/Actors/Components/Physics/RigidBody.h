#pragma once
#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <Physics\PxPhysicsWorld.h>
using physx::PxRigidDynamic;
using physx::PxContactPairHeader;
using physx::PxContactPair;
using physx::PxU32;
using physx::PxTriggerPair;
using physx::PxConstraintInfo;
using physx::PxActor;

class ENGINE_SHARED RigidBody
	: public ActorComponent
{
	bool isInitialized;
	bool isKinematic;
	vec3 m_scale;
	quat quaternion;
	vec3 position;

	void MakeBody();
public:
	PxRigidDynamic* body;
	PxMaterial* material;
	RigidBody(){}
	explicit RigidBody(physx::PxPhysicsWorld* world,
		float staticFric = 0.5f,
		float dynamicFric = 0.45f,
		float rest = 0.8f,
		bool isKinematic = false);

	~RigidBody(void);


	void Update(float dt) override;
	void Destroy() override;

	Component_id GetComponentId() const override; 

	void InitializeWithBox(physx::PxPhysicsWorld* sdk,
						vec3& scale,
						quat& quat,
						vec3& pos,
						float density = 1.0f,
						vec3 offset = vec3(0.0f));

	void InitializeWithSphere(physx::PxPhysicsWorld* sdk,
						float& radius,
						quat& quat,
						vec3& pos,
						float density = 1.0f);

	void InitializeWithCapsule(physx::PxPhysicsWorld* sdk,
		 vec3& pos,
		 quat quat = quat(),
		 float radius = 0.5f,
		 float height = 1.8f,
		 float density = 1.0f);


	void SetBodyUserData(void* data);

	void SetEnableGravity(bool enabled);

	void MakeBodyKinematic();

	void MakeBodyDynamic();

	void SetCollisionEnabled(bool enabled);

	bool IsTrigger();

	bool IsKinematic();
};

