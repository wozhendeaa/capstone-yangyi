#include "InputComponent.h"
#include <MemoryDebug.h>


InputComponent::InputComponent(fastdelegate::FastDelegate0<> updateInput)
{
	delegation = updateInput;
}

Component_id InputComponent::GetComponentId() const
{
	static Component_id id = 
		(typeid(InputComponent).name());
	return id;
}

void InputComponent::Update(float dt)
{
	dt;
	delegation();
}

void InputComponent::Destroy() 
{

}

InputComponent::~InputComponent(void)
{

}
