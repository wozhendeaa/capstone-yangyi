#pragma once
#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <Delegation\fastdelegate.h>
class ENGINE_SHARED InputComponent :
	public ActorComponent
{
	fastdelegate::FastDelegate0<> delegation;
public:
	InputComponent(fastdelegate::FastDelegate0<> updateInput);
	~InputComponent(void);

	void Update(float dt) override;
	void Destroy() override;
	Component_id GetComponentId() const override; 

};

