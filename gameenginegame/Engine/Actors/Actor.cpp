#include "Actor.h"
#include <iostream>
#include <MemoryDebug.h>
#include "Components\MeshComponents\MeshRenderer.h"
class MeshRenderer;

 Actor::Actor(std::string actorName)
{
	a_id = reinterpret_cast<unsigned>(typeid(this).name());
	name = actorName;	
	m_transform =	  new glm::mat4();
	m_scale =		  new vec3(1.0f);
	m_rotation =	  new glm::quat();
	m_translation = new glm::vec3();
}

 bool Actor::Init(ActorComponents&& c)
 {
	 a_components = c;
	 c.clear();
	 return true;
 }

 void Actor::Update(float dt)
 {
	 for(const auto& c : a_components)
	 {
		 if(c.second.get() != nullptr)
		 {
			c.second->Update(dt);
		 }
	 }

	 for (const auto& c : m_children)
	 {
		 if (c.second != nullptr)
		 {
			 c.second->setTransform(*c.second->m_scale,
				 *m_rotation,
				 *m_translation);
		 }
	 }

 }
 void Actor::Attach(Actor* actor)
 {
	 assert(actor != nullptr);
	 auto actor_id = actor->a_id;
	 auto temp = m_children.find(actor_id);

	 if (temp == m_children.end())
	 {
		 actor->parent = this;
		 auto pair = std::pair<Actor_id, Actor*>(actor_id, actor);
		 m_children.insert(pair);
	 }
	 else
	 {
		 temp->second = actor;
	 } 
 }

 void Actor::Draw(BatEngine::Renderer* r, GLuint passIndex)
 {
	  for(const auto& c : a_components)
	 {
		 c.second->Draw(r, passIndex);
	 }
 }


void Actor::Destroy()
 {
	 for(const auto& c : a_components)
	 {
		 c.second->Destroy();
		 //delete c.second;
	 }

	 a_components.clear();

	 if(m_transform != nullptr)
	 {
		 delete m_transform;
		 m_transform = nullptr;
	 }

	 if(m_scale != nullptr)
	 {
		 delete m_scale;
		 m_scale = nullptr;
	 }

	 if(m_rotation != nullptr) 
	 {
		 delete m_rotation;
		 m_rotation = nullptr;
	 }

	 if(m_translation != nullptr) 
	 {
		 delete m_translation;
		 m_translation = nullptr;
	 }


}

void Actor::AddComponent(StrongActorComponent comp)
{
	assert(comp != nullptr);
	auto actor_id = comp->GetComponentId();
	auto temp = a_components.find(actor_id);
	
	if(temp == a_components.end())
	{
		comp->SetOwner(this);
		auto pair = std::pair<Component_id, 
					StrongActorComponent>(actor_id, comp);	

		a_components.insert(pair);
	}
	else 
	{
		temp->second = comp;
	}
}


Actor::~Actor(void)
{
	Destroy();
}
