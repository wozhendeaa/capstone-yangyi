#pragma once
#pragma warning( disable : 4251)
#pragma warning (disable:4100)

#include <ExportHeader.h>
#include <map>
#include <memory>
#include <cassert>
#include <glm\glm.hpp>
#include <glm\gtx\quaternion.hpp>
#include <string>
#include <Renderer.h>
#include <..\PhysX\include\PxPhysicsAPI.h>

class Actor;
class ActorComponent;

typedef unsigned Actor_id;
typedef std::string Component_id;
typedef std::shared_ptr<ActorComponent> StrongActorComponent;
typedef std::weak_ptr<ActorComponent> WeakActorComponent;

using glm::quat;
using BatEngine::Renderer;
using BatEngine::Renderable;


class ENGINE_SHARED ActorComponent
{
protected:
	Actor* parent;

public:
	virtual ~ActorComponent(){}
	virtual void Destroy(){}
	virtual void Update(float dt) {dt;}
	virtual void Draw(BatEngine::Renderer* r, GLuint passIndex = 0){ r; }
	virtual Component_id GetComponentId() const =0;
private:
	friend class Actor;
	void SetOwner(Actor* p) {parent = p;}
};

class ENGINE_SHARED Actor
{
	typedef std::map<Component_id, StrongActorComponent> ActorComponents;
	std::map<Actor_id, Actor*> m_children;
	Actor* parent = nullptr;
	
	Actor_id a_id;
	ActorComponents a_components;

protected:
	virtual void Init(){}
public: 
	std::string name;
	glm::mat4* m_transform;
	glm::vec3 m_offset;
	glm::vec3* m_scale;
	glm::quat* m_rotation;
	glm::vec3* m_translation;
	bool disposable = false;

	explicit Actor(std::string actorName = "");
	~Actor(void);
	
	virtual bool Init(ActorComponents&& a_components);
	virtual void Destroy();	
	virtual void Attach(Actor* actor);
	virtual void Draw(BatEngine::Renderer* r , GLuint passIndex = 0);
	virtual void Update(float dt);
	virtual bool IsDisposable(){ return disposable; }

	virtual void							onContact(Actor* other){}
	virtual void							onTrigger(Actor* other){}
	virtual void							onConstraintBreak(physx::PxConstraintInfo*, physx::PxU32) {}
	virtual void							onWake(physx::PxActor**,  physx::PxU32) {}
	virtual void							onSleep(physx::PxActor**, physx::PxU32){}

	void setTransform(vec3 pscale = vec3(1.0f), 
		quat q = quat(),
		vec3 pos = vec3())
	{
		*m_scale = pscale;
		*m_rotation = q;
		*m_translation = pos;
		auto scale = glm::scale(glm::mat4(), pscale);
		auto rotation = glm::mat4_cast(q);
		auto translation = glm::translate(mat4(), pos);
		*m_transform = translation * rotation * scale;
	}

	void setTransform(vec3 pscale = vec3(1.0f), 
		mat4 rot = mat4(),
		vec3 pos = vec3())
	{
		*m_scale = pscale;
		*m_rotation = glm::quat_cast(rot);;
		*m_translation = pos;
		auto scale = glm::scale(glm::mat4(), pscale);
		auto rotation = rot;
		auto translation = glm::translate(mat4(), pos);
		*m_transform = translation * rot * scale;
	}

	void setTOffsetransform(vec3 pscale = vec3(1.0f), 
		mat4 rot = mat4(),
		vec3 pos = vec3(),
		vec3 offset = vec3())
	{
		*m_scale = pscale;
		*m_rotation = glm::quat_cast(rot);
		*m_translation = pos;

		auto offsetMat = glm::translate(mat4(), offset);
		auto scale = glm::scale(mat4(), pscale);
		auto rotation = rot;
		auto translation = glm::translate(mat4(), pos);
		
		m_offset = offset;

		*m_transform = translation * 
			rotation *
			offsetMat *
			scale;
	}

	virtual Actor_id GetTypeId() const {return a_id;}

	template<class ComponentType>
	std::weak_ptr<ComponentType> GetComponent(Component_id id = typeid(ComponentType).name())
	{
		if(a_components.size() > 0 )
		{
			auto comp = a_components.find(id);
			if(comp != a_components.end())
			{
				StrongActorComponent base(comp->second);
				auto sub = std::tr1::static_pointer_cast<ComponentType>(base);
				return std::weak_ptr<ComponentType>(sub);
			}
		}
		return std::weak_ptr<ComponentType>();
	}

	void AddComponent(StrongActorComponent comp);
};

