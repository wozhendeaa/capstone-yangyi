#include "ActorContainer.h"

uint ActorContainer::actorCount = 0;
ActorContainer::ActorContainer(void)
{

}

Actor* ActorContainer::Get(std::string key)
{
	return (actorContainer[key]);
}

std::map<std::string, Actor*>& ActorContainer::GetContainer()
{
	return actorContainer;
}


void ActorContainer::AddActor(Actor* actor)
{
	assert(actor != nullptr);
	auto actor_name = actor->name;	

	if(actor_name.length() <= 0)
	{
		actor_name = std::string("actor")
			.append(std::to_string(actorCount++));
		actor->name = actor_name;
	}
	
	auto temp = actorContainer.find(actor_name);

	if(temp == actorContainer.end())
	{
		auto pair = std::pair<std::string, 
					Actor*>(actor_name, actor);	

		actorContainer.insert(pair);
	}
	else 
	{
		temp->second = actor;
	}
}

void ActorContainer::RemoveActor(Actor* actor)
{
	assert(actor != nullptr);
	actorContainer.erase(actor->name);
	actor->Destroy();

}

void ActorContainer::Destroy()
{
	for(const auto& a : actorContainer)
	{
		a.second->Destroy();
		delete a.second;
	}

	actorContainer.clear();
}

int ActorContainer::size()
{
	return actorContainer.size();
}


void ActorContainer::Update(float dt)
{
	for(const auto& a : actorContainer)
	{
		a.second->Update(dt);
	}
}

void ActorContainer::Draw(BatEngine::Renderer* r,
	GLuint passIndex)
{
	for(const auto& a : actorContainer)
	{
		a.second->Draw(r, passIndex);
	}
}

ActorContainer::~ActorContainer(void)
{

}
