#include "ParticleSystem.h"
#include <MemoryDebug.h>

ParticleSystem::ParticleSystem(physx::PxPhysicsWorld *w,
	Renderer* r,
	int maxParticles,
	bool perParticleOffset) 
	: world(w),
	renderer(r),
	m_maxParticles(maxParticles)

{
	ps = world->GetSDK()->createParticleSystem(maxParticles, perParticleOffset);
	m_indexPool = physx::PxParticleExt::createIndexPool(maxParticles);
	m_positions.resize(m_maxParticles);
	m_velocities.resize(m_maxParticles);
}

ParticleSystem::~ParticleSystem()
{

}

void ParticleSystem::SetRenderable(TextureInfo texture,
	vec3 scale,
	ParticleType type)
{
	switch (type)
	{
		case BILLBOARD:
			InitRendererWithBillBoard(scale, texture);
			break;
		case CUBE:
			InitRendererWithCube(scale, texture);
			break;
		case SHPERE:
			InitRendererWithShpere(scale, texture);
			break;
		default:
			break;
	}
}

void ParticleSystem::InitRendererWithBillBoard(vec3 scale, 
	TextureInfo texture)
{
	delete particleRenderer;
	particleRenderer = renderer->GenBillboard(2,
		texture,
		*m_translation,
		scale.x,
		scale.y);

	particleRenderer->enable_blending = true;
}

void ParticleSystem::InitRendererWithCube(vec3 scale, TextureInfo texture)
{

}

void ParticleSystem::InitRendererWithShpere(vec3 scale, TextureInfo texture)
{

}



void ParticleSystem::Destroy()
{
	Actor::Destroy();
	ps->releaseParticles(m_numParticles, physx::PxStrideIterator<PxU32>(&m_indicies[0]));
	ps->release();

	delete emitter;
	delete particleRenderer;
	
	m_positions.~vector();
	m_velocities.~vector();
	m_indicies.~vector();
	m_indexPool->freeIndices();
	m_indexPool->release();
}


void ParticleSystem::InitMaxMotionDistance(float val)
{
	if (ps != nullptr)
	{
		ps->setMaxMotionDistance(val);
	}
}

void ParticleSystem::InitRestOffset(float val)
{
	if (ps != nullptr)
	{
		ps->setRestOffset(val);
	}
}

void ParticleSystem::InitContactOffset(float val)
{
	if (ps != nullptr)
	{
		ps->setContactOffset(val);
	}
}

void ParticleSystem::InitGridSize(float val)
{
	if (ps != nullptr)
	{
		ps->setGridSize(val);
	}
}

void ParticleSystem::InitEnableGPUAccleration(bool enabled)
{
	if (ps != nullptr)
	{
		ps->setParticleBaseFlag(physx::PxParticleBaseFlag::eGPU, enabled);
	}
}

void ParticleSystem::InitEnableTwoWayCollision(float val)
{
	if (ps != nullptr)
	{
		ps->setParticleBaseFlag(physx::PxParticleBaseFlag::eCOLLISION_TWOWAY, val);

	}
}

void ParticleSystem::InitParticleSystem(
	PxVec3 pos,
	float p_positionOffset,
	PxVec3 vel,
	float p_velocityOffset,
	float maxMotionDistance,
	float restOffset,
	float contactOffset,
	bool enableTwoWayCollision,
	bool enableGPU,
	float gridSize)
{
	if (ps != nullptr)
	{
		InitMaxMotionDistance(maxMotionDistance);
		InitRestOffset(restOffset);
		InitContactOffset(contactOffset);
		InitEnableTwoWayCollision(enableTwoWayCollision);
		InitEnableGPUAccleration(enableGPU);
		InitGridSize(gridSize);		
		SetProperties();
		*m_translation= physx::ToVec3(pos);


		emitter = new ParticleEmitter(pos, vel, PxVec3(.0f));
		positionOffset = p_positionOffset;
		velocityOffset = p_velocityOffset;
		emitter->InitPositions(m_positions, positionOffset);
		emitter->InitVelocities(m_velocities, velocityOffset);
		world->GetPhysicsWorld()->addActor(*ps);
	}
}

void ParticleSystem::CreateParticles(
	bool useGravity,
	physx::PxReal mass )
{
	assert(emitter != nullptr);
	m_indicies.resize(m_maxParticles);
	m_positions.resize(m_maxParticles);
	m_velocities.resize(m_maxParticles);
	
	physx::PxStrideIterator<PxU32> indexData(&m_indicies[0]);
	m_indexPool->allocateIndices(m_maxParticles, indexData);

	physx::PxParticleCreationData data;
	data.numParticles = m_maxParticles;
	data.indexBuffer = indexData;

	assert(m_positions.size() > 0 && m_velocities.size() > 0 && m_indicies.size() > 0);
	
	data.indexBuffer = physx::PxStrideIterator<const PxU32>(&m_indicies[0]);
	data.positionBuffer = physx::PxStrideIterator<const PxVec3>(&m_positions[0]);
	data.velocityBuffer = physx::PxStrideIterator<const PxVec3>(&m_velocities[0]);
	
	ps->setParticleBaseFlag(physx::PxParticleBaseFlag::eGPU, true);
	ps->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, !useGravity);

	ps->setParticleMass(mass);
	
	//default renderable
	TextureInfo particle;
	particle.addColorTexture("Resources\\Texture\\star02.png",
		FIF_PNG,
		renderer->shaders[2]->program_id);

	InitRendererWithBillBoard(vec3(.1f, .1f, 1.0f), particle);

	ps->createParticles(data);
}

void ParticleSystem::SetProperties(float resitution,
	float staticFriction,
	float dynamicFriction,
	float damping)
{
	ps->setRestitution(resitution);
	ps->setStaticFriction(staticFriction);
	ps->setDynamicFriction(dynamicFriction);
	ps->setDamping(damping);
	ps->setStaticFriction(staticFriction);
}


void ParticleSystem::SetEnableGravity(bool enabled)
{
	ps->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, !enabled);
}

void ParticleSystem::SetEnableParticleSystem(bool enabled)
{
	ps->setActorFlag(physx::PxActorFlag::eDISABLE_SIMULATION, !enabled);

}

void ParticleSystem::setUseLifetime(bool use)
{
	useLifeTime = use;
}

bool ParticleSystem::useLifetime()
{	
	return useLifeTime;
}

void ParticleSystem::setLifetime(float lt, float offsetRange)
{
	uniformLife = lt;
	lifeOffset = offsetRange;
	useLifeTime = true;
	m_lifes.resize(m_maxParticles);
	emitter->InitLifeTime(m_lifes, lt, offsetRange);
}

void ParticleSystem::SetLoop(bool loops)
{
	loop = loops;
}

bool ParticleSystem::GetIfLoop() const
{
	return loop;
}


void ParticleSystem::SetPositions(physx::PxVec3& pos, float offsetRange)
{
	emitter->m_pos = pos;
	positionOffset = offsetRange;
	m_positions.resize(m_maxParticles);
	emitter->InitPositions(m_positions, offsetRange);
}

void ParticleSystem::SetVelocities(physx::PxVec3& vel, float offsetRange)
{
	emitter->m_velocity = vel;
	velocityOffset = offsetRange;
	m_velocities.resize(m_maxParticles);
	emitter->InitVelocities(m_velocities, offsetRange);
}


void ParticleSystem::SetParticletPosition(physx::PxVec3& pos, uint index, float offsetRange)
{
	emitter->m_pos = pos;
	positionOffset = offsetRange;
	//emitter->ComputePosition(m_positions[index], offsetRange);
}

void ParticleSystem::SetParticletVelocity(physx::PxVec3& vel, uint index, float offsetRange)
{
	emitter->m_velocity = vel;
	velocityOffset = offsetRange;
	//emitter->InitVelocities(m_velocities[index], offsetRange);
}


const std::vector<PxVec3>& ParticleSystem::getPositions()
{
	return m_positions;
}

const std::vector<float>&  ParticleSystem::getLifetimes()
{
	return m_lifes;
}

const std::vector<PxVec3>& ParticleSystem::getVelocities()
{
	return m_velocities;
}

const std::vector<physx::PxMat33>& ParticleSystem::getOrientations()
{
	return m_orientations;
}

void ParticleSystem::Update(float dt)
{
	auto particleData(ps->lockParticleReadData());
	auto posBuffer( particleData->positionBuffer);
	auto flagBuffer( particleData->flagsBuffer);
	//auto indexBuffer()
		
	if (particleData != nullptr && m_translation != nullptr)
	{
		emitter->m_pos = physx::ToPxVec3(*m_translation);
		for (size_t i = 0; i < particleData->validParticleRange; i++, ++posBuffer, ++flagBuffer)
		{
			bool remove = false;
			if (useLifeTime)
			{
				m_lifes[i] -= dt;

				remove = m_lifes[i] <= .0f;
			}

			if (remove && useLifeTime)
			{
				emitter->ComputeDuration(m_lifes[i], uniformLife, lifeOffset);
				emitter->ComputePosition(m_positions[i], positionOffset);
				emitter->ComputeVelocity(m_velocities[i], velocityOffset);
			}
			else if (flagBuffer->isSet(physx::PxParticleFlag::eVALID))
			{
				m_positions[i] = *posBuffer;
			}
		}

		particleData->unlock();
		physx::PxStrideIterator<PxVec3> newPosBuffer(&m_positions[0]);
		physx::PxStrideIterator<PxU32> indexData(&m_indicies[0]);
		physx::PxStrideIterator<PxVec3> velocityData(&m_velocities[0]);
		ps->setPositions(0, indexData, newPosBuffer);
		ps->setVelocities(0, indexData, velocityData);
	}
}


void ParticleSystem::Draw(Renderer* r, GLuint passIndex)
{
	//r->InstanceDraw(particleRenderer, m_numParticles);
	for (size_t i = 0; i < m_maxParticles; i++)
	{
		if (useLifeTime)
		{
			particleRenderer->visible = (m_lifes[i] > 0.0f);
		}

		particleRenderer->setTransform(vec3(1.0f),
			mat4(), physx::ToVec3(m_positions[i]));
		r->Draw(particleRenderer, passIndex);
	}
}

