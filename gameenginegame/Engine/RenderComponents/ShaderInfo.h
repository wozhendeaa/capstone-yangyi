#pragma once
#include <GL\glew.h>
#include <ExportHeader.h>
#define UNIFORM_NUM_MAX 40

enum ENGINE_SHARED ShaderUniformParameterType
{
	SHADER_Sampler2D,
	SHADER_Sampler2DShadow,
	SHADER_VEC3,
	SHADER_VEC2,
	SHADER_VEC4,
	SHADER_MAT3,
	SHADER_MAT4,
	SHADER_FLOAT,
	SHADER_BOOL,
};

struct ENGINE_SHARED ShaderUniformParameter
{
	GLuint program_id;
	GLuint parameter_location;	
	void* value;
	ShaderUniformParameterType uniform_type;
	GLchar* name;

};

struct ENGINE_SHARED ShaderInfo
{
	GLuint program_id;
	GLuint shaderInfoIndex;
};