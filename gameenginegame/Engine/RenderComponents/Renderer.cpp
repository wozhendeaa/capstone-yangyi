#include "Renderer.h"
#include "Vertex.h"
#include <fstream>
#include <iostream>
#include <MemoryDebug.h>
//temp include!!!
#include <Player\Player.h>
#define UNIFORM_MISSING 4294967295
#define TWO_PI 2 * 3.1415926
#define DEBUG_LIGHTING
#define BUFFER_SIZE 1048576 * 30

extern bool useLightViewGlobal = false;

namespace BatEngine
{

	void Renderer::initialize()
	{
		current_buffers_count = 0;
		current_geometry_count = 0;
		current_shader_count = 0;
		current_cams_count = 0;
		current_light_count = 0;
		current_uniform_count = 0;
		current_shadow_map_count = 0;
		current_global_texture_count = 0;
		currentShaderIndex = 0;
		data = new UIData;
		can_update = true;
		enableShadow = false;
		enableSoftShadow = false;
		softShadowTexSize = vec3(0.0f);

		for (uint i = 0; i < MAX_BUFFER_NUM; i++)	
		{
			buffers[i] = nullptr;
		}

		for (uint i = 0; i < MAX_SHADER_NUM; i++)	
		{
			shaders[i] = nullptr;
		}

		for (uint i = 0; i < MAX_UNIFORM_NUM; i++)	
		{
			uniforms[i] = nullptr;
		}

		for (uint i = 0; i < MAX_SHADER_NUM; i++)	
		{
			shadowMaps[i] = nullptr;
		}

		for (uint i = 0; i < MAX_GLOBAL_TEX_NUM; i++)	
		{
			global_textures[i] = nullptr;
		}

		Matrices.projection = glm::perspective(60.0f, ((float)width() / height()), 0.1f, 200.0f);
		cams[0] = new Camera;
		Matrices.world_view = cams[0]->GetWorldToViewMat();
		cams[0]->SetProjection(Matrices.projection);
		Matrices.biased_matrix = mat4(0.5f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.5f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.5f, 0.5f, 0.5f, 1.0f);	

		//	std::tr1::shared_ptr<Camera> cam_ptr(new Camera);

		//cams[1] = new Camera;
		current_cam = cams[0];

		TextureInfo::reset();
	}

	Renderer::Renderer()
	{
		initialize();
	}

	void Renderer::switchShader(GLuint shaderIndex, GLuint passIndex)
	{
		auto current_shader_id = shaders[shaderIndex]->program_id;
		glUseProgram(current_shader_id);
		currentShaderIndex = shaderIndex;

		if(passIndex == 0)
		{
			setNormalRenderUniform(current_shader_id);	
		}
		else if(passIndex == 1)
		{
			setShadowUniform(current_shader_id);
		}

		plusAllUniforms();
	}


	void Renderer::setShadowUniform(GLuint program_id)
	{
		Matrices.light_view = cams[1]->GetWorldToViewMat();

		Matrices.light_proj = Matrices.projection;
		GlobalUniformLocations.light_projc = glGetUniformLocation(program_id, "light_proj");
		GlobalUniformLocations.light_view = glGetUniformLocation(program_id, "light_view");

		glUniformMatrix4fv(GlobalUniformLocations.light_view, 1, GL_FALSE, &Matrices.light_view[0][0]);
		glUniformMatrix4fv(GlobalUniformLocations.light_projc, 1, GL_FALSE, &Matrices.light_proj[0][0]);
	}


	void Renderer::addUniform(ShaderUniformParameter* uniform)
	{
		uniform->parameter_location = glGetUniformLocation(uniform->program_id, uniform->name);
		uniforms[current_uniform_count++] = uniform;
	}

	void Renderer::setLevelEditorUniform(
		GLuint program_id)
	{
		glUseProgram(program_id);
		GlobalUniformLocations.mvp =	    glGetUniformLocation(program_id, "mvp");	

		auto mvp = Matrices.projection * Matrices.world_view * Matrices.world_model;
		glUniformMatrix4fv(GlobalUniformLocations.mvp, 1, GL_FALSE, &mvp[0][0]);

	}

	void Renderer::setNormalRenderUniform(GLuint program_id)
	{	
		//view related uniforms
		// light_vec = vec3(data->directional * 20.0f);
		//Matrices.light_view = glm::lookAt(light_vec, vec3(1.0f), vec3(0.0f, 1.0f, 0.0f));


		Matrices.light_proj = Matrices.projection;
		//Matrices.light_proj =  glm::ortho<float>(-20,20,-20,20,-20,40);
		//Matrices.world_view = Matrices.light_view;
		Matrices.world_view = current_cam->GetWorldToViewMat();
		GlobalUniformLocations.eye =	    glGetUniformLocation(program_id, "camera_position");	
		GlobalUniformLocations.projection = glGetUniformLocation(program_id, "projection");
		GlobalUniformLocations.world_view = glGetUniformLocation(program_id, "world_view");

		glUniformMatrix4fv(GlobalUniformLocations.world_view, 1, GL_FALSE, &Matrices.world_view[0][0]);
		glUniformMatrix4fv(GlobalUniformLocations.projection, 1, GL_FALSE, &Matrices.projection[0][0]);
		glUniform3fv(GlobalUniformLocations.eye, 1, &current_cam->position.x);

		//std::cout << current_cam->position.x << "." 
		//	<< current_cam->position.y << "," 
		//	<< current_cam->position.z << std::endl;

		//light related uniforms
		GlobalUniformLocations.directional_light = glGetUniformLocation(program_id, "directional_light");
		GlobalUniformLocations.light_position    = glGetUniformLocation(program_id, "lightPosition");
		GlobalUniformLocations.specular			 = glGetUniformLocation(program_id, "specular");
		GlobalUniformLocations.specular_power    = glGetUniformLocation(program_id, "specular_power");
		GlobalUniformLocations.diffuse           = glGetUniformLocation(program_id, "diffuseColor");
		GlobalUniformLocations.ambient			 = glGetUniformLocation(program_id, "ambient");
		GlobalUniformLocations.enable_lighting   = glGetUniformLocation(program_id, "enable_lighting");
		GlobalUniformLocations.global_color      = glGetUniformLocation(program_id, "color");

		glUniform3fv(GlobalUniformLocations.light_position,    1, &data->lightPosition.x);
		glUniform3fv(GlobalUniformLocations.directional_light, 1, &data->directional.x);
		glUniform3fv(GlobalUniformLocations.ambient,           1, &data->AmbientLightColor.r);
		glUniform3fv(GlobalUniformLocations.specular,          1, &data->SpecColor.x);
		glUniform3fv(GlobalUniformLocations.diffuse,           1, &data->Diffuse_color.r);
		glUniform1i(GlobalUniformLocations.specular_power,		   data->specExpo);
		glUniform1i(GlobalUniformLocations.enable_lighting,		   data->enable_lighting);

		//shadow related uniforms
		GlobalUniformLocations.shadow_map      = glGetUniformLocation(program_id, "shadow_map");
		GlobalUniformLocations.biased_matrix   = glGetUniformLocation(program_id, "biased_mat");	
		GlobalUniformLocations.light_projc     = glGetUniformLocation(program_id, "light_proj");
		GlobalUniformLocations.light_view      = glGetUniformLocation(program_id, "light_view");
		//Matrices.biased_matrix = glm::transpose(Matrices.biased_matrix);
		glUniformMatrix4fv(GlobalUniformLocations.biased_matrix, 1, GL_FALSE, &Matrices.biased_matrix[0][0]);
		glUniformMatrix4fv(GlobalUniformLocations.light_view, 1, GL_FALSE, &Matrices.light_view[0][0]);
		glUniformMatrix4fv(GlobalUniformLocations.light_projc, 1, GL_FALSE, &Matrices.light_proj[0][0]);
		//vertex color
		GlobalUniformLocations.enable_vertex_color = glGetUniformLocation(program_id, "vertex_color_on");

		//texture related uniforms
		GlobalUniformLocations.texture = glGetUniformLocation(program_id, "color_tex");
		GlobalUniformLocations.texture_alpha = glGetUniformLocation(program_id, "alpha_tex");
		GlobalUniformLocations.texture_spec = glGetUniformLocation(program_id, "spec_tex");
		GlobalUniformLocations.texture_normal = glGetUniformLocation(program_id, "normal_tex");
		GlobalUniformLocations.texture_ao = glGetUniformLocation(program_id, "ao_tex");
		GlobalUniformLocations.texture_noise = glGetUniformLocation(program_id, "noise_tex");
		GlobalUniformLocations.texture_useNormalLighting  = glGetUniformLocation(program_id, "userNormalMapLighting");
		GlobalUniformLocations.texture_useDiffuseMap      = glGetUniformLocation(program_id, "useDiffuseMap");
		GlobalUniformLocations.texture_useSpecMap           = glGetUniformLocation(program_id, "useSpecMap");
		GlobalUniformLocations.texture_useAlpha           = glGetUniformLocation(program_id, "useAlpha");
		GlobalUniformLocations.texture_useAOMap             = glGetUniformLocation(program_id, "useAOMap");
		GlobalUniformLocations.texture_noise_octave             = glGetUniformLocation(program_id, "octave");
		GlobalUniformLocations.texture_useNoiseAsHeightMap             = glGetUniformLocation(program_id, "useNoiseAsHeightMap");

		glUniform1f(GlobalUniformLocations.texture_noise_octave, data->octave);
		glUniform1i(GlobalUniformLocations.texture_useNoiseAsHeightMap, data->useNoiseAsHeightMap);
	}

	bool Renderer::canUpdate()
	{
		return can_update;
	}

	void Renderer::shutdown()
	{
	
			if(cams[0] != nullptr)
			{
				delete cams[0];	
				cams[0]	= nullptr;
			}

			for (uint i = 0; i < MAX_BUFFER_NUM; i++)	
			{
				if(buffers[i] != nullptr)
				{
					delete buffers[i];
					buffers[i] = nullptr;
				}
			}
			
			if (m_gBuffer != nullptr)
			{
				delete m_gBuffer;
				m_gBuffer = nullptr;
			}

			for (uint i = 0; i < MAX_SHADER_NUM; i++)	
			{
				if(shaders[i] != nullptr)
				{
					delete shaders[i];
					shaders[i] = nullptr;
				}
			}

			for (uint i = 0; i < MAX_SHADER_NUM; i++)	
			{
				if(uniforms[i] != nullptr)
				{
					delete uniforms[i];
					uniforms[i] = nullptr;
				}
			}

			//for (uint i = 0; i < current_shadow_map_count; i++)	
			//{
			//	if(shadowMaps[i] != nullptr)
			//	{
			//		delete shadowMaps[i];
			//		shadowMaps[i] = nullptr;
			//	}
			//}


			for (uint i = 0; i < MAX_GLOBAL_TEX_NUM; i++)	
			{
				if(global_textures[i] != nullptr)
				{
					delete global_textures[i];
					global_textures[i] = nullptr;
				}
			}


			enableShadow = false;
			can_update = false;
			enableSoftShadow = false;

			delete data;
			data = nullptr;

			//delete Player::a_m;
	}

	void Renderer::plusAllUniforms()
	{
		for (GLuint i = 0; i < current_uniform_count; i++)
		{
			setUniform(uniforms[i]);
		}
	}

	GeometryInfo* Renderer::addGeometry(
		const void* verts, GLuint numVerts, 
		GLuint* indices, GLuint numIndices,
		GLuint stride, GLuint indexingMode, 
		GLuint drawingMode,
		bool withTangent, bool serializable,
		GLenum usage,
		bool instanceDraw)
	{
		auto result = glewInit();
		if(result != GLEW_OK)
		{
			fprintf(stderr,"Errir:s", glewGetString(GLEW_OK));
			return nullptr;
		}
		bool buffer_found = false;
		auto geo_info = new GeometryInfo;
		auto total_size = stride * numVerts + sizeof(indices) * numIndices;

		for (uint i = 0; i < MAX_BUFFER_NUM && !buffer_found; i++)	
		{
			if(i == current_buffers_count)
			{
				if(buffers[i] == nullptr)
				{
					buffers[i] = new BufferInfo;
					buffers[i]->remaining_size = BUFFER_SIZE;
				}
				glGenBuffers(1, &buffers[i]->buffer_id);
				glBindBuffer(GL_ARRAY_BUFFER, buffers[i]->buffer_id);	
				glBufferData(GL_ARRAY_BUFFER, BUFFER_SIZE, 0, usage);
				current_buffers_count++;
			}

			buffer_found = (buffers[i]->remaining_size > total_size);

			if(buffer_found)
			{
				geo_info->buffer_id = buffers[i]->buffer_id;
				geo_info->offset = BUFFER_SIZE - buffers[i]->remaining_size;
				geo_info->index_offset = geo_info->offset + stride * numVerts;
				geo_info->total_size = total_size;			
				geo_info->index_mode = indexingMode;
				geo_info->drawing_mode = drawingMode;
				geo_info->num_vertex = numVerts;
				geo_info->num_index = numIndices;			
				geo_info->verts = const_cast<void*>(verts);
				geo_info->indices = (indices);
				geo_info->serializeable = serializable;
				geo_info->useInstanceDraw = instanceDraw;
				geo_info->bufferIndex = i;

				glBindBuffer(GL_ARRAY_BUFFER, geo_info->buffer_id);	
				glBufferSubData(GL_ARRAY_BUFFER, geo_info->offset, stride * numVerts, (void*)verts);
				glBufferSubData(GL_ARRAY_BUFFER,geo_info->index_offset, sizeof(indices) * numIndices, (void*)indices);
				glGenVertexArrays(1, &geo_info->vertex_array_id);
				buffers[i]->remaining_size -= total_size;
			}
		}

		if(buffer_found)
		{				
			if(withTangent)
			{
				addShaderStreamedParameter(0, VertexV2::POSITION_OFFSET, Renderer::ParameterType::PT_VEC3, geo_info, VertexV2::STRIDE, instanceDraw);
				addShaderStreamedParameter(1, VertexV2::UV_OFFSET, Renderer::ParameterType::PT_VEC2, geo_info, VertexV2::STRIDE, instanceDraw);
				addShaderStreamedParameter(2, VertexV2::NORMAL_OFFSET, Renderer::ParameterType::PT_VEC3, geo_info, VertexV2::STRIDE, instanceDraw);
				addShaderStreamedParameter(3, VertexV2::COLOR_OFFSET, Renderer::ParameterType::PT_VEC4, geo_info, VertexV2::STRIDE, instanceDraw);
				addShaderStreamedParameter(4, VertexV2::TANGENT_OFFSET, Renderer::ParameterType::PT_VEC3, geo_info, VertexV2::STRIDE, instanceDraw);

			}
			else
			{
				addShaderStreamedParameter(0, Vertex::POSITION_OFFSET, Renderer::ParameterType::PT_VEC3, geo_info, Vertex::STRIDE, instanceDraw);
				addShaderStreamedParameter(1, Vertex::UV_OFFSET, Renderer::ParameterType::PT_VEC2, geo_info, Vertex::STRIDE, instanceDraw);
				addShaderStreamedParameter(2, Vertex::NORMAL_OFFSET, Renderer::ParameterType::PT_VEC3, geo_info, Vertex::STRIDE, instanceDraw);
				addShaderStreamedParameter(3, Vertex::COLOR_OFFSET, Renderer::ParameterType::PT_VEC4, geo_info, Vertex::STRIDE, instanceDraw);
				addShaderStreamedParameter(4, VertexV2::TANGENT_OFFSET, Renderer::ParameterType::PT_VEC3, geo_info, Vertex::STRIDE, instanceDraw);
			}

		}
		else
		{
			geo_info = nullptr;
		}

		return geo_info;
	}

	void Renderer::addGlobalTexture(TextureInfo* tex)
	{
		if(current_global_texture_count < MAX_GLOBAL_TEX_NUM)
		{
			global_textures[current_global_texture_count++] = tex;
		}
	}

	Renderable* Renderer::addRenderable(
		GeometryInfo geometry,
		glm::mat4* whereMatrix,
		TextureInfo texture,
		GLuint howShaders,
		bool depth_test_on,
		bool vertex_color_on,
		bool serializable,
		bool enable_blending,
		bool recievesLight,
		bool isSkybox,
		bool textureColorOnly
		)
	{
		serializable;
		Renderable* renderable = new Renderable;

		renderable->visible = true;
		*renderable->geometry = geometry;
		renderable->where = whereMatrix;		
		*renderable->textureInfo = texture;
		renderable->numUniformParameters = 0;
		renderable->depth_test_on = depth_test_on;
		renderable->vertex_color_on = vertex_color_on;
		renderable->enable_blending = enable_blending;
		renderable->recievesLight = recievesLight;
		renderable->isSkyBox = isSkybox;	
		renderable->textureColorOnly = textureColorOnly;	
		renderable->setShaderForPass(0, howShaders);

		//delete whereMatrix;

		addRenderableUniformParameter(renderable, "enable_vertex_color", ShaderUniformParameterType::SHADER_BOOL, (char*)&renderable->vertex_color_on);
		addRenderableUniformParameter(renderable, "lightRotation", ShaderUniformParameterType::SHADER_MAT4, (char*)&renderable->rotation[0][0]);
		addRenderableUniformParameter(renderable, "world_model", ShaderUniformParameterType::SHADER_MAT4, (char*)&whereMatrix[0][0]);
		addRenderableUniformParameter(renderable, "recievesLight", ShaderUniformParameterType::SHADER_BOOL, (char*)&renderable->recievesLight);
		addRenderableUniformParameter(renderable, "isSkyBox", ShaderUniformParameterType::SHADER_BOOL, (char*)&renderable->isSkyBox);
		addRenderableUniformParameter(renderable, "tex_color_only", ShaderUniformParameterType::SHADER_BOOL, (char*)&renderable->textureColorOnly);

		return renderable;
	}

	void Renderer::addShaderStreamedParameter(
		GLuint layoutlocation, 
		GLuint attrib_offset,
		ParameterType psize,
		GeometryInfo* geo_info,
		GLuint bufferStride,
		bool useInstanceDraw)
	{
		auto comp = psize / sizeof(GLfloat);				
		glBindVertexArray(geo_info->vertex_array_id);
		glBindBuffer(GL_ARRAY_BUFFER, geo_info->buffer_id);
		glEnableVertexAttribArray(layoutlocation);
		glVertexAttribPointer(layoutlocation, comp, GL_FLOAT, GL_FALSE, bufferStride, (void*)(attrib_offset + geo_info->offset));
		if (useInstanceDraw)
		{
			//glVertexattridivisor(layoutlocation, 1);
		}
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geo_info->buffer_id);
	}

	GLuint Renderer::addRenderableUniformParameter(
		Renderable* renderable,
		const GLchar* name,
		ShaderUniformParameterType parameterType, 
		char* value)
	{
		auto program_id = shaders[renderable->howShaderIndex]->program_id;
		GLuint location = glGetUniformLocation(program_id, name);

		auto shader_para = ShaderUniformParameter();
		shader_para.name = const_cast<GLchar*>(name);
		shader_para.program_id = program_id;
		shader_para.parameter_location = location;
		shader_para.value = (value);
		shader_para.uniform_type = parameterType;

		auto index = renderable->numUniformParameters++;
		renderable->uniformParameters[index] = shader_para;

		return location;
	}

	void Renderer::Update(float dt)
	{
		dt;

	}


	void Renderer::setUniform(ShaderUniformParameter* para)
	{
		auto location = para->parameter_location;
		auto parameterType = para->uniform_type;

		if(location != UNIFORM_MISSING)
		{
			if(parameterType == ShaderUniformParameterType::SHADER_FLOAT)
			{
				glUniform1f(location, *(static_cast<GLfloat*>(para->value)));
			} 
			else if(parameterType == ShaderUniformParameterType::SHADER_MAT3)
			{
				glUniformMatrix3fv(location, 1, GL_FALSE, static_cast<GLfloat*>(para->value));
			} 
			else if(parameterType == ShaderUniformParameterType::SHADER_MAT4)
			{
				glUniformMatrix4fv(location, 1, GL_FALSE, static_cast<GLfloat*>(para->value));
			}
			else if(parameterType == ShaderUniformParameterType::SHADER_VEC2)
			{
				glUniform2fv(location, 1, static_cast<GLfloat*>(para->value));
			}
			else if(parameterType == ShaderUniformParameterType::SHADER_VEC3)
			{
				glUniform3fv(location, 1, static_cast<GLfloat*>(para->value));
			}
			else if(parameterType == ShaderUniformParameterType::SHADER_VEC4)
			{
				glUniform4fv(location, 1, static_cast<GLfloat*>(para->value));
			}
			else if(parameterType == ShaderUniformParameterType::SHADER_Sampler2D || parameterType == ShaderUniformParameterType::SHADER_Sampler2DShadow)
			{
				glUniform1i(location, static_cast<GLuint>(*(int*)para->value));
			}
			else if(parameterType == ShaderUniformParameterType::SHADER_BOOL)
			{		
				glUniform1i(location, *(bool*)para->value);
			}
		}
	}

	void Renderer::addUniform(
		GLchar* name,
		const GLuint& shaderID,
		const ShaderUniformParameterType& type,
		void* value)
	{
		auto uniform = new ShaderUniformParameter;
		uniform->name = name;
		uniform->program_id = shaderID;
		uniform->uniform_type = type;
		uniform->value = value;

		addUniform(uniform);
	}


	void Renderer::resetUniformByProgramid(GLuint program_id,
										   ShaderUniformParameter* uniform)
	{
		uniform->program_id = program_id;
		uniform->parameter_location = glGetUniformLocation(program_id, uniform->name);
	}



	void Renderer::Draw(Renderable* r,
						GLuint passIndex)
	{

		if(r != nullptr && r->visible)
		{
			auto error = glGetError();
			auto shaderIndex = r->getShaderIndexByPass(passIndex);

			auto currentProgramId = shaders[shaderIndex]->program_id;
			switchShader(shaderIndex, passIndex);
			 error = glGetError();
			
			if(r->textureInfo != nullptr)
			{
				r->textureInfo->bindColorTexture(r->textureInfo->colorUniformLocation);
				r->textureInfo->bindAlphaTexture(r->textureInfo->alphaUniformLocation);
				r->textureInfo->bindSpecTexture(r->textureInfo->specUniformLocation);
				r->textureInfo->bindNormalTexture(r->textureInfo->normalUniformLocation);
				r->textureInfo->bindAOTexture(r->textureInfo->aoUniformLocation);
				r->textureInfo->bindNoiseTexture(r->textureInfo->noiseUniformLocation);

				//r->textureInfo->bindColorTexture();
				//r->textureInfo->bindAlphaTexture();
				//r->textureInfo->bindSpecTexture();
				//r->textureInfo->bindNormalTexture();
				//r->textureInfo->bindAOTexture();
				//r->textureInfo->bindNoiseTexture();
				glUniform1i(GlobalUniformLocations.texture_useNormalLighting , r->textureInfo->userNormalMapLighting);
				glUniform1i(GlobalUniformLocations.texture_useDiffuseMap     , r->textureInfo->useDiffuseMap);
				glUniform1i(GlobalUniformLocations.texture_useSpecMap        , r->textureInfo->useSpecMap);
				glUniform1i(GlobalUniformLocations.texture_useAlpha          , r->textureInfo->useAlphaMap);
				glUniform1i(GlobalUniformLocations.texture_useAOMap          , r->textureInfo->useAOMap);
				glUniform1i(GlobalUniformLocations.texture_useNoiseMap       , r->textureInfo->useNoiseMap);
			}


			//set renderalbe uniforms
			for (uint i = 0; i < r->numUniformParameters; i++)
			{
				auto r_uniform = &r->uniformParameters[i];
				if(r_uniform->program_id != currentProgramId)
				{
					resetUniformByProgramid(currentProgramId, r_uniform);
				}

				setUniform(r_uniform);
				 error = glGetError();

			}

			//apply shadows
			for (GLuint i = 0; i < current_shadow_map_count && enableShadow; i++)
			{
				glActiveTexture(GL_TEXTURE0 + shadowMaps[i]->getColorTexUnit());
				glBindTexture(GL_TEXTURE_2D, shadowMaps[i]->color_tex_id);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
				if(shaderIndex == 1)
				{
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE);
				}
				else if(shaderIndex == 0)
				{
					glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);

				}

				glUniform1i(GlobalUniformLocations.shadow_map,
					shadowMaps[i]->color_tex_id);

				auto useShadowMapLocation = glGetUniformLocation(currentProgramId, "useShadowMap");
				glUniform1i(useShadowMapLocation, enableShadow);

				if(enableSoftShadow)
				{
					auto location = glGetUniformLocation(currentProgramId, "enableSoftShadow");
					glUniform1i(location, true);
				}
			}

			//apply global textures
			for (GLuint i = 0; i < MAX_GLOBAL_TEX_NUM; i++)
			{
				if(global_textures[i] != nullptr)
				{
					auto l = glGetUniformLocation(currentProgramId, global_textures[i]->name);
					global_textures[i]->bindColorTexture(l);
				}

			}

			if(r->depth_test_on)
			{
				glEnable(GL_DEPTH_TEST);
			}
			else 
			{
				glDisable(GL_DEPTH_TEST);
			}

			if(r->enable_blending)
			{
				glEnable(GL_BLEND);
				glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
				auto location = glGetUniformLocation(shaders[r->howShaderIndex]->program_id, "blend");
				glUniform1i(location, true);
				//glBlendEquation(GL_FUNC_ADD);
			}

			auto geo_info = r->geometry;
			glBindBuffer(GL_ARRAY_BUFFER, geo_info->buffer_id);		
			glBindVertexArray(geo_info->vertex_array_id);
			glDrawElements(geo_info->drawing_mode, geo_info->num_index, geo_info->index_mode, (void*)(geo_info->index_offset));	
			glDisable(GL_BLEND);
			auto location = glGetUniformLocation(shaders[r->howShaderIndex]->program_id, "blend");
			glUniform1i(location, false);
		}
	}

	void Renderer::GeometryPassDraw()
	{
		if (m_gBuffer == nullptr)
		{
			//m_gBuffer = new GBuffer;
		}
	}

	void Renderer::LightingPassDraw()
	{

	}


	void Renderer::AttachTextureToFramebuffer(GLuint& fbo_id, 
		TextureInfo* colorTexture,
		const GLuint& colorTexture_shaderIndex,
		TextureInfo* depthTexture,
		const GLuint& depthTexture_shaderIndex,
		const char* colorUniformName,
		const char* depthUniformName)
	{

		glGenFramebuffers(1, &fbo_id);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);

		if(depthTexture != nullptr)
		{
			depthTexture->addColorTexture(nullptr, FIF_BMP,
				shaders[depthTexture_shaderIndex]->program_id,
				depthUniformName);

			glGenTextures(1, &depthTexture->color_tex_id);
			glActiveTexture(GL_TEXTURE0 + depthTexture->getColorTexUnit());
			glBindTexture(GL_TEXTURE_2D, depthTexture->color_tex_id);

			glTexImage2D(GL_TEXTURE_2D, 
				0,
				GL_DEPTH_COMPONENT32, 
				depthTexture->width, 
				depthTexture->height, 
				0, 
				GL_DEPTH_COMPONENT, 
				GL_FLOAT, 
				0);

			GLfloat border[]={1.0f,0.0f,0.0f,0.0f};
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
			glTexParameterfv(GL_TEXTURE_2D,GL_TEXTURE_BORDER_COLOR, border);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT , 
				GL_TEXTURE_2D,
				depthTexture->color_tex_id,
				0);

			shadowMaps[current_shadow_map_count++] = depthTexture;
		}

		if(colorTexture != nullptr)
		{
			colorTexture->addColorTexture(nullptr, FIF_BMP, 
				shaders[colorTexture_shaderIndex]->program_id,
				colorUniformName);
			glGenTextures(1, &colorTexture->color_tex_id);
			glActiveTexture(GL_TEXTURE0 + colorTexture->getColorTexUnit());
			glBindTexture(GL_TEXTURE_2D, colorTexture->color_tex_id);

			glTexImage2D(GL_TEXTURE_2D, 
				0,
				GL_RGBA, 
				colorTexture->width,
				colorTexture->height,
				0,
				GL_RGBA,
				GL_UNSIGNED_BYTE,
				0);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
				GL_TEXTURE_2D, 
				colorTexture->color_tex_id,
				0);
		}

		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			fprintf(stderr, "Framebuffer goes wrong2");

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void Renderer::AttachTextureToHDRFramebuffer(GLuint& fbo_id, 
		TextureInfo* colorTexture,
		const GLuint& colorTexture_shaderIndex)
	{
		glGenFramebuffers(1, &fbo_id);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);

		GLuint temp;
		glGenTextures(1, &temp);
		glActiveTexture(GL_TEXTURE0 + 4);
		glBindTexture(GL_TEXTURE_2D, temp);


		glTexImage2D(GL_TEXTURE_2D, 
			0,
			GL_DEPTH_COMPONENT32, 
			colorTexture->width, 
			colorTexture->height, 
			0, 
			GL_DEPTH_COMPONENT, 
			GL_FLOAT, 
			0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT , 
			GL_TEXTURE_2D,
			temp,
			0);


		colorTexture->addColorTexture(nullptr, FIF_HDR, 
			shaders[colorTexture_shaderIndex]->program_id,
			"blur_tex");

		glGenTextures(1, &colorTexture->color_tex_id);
		glActiveTexture(GL_TEXTURE0 + colorTexture->getColorTexUnit());
		glBindTexture(GL_TEXTURE_2D, colorTexture->color_tex_id);

		glTexImage2D(GL_TEXTURE_2D, 
			0,
			GL_RGBA16F, 
			colorTexture->width,
			colorTexture->height,
			0,
			GL_RGBA,
			GL_FLOAT,
			0);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, 
			GL_TEXTURE_2D, 
			colorTexture->color_tex_id,
			0);


		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			fprintf(stderr, "Framebuffer goes wrong2");

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	Renderable* Renderer::GenGuiTexture(GLuint shaderIndex, 
		TextureInfo texture, 
		float& x,
		float& y,
		const char* uniformName,
		float width ,								   
		float height,
		bool isNearPlane ,
		bool flipHorizanontal,
		bool flipVertical    )
	{
		uniformName;
		//construct line vert data
		Neumont::Vertex v1;
		static float z_offset = 0.999999f;
		auto z = isNearPlane ? -0.9999999 : z_offset;
		//z_offset -= 0.01f;

		v1.position =  vec3(x - width, y - height, z);
		if(flipHorizanontal && flipVertical)
		{
			v1.uv = vec2(1.0f, 1.0f);
		} 
		else if(flipHorizanontal)
		{
			v1.uv = vec2(1.0f, 0.0f);
		}
		else if(flipVertical)
		{
			v1.uv = vec2(0.0f, 1.0f);
		}
		else
		{
			v1.uv = vec2(0.0f, 0.0f);
		}

		v1.color = vec4(1, 0 , 0 , 1);

		Neumont::Vertex v1_2;
		//v1_2.position =vec3(-1.0f, 1.0f, 0.9f);
		v1_2.position =vec3(x - width, y + height, z);
		if(flipHorizanontal && flipVertical)
		{
			v1_2.uv = vec2(1.0f, 0.0f);
		} 
		else if(flipHorizanontal)
		{
			v1_2.uv = vec2(1.0f, 1.0f);
		}
		else if(flipVertical)
		{
			v1_2.uv = vec2(0.0f, 0.0f);
		}
		else
		{
			v1_2.uv = vec2(0.0f, 1.0f);
		}

		v1_2.color = vec4(0, 1 , 0 , 1);

		Neumont::Vertex v2;
		//v2.position = vec3(1.0f, 1.0f, 0.9f);
		v2.position = vec3(x + width , y + height , z);
		if(flipHorizanontal && flipVertical)
		{
			v2.uv = vec2(0.0f, 0.0f);
		} 
		else if(flipHorizanontal)
		{
			v2.uv = vec2(0.0f, 1.0f);
		}
		else if(flipVertical)
		{
			v2.uv = vec2(1.0f, 0.0f);
		}
		else
		{
			v2.uv = vec2(1.0f, 1.0f);
		}

		v2.color = vec4(0, 0 , 1 , 1);

		Neumont::Vertex v2_2;
		//v2_2.position =  vec3(1.0f, -1.0f, 0.9f);
		v2_2.position =  vec3(x + width , y - height , z);
		if(flipHorizanontal && flipVertical)
		{
			v2_2.uv = vec2(0.0f, 1.0f);
		} 
		else if(flipHorizanontal)
		{
			v2_2.uv = vec2(0.0f, 0.0f);
		}
		else if(flipVertical)
		{
			v2_2.uv = vec2(1.0f, 1.0f);
		}
		else
		{
			v2_2.uv = vec2(1.0f, 0.0f);
		}

		v2_2.color = vec4(1, 1 , 0 , 1);

		const GLuint num_index = 6;
		Neumont::Vertex verts[4] = {v1, v1_2, v2, v2_2};
		uint indicies[num_index] = {0, 1, 2, 0 , 2, 3};

		std::auto_ptr<GeometryInfo> g (addGeometry(verts, 
			4, 
			indicies,
			num_index, 
			Neumont::Vertex::STRIDE,
			GL_UNSIGNED_INT, 
			GL_TRIANGLES,
			false,
			false,
			GL_STREAM_DRAW));		

		auto r = addRenderable(*g, 
			new mat4() ,
			texture,
			shaderIndex);

		return r;
	}


	Renderable* Renderer::GenBillboard(GLuint shaderIndex,
		TextureInfo texture,
		vec3& pos,
		float width,
		float height,
		bool instanceDraw,
		bool flipHorizanontal,
		bool flipVertical )
	{
		Neumont::Vertex v1;

		v1.position = vec3(-width * .5f, -height * .5f, pos.z);
		if (flipHorizanontal && flipVertical)
		{
			v1.uv = vec2(1.0f, 1.0f);
		}
		else if (flipHorizanontal)
		{
			v1.uv = vec2(1.0f, 0.0f);
		}
		else if (flipVertical)
		{
			v1.uv = vec2(0.0f, 1.0f);
		}
		else
		{
			v1.uv = vec2(0.0f, 0.0f);
		}

		v1.color = vec4(1, 0, 0, 1);

		Neumont::Vertex v1_2;
		//v1_2.position =vec3(-1.0f, 1.0f, 0.9f);
		v1_2.position = vec3(-width * .5f, height * .5f, pos.z);
		if (flipHorizanontal && flipVertical)
		{
			v1_2.uv = vec2(1.0f, 0.0f);
		}
		else if (flipHorizanontal)
		{
			v1_2.uv = vec2(1.0f, 1.0f);
		}
		else if (flipVertical)
		{
			v1_2.uv = vec2(0.0f, 0.0f);
		}
		else
		{
			v1_2.uv = vec2(0.0f, 1.0f);
		}

		v1_2.color = vec4(0, 1, 0, 1);

		Neumont::Vertex v2;
		//v2.position = vec3(1.0f, 1.0f, 0.9f);
		v2.position = vec3(width * .5f, height * .5f, pos.z);
		if (flipHorizanontal && flipVertical)
		{
			v2.uv = vec2(0.0f, 0.0f);
		}
		else if (flipHorizanontal)
		{
			v2.uv = vec2(0.0f, 1.0f);
		}
		else if (flipVertical)
		{
			v2.uv = vec2(1.0f, 0.0f);
		}
		else
		{
			v2.uv = vec2(1.0f, 1.0f);
		}

		v2.color = vec4(0, 0, 1, 1);

		Neumont::Vertex v2_2;
		//v2_2.position =  vec3(1.0f, -1.0f, 0.9f);
		v2_2.position = vec3(width * .5f, -height * .5f, pos.z);
		if (flipHorizanontal && flipVertical)
		{
			v2_2.uv = vec2(0.0f, 1.0f);
		}
		else if (flipHorizanontal)
		{
			v2_2.uv = vec2(0.0f, 0.0f);
		}
		else if (flipVertical)
		{
			v2_2.uv = vec2(1.0f, 1.0f);
		}
		else
		{
			v2_2.uv = vec2(1.0f, 0.0f);
		}

		v2_2.color = vec4(1, 1, 0, 1);

		const GLuint num_index = 6;
		Neumont::Vertex verts[4] = { v1, v1_2, v2, v2_2 };
		uint indicies[num_index] = { 0, 1, 2, 0, 2, 3 };

		std::auto_ptr<GeometryInfo> g(addGeometry(verts,
			4,
			indicies,
			num_index,
			Neumont::Vertex::STRIDE,
			GL_UNSIGNED_INT,
			GL_TRIANGLES,
			false, 
			false,
			GL_DYNAMIC_DRAW,
			instanceDraw));

		auto mat = new mat4;
		auto r = addRenderable(*g,
			mat,
			texture,
			shaderIndex);
		r->setTransform(vec3(1.0f), mat4(), pos);

		addRenderableUniformParameter(r,
			"billboard_center",
			ShaderUniformParameterType::SHADER_VEC3,
			(char*)r->translation);

		return r;
	}

	void Renderer::selectSubroutines(const char* vertex_sub,
		const char* frag_sub, 
		ShaderInfo* shader)
	{
		auto vertex_index = glGetSubroutineIndex(shader->program_id, GL_VERTEX_SHADER, vertex_sub);
		auto frag_index = glGetSubroutineIndex(shader->program_id, GL_FRAGMENT_SHADER, frag_sub);
		glUniformSubroutinesuiv(GL_VERTEX_SHADER, 1, &vertex_index);
		glUniformSubroutinesuiv(GL_FRAGMENT_SHADER, 1,&frag_index);
	}

	bool Renderer::checkStatus(GLint id, 
		PFNGLGETSHADERIVPROC object_func, 
		PFNGLGETSHADERINFOLOGPROC log_func, 
		GLenum type)
	{
		bool compile_succeeded = true;
		GLint result;
		object_func(id, type, &result);

		if(result == GL_FALSE)
		{

			GLint log_length;		
			object_func(id, GL_INFO_LOG_LENGTH, &log_length);

			GLchar* buffer = new GLchar[log_length];
			GLsizei size;
			log_func(id, log_length, &size, buffer);
			fprintf(stderr, buffer);

			delete [] buffer;

			compile_succeeded = false;
		}

		return compile_succeeded;
	}



	std::string Renderer::ReadShaderCode(const char* file_name)
	{
		std::ifstream meInput(file_name);

		if(!meInput.good())
		{
			std::cout << "no file found" << std::endl;
			exit(1);
		}

		return std::string(
			std::istreambuf_iterator<char>(meInput),
			std::istreambuf_iterator<char>()
			);
	}

	ShaderInfo* Renderer::createShaderInfo(const char* vertex_shader, const char* fragment_shader)
	{
		auto vertex_shader_id = glCreateShader(GL_VERTEX_SHADER);
		auto frag_shader_id = glCreateShader(GL_FRAGMENT_SHADER);
		GLuint program_id;

		const GLchar* adapter[1];
		auto code = ReadShaderCode(vertex_shader);
		adapter[0] = code.c_str();
		glShaderSource(vertex_shader_id, 1, adapter, nullptr);

		code = ReadShaderCode(fragment_shader);
		adapter[0] = code.c_str();
		glShaderSource(frag_shader_id, 1, adapter, nullptr);

		glCompileShader(vertex_shader_id);
		glCompileShader(frag_shader_id);


		if(checkStatus(vertex_shader_id, glGetShaderiv,glGetShaderInfoLog, GL_COMPILE_STATUS) &&
			checkStatus(frag_shader_id, glGetShaderiv, glGetShaderInfoLog, GL_COMPILE_STATUS))
		{
			program_id = glCreateProgram();	
			glAttachShader(program_id, vertex_shader_id);
			glAttachShader(program_id, frag_shader_id);

			glLinkProgram(program_id);

			if(checkStatus(program_id, glGetProgramiv, glGetProgramInfoLog, GL_LINK_STATUS))
			{
				glUseProgram(program_id);
			}
		}

		glDeleteShader(vertex_shader_id);
		glDeleteShader(frag_shader_id);

		auto shader = new ShaderInfo;
		//std::tr1::shared_ptr<ShaderInfo> shader_ptr(shader);
		shader->program_id = program_id;
		shader->shaderInfoIndex = current_shader_count;
		shaders[current_shader_count] = shader;
		return shaders[current_shader_count++];
	}

	void Renderer::serializeRenderer(std::ofstream& out_stream,
		GeometryInfo** geometries,
		const GLuint& geoCount)
	{
		uint geo_size = 0;
		for (uint i = 0; i < geoCount; i++)
		{
			auto geo = geometries[i];
			if(geo != nullptr && geo->serializeable)
			{
				out_stream.write(TO_BYTES(geo->num_vertex), sizeof(uint));
				out_stream.write(TO_BYTES(geo->num_index), sizeof(uint));
				geo_size = sizeof(uint) * 2;

				auto verts = (Neumont::Vertex*)geo->verts;
				for (GLuint j = 0; j < geo->num_vertex; j++)
				{
					auto vert = verts[j];
					out_stream.write(TO_BYTES(vert.position), sizeof(vec3));

					out_stream.write(TO_BYTES(vert.uv), sizeof(vec2));

					out_stream.write(TO_BYTES(vert.normal), sizeof(vec3));

					out_stream.write(TO_BYTES(vert.color), sizeof(vec4));

					geo_size += sizeof(vec3) * 2 + sizeof(vec2) + sizeof(vec4);

				}

				for (GLuint j = 0; j < geo->num_index; j++)
				{
					auto index = geo->indices[j];
					out_stream.write(TO_BYTES(index), sizeof(uint));
					geo_size += sizeof(uint);
				}
			}
		}

		long tellp = out_stream.tellp();
		auto add = out_stream.tellp();
		out_stream.seekp(sizeof(uint));
		add = out_stream.tellp();

		out_stream.write(TO_BYTES(geo_size), sizeof(uint));

		out_stream.seekp(tellp, std::ios::beg);

	}

	void Renderer::reset()
	{
		initialize();
	}

	char* Renderer::deserializeRenderer(char* _data, uint geo_num, uint geo_size)
	{
		geo_size;
		ObjLoader loader;
		for (uint i = 0; i < geo_num; i++)
		{
			_data = loader.loadLevelFromBinary(_data);
			auto obj = loader.getModel();

			//auto geo = addGeometry(obj.verts, obj.numVerts, obj.indices,  obj.numIndices, Vertex::STRIDE, GL_UNSIGNED_INT,GL_TRIANGLES);
			//addRenderable(geo, new mat4(), nullptr, 0);
		}

		return _data;
	}

	int Renderer::serializableGeoCount(
		GeometryInfo** geometries,
		const GLuint& geoCount)
	{
		int count = 0;

		for (uint i = 0; i < geoCount; i++)
		{
			auto g = geometries[i];

			if(g != nullptr && g->serializeable)
				count ++;
		}

		return count;
	}

	Renderer::~Renderer()
	{
		shutdown();
	}
}

//uniform mat4 light_proj;
//uniform mat4 light_view;
//uniform mat4 world_model;

//gl_Position = light_proj * light_view * world_model * vec4(VertexPosition, 1);
