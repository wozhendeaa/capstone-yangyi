#include "GeometryInfo.h"
#include <MemoryDebug.h>


GeometryInfo::GeometryInfo(glm::vec3&& scale)
{
	serializeable = true;
	this->scale = scale;
}

void GeometryInfo::addVaryingPositionBuffer(void* data,
	GLuint bufferSize,
	GLuint location)
{
	glBindBuffer(GL_ARRAY_BUFFER, buffer_id);
	glBufferSubData(GL_ARRAY_BUFFER, 
		total_size,
		bufferSize,
		data);
	
	glEnableVertexAttribArray(location);
	glVertexAttribPointer(location,
		3,
		GL_FLOAT,
		GL_FALSE, 
		sizeof(glm::vec3),
		(void*)total_size);
	glVertexAttribDivisor(location, 1);
	total_size += bufferSize;

}


GeometryInfo::~GeometryInfo(void)
{
}
