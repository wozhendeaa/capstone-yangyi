#pragma once
#include <BaseLight.h>

struct PointLight : public BaseLight
{
	glm::vec3 position;
	struct
	{
		float Constant;
		float Linear;
		float Exp;
	} Attenuation;


};

