#pragma once
#include <GL\glew.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <ExportHeader.h>
#include <RenderComponents\ShaderInfo.h>
#include <RenderComponents\TextureInfo.h>

#define MAX_UNIFORM_PARAMETERS 20
#define MAX_PASS_COUNT 5

namespace BatEngine
{
class Renderer;
class DebugShapeManager;
using glm::vec3;
using glm::mat4;
struct ENGINE_SHARED Renderable							
{
	TextureInfo* textureInfo;
	GeometryInfo* geometry;
	mat4* where;
	mat4* scale;
	mat4* rotation;
	mat4* translation;
	vec3 offset;

	GLuint howShaderIndex;

	bool visible;
	bool depth_test_on;
	bool vertex_color_on;
	bool serializable;
	bool enable_blending;
	bool recievesLight;
	bool isSkyBox;
	bool textureColorOnly;

	explicit Renderable();

	~Renderable() ;

	
	void setTransform(vec3 pscale = vec3(1.0f), 
		mat4 rot = mat4(),
		vec3 pos = vec3());

	void setTOffsetransform(vec3 pscale = vec3(1.0f), 
		mat4 rot = mat4(),
		vec3 pos = vec3(),
		vec3 offset = vec3());

	void setShaderForPass(GLuint passIndex, GLuint shaderIndex, bool incrementShaderCount = true);

	GLuint getShaderIndexByPass(GLuint passIndex);

	void shutDown();

	
private:
	friend class Renderer;
	friend class DebugShapeManager;
	GLuint numUniformParameters;
	GLuint numShaders;
	ShaderUniformParameter uniformParameters[MAX_UNIFORM_PARAMETERS];
	GLuint shaderIndexForPass[MAX_PASS_COUNT];
};

}