#include "Renderable.h"

namespace BatEngine
{
	Renderable::Renderable() : where(nullptr), 
		textureInfo(new TextureInfo), 
		geometry(new GeometryInfo),
		scale(new mat4),
		rotation(new mat4),
		translation(new mat4),
		numShaders(0),
		numUniformParameters(0)
	{

	}

	Renderable::~Renderable() 
	{
		shutDown();
	}


	void Renderable::setTransform(vec3 pscale , 
		mat4 rot,
		vec3 pos )
	{
		*scale = glm::scale(glm::mat4(), pscale);
		*rotation = rot;
		*translation = glm::translate(mat4(), pos);
		*where = *this->translation * *this->rotation * *this->scale;
	}

	
	void Renderable::setTOffsetransform(vec3 pscale, 
		mat4 rot ,
		vec3 pos ,
		vec3 offset)
	{
		auto offsetMat = glm::translate(mat4(), offset);
		*scale = glm::scale(mat4(), pscale);
		*rotation = rot;
		*translation = glm::translate(mat4(), pos);
		
		*where = *this->translation * 
			*this->rotation *
			*this->scale *
			offsetMat;
		
	}


	void Renderable::setShaderForPass(GLuint passIndex, 
		GLuint shaderIndex,
		bool incrementShaderCount)
	{
		if(passIndex >= 0 && passIndex < MAX_PASS_COUNT)
		{
			shaderIndexForPass[passIndex] = shaderIndex;
			numShaders = incrementShaderCount ? ++numShaders : numShaders;

			{
				howShaderIndex = shaderIndex;
			}
		}
	}


	GLuint Renderable::getShaderIndexByPass(GLuint passIndex)
	{
		passIndex = (passIndex < 0 || passIndex >= numShaders) ? 0 : passIndex;
		howShaderIndex = passIndex;
		return shaderIndexForPass[passIndex];
	}

	void Renderable::shutDown()
	{
		if(where != nullptr)
		{
			delete where;
			where = nullptr;
		}

		if(scale != nullptr)
		{
			delete scale;
			scale = nullptr;
		}

		if(rotation != nullptr) 
		{
			delete rotation;
			rotation = nullptr;
		}

		if(translation != nullptr) 
		{
			delete translation;
			translation = nullptr;
		}

		if(textureInfo != nullptr)
		{
			textureInfo->shutDown();
			delete textureInfo;
			textureInfo = nullptr;
		}
	}

}