//#include "Player.h"
//#include "PlayerState.h"
//#include "Qt\qdebug.h"
//
//AStarManagerDebug* Player::a_m = new AStarManagerDebug;
//
//Player::Player(void)
//{
//	translation = new mat4;
//	goal = start = velocity = vec3(0.0f);
//	model = Neumont::ShapeGenerator::makeArrow();
//	can_move = true;
//	position = vec3(0.0f, 0.0f, 0.0f);
//	speed = 1.0f;
//	distance_traveled = 0.0f;
//	current_goal_index = 0;
//	base = vec3(0.0f);
//
//	
//}
//
//void Player::setDirection()
//{
//	glm::vec3 theLine = goal - position;
//	glm::vec3 xBasis = glm::normalize(theLine);
//	glm::mat4 theRotation(glm::orientation(xBasis, vec3(0.0f, 0.0f, -1.0f)));
//
//	*translation = 
//		glm::translate(position) *
//		theRotation * 
//		glm::scale(vec3(1.0f));
//
//	velocity = xBasis * 0.1f * speed;
//}
//
//void Player::update()
//{
//	setDirection();
//
//	auto speed = glm::length(velocity);
//	if(canMove() && speed > 0.0001f)
//	{
//		position = velocity + position;
//		distance_traveled += speed;
//
//	} 
//	
//	if(a_m->path.size() > 0 && !canMove())
//	{
//		if(current_goal_index < path.size())
//		{
//			auto i = path.size();
//			goal = path[current_goal_index++];	
//			start = position;
//			distance_traveled = 0.0f;
//		}
//	}
//	
//}
//
//void Player::findNewPathToTarget(const vec3& from, const vec3& to)
//{
//	auto start = a_m->getNearestNode(from);
//	auto goal = a_m->getNearestNode(to);
//
//	if(start != nullptr && goal != nullptr)
//	{
//		path.clear();		
//
//		auto has_path = a_m->initializeAstars(start, goal);
//
//		if(has_path)
//		{
//			for (int i = 0; i < a_m->path.size(); i++)
//			{
//				path.push_back(a_m->path[i]->position);
//			}
//
//			path.push_back(to);
//		}
//	}
//}
//
//void Player::findPathToBase()
//{
//	auto start = a_m->getNearestNode(position);
//	auto goal = a_m->getNearestNode(base);
//	
//	if(start != nullptr && goal != nullptr)
//	{
//		path.clear();		
//
//		auto has_path = a_m->initializeAstars(start, goal);
//
//		if(has_path)
//		{
//			for (int i = 0; i < a_m->path.size(); i++)
//			{
//				path.push_back(a_m->path[i]->position);
//			}
//
//			path.push_back(base);
//		}
//	}
//}
//
//void Player::switchToReturnState()
//{
//	state = returning_state;
//	if(current_goal_index >= path.size())
//	{
//		findPathToBase();
//		current_goal_index = 0;
//	}
//	
//}
//
//void Player::switchToRetriveState()
//{
//	state = retriving_state;
//	
//	if(current_goal_index >= path.size())
//	{
//		findNewPathToTarget(position, flag->position);
//		current_goal_index = 0;
//	}
//}
//
//
//
//bool Player::canMove()
//{
//	can_move = distance_traveled < glm::length(goal - start);
//	return can_move;
//}
//
//Player::~Player(void)
//{
//}
