#include "Camera.h"
#include <MemoryDebug.h>
Camera::Camera() : viewDirection(0.0f, 0.0f, 1.0f)
	
{
	rotation_speed = 0.4f;
	move_speed = 0.1f;
	position = vec3(0.0f, 0.0f, 0.0f);
	prev_mouse_pos = vec2(0.0f);
	UP = vec3(0.0f, 1.0f, 0.0f);
}

mat4 Camera::GetWorldToViewMat() const
{
	return glm::lookAt(position, position + viewDirection, UP);
}

void Camera::SetProjection(const mat4& proj)
{
	projection = proj;
}


mat4 Camera::GetProjection() const
{
	return projection;
}

vec3 Camera::GetViewDirection()
{
	return viewDirection;
}


void Camera::MouseUpdate(const vec2& new_pos)
{
	auto delta = new_pos - prev_mouse_pos;
	mouseCoord = new_pos;

	if(glm::length(delta) < 50.0f)
	{
		shift_rotation = glm::cross(viewDirection, UP);
		viewDirection = mat3(glm::rotate(-delta.y * rotation_speed, shift_rotation)) *
			mat3(glm::rotate(-delta.x * rotation_speed, UP)) * viewDirection;		
	}

	prev_mouse_pos = new_pos;
}


void Camera::SetSceenSize(const float& w,
	const float& h)
{
	screen_height = h;
	screen_width = w;
}

float Camera::ScreenWidth() const
{
	return screen_width;
}

float Camera::ScreenHeight() const
{
	return screen_width;

}


void Camera::lookAt(const vec3& _viewDirection)
{
	viewDirection = _viewDirection;
}

vec2 Camera::GetMousePosition() const
{
	return mouseCoord;
}


void Camera::moveForward()
{
	position += move_speed * viewDirection;
}
void Camera::moveBackForward()
{

	position -= move_speed * viewDirection;
}
void Camera::moveLeft()
{
	shift_rotation = glm::cross(viewDirection, UP);
	position += -move_speed * shift_rotation;
}
void Camera::moveRight()
{
	shift_rotation = glm::cross(viewDirection, UP);
	position += move_speed * shift_rotation;
}
void Camera::moveUp()
{
	position += move_speed * UP;
}
void Camera::moveDown()
{
	position += -move_speed * UP;
}

vec3 Camera::GetForwardtDirection()
{
	auto dir = viewDirection;
	dir.y = 0.0f;
	return glm::normalize(dir);
}

vec3 Camera::GetLeftDirection()
{
	auto dir = -glm::cross(viewDirection, UP);
	dir.y = 0.0f;
	return glm::normalize(dir);
}


vec3 Camera::castRay(const float& x, 
	const float& y,
	const float width,
	const float height)
{
	vec2 screen_center(width / 2.0f, height / 2.0f);
	vec2 screen_clicked(x, y);

	vec4 result = vec4(screen_clicked - screen_center, -1.0f, 1.0f);
	result.x = result.x * 2 / width;
	result.y = -result.y * 2 / height;

	auto ray_eye = glm::inverse(projection) * result;
	ray_eye = vec4(ray_eye.x, ray_eye.y, -1.0f, 0.0f);

	auto l = glm::normalize(vec3(glm::inverse(GetWorldToViewMat()) * ray_eye));

	return l;
}