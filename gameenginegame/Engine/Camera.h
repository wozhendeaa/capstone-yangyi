#pragma once
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\transform.hpp>
#include <ExportHeader.h>
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3;
using glm::mat4;
class ENGINE_SHARED Camera
{
private:
	 vec3 UP;
	vec2 prev_mouse_pos;
	float rotation_speed;
	float move_speed;
	vec3 shift_rotation;
	friend class Editor;
	vec3 viewDirection;
	mat4 projection = mat4();
	vec2 mouseCoord;
	float screen_width = 1024.0f;
	float screen_height = 726.0f;

public:
	Camera(void);
	vec3 position;
	mat4 GetWorldToViewMat() const;
	void SetProjection(const mat4& proj);
	mat4 GetProjection() const;
	vec3 GetViewDirection();
	vec2 GetMousePosition() const;
	void MouseUpdate(const vec2& new_pos);
	void SetSceenSize(const float& screen_width,
		const float& screen_height);

	float ScreenWidth() const;
	float ScreenHeight() const;

	void moveForward();
	void moveBackForward();
	void moveLeft();
	void moveRight();
	void moveUp();
	void moveDown();
	void lookAt(const vec3& _viewDirection);

	vec3 GetForwardtDirection();
	vec3 GetLeftDirection();

	vec3 castRay(const float& x,
		const float& y, 
		const float width, 
		const float height);
};

