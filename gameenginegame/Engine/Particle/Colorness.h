#pragma once
#include "Core\Engine.h"
using Core::RGB;
struct Colorness
{
	int r, g, b;
public:
	RGB VaryColor(RGB color, int variance);
	RGB Brightness(RGB color, float brightness);
};