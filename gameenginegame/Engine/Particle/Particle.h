#pragma once
#include <cstdlib>
#include "Core\Engine.h"
#include <glm\glm.hpp>
namespace BatEngine
{
	class Particle
	{
		public:
			glm::vec2 position;
			glm::vec2 velocity;
			float lifetime;
			float brightness;
			Core::RGB color;
			int particleSize;

			Particle();
	};

}