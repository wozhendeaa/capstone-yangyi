#include "Colorness.h"
#include "Random.h"

RGB Colorness::VaryColor(RGB color, int variance)
{
	variance = variance > 255? 0 : variance;
	variance = variance < 0? 255 : variance;
	r = GetRValue(color) + int(Random::RandomInRange(variance, -variance + 1)) % 255;
	g = GetGValue(color) + int(Random::RandomInRange(variance, -variance + 1)) % 255;
	b = GetBValue(color) + int(Random::RandomInRange(variance, -variance + 1)) % 255;

	return RGB(r, g, b);
}

RGB Colorness::Brightness(RGB color, float brightness)
{
	r = GetRValue(color) * brightness;
	g = GetGValue(color) * brightness;
	b = GetBValue(color) * brightness;

	return RGB(r, g, b);
}
