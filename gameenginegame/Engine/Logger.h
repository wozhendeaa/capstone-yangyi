//#ifndef LOGGER
//#define LOGGER
//
//#include <vector>
//#include <fstream>
//#include <sstream>
//#include <string>
//#include "..\ExportHeader.h"
//
//using std::vector;
//using std::string;
//using std::ios;
//
//enum SEVERITY { _INFO_, _WARNING_, _ERROR_,_SEVERE_ };
//
//#define LOG( severity, message) Logger::Log( severity, message, __FILE__ ,__LINE__ )
//#define END_LOG Logger::shutDown();
//
//#pragma warning ( disable : 4100)
//
//class Logger
//{
//public:
//#if LOGGING_ON
//	ENGINE_SHARED Logger(void);
//	ENGINE_SHARED ~Logger(void);
//	ENGINE_SHARED static void StringReplace(string& str, const string& from, const string& to);
//	ENGINE_SHARED static string Sanitize(string str);
//	ENGINE_SHARED static void Log( SEVERITY severity , const char* message, const char * logFile, int logLine);
//	ENGINE_SHARED static void shutDown();
//#else
//	ENGINE_SHARED Logger(void){}
//	ENGINE_SHARED ~Logger(void){}
//	ENGINE_SHARED static void StringReplace(string& str, const string& from, const string& to){}
//	ENGINE_SHARED static string Sanitize(string str){}
//	ENGINE_SHARED static void Log( Severity severity, const char* message, const char * logFile, int logLine){}
//	ENGINE_SHARED static void shutDown(){}
//#endif
//
//private:
//#if LOGGING_ON
//	static vector <string> logList;
//	static vector <SEVERITY> severityList;
//	static void WriteFile();
//#endif
//};
//
//#pragma warning ( default : 4100)
//
//#endif

#pragma once

#include <stdio.h>
#include <windows.h>
#include <ExportHeader.h>
#include <string>
#include <sstream>
#include <vector>
#include <ostream>
#include <fstream>
#include "SeverityEnum.inl"
#include <iostream>

using std::ofstream;

using std::string;
using std::stringstream;
using std::vector;
using std::ios;
using std::ostream;

#define LOG(_severity,_message) Logger::getInstance().VerboseDebugPrintF(_severity,_message,__LINE__,__FILE__)

class Logger
{
public:
	ENGINE_SHARED Logger(void);
	ENGINE_SHARED ~Logger(void);
	ENGINE_SHARED static Logger& getInstance()
	{
		static Logger instance;
		return instance;
	}
	ENGINE_SHARED static const char * LOG_FILE_NAME;
	ENGINE_SHARED int VDebugPrintF(const char * format, va_list argList);

	ENGINE_SHARED int DebugPrintF(const char * format, ...);
	ENGINE_SHARED void createFile();
#if LOGGING_ON
	ENGINE_SHARED void VerboseDebugPrintF(SEVERITY severity, const char * format, int lineNumber, const char * fileName, ...);
	ENGINE_SHARED static string Sanitize(string str);
	ENGINE_SHARED static void StringReplace(string& str, const string& from, const string& to);
#else
	void VerboseDebugPrintF(SEVERITY severity,const char * format, ...){}
	static string Sanitize(string str){return "";}
	static void StringReplace(string& str, const string& from, const string& to){}
#endif

private:
#pragma warning(disable:4251)
	vector<string> logInfo;
};