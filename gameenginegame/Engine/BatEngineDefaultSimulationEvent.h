#pragma once
#include <ExportHeader.h>
#include <glm\glm.hpp>
#include <Actors\Actor.h>

using physx::PxContactPairHeader;
using physx::PxContactPair;
using physx::PxU32;
using physx::PxTriggerPair;
using physx::PxConstraintInfo;
using physx::PxActor;


class ENGINE_SHARED BatEngineDefaultSimulationEvent
	: public physx::PxSimulationEventCallback
{
	virtual void onContact(const PxContactPairHeader& pairHeader, 
		const PxContactPair* pairs,
		PxU32 nbPairs) 
	{
		for (PxU32 i = 0; i < nbPairs; i++)
		{
			const PxContactPair& cp = pairs[i];

			if (cp.events & physx::PxPairFlag::eNOTIFY_TOUCH_FOUND)
			{
				auto first = reinterpret_cast<Actor*>(pairHeader.actors[0]->userData);
				auto second = reinterpret_cast<Actor*>(pairHeader.actors[1]->userData);
				first->onContact(second);
			}
		}
	}

	virtual void onTrigger(PxTriggerPair* pairs, PxU32 count) 
	{

	}
	
	virtual void onConstraintBreak(PxConstraintInfo*, PxU32) 
	{
		int i = 0;
		i;
	}
	
	virtual void onWake(PxActor**, PxU32) override {}
	
	virtual void onSleep(PxActor**, PxU32) override {}
};