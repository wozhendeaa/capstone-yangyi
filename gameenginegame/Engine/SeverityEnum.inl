#ifndef SEVERITY_ENUM_FILE
#define SEVERITY_ENUM_FILE
#include <ExportHeader.h>
#include <string>
#include <ostream>
using std::string;
using std::ostream;

enum ENGINE_SHARED SEVERITY {_INFO_, _WARNING_, _ERROR_, _SEVERE_};
namespace Severity
{
	inline string toString (SEVERITY s)
	{
		string returnString;
		switch (s)
		{
		case SEVERITY::_INFO_:
			returnString = "INFO";
			break;
		case SEVERITY::_WARNING_:
			returnString = "WARNING";
			break;
		case SEVERITY::_ERROR_:
			returnString = "ERROR";
			break;
		case SEVERITY::_SEVERE_:
			returnString = "SEVERE";
			break;
		}
		return returnString;
	}

	inline ostream& operator<<(ostream& stream, SEVERITY s)
	{
		string returnString;
		switch (s)
		{
		case SEVERITY::_INFO_:
			returnString = "INFO";
			break;
		case SEVERITY::_WARNING_:
			returnString = "WARNING";
			break;
		case SEVERITY::_ERROR_:
			returnString = "ERROR";
			break;
		case SEVERITY::_SEVERE_:
			returnString = "SEVERE";
			break;
		}

		//cout << returnString;
		return stream;
	}
}
#endif 