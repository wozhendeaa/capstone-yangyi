#pragma once
#include <glm\glm.hpp>
#include "DebugShape.h"
#include <ShapeGenerator.h>
#include <..\RenderComponents\Renderer.h>
#include <vector>
#include <QtGui\QVBoxLayout>
#include <QtGui\QHBoxLayout>
#include <ExportHeader.h>
#include <MemoryDebug.h>


using Neumont::ShapeGenerator;
using Neumont::Vertex;
using Neumont::ShapeData;
using BatEngine::Renderer;
class Connection;

class ENGINE_SHARED DebugShapesManager 
{
#if DEBUG_SHAPE_ON
	friend class Connection;
	class ENGINE_SHARED VectorGraphic
	{
		friend class DebugShapesManager;
		Renderable* coneRenderable;
		Renderable* stemRenderable;
	public:

		void setEndPoints(const glm::vec3& from, const glm::vec3& to, float lengthDelta);
		void setVisible(bool visible);
		void dispose();
	};

private:
	GeometryInfo* coneGeometry;
	GeometryInfo* stemGeometry;
	std::vector<DebugShape*> shapes;
	GeometryInfo* lineGeo;
	GeometryInfo* pointGeo;
	GeometryInfo* cubeGeo;
	GeometryInfo* sphereGeo;
	GeometryInfo* vectorGeo;
	GeometryInfo* unitVectorGeo;
	Renderer* renderer;
	static bool instanceFlag;
	static DebugShapesManager* instance;
	
	DebugShapesManager();
	~DebugShapesManager();
	public:
		void clear();

		void setRenderer(Renderer* r);
		static DebugShapesManager* getInstance();

		void shutDown();

		void  AddLine( 
			const vec3& fromPosition, 
			const  vec3& toPosition,
			vec4  color,
			float  duration = 0.0f,
			bool  depthEnabled = true);

		void  AddPoint( 
			const vec3& position,
			float  size,
			glm::mat4* transform,
			float  duration = 0.0f,
			bool  depthEnabled = true);

		void  AddCube( const vec3& position,
			glm::vec4 color,
			float  size,
			glm::mat4*  transform,
			float  duration = 0.0f,
			bool  depthEnabled = true);

		void  AddSphere( const vec3& centerPosition,
			float  radius,
			glm::vec4  color,
			glm::mat4*  transform,
			float  duration = 0.0f,
			bool  depthEnabled = true);		
		
		void  addUnitVector( 
			vec3& vec,
			glm::vec4  color,
			glm::mat4*  transform,
			float  duration = 0.0f,
			bool  depthEnabled = true);


		void setEndPoints(
			const glm::vec3& from, 
			const glm::vec3& to,
			float lengthDelta);

		VectorGraphic* addVector(
			const glm::vec3& from,
			const glm::vec3& to, 
			float lengthDelta);

		

		void update(float dt);

		
	
#else 
	std::vector<DebugShape> shapes;
	friend class Connection;
		class ENGINE_SHARED VectorGraphic
		{
		public:
			void dispose(){}
			void setVisible(bool visible = true){}
		};

	
		DebugShapesManager(){}
		~DebugShapesManager(){}
	public:
		void clear(){}
		void setRenderer(Renderer* r);

		static DebugShapesManager* getInstance(){}
		void  AddLine( 
			const vec3& fromPosition, 
			const  vec3& toPosition,
			vec4  color,
			float  duration = 0.0f,
			bool  depthEnabled = true){}

		VectorGraphic* addVector(
			const glm::vec3& from,
			const glm::vec3& to, 
			float lengthDelta){}

		void  AddPoint( 
			const vec3& position,
			float  size,
			glm::mat4* transform,
			float  duration = 0.0f,
			bool  depthEnabled = true){}

		void  AddCube( const vec3& position,
			glm::vec4 color,
			float  size,
			glm::mat4*  transform,
			float  duration = 0.0f,
			bool  depthEnabled = true){}

		void  AddSphere( const vec3& centerPosition,
			float  radius,
			glm::vec4  color,
			glm::mat4*  transform,
			float  duration = 0.0f,
			bool  depthEnabled = true){}
		
		void  addUnitVector( 
			vec3& vec,
			glm::vec4  color,
			glm::mat4*  transform,
			float  duration = 0.0f,
			bool  depthEnabled = true){}
	
		void shutDown(){}


		void update(float dt){}
#endif

};

