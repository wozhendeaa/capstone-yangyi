#pragma once
#include <QtGui\qwidget>
#include <Delegation\FastDelegate.h>
#include <QtGui\qpushbutton.h>
#include <ExportHeader.h>
#include <MemoryDebug.h>

struct ENGINE_SHARED DebugButton : public QObject
{
	Q_OBJECT
public:
	DebugButton(const char* text, fastdelegate::FastDelegate0<> delegation);
	QPushButton* button;
	fastdelegate::FastDelegate0<> delegation;
	~DebugButton();
private slots:
	void buttonClicked();
};
