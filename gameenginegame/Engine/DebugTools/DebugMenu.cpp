#include "DebugMenu.h"
#include "DebugSlider.h"
#include <Delegation\FastDelegate.h>
#include <MemoryDebug.h>
#include <iostream>
#include <cassert>

#ifdef DEBUG_ON
static const int SLIDER_HEIGHT = 50;
static const int SLIDER_WIDTH = 300;

DebugMenu::DebugMenu(bool visible)
{
	setMinimumSize(1400, 200);
	initializeComponent();
	setVisible(visible);	
}

void DebugMenu::addTabLayout()
{
	tab_layout = new QVBoxLayout();
	tab_layout->setDirection(QBoxLayout::Direction::TopToBottom);
	tab_layout->setAlignment(Qt::Alignment(Qt::AlignmentFlag::AlignTop));
	tab = new QTabWidget;
	tab_layout->addWidget(tab);
	vLayout->addLayout(tab_layout);

}

void DebugMenu::addFloatsLayout()
{
	float_Layout = new QVBoxLayout();
	float_Layout->setSizeConstraint(float_Layout->SetMinimumSize);
	float_Layout->setDirection(QBoxLayout::Direction::TopToBottom);
	auto *float_box = new QGroupBox("floats");
	float_box->setLayout(float_Layout);
	vLayout->addWidget(float_box);

}

void DebugMenu::addFloatsSliderLayout()
{
	float_slider_Layout = new QVBoxLayout();
	float_slider_Layout->setDirection(QBoxLayout::Direction::TopToBottom);
	auto *float_slider_box = new QGroupBox("float sliders");
	float_slider_box->setLayout(float_slider_Layout);
	vLayout->addWidget(float_slider_box);

}

void DebugMenu::addBoolLayout()
{
	bool_Layout = new QVBoxLayout();
	bool_Layout->setDirection(QBoxLayout::Direction::TopToBottom);
	bool_box = new QGroupBox("bool");
	bool_box->setLayout(bool_Layout);
	vLayout->addWidget(bool_box);


}

void DebugMenu::addbuttonLayout()
{

	button_Layout = new QVBoxLayout();
	button_Layout->setDirection(QBoxLayout::Direction::TopToBottom);
	auto button_box = new QGroupBox("");
	button_box->setLayout(button_Layout);
	vLayout->addWidget(button_box);
}



void DebugMenu::initializeComponent()
{
	vLayout = new QVBoxLayout(this);
	vLayout->setDirection(QBoxLayout::Direction::TopToBottom);
	setLayout(vLayout);
	bool_Layout = nullptr;
	float_slider_Layout = nullptr;
	float_Layout = nullptr;
	tab_layout = nullptr;
	button_Layout = nullptr;
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::WindowStaysOnTopHint | Qt::CustomizeWindowHint);
}

void DebugMenu::watchFloat(const char* message, float* value)
{
	assert(float_Layout != nullptr);
	auto label_Layout = new QHBoxLayout;
	auto label = new QLabel(message);
	label->setMaximumWidth(120);
	label_Layout->addWidget(label);

	auto value_label = new QLabel;
	value_label->setText(QString::number(*value));
	label_Layout->addWidget(value_label);
	float_Layout->addLayout(label_Layout);

	floats.floatPoints.push_back(value);
	floats.labels.push_back(value_label);
}

void DebugMenu::removeLayout(QLayout* layout)
{
    QLayoutItem* child;
    while((child = layout->takeAt(0)) != 0)
    {
        if(child->layout() != 0)
        {
            removeLayout(child->layout());
        }
        else if(child->widget() != 0)
        {
            delete child->widget();
        }
 
        delete child;
    }
}

void DebugMenu::clear()
{

	if(float_Layout     != nullptr		 ) removeLayout( float_Layout);
	if(float_slider_Layout!= nullptr ) removeLayout( float_slider_Layout);
	if(bool_Layout!= nullptr		 ) removeLayout( bool_Layout);
	if(button_Layout!= nullptr		 ) removeLayout( button_Layout);
	if(tab_layout!= nullptr			 ) removeLayout( tab_layout);
	if(vLayout!= nullptr			 )
	{
		//  delete vLayout;
		 QLayoutItem* child;
		while ((child = vLayout->takeAt(0)) != 0)
		{
			delete child;
		}    
		delete vLayout;
		vLayout = new QVBoxLayout(this);
	}

	bools.clear();
	sliders.clear();
	floats.clear();
}

void DebugMenu::keyPressEvent(QKeyEvent* e)
{
	if(e->key() == 0x60 && is_menu_on)
	{
		setVisible(true);
		is_menu_on = !is_menu_on;
	} 
	else if(e->key() == 0x60 && !is_menu_on)
	{
		setVisible(false);
		is_menu_on = !is_menu_on;
	}
}

void DebugMenu::toggleBool(const char* message, bool* value)
{
	assert(bool_Layout != nullptr);
	auto label_Layout = new QHBoxLayout;

	auto check_box = new QCheckBox(message);
	check_box->setChecked(*value);

	label_Layout->addWidget(check_box);
	bool_Layout->addLayout(label_Layout);

	bools.bools.push_back(value);
	bools.boxes.push_back(check_box);
}

void DebugMenu::watchFloatSlider(const char* message, float max, float min, float* value)
{
	assert(float_slider_Layout != nullptr);
	auto label_Layout = new QHBoxLayout;
	auto label = new QLabel(message);
	label->setMaximumWidth(50);
	label->setMaximumHeight(10);
	label_Layout->addWidget(label);

	auto slider = new DebugSlider(min, max, true);
	slider->setMaximumWidth(400);
	slider->setMaximumHeight(30);
	slider->setValue(*value);

	label_Layout->addWidget(slider);
	float_slider_Layout->addLayout(label_Layout);

	sliders.floatPoints.push_back(value);
	sliders.sliders.push_back(slider);
	sliders.labels.push_back(label);
}

void DebugMenu::addButton(const char* text, 
					 fastdelegate::FastDelegate0<> delegation)
{
	assert(button_Layout != nullptr);
	auto label_Layout = new QHBoxLayout;
	auto label = new QLabel(text);
	label->setMaximumWidth(80);
	label_Layout->addWidget(label);

	auto button = new DebugButton(text, delegation);
	label_Layout->addWidget(button->button);
	button_Layout->addLayout(label_Layout);
}

void DebugMenu::addTab(const char* tabName, 
					   fastdelegate::FastDelegate0<> delegation)
{
	assert(tab_layout != nullptr);
	auto menu = new DebugMenu;
	tab->addTab(menu, QString(tabName));
	delegation;

}

void DebugMenu::removeWiget(QWidget* widget)
{
	vLayout->removeWidget(widget);
}

void DebugMenu::update(float dt)
{
	dt;
	floats.Update();
	sliders.Update();
	bools.Update();
}

DebugMenu::~DebugMenu(void)
{
	delete vLayout;
	delete float_Layout;
	delete float_slider_Layout;
	delete bool_Layout;
	delete button_Layout;

}
#endif