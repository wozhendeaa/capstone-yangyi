#pragma once
#include <QtGui\qwidget>
#include <ExportHeader.h>
#include <MemoryDebug.h>
class QSlider;
class QLabel;

class ENGINE_SHARED DebugSlider : public QWidget
{
	Q_OBJECT
	QSlider* slider;
	QLabel* label;
	float sliderGranularity;
	float min;
	float max;

	private slots:
		void sliderValueChanged();

signals:
		void valueChanged(float newValue);
public:
	DebugSlider(
		float min = -10.0f, float max = 10.0f, 
		bool textOnLeft = false, float granularity = 40.0);
	float value() const;
	void setValue(float newValue);
};