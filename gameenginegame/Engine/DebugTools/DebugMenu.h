#pragma once
#include <ExportHeader.h>
#include <QtGui\qwidget>
#include <QtGui\QKeyEvent>
#include <vector>
#include <QtGui\qcheckbox.h>
#include <QtGui\QVBoxLayout>
#include <QtGui\QHBoxLayout>
#include <QtGui\qlabel.h>
#include <QtGui\qgroupbox.h>
#include <DebugTools\DebugMenuDataItem.h>
#include <DebugTools\DebugButton.h>
#include <QtGui\QTabWidget>
#include <MemoryDebug.h>

class ENGINE_SHARED DebugMenu : public QWidget
{
#ifdef DEBUG_ON
	void initializeComponent();
	QVBoxLayout* vLayout;
	QVBoxLayout* float_Layout;
	QVBoxLayout* float_slider_Layout;
	QVBoxLayout* bool_Layout;
	QVBoxLayout* button_Layout;
	QVBoxLayout* tab_layout;
	QGroupBox* bool_box;
	
	BoolCollection bools;
	FloatSliderCollection sliders;
	FloatCollection floats;
	std::vector<DebugButton*> buttons;
	void removeLayout(QLayout* layout);
public:
	QTabWidget* tab;
	bool is_menu_on;
	DebugMenu(bool visible = false);
	void addTabLayout();
	void addFloatsLayout();
	void addFloatsSliderLayout();
	void addbuttonLayout();
	void addBoolLayout();
	void keyPressEvent(QKeyEvent* e);
	void watchFloat(const char* message, float* value);
	void watchFloatSlider(const char* message, float max, float min, float* value);
	void toggleBool(const char* message, bool* value);
	void addButton(const char* text, fastdelegate::FastDelegate0<> delegation);
	void addTab(const char* tabName, fastdelegate::FastDelegate0<> delegation);
	void update(float dt);
	void clear();
	void removeWiget(QWidget* widget);
	~DebugMenu(void);
#else
	void initializeComponent(){}

	QVBoxLayout* vLayout;
	QVBoxLayout* float_Layout;
	QVBoxLayout* float_slider_Layout;
	QVBoxLayout* bool_Layout;
	QVBoxLayout* button_Layout;
	QGroupBox*   bool_box;
	
	BoolCollection bools;
	FloatSliderCollection sliders;
	FloatCollection floats;

public:
	bool is_menu_on;
	DebugMenu(void){}

	void addTabLayout(){}
	void addbuttonLayout(){}
	void addFloatsLayout(){}
	void addFloatsSliderLayout(){}
	void addBoolLayout(){}
	void keyPressEvent(QKeyEvent* e){}
	void watchFloat(const char* message, float* value){}
	void watchFloatSlider(const char* message, float max, float min, float* value){}
	void addTab(const char* tabName, fastdelegate::FastDelegate0<> delegation);
	void toggleBool(const char* message, bool* value){}
	void addButton(const char* text, fastdelegate::FastDelegate0<> delegation);
	void paintEvent(QPaintEvent* e) override{}
	void removeWiget(QWidget* widget){}
	void update(float dt){}
	void clear(){}
	~DebugMenu(void){}
#endif

};

