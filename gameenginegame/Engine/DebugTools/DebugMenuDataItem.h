#pragma once
#include <ExportHeader.h>
#include <vector>
#include <DebugTools\DebugSlider.h>
#include <iostream>


struct ENGINE_SHARED DebugMenuDataItem
{
	virtual void Update() {}
	virtual void clear() {}
};

struct ENGINE_SHARED FloatCollection : public DebugMenuDataItem
{
	std::vector<float*> floatPoints;
	std::vector<QLabel*> labels; 

	virtual void Update() override
	{
		for (uint i = 0; i < floatPoints.size(); i++)
		{
			auto text = QString::number(*floatPoints[i]);
			labels[i]->setText(text);

		}
	}

	virtual void clear() override
	{
		floatPoints.clear();
		labels.clear();
	}

};

struct ENGINE_SHARED FloatSliderCollection : public FloatCollection
{
	std::vector<DebugSlider*> sliders;

	void Update() override
	{

		for (uint i = 0; i < sliders.size(); i++)
		{

			*floatPoints[i] = static_cast<float>(sliders[i]->value());
		}
	}
		
	virtual void clear() override
	{
		FloatCollection::clear();
		sliders.clear();
	}

};

struct ENGINE_SHARED BoolCollection : public DebugMenuDataItem
{
	std::vector<bool*> bools;
	std::vector<QCheckBox*> boxes; 

	void Update() override
	{
		for (uint i = 0; i < bools.size(); i++)
		{
			*bools[i] = boxes[i]->isChecked();
		}
	}

	virtual void clear() override
	{
		bools.clear();
		boxes.clear();
	}
};

