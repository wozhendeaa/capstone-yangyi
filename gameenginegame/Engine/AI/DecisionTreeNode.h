#pragma once
#include <Player\PlayerState.h>
#include <Delegation\FastDelegate.h>
#include <ExportHeader.h>
class FlagPlayer;
class ENGINE_SHARED DecisionTreeNode
{
	DecisionTreeNode *true_node, *false_node;
	PlayerState* state;
public:

	//this is a delegate, works just like the one in C#
	fastdelegate::FastDelegate1<FlagPlayer*, bool> situation;

	//constructor
	DecisionTreeNode(PlayerState* returnState);

	//constructor overload
	DecisionTreeNode(
		fastdelegate::FastDelegate1<FlagPlayer*, bool> situation,
		DecisionTreeNode* _trueNode,
		DecisionTreeNode* _falseNode) ;

	
	PlayerState* evaluate(FlagPlayer* target) const;

	//you dont need to care about his
	DecisionTreeNode(void);
	~DecisionTreeNode(void);
};
 