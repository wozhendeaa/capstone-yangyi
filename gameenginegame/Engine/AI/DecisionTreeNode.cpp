#include "DecisionTreeNode.h"
#include <MemoryDebug.h>


DecisionTreeNode::DecisionTreeNode(PlayerState* returnState) : state(nullptr), true_node(nullptr), false_node(nullptr) 
{ 
	this->state = returnState; 
}

	
DecisionTreeNode::DecisionTreeNode(
	fastdelegate::FastDelegate1<FlagPlayer*, bool> situation,
	DecisionTreeNode* _trueNode,
	DecisionTreeNode* _falseNode) : state(nullptr), true_node(nullptr), false_node(nullptr) 
{
	this->situation = situation;
	this->true_node = _trueNode;
	this->false_node = _falseNode;
}

PlayerState* DecisionTreeNode::evaluate(FlagPlayer* target) const
{
	if(state != nullptr)
		return state;
	
	auto isTrue = situation(target);
	auto true_n = true_node->evaluate(target);
	auto false_n = false_node->evaluate(target);
	return isTrue ? true_n : false_n;
		
}

DecisionTreeNode::~DecisionTreeNode(void)
{
	delete true_node;
	delete false_node;
	true_node = false_node = nullptr;
	state = nullptr;

}