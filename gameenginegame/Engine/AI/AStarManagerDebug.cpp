#include "AStarManagerDebug.h"
#include <MemoryDebug.h>


AStarManagerDebug::AStarManagerDebug(void)
{
	node_geo = nullptr;
	
	current_goal_index = 0;
}

PathNodeDebug* AStarManagerDebug::toPathNodeDebug(EditorNode* gn)
{
	assert(gn != nullptr);
	auto path_node = new PathNodeDebug; 
	path_node->this_node = gn;
	path_node->p_node = nullptr;
	return path_node;
}

PathNodeDebug* AStarManagerDebug::getNearestNode(const vec3& position)
{
	if(path_nodes.size() > 0)
	{
		auto* node = path_nodes[0];
		for (int i = 1; i < path_nodes.size(); i++)
		{
			auto distance1 = glm::length(position - node->this_node->position);
			auto distance2 = glm::length(position - path_nodes[i]->this_node->position);

			if (distance2 < distance1)
			{
				node = path_nodes[i];
			}
		}

		return node;
	}

	return nullptr;
}

void AStarManagerDebug::setupNodes()
{
	for (int i = 0; i < game_nodes.size(); i++)
	{
		path_nodes.push_back(toPathNodeDebug(game_nodes[i]));
	}
}


bool AStarManagerDebug::initializeAstars(PathNodeDebug* _from, PathNodeDebug* _to)
{
	assert(game_nodes.size() > 0);

	from = _from->this_node;
	to = _to->this_node;
	from->current_color = vec4(1.0f, 1.0f, 0.0f, 1.0f);;
	to->current_color = vec4(0.0f, 1.0f, 0.0f, 1.0f);;


	curr_node = _from;
	open.push_back(curr_node);
	bool path_generated = false;

	while(open.size() > 0)
	{		
		//auto count = open.size();
		auto con_num = curr_node->this_node->connections.size();
		removeCurrentFromOpen();
	
		//process children nodes
		processNode(curr_node);

		if(con_num > 0)
		{
			if(!isNodeInClose(curr_node))
				close.push_back(curr_node);

			//check if we have the final node
			if(isEndNode(curr_node))
			{
				gen_path(curr_node);
				path_generated = true;
				break;
			}

		}
		else if(isEndNode(curr_node))
		{
			removeCurrentFromOpen();
			close.push_back(curr_node);
			gen_path(curr_node);
			path_generated = true;
		}
		else
		{
			removeCurrentFromOpen();
		}

		if(open.size() > 0)
		{
			curr_node = getLowFromOpen();
		}
	}

	return path_generated;
}

void AStarManagerDebug::removeCurrentFromOpen()
{
	//put current node into close list
	for (int i = 0; i < open.size(); i++)
	{
		if(open[i] == curr_node)
		{
			open.erase(open.begin() + i);
		}
	}
}

void AStarManagerDebug::moveCurrentToClose()
{
	close.push_back(curr_node);
	curr_node = nullptr;
}


void AStarManagerDebug::gen_path(PathNodeDebug* node)
{
	clear();
	clearColor();

	auto temp = node;
	static int count = 0;
	while(temp != nullptr)
	{
		path.insert(0, temp->this_node);
		temp->this_node->current_color = vec4(1.0f, 1.0f, 0.0f, 1.0f);
		temp = temp->p_node;
		count ++;
	}

	//set node color
	path[0]->current_color =  EditorNode::selected_color;
	path[count - 1]->current_color =  EditorNode::connected_color;

	count = 0;

}


bool AStarManagerDebug::isEndNode(PathNodeDebug* node)
{
	auto isEnd = node->this_node == to;
	return isEnd;
}

bool AStarManagerDebug::isNodeInClose(PathNodeDebug* node)
{
	bool found = false;
	for (int i = 0; i < close.size() && !found; i++)
	{
		found = node->this_node == close[i]->this_node;
	}

	return found;
}
				

void AStarManagerDebug::processNode(PathNodeDebug* current)
{
	current->h = glm::length(to->position - current->this_node->position);

	if(current->p_node != nullptr)
		current->g = glm::length(current->p_node->this_node->position - current->this_node->position);
	else 
		current->g = 0;
	current->f = current->g + current->h;

	for (unsigned i = 0; i < current->this_node->connections.size(); i++)
	{
		auto con = current->this_node->connections[i];
		auto node = toPathNodeDebug(con->target);

		if(!isNodeInClose(node))
		{
			node->h = glm::length(to->position - path_nodes[i]->this_node->position);
			node->g = con[i].cost;
			node->f = node->g + node->h;

			node->p_node = current;
			
			open.push_back(node);
		}
	}
}



PathNodeDebug* AStarManagerDebug::getLowFromOpen()
{
	for (int i = 0; i < open.size(); i++)
	{
        for (int j =  i + 1; j < open.size(); j++)
        {
            if (open[i]->g < open[j]->g)
            {
                auto temp = open[j];
                open[j] = open[i];
                open[i] = temp;
            }
        }
        
	}
	return open[0];
}

PathNodeDebug* AStarManagerDebug::getLowFromClose()
{
	for (int i = 0; i < close.size(); i++)
	{
        for (int j =  i + 1; j < close.size(); j++)
        {
            if (close[i]->g > close[j]->g)
            {
                auto temp = close[j];
                close[j] = close[i];
                close[i] = temp;
            }
        }
	}
	
	return close[0];
}


void AStarManagerDebug::clear()
{
	path.clear();
	open.clear();
	close.clear();
}

void AStarManagerDebug::clearColor()
{
	for (int i = 0; i < game_nodes.size(); i++)
	{
		game_nodes[i]->current_color = EditorNode::no_color;
	}
}


AStarManagerDebug::~AStarManagerDebug(void)
{
	path.clear();
	
	path_nodes.clear();
	open.clear();
	close.clear();

}
