#pragma once
#include <KinectModule\KinectSources.h>
#include <KinectModule\ImageRenderer.h>
#include <Physics\PxPhysicsWorld.h>
#include <Actors\ActorContainer.h>
#include <ShockWaveEffect.h>
#include <KinectModule\KinectSwipeDetector.h>
#include <Game\GameObjects\PlayerModes\Jedi.h>
#include <Game\GameObjects\PlayerModes\Batman.h>
#include <Game\GameObjects\PlayerModes\Wizard.h>

enum  PlayerMode
{
	BATMAN,
	JEDI,
	WIZARD
};

class KinectPlayer;
class DebugShapesManager;
class EnemySpawner;
class KinectGame
{
	KinectSources* kinectSources;
	ImageRenderer* imageRenderer;
	Renderer* renderer;
	DebugShapesManager* debugShapeManager;
	EnemySpawner* enemySpawner = nullptr;

	Renderable* splashScreen = nullptr;
	Renderable* colorScene = nullptr;
	Renderable* depthScene = nullptr;
	Renderable* infraredScene = nullptr;
	Renderable* normalMapScene = nullptr;
	Renderable* positionMapScene = nullptr;
	Renderable* positionMapScene1 = nullptr;
	Renderable* virtualShadowScene = nullptr;
	Renderable* virtualShadowScene1 = nullptr;
	Renderable* arShadowScene = nullptr;
	Renderable* arShadowScene1 = nullptr;
	Renderable* virtualGameScene = nullptr;
	Renderable* arDepth = nullptr;
	KinectPlayer* player = nullptr;
	
	Batman* batmanPlayer = nullptr;
	Jedi* jediPlayer = nullptr;
	Wizard* wizardPlayer = nullptr;
	PlayerMode playerMode = PlayerMode::BATMAN;


	vec3 lightPosition = vec3(.0f);
	vec3 shadowLightPosition = vec3(.1f, 1.5f, -.8f);
	mat3 cameraSpaceOrientation;
	float kinectHeight = .0f;
	GLuint shadow_fbo_id;
	GLuint ar_shadow_fbo_id;
	TextureInfo virtual_shadow_map;
	TextureInfo virtual_color_map;
	TextureInfo ar_shadow_map;
	TextureInfo ar_color_map;

	bool runGame = false;
	bool runPlayer = false;

	physx::PxPhysicsWorld* physicsWorld;

	void prepPhysics();
	void prepMeshs();
	void prepGameObjects();
	void prepKinect();
	bool meshReady();
	void SwitchToBatman();
	void SwitchToJedi();
	void SwitchToWizard();

	void updateInput(float dt);
	void SpawnTestParticles(float dt);
	void UpdateTestParticles(float dt);
	void SwitchMode(float dt);
	void updateVisionMode(float dt);
	float width, height;
	float loadingCounter = .0f;

	ActorContainer actors;
	ActorContainer testParticles;
	bool testParticlesSpawned = false;
public:
	bool isRoomDark = false;

	void setGameWindowSize(int width, 
		int height);
	void InitGame();

	void Update(float dt);
	void Draw();

	void mousePressEvent(const float& x, 
						 const float& y);

	KinectGame(Renderer* r);
	~KinectGame(void);

};

