#include "Wizard.h"
#include <Actors\ParticleSystem\ParticleSystem.h>

Wizard::Wizard(KinectBodyFrame* kBody,
	Renderer* r,
	physx::PxPhysicsWorld* m_world) 
	: KinectPlayer(kBody,r,	m_world)
{

	fireBall = new ParticleSystem(m_world, r, 30);
	fireBall->InitParticleSystem(PxVec3(1.0f, 0.0f, 0.0f),
		.0f,
		PxVec3(0.0f, 0.4f, .0f),
		.0f,
		1.0f);

	fireBall->setLifetime(.1f, .3f);
	fireBall->CreateParticles(false, 30.0f);
	fireBall->SetDamping();
	//fireBall->EnableFadeOverDistance();
}

void Wizard::AttachLight(vec3* lpos)
{
	lightPosition = lpos;
}

vec3 Wizard::GetLightPosition() const
{
	return *lightPosition;
}


void Wizard::Destroy()
{
	Actor::Destroy();
}

void Wizard::UpdateGesture()
{
	auto joints = bodyFrame->getJointsData();
	auto elbow = joints[JointType_ElbowLeft];
	auto wrist = joints[JointType_WristLeft];

	if (elbow.TrackingState != TrackingState::TrackingState_NotTracked
		&& wrist.TrackingState != TrackingState::TrackingState_NotTracked)
	{
		activeFire = wrist.Position.Y > elbow.Position.Y;
	}
	else
	{
		activeFire = false;
	}

	if (activeFire)
	{
		auto firePosition = vec3(-wrist.Position.X, wrist.Position.Y, wrist.Position.Z);

		*lightPosition = firePosition;

		fireBall->SetEmissionPosition(firePosition);
	}
}

void Wizard::Update(float dt)
{
	KinectPlayer::Update(dt);
	Actor::Update(dt);
	UpdateGesture();

	if (activeFire)
	{
		fireBall->Update(dt);
	}
}

void Wizard::Draw(Renderer* r,
	GLuint passIndex)
{
	Actor::Draw(r, passIndex);

	if (activeFire)
	{
	//	fireBall->Draw(r, passIndex);
	}
}


Wizard::~Wizard()
{
	//delete lightPosition;
	fireBall->Destroy();
	delete fireBall;
}
