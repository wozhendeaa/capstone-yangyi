#pragma once
#include <Actors\GameObjects\KinectPlayer.h>
class ParticleSystem;
class Wizard : public KinectPlayer
{
	vec3* lightPosition;
	ParticleSystem* fireBall{ nullptr };

	bool activeFire = false;
	void UpdateGesture();
public:
	explicit Wizard(KinectBodyFrame* kBody,
		Renderer* r,
		physx::PxPhysicsWorld* m_world = nullptr) ;

	virtual void Destroy() override;
	virtual void Update(float dt) override;
	virtual void Draw(Renderer* r, GLuint passIndex = 0) override;

	void AttachLight(vec3* lightPosition);

	vec3 GetLightPosition() const;
	~Wizard();
};

