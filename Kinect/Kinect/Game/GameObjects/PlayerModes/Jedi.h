#pragma once
#include <Actors\GameObjects\KinectPlayer.h>

class LightSaber;
class Jedi : public KinectPlayer
{
	//PxVec3 CastRightHandRay() override;
	//PxVec3 CastLeftHandRay() override;
	std::vector<Actor*> forces;

	vec3 forceFildScale {vec3(1.0f, .5f, .5f)};
	LightSaber* lightSaber = nullptr;	
	RigidBody* lightSaberBody = nullptr;
	bool isLightsaberLeftHand = false;
	void InitComponent();
public:
	explicit Jedi(KinectBodyFrame* kBody,
		Renderer* r,
		physx::PxPhysicsWorld* m_world = nullptr);

	virtual void Destroy() override;
	virtual void Update(float dt) override;
	virtual void Draw(Renderer* r, GLuint passIndex = 0) override;

	void LeftHandRayHitCallback(Actor* obj) override;
	void RightHandRayHitCallback(Actor* obj) override;
	void LeftHandEnergyHitMax() override;
	void RightHandEnergyHitMax() override;

	~Jedi();
};

