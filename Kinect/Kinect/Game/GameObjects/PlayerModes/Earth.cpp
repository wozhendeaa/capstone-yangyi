#include "Earth.h"
#include <Actors\Components\MeshComponents\MeshRenderer.h>
#include <Game\VirtualTopDefense.h>
#include <glm\gtx\rotate_vector.hpp>
#include <Game\GameObjects\Enemies\SphereEnemy.h>
#include <Game\GameObjects\Enemies\CubeEnemy.h>

Earth::Earth(
	physx::PxPhysicsWorld* world,
	Renderer* r,
	const vec3 pos)
{
	rigidBody = new RigidBody(m_world, 0.8f, 0.8f, 0.0f, true);
	float radius = 0.5f;
	*m_scale = vec3(0.1f);
	*m_translation = pos;
	rigidBody->InitializeWithSphere(m_world,
		m_scale->x,
		*m_rotation,
		*m_translation);

	rigidBody->SetBodyUserData(this);
	rigidBody->SetEnableGravity(false);
	m_world->SetFilterData(rigidBody->body, VirtualTopDefenseFilterGroup::eBody,
		VirtualTopDefenseFilterGroup::eEnemy);

	AddComponent(static_cast<StrongActorComponent>(rigidBody));



	//std::auto_ptr<GeometryInfo> verts(r->addGeometry(model.verts,
	//	model.numVerts,
	//	reinterpret_cast<GLuint*>(model.indices),
	//	model.numIndices,
	//	Neumont::Vertex::STRIDE,
	//	GL_UNSIGNED_INT,
	//	GL_TRIANGLES));

	//model.cleanUp();

	std::string file = "Resources//Mesh//sphere.bin";

	ObjLoader loader;
	loader.loadFromFile(file.c_str());
	auto sphere = loader.getModel();
	auto geo = r->addGeometry(sphere.verts,
		sphere.numVerts,
		reinterpret_cast<GLuint*>(sphere.indices),
		sphere.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_INT,
		GL_TRIANGLES);
	sphere.cleanUp();

	TextureInfo earthTex;
	earthTex.addColorTexture("Resources//Texture//earth_color.jpg", FIF_JPEG,
		r->shaders[0]->program_id);


	auto sphereRenderable = r->addRenderable(*geo,
		new mat4,
		earthTex,
		0);


	auto meshComp = new MeshRenderer(sphereRenderable);
	AddComponent(static_cast<StrongActorComponent>(meshComp));
}

void Earth::TakeHit()
{
	maxHit--;
	
}

void Earth::Update(float dt)
{
	Actor::Update(dt);
	static float currentAngle = 0.0f;
	auto x = glm::rotate(mat4(), 30.0f, vec3(.0f, .0f, 1.0f));
	auto y = glm::rotate(mat4(), currentAngle, vec3(.0f, 1.0f, .0f));
	currentAngle += 0.5f;

	setTransform(*m_scale, x * y, *m_translation);
}




bool Earth::IsAlive()
{
	return maxHit > 0;
}


void Earth::onContact(Actor* other)
{


}

Earth::~Earth()
{
}
