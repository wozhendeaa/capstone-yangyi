#include "Jedi.h"
#include <Game\GameObjects\PlayerModes\Weapons\Lightsaber.h>
#include <glm\gtx\rotate_vector.hpp>
#include <Game\GameObjects\PlayerModes\Weapons\Batarang.h>
Jedi::Jedi(KinectBodyFrame* kBody,
	Renderer* r,
	physx::PxPhysicsWorld* m_world) : 
	KinectPlayer(kBody, r, m_world)
{
	InitComponent();
}

Jedi::~Jedi()
{

}


void Jedi::InitComponent()
{
	//auto rightForceField = new RigidBody(m_world, 0.8f, 0.8f, 0.0f, true);
	//float radius = 0.5f;


	//rightForceField->InitializeWithBox(m_world,
	//	forceFildScale,
	//	*m_rotation,
	//	*m_translation);

	//rightForceField->SetBodyUserData(this);
	//rightForceField->SetEnableGravity(false);
	//rightForceField->SetCollisionEnabled(false);
	////m_world->SetFilterData(rigidBody->body, TopDefenseFilterGroup::eLightSaber, 
	////	TopDefenseFilterGroup::eEnemy);
 //   rightLowerArm->AddComponent(static_cast<StrongActorComponent>(rightForceField));

	//auto leftForceField = new RigidBody(m_world, 0.8f, 0.8f, 0.0f, true);

	//leftForceField->InitializeWithBox(m_world,
	//	forceFildScale,
	//	*m_rotation,
	//	*m_translation);

	//leftForceField->SetBodyUserData(this);
	//leftForceField->SetEnableGravity(false);
	//leftForceField->SetCollisionEnabled(false);
	////m_world->SetFilterData(rigidBody->body, TopDefenseFilterGroup::eLightSaber, 
	////	TopDefenseFilterGroup::eEnemy);
	//leftLowerArm->AddComponent(static_cast<StrongActorComponent>(leftForceField));
}



void Jedi::RightHandRayHitCallback(Actor* obj)
{
	auto castValidObject = dynamic_cast<LightSaber*>(obj);

	if (castValidObject != nullptr)
	{
		castValidObject->DetectedByForce(0.0f, 0.0f);
		lightSaber = castValidObject;
		isLightsaberLeftHand = false;
	}
}


void Jedi::LeftHandRayHitCallback(Actor* obj)
{
	auto castValidObject = dynamic_cast<LightSaber*>(obj);

	if (castValidObject != nullptr)
	{
		castValidObject->DetectedByForce(0.0f, 0.0f);
		lightSaber = castValidObject;
		isLightsaberLeftHand = true;
	}
}

void Jedi::LeftHandEnergyHitMax()
{

}

void Jedi::RightHandEnergyHitMax()
{
}


void Jedi::Destroy()
{

}

void Jedi::Update(float dt)
{
	KinectPlayer::Update(dt);

	auto leftRay = CastLeftHandRay();
	auto mat = glm::orientation(physx::ToVec3(leftRay), vec3(1.0f, 0.0f, 0.0f));
	auto joints = bodyFrame->getJointsData();
	auto leftHandJoint = joints[JointType_HandLeft].Position;
	auto leftHandVec3 = vec3(-leftHandJoint.X, leftHandJoint.Y, leftHandJoint.Z);
	auto rightHandJoint = joints[JointType_HandRight].Position;
	auto rightHandVec3 = vec3(-rightHandJoint.X, rightHandJoint.Y, rightHandJoint.Z);
	auto pos = isLightsaberLeftHand ? leftHandVec3 : rightHandVec3;

	if (lightSaber != nullptr && lightSaber->CanFetch())
	{
		lightSaber->Pulling(pos, dt);
	}

	if (lightSaber != nullptr && lightSaber->IsFetched())
	{
		auto mat2 = glm::orientation(physx::ToVec3(leftRay), vec3(0.0f, 1.0f, 0.0f));
		lightSaber->GetComponent<RigidBody>().lock()->MakeBodyKinematic();
		
		lightSaber->setTOffsetransform(*lightSaber->m_scale, mat2, pos, vec3(.0f, .01f, .0f));
	}
}

void Jedi::Draw(Renderer* r, GLuint passIndex)
{
	KinectPlayer::Draw(r, passIndex);
#ifdef DEBUG_ON
//	r->Draw(ray, 0);
#endif
}



