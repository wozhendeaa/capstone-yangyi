#pragma once
#include <Actors\Actor.h>
#include <Physics\PxPhysicsWorld.h>
#include <Actors\Components\Physics\RigidBody.h>
class RigidBody;
class Earth :	public Actor
{
	physx::PxPhysicsWorld* m_world;
	Renderer* r;

	int maxHit = 3;
	RigidBody* rigidBody;
	bool isAlive = true;
	void Init(physx::PxPhysicsWorld* world,
		Renderer* r);
public:
	explicit Earth(physx::PxPhysicsWorld* world,
		Renderer* r,
		const vec3 pos = vec3(0.0f));

	void TakeHit();

	bool IsAlive();

	void Update(float dt) override;

	void onContact(Actor* other) override;

	~Earth();
};

