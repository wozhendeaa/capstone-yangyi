#pragma once
#include <glm\glm.hpp>
class FetchableByForce
{
public:
	virtual void DetectedByForce(float floatingHeight = 0.1f, float detectionDuration = 2.0f) = 0;
	virtual void Pulling(const glm::vec3& destination, float dt) = 0;
	virtual bool CanFetch() = 0;
	virtual bool IsFetched() = 0;

};