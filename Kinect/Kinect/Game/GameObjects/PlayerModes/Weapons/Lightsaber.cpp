#include "LightSaber.h"
#include <Actors\Components\MeshComponents\MeshRenderer.h>
#include <Actors\Components\Physics\RigidBody.h>
#include <glm\gtx\rotate_vector.hpp>
#include <MemoryDebug.h>
#include <Game\VirtualTopDefense.h>

LightSaber::LightSaber(physx::PxPhysicsWorld* world,
			   Renderer* renderer, 
	           const vec3& p)
			    :
			   m_world(world),
			   r(renderer)

{
	setTransform(vec3(0.1f), glm::mat4(), p);
	InitComponents();
}

void LightSaber::SetActivated(bool enabled)
{
	isActivated = enabled;
}


void LightSaber::InitComponents()
{
	auto rigidBody = new RigidBody(m_world, 0.8f, 0.8f, 0.0f);
	
	bladeScale.y = bladeMaxLength;
	rigidBody->InitializeWithBox(m_world,
		*m_scale * 1.5f,
		*m_rotation, 
		*m_translation);

	rigidBody->SetBodyUserData(this);
	rigidBody->SetEnableGravity(true);


	AddComponent(static_cast<StrongActorComponent>(rigidBody));

	std::string file = "Resources//Mesh//lightsaber.bin";

	ObjLoader loader;
	loader.loadFromFile(file.c_str());
	auto batarang_model = loader.getModel();

	std::auto_ptr<GeometryInfo> verts(r->addGeometry(batarang_model.verts,
		batarang_model.numVerts,
		reinterpret_cast<GLuint*>(batarang_model.indices),
		batarang_model.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_INT,
		GL_TRIANGLES));

	batarang_model.cleanUp();


	auto lightSaberMesh = r->addRenderable(*verts,
		new mat4,
		TextureInfo(),
		0);

	r->addRenderableUniformParameter(lightSaberMesh,
		"isSkyBox",
		ShaderUniformParameterType::SHADER_BOOL,
		(char*)&lightSaberMesh->recievesLight);

	auto meshComp = new MeshRenderer(lightSaberMesh);
	AddComponent(static_cast<StrongActorComponent>(meshComp));

	//bladeBody = new RigidBody(m_world, 0.8f, 0.8f, 0.0f, true);

	//bladeBody->InitializeWithBox(m_world,
	//	bladeScale,
	//	*m_rotation,
	//	*m_translation);


	//bladeBody->SetBodyUserData(this);
	//bladeBody->SetEnableGravity(false);
	//blade->AddComponent(static_cast<StrongActorComponent>(bladeBody));
	blade = new Actor;

	file = "Resources//Mesh//lightsaberBlade.bin";

	loader.loadFromFile(file.c_str());
	auto bladeModel = loader.getModel();

	std::auto_ptr<GeometryInfo> bladeVerts(r->addGeometry(bladeModel.verts,
		bladeModel.numVerts,
		reinterpret_cast<GLuint*>(bladeModel.indices),
		bladeModel.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_INT,
		GL_TRIANGLES));

	bladeModel.cleanUp();

	TextureInfo bladeTex;
	bladeTex.addColorTexture("Resources\\Texture\\red.png",
		FIF_PNG,
		r->shaders[0]->program_id);
	auto bladeMesh = r->addRenderable(*bladeVerts,
		new mat4,
		bladeTex,
		0);

	r->addRenderableUniformParameter(bladeMesh,
		"isSkyBox",
		ShaderUniformParameterType::SHADER_BOOL,
		(char*)&bladeMesh->recievesLight);
	

	bladeMesh->setTransform(vec3(0.001f),
		mat4(),
		vec3(0.0f, 0.0, -10.0f));
	
	meshComp = new MeshRenderer(bladeMesh);
	blade->AddComponent(static_cast<StrongActorComponent>(meshComp));


	//m_world->SetFilterData(bladeBody->body, VirtualTopDefenseFilterGroup::eWeapon,
	//	VirtualTopDefenseFilterGroup::eEnemy);
}

vec3 LightSaber::GetLightSaberScale() const
{
	return bladeScale;
}


void LightSaber::onContact(Actor* other)
{

}

bool LightSaber::Disposable()
{
	//disposable = timer >= lifeTime;
	return disposable;
}

void LightSaber::Update(float dt)
{
	if (isActivated)
	{
		UpdateFetchDuration(dt);
		Actor::Update(dt);
		UpdateBlade(dt);
	}
}

void LightSaber::Draw(Renderer* r, GLuint passIndex)
{

	if (isActivated)
	{
		Actor::Draw(r, passIndex);
		if (isDetected)
		{
			blade->Draw(r, passIndex);
		}

	}
}

void LightSaber::UpdateBlade(float dt)
{
	if (isObtained && bladeScale.y < 20.0f)
	{
		bladeScale.y += bladeGrowSpeed * dt;
	}

	if (isObtained)
	{
		if (bladeBody == nullptr)
		{
			bladeBody = new RigidBody(m_world, 0.8f, 0.8f, 0.0f, true);

			bladeBody->InitializeWithBox(m_world,
				vec3(.1f, bladeMaxLength, .1f),
				*m_rotation,
				*m_translation);

			blade->AddComponent(static_cast<StrongActorComponent>(bladeBody));
		}

		blade->setTOffsetransform(bladeScale,
			glm::toMat4(*m_rotation),
			*m_translation,
			vec3(.0f, .05f, .0f));

		blade->Update(dt);
	}
}


void LightSaber::UpdateFetchDuration(float dt)
{
	if (isDetected && timer < m_detectionDuration)
	{
		timer += dt;		
		auto b = GetComponent<RigidBody>().lock()->body;
		auto p = b->getGlobalPose().p;
		static float verticalDistanceMoved = .0f;
		
		if (verticalDistanceMoved < m_floatingHeight)
		{
			auto moveDistance = m_speed;
			verticalDistanceMoved += moveDistance;
			p.y += moveDistance;
			b->setGlobalPose(PxTransform(p));
		}
	}
}

void LightSaber::Destroy()
{
	Actor::Destroy();
	blade->Destroy();
	delete blade;
}


void LightSaber::DetectedByForce(float floatingHeight,
	float detectionDuration)
{
	m_detectionDuration = detectionDuration;
	m_floatingHeight = floatingHeight;


	isDetected = true;

}

void LightSaber::Pulling(const glm::vec3& destination, float dt)
{
	dir = glm::normalize(destination - *m_translation);
	
	auto b = GetComponent<RigidBody>().lock();
	b->SetEnableGravity(false);

	if (glm::distance(destination, *m_translation) < 0.5f)
	{
		isObtained = true;
		b->body->setGlobalPose(PxTransform(physx::ToPxVec3(destination)));
	}
	else
	{
		auto p = b->body->getGlobalPose().p;
		p += physx::ToPxVec3(dir) * m_speed * dt;
		b->body->setGlobalPose(PxTransform(p));
	}
}

bool LightSaber::CanFetch()
{
	return timer >= m_detectionDuration;
}

bool LightSaber::IsFetched()
{
	return isObtained;
}



LightSaber::~LightSaber()
{
}
