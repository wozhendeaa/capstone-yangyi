#pragma once
#include <Actors\Actor.h>
#include <Physics\PxPhysicsWorld.h>
#include <Game\GameObjects\PlayerModes\Weapons\FetchableByForce.h>

class RigidBody;
class LightSaber :
	public Actor, public FetchableByForce
{
	physx::PxPhysicsWorld* m_world;
	Renderer* r;
	Actor* blade = nullptr;
	RigidBody* bladeBody = nullptr;
	vec3 dir;
	vec3 bladeScale{ 0.5f};
	float timer = 0.0f;
	float m_speed = 1.7f;

	float bladeMaxLength = 0.5f;
	float bladeGrowSpeed = 4.0f;
	float m_detectionDuration = 1.0f;
	float m_floatingHeight = 0.5f;

	bool isDetected = false;
	bool disposable = false;
	bool isObtained = false;	
	bool isActivated = false;

	void InitComponents();
	void UpdateBlade(float dt);
public:
	explicit LightSaber(physx::PxPhysicsWorld* world,
					Renderer* r,
					const vec3& p);

	void SetActivated(bool enabled);

	vec3 GetLightSaberScale() const;

	void onContact(Actor* other) override;

	void Destroy() override;

	~LightSaber();
	
	bool Disposable();
	void Update(float dt) override;
	void Draw(Renderer* r, GLuint passIndex = 0);

	void UpdateFetchDuration(float dt);

	//interface implementation
	void DetectedByForce(float floatingHeight,
		float detectionDuration) override;

	void Pulling(const glm::vec3& destination, float dt) override;

	bool CanFetch() override;

	bool IsFetched() override;
};

