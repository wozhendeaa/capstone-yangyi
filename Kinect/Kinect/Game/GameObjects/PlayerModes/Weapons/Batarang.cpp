#include "Batarang.h"
#include <Actors\Components\MeshComponents\MeshRenderer.h>
#include <Actors\Components\Physics\RigidBody.h>
#include <glm\gtx\rotate_vector.hpp>
#include <MemoryDebug.h>
#include <Game\VirtualTopDefense.h>

Batarang::Batarang(physx::PxPhysicsWorld* world,
	Renderer* renderer,
	const vec3& p)
	:
	m_world(world),
	r(renderer)

{
	setTransform(scale, glm::mat4(), p);
	InitComponents();
}

void Batarang::InitComponents()
{
	rigidBody = new RigidBody(m_world, 0.8f, 0.8f, 0.0f, true);

	rigidBody->InitializeWithBox(m_world,
		*m_scale,
		*m_rotation,
		*m_translation);

	rigidBody->SetBodyUserData(this);
	rigidBody->SetEnableGravity(false);
	
	m_world->SetFilterData(rigidBody->body, VirtualTopDefenseFilterGroup::eWeapon, 
		VirtualTopDefenseFilterGroup::eEnemy);

	AddComponent(static_cast<StrongActorComponent>(rigidBody));

	std::string file = "Resources//Mesh//batarang.bin";

	ObjLoader loader;
	loader.loadFromFile(file.c_str());
	auto batarang_model = loader.getModel();

	std::auto_ptr<GeometryInfo> verts(r->addGeometry(batarang_model.verts,
		batarang_model.numVerts,
		reinterpret_cast<GLuint*>(batarang_model.indices),
		batarang_model.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_INT,
		GL_TRIANGLES));

	batarang_model.cleanUp();
	//TextureInfo percepTexture;
	//percepTexture.addColorTexture("Resources\\Texture\\pattern.bmp",
	//	FIF_BMP,
	//	renderer->shaders[0]->program_id);

	auto batarang = r->addRenderable(*verts,
		new mat4(),
		TextureInfo(),
		0);

	r->addRenderableUniformParameter(batarang,
		"isSkyBox",
		ShaderUniformParameterType::SHADER_BOOL,
		(char*)&batarang->recievesLight);

	auto meshComp = new MeshRenderer(batarang);
	AddComponent(static_cast<StrongActorComponent>(meshComp));
}

void Batarang::onContact(Actor* other)
{

}

void Batarang::SetThrow(bool isThrown)
{
	m_isThrown = isThrown;
}


void Batarang::SetMotion(vec3 dir)
{
	m_isThrown = true;
	this->dir = dir;
}



bool Batarang::IsThrown()
{
	return m_isThrown;
}


bool Batarang::Disposable()
{
	disposable = timer >= lifeDuration;
	return disposable;
}

void Batarang::Update(float dt)
{
	Actor::Update(dt);

	if (m_isThrown)
	{
		timer += dt;
		auto body = GetComponent<RigidBody>().lock();
		auto pose = body->body->getGlobalPose();
		auto p = pose.p + physx::ToPxVec3(dir) * dt * m_speed;
		setTransform(*m_scale,physx::ToQuat(pose.q), physx::ToVec3(p));

	}
}

void Batarang::Draw(Renderer* r, GLuint passIndex)
{
	Actor::Draw(r, passIndex);
}


Batarang::~Batarang()
{
}
