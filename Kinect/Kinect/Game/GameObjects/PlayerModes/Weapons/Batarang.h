#pragma once
#include <Actors\Actor.h>
#include <Physics\PxPhysicsWorld.h>

class RigidBody;
class Batarang :
	public Actor
{
	physx::PxPhysicsWorld* m_world;
	Renderer* r;
	RigidBody* rigidBody = nullptr;
	vec3 scale = vec3(0.3f);
	vec3 dir{ .0f };
	float m_speed = 2.7f;
	float timer = 0.0f;

	float m_detectionDuration = 1.0f;
	float m_floatingHeight = 0.5f;
	float lifeDuration = 5.0f;

	bool m_isEquiped = false;
	bool m_isThrown = false;
	bool disposable = false;

	void InitComponents();
public:
	explicit Batarang(physx::PxPhysicsWorld* world,
		Renderer* r,
		const vec3& p);

	void onContact(Actor* other) override;
	void SetThrow(bool isThrown);
	void SetMotion(vec3 dir);
	bool IsThrown();

	~Batarang();

	bool Disposable();
	void Update(float dt) override;
	void Draw(Renderer* r, GLuint passIndex = 0) override;
	void UpdateFetchDuration(float dt);

};

