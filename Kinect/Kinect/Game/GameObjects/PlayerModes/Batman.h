#pragma once
#include <Actors\GameObjects\KinectPlayer.h>

class Batarang;
class ClothActor;
class Batman : public KinectPlayer
{
	Batarang* batarangLeft = nullptr;
	Batarang* batarangRight = nullptr;
	std::vector<Batarang*> usedBatarangs;
	Actor* batMask;
	ClothActor* cape = nullptr;
	vec3 batarangScale = vec3(1.0f);
	bool rightLowerArmInitialized = false;
	bool leftLowerArmInitialized = false;
	bool rightHiveInitialized = false;
	bool leftHiveInitialized = false;

	void DisposeLeft();
	void DisposeRight();

	void InitComponents();
	void InitMask();
	void InitLeftArmor();
	void InitRightArmor();
	void InitBelt();

	void UpdateBatarangs();
	void UpdateBatarangThrow(Batarang* ba, 
		const vec3& dir,
		float speed);

	void LowerLeftArmCollisionBuilt() override;
	void LowerRightArmCollisionBuilt() override;
	void LowerLeftLegCollisionBuilt() override;
	void LowerRightLegCollisionBuilt() override;

public:
	explicit Batman(KinectBodyFrame* kBody,
		Renderer* r,
		physx::PxPhysicsWorld* m_world = nullptr);

	virtual void Destroy() override;
	virtual void Update(float dt) override;
	virtual void Draw(Renderer* r, GLuint passIndex = 0) override;
	void LeftHandEnergyHitMax() override;
	void RightHandEnergyHitMax() override;
	~Batman();
};

