#include "Batman.h"
#include <Game\GameObjects\PlayerModes\Weapons\Batarang.h>
#include <glm\gtx\rotate_vector.hpp>
#include <Game\VirtualTopDefense.h>
#include <Actors\GameObjects\ClothActor.h>
Batman::Batman(KinectBodyFrame* kBody,
	Renderer* r,
	physx::PxPhysicsWorld* m_world) :
	KinectPlayer(kBody, r, m_world)
{
	InitComponents();
}


void Batman::InitComponents()
{
	InitMask();
	//InitLeftArmor();
	//InitRightArmor();
	//InitBelt();

	cape = new ClothActor(renderer, m_world);
	cape->SetVerticalStretch(1.1f, 1.0f, 1.0f, 1.5f);
	cape->SetHorizontalStretch(0.3f, 0.10f, .0f, 1.5f);
	cape->SetBending(4.1f, 0.010f, 0.01f, 1.5f);
	cape->setTransform(vec3(1.0f), quat(), vec3(1.0f, 0.0f, 2.0f));
}

void Batman::InitMask()
{
	std::string file = "Resources//Mesh//batmask.bin";

	ObjLoader loader;
	loader.loadFromFile(file.c_str());
	auto model = loader.getModel();

	std::auto_ptr<GeometryInfo> verts(renderer->addGeometry(model.verts,
		model.numVerts,
		reinterpret_cast<GLuint*>(model.indices),
		model.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_INT,
		GL_TRIANGLES));

	model.cleanUp();

	TextureInfo maskTex;
	maskTex.addColorTexture("Resources//Texture//batmask.png", FIF_PNG,
		renderer->shaders[0]->program_id);

	auto mesh = renderer->addRenderable(*verts,
		new mat4,
		maskTex,
		0);

	renderer->addRenderableUniformParameter(mesh,
		"isSkyBox",
		ShaderUniformParameterType::SHADER_BOOL,
		(char*)&mesh->recievesLight);

	batMask = new Actor;
	auto meshComp = new MeshRenderer(mesh);
	batMask->AddComponent(static_cast<StrongActorComponent>(meshComp));
}

void Batman::InitLeftArmor()
{
	std::string file = "Resources//Mesh//batarmor.bin";

	ObjLoader loader;
	loader.loadFromFile(file.c_str());
	auto m = loader.getModelShort();
	auto model = loader.getModelWithTangent(m);

	std::auto_ptr<GeometryInfo> verts(renderer->addGeometry(model.verts,
		model.numVerts,
		reinterpret_cast<GLuint*>(model.indices),
		model.numIndices,
		Neumont::VertexV2::STRIDE,
		GL_UNSIGNED_SHORT,
		GL_TRIANGLES,
		true));

	model.cleanUp();

	auto mesh = renderer->addRenderable(*verts,
		new mat4,
		TextureInfo(),
		0);

	auto meshComp = new MeshRenderer(mesh);
	leftLowerArm->AddComponent(static_cast<StrongActorComponent>(meshComp));

}

void Batman::InitRightArmor()
{
	std::string file = "Resources//Mesh//batarmor.bin";

	ObjLoader loader;
	loader.loadFromFile(file.c_str());
	auto model = loader.getModel();

	std::auto_ptr<GeometryInfo> verts(renderer->addGeometry(model.verts,
		model.numVerts,
		reinterpret_cast<GLuint*>(model.indices),
		model.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_INT,
		GL_TRIANGLES));

	model.cleanUp();

	auto mesh = renderer->addRenderable(*verts,
		new mat4,
		TextureInfo(),
		0);

	auto meshComp = new MeshRenderer(mesh);
	rightLowerArm->AddComponent(static_cast<StrongActorComponent>(meshComp));

	////add cloth
	//cape = new ClothActor(renderer, m_world);
	//cape->SetVerticalStretch(1.1f, 2.0f, 1.0f, 1.5f);
	//cape->SetHorizontalStretch(1.1f, 0.10f, .0f, 1.5f);
	//cape->SetBending(4.1f, 0.010f, 0.01f, 1.5f);
	//cape->setTransform(vec3(0.6f), quat(), vec3(0.0f, 0.0f, 3.0f));
}

void Batman::InitBelt()
{
	std::string file = "Resources//Mesh//batbelt.bin";

	ObjLoader loader;
	loader.loadFromFile(file.c_str());
	auto model = loader.getModelShort();

	std::auto_ptr<GeometryInfo> verts(renderer->addGeometry(model.verts,
		model.numVerts,
		reinterpret_cast<GLuint*>(model.indices),
		model.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_SHORT,
		GL_TRIANGLES));

	model.cleanUp();

	auto mesh = renderer->addRenderable(*verts,
		new mat4,
		TextureInfo(),
		0);

	auto meshComp = new MeshRenderer(mesh);
	AddComponent(static_cast<StrongActorComponent>(meshComp));
	setTransform(vec3(1.4f), mat4(), vec3(.0, .0, 200));

}

void Batman::DisposeLeft()
{
	delete batarangLeft;
	batarangLeft = nullptr;
}

void Batman::DisposeRight()
{
	delete batarangRight;
	batarangRight = nullptr;
}

void Batman::UpdateBatarangs()
{
	auto joints = bodyFrame->getJointsData();
	auto rightHandJoint = joints[JointType_HandRight].Position;
	auto rightHandVec3 = vec3(-rightHandJoint.X, rightHandJoint.Y, rightHandJoint.Z);

	auto leftHandJoint = joints[JointType_HandLeft].Position;
	auto leftHandVec3 = vec3(-leftHandJoint.X, leftHandJoint.Y, leftHandJoint.Z);

	auto headJoint = joints[JointType_Head].Position;
	auto headVec3 = vec3(-headJoint.X - 0.09f, headJoint.Y, headJoint.Z);

	if (rightHandJoint.Y > headJoint.Y + 0.1f && batarangRight == nullptr )
	{
		batarangRight = new Batarang(m_world, renderer, rightHandVec3);
	}
	else if (batarangRight != nullptr && 
		!batarangRight->IsThrown() &&
		joints[JointType_HandRight].TrackingState == TrackingState::TrackingState_Tracked)
	{
		batarangRight->setTransform(batarangScale, mat4(GetBodyOrientation()), rightHandVec3);
	}

	if (leftHandJoint.Y > headJoint.Y + 0.1f && batarangLeft == nullptr)
	{
		batarangLeft = new Batarang(m_world, renderer, leftHandVec3);
	}
	else if (batarangLeft != nullptr &&
		!batarangLeft->IsThrown() &&
		joints[JointType_HandLeft].TrackingState == TrackingState::TrackingState_Tracked)
	{
		batarangLeft->setTransform(batarangScale, mat4(GetBodyOrientation()), leftHandVec3);
	}
}

void Batman::LeftHandEnergyHitMax()
{
	auto momentum = GetLeftHandKinematicEnergy();
	auto speed = glm::length(momentum);
	auto throwDir = glm::normalize(momentum);

	UpdateBatarangThrow(batarangLeft, throwDir, speed);
	if (batarangLeft != nullptr)
	{
		auto prevBatarang = batarangLeft;
		usedBatarangs.push_back(prevBatarang);
		batarangLeft = nullptr;
	}
}


void Batman::RightHandEnergyHitMax()
{
	auto momentum = GetRightHandKinematicEnergy();
	auto speed = glm::length(momentum);
	auto throwDir = glm::normalize(momentum);

	UpdateBatarangThrow(batarangRight, throwDir, speed);
	if (batarangRight != nullptr)
	{
		auto prevBatarang = batarangRight;
		usedBatarangs.push_back(prevBatarang);
		batarangRight = nullptr;
	}
}

void Batman::UpdateBatarangThrow(Batarang* ba, const vec3& dir,
	float speed)
{
	if (ba != nullptr)
	{
		ba->SetMotion(dir);
	}
}



void Batman::Destroy()
{
	delete batMask;
	batMask = nullptr;

	delete batarangLeft;
	batarangLeft = nullptr;

	delete batarangRight;
	batarangRight = nullptr;

	cape->Destroy();
	delete cape;

	for (size_t i = 0; i < usedBatarangs.size(); i++)
	{
		usedBatarangs[i]->Destroy();
		delete usedBatarangs[i];
		usedBatarangs.erase(usedBatarangs.begin() + i);
	}
}

void Batman::Update(float dt)
{
	KinectPlayer::Update(dt);
	UpdateBatarangs();
	if (batarangLeft != nullptr)
	{
		batarangLeft->Update(dt);
	}

	if (batarangRight != nullptr)
	{
		batarangRight->Update(dt);
	}

	auto bodyOrientation = GetBodyOrientation();



	auto h = bodyFrame->getJointsData()[JointType_Head].Position;
	auto n = bodyFrame->getJointsData()[JointType_SpineShoulder].Position;

	auto s = bodyFrame->getJointsData()[JointType_ShoulderLeft].Position;


	auto h_vec3 = vec3(-h.X, h.Y, h.Z);
	auto n_vec3 = vec3(-n.X, n.Y, n.Z);
	auto s_vec3 = vec3(-s.X, s.Y, s.Z);

	auto y = glm::normalize(h_vec3 - n_vec3);
	auto x = glm::normalize(s_vec3 - n_vec3);
	auto z = glm::cross(x, y);

	auto rot = mat4(mat3(x, y, z));
	
	//rot[0].y *= -1;
	batMask->setTransform(vec3(4.5f, 5.5f, 5.0f), rot, vec3(-h.X, h.Y + 0.01, h.Z));
	batMask->Update(dt);


	if (cape != nullptr)
	{
		auto rot = GetBodyOrientation();
		cape->SetClothPose(n_vec3,
			quat());
		cape->Update(dt);
	}

	for (int i = 0; i < usedBatarangs.size(); i ++)
	{
		if (usedBatarangs[i] != nullptr)
		{
			if (usedBatarangs[i]->IsDisposable())
			{
				usedBatarangs[i]->Destroy();
				usedBatarangs[i] = nullptr;
				usedBatarangs.erase(usedBatarangs.begin() + i);
			}
			else
			{
				usedBatarangs[i]->Update(dt);
			}
		}
	}

}

void Batman::LowerLeftArmCollisionBuilt()
{
	auto rbody = leftLowerArm->GetComponent<RigidBody>().lock().get();
	m_world->SetFilterData(rbody->body, VirtualTopDefenseFilterGroup::eWeapon,
		VirtualTopDefenseFilterGroup::eEnemy);

}

void Batman::LowerRightArmCollisionBuilt() 
{
	auto rbody = rightLowerArm->GetComponent<RigidBody>().lock().get();
	m_world->SetFilterData(rbody->body, VirtualTopDefenseFilterGroup::eWeapon,
		VirtualTopDefenseFilterGroup::eEnemy);

}

void Batman::LowerLeftLegCollisionBuilt()
{
	auto rbody = leftHive->GetComponent<RigidBody>().lock().get();
	m_world->SetFilterData(rbody->body, VirtualTopDefenseFilterGroup::eWeapon,
		VirtualTopDefenseFilterGroup::eEnemy);

}

void Batman::LowerRightLegCollisionBuilt()
{
	auto rbody = rightHive->GetComponent<RigidBody>().lock().get();
	m_world->SetFilterData(rbody->body, VirtualTopDefenseFilterGroup::eWeapon,
		VirtualTopDefenseFilterGroup::eEnemy);

}


void Batman::Draw(Renderer* r,
	GLuint passIndex)
{
	KinectPlayer::Draw(r, passIndex);

	if (batarangLeft != nullptr)
	{
		batarangLeft->Draw(renderer, passIndex);
	}

	if (batarangRight != nullptr)
	{
		batarangRight->Draw(renderer, passIndex);
	}

	//head->Draw(r, passIndex);
	//leftLowerArm->Draw(r, passIndex);
	//rightLowerArm->Draw(r, passIndex);
	/*head->Draw(r, passIndex);*/
	cape->Draw(r, passIndex);

	if (passIndex == 0)
		batMask->Draw(r, 0);
	
	for (auto* b : usedBatarangs)
	{
		if (b != nullptr)
		{
			b->Draw(r, passIndex);
		}
	}
}



Batman::~Batman()
{
}

