#include "CubeEnemy.h"
#include <Actors\Components\MeshComponents\MeshRenderer.h>
#include <Game\GameObjects\PlayerModes\Earth.h>
#include <Actors\GameObjects\KinectPlayer.h>
#include <Game\VirtualTopDefense.h>
GeometryInfo* CubeEnemy::geo = nullptr;
TextureInfo CubeEnemy::cubeTex = TextureInfo();


CubeEnemy::CubeEnemy(physx::PxPhysicsWorld* world,
	Renderer* renderer,
	const vec3& p)
	: m_world(world),
	r(renderer)
{
	*m_translation = p;
}

void CubeEnemy::InitData(bool useGravity, vec3& goal, vec3 scale, float density)
{
	setTransform(scale, mat4(), *m_translation);

    rigidBody = new RigidBody(m_world, 0.8f, 0.8f, 0.0f);

	m_goal = goal;

	rigidBody->InitializeWithBox(m_world,
		*m_scale,
		*m_rotation,
		*m_translation,
		density);

	rigidBody->SetBodyUserData(this);
	rigidBody->SetEnableGravity(useGravity);

	m_world->SetFilterData(rigidBody->body, VirtualTopDefenseFilterGroup::eEnemy,
		VirtualTopDefenseFilterGroup::eBody);

	AddComponent(static_cast<StrongActorComponent>(rigidBody));

	 
	if (geo == nullptr)
	{
		std::string file = "Resources//Mesh//cube.bin";

		ObjLoader loader;
		loader.loadFromFile(file.c_str());
		auto sphere = loader.getModel();

		geo = r->addGeometry(sphere.verts,
			sphere.numVerts,
			reinterpret_cast<GLuint*>(sphere.indices),
			sphere.numIndices,
			Neumont::Vertex::STRIDE,
			GL_UNSIGNED_INT,
			GL_TRIANGLES);

		cubeTex.addColorTexture("Resources\\Texture\\gold_color.jpg",
			FIF_JPEG,
			r->shaders[0]->program_id);

		//cubeTex.addNormalTexture("Resources\\Texture\\rock_normal.jpg",
		//	FIF_JPEG,
		//	r->shaders[0]->program_id);
		sphere.cleanUp();

	}


	auto sphereRenderable = r->addRenderable(*geo,
		new mat4,
		cubeTex,
		0);

	auto meshComp = new MeshRenderer(sphereRenderable);
	AddComponent(static_cast<StrongActorComponent>(meshComp));
}


void CubeEnemy::onContact(Actor* other)
{
	auto earth = dynamic_cast<Earth*>(other);

	if (earth != nullptr)
	{
		disposable = true;
		earth->TakeHit();
	}

	Die();

}


bool CubeEnemy::IsDisposable()
{

	return disposable;
}

void CubeEnemy::Update(float dt)
{
	Actor::Update(dt);
	auto pos = *m_translation;
	m_dir = glm::normalize(m_goal - pos);

	auto force = m_dir * m_speed * dt;
	if (isAlive)
	{
		rigidBody->body->setLinearVelocity(physx::ToPxVec3(force));
	}
	else
	{
		timer += dt;

		if (timer > 3.0f)
		{
			disposable = true;
		}
	}

	rigidBody->body->addTorque(physx::ToPxVec3(force));
	if (pos.x < -4.0f || pos.x > 4.0 || pos.y < -4.0f || pos.y > 4.0f ||
		pos.z < -5.0f || pos.z > 8.0f)
	{
		disposable = true;
	}
}

void CubeEnemy::Die()
{
	isAlive = false;
	rigidBody->SetEnableGravity(true);
}

CubeEnemy::~CubeEnemy()
{
}
