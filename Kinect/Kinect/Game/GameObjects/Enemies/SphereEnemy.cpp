#include "SphereEnemy.h"
#include <Game\VirtualTopDefense.h>
#include <Game\GameObjects\PlayerModes\Earth.h>
#include <Actors\Components\MeshComponents\MeshRenderer.h>
#include <Actors\GameObjects\KinectPlayer.h>
GeometryInfo* SphereEnemy::geo = nullptr;
TextureInfo SphereEnemy::sphereTex = TextureInfo();

SphereEnemy::SphereEnemy(physx::PxPhysicsWorld* world,
	Renderer* renderer,
	const vec3& p)
	: m_world(world),
	r(renderer)
{
	*m_translation = p;
}

void SphereEnemy::InitData(bool useGravity, vec3& goal, vec3 scale, float density)
{
	setTransform(scale, mat4(), *m_translation);
	
	rigidBody = new RigidBody(m_world, 0.8f, 0.8f, 0.1f);
	float radius = 0.5f;
	auto tempScale = m_scale->x ;
	rigidBody->InitializeWithSphere(m_world,
		tempScale,
		*m_rotation,
		*m_translation,
		density);

	rigidBody->SetBodyUserData(this);
	rigidBody->SetEnableGravity(useGravity);
	m_world->SetFilterData(rigidBody->body, VirtualTopDefenseFilterGroup::eEnemy,
		VirtualTopDefenseFilterGroup::eBody);


	AddComponent(static_cast<StrongActorComponent>(rigidBody));

	m_goal = goal;

	if (geo == nullptr)
	{
		std::string file = "Resources//Mesh//sphere.bin";

		ObjLoader loader;
		loader.loadFromFile(file.c_str());
		auto sphere = loader.getModel();
		geo = r->addGeometry(sphere.verts,
			sphere.numVerts,
			reinterpret_cast<GLuint*>(sphere.indices),
			sphere.numIndices,
			Neumont::Vertex::STRIDE,
			GL_UNSIGNED_INT,
			GL_TRIANGLES);
		sphere.cleanUp();
		sphereTex.addColorTexture("Resources\\Texture\\rock_color.jpg",
			FIF_JPEG,
			r->shaders[0]->program_id);

		//sphereTex.addNormalTexture("Resources\\Texture\\rock_normal.jpg",
		//	FIF_JPEG,
		//	r->shaders[0]->program_id);
	}

	auto sphereRenderable = r->addRenderable(*geo,
		new mat4,
		sphereTex,
		0);

	auto meshComp = new MeshRenderer(sphereRenderable);
	AddComponent(static_cast<StrongActorComponent>(meshComp));
}

void SphereEnemy::onContact(Actor* other)
{
	auto earth = dynamic_cast<Earth*>(other);

	if (earth != nullptr)
	{
		earth->TakeHit();
		disposable = true;
	}

	Die();
}

bool SphereEnemy::IsDisposable()
{

	return disposable;
}


void SphereEnemy::Update(float dt)
{
	Actor::Update(dt);
	auto pos = *m_translation;
	m_dir = glm::normalize(m_goal - pos);

	auto force = m_dir * m_speed * dt;
	if (isAlive)
	{
		rigidBody->body->setLinearVelocity(physx::ToPxVec3(force));
	}

	rigidBody->body->addTorque(physx::ToPxVec3(force));

	if (pos.x < -4.0f || pos.x > 4.0 || pos.y < -4.0f || pos.y > 4.0f ||
		pos.z < -5.0f || pos.z > 8.0f)
	{
		disposable = true;
	}

	if (!isAlive)
	{
		timer += dt;

		if (timer > 3.0f)
		{
			disposable = true;
		}
	}

}


void SphereEnemy::Die()
{
	isAlive = false;
	rigidBody->SetEnableGravity(true);
}

SphereEnemy::~SphereEnemy()
{
}
