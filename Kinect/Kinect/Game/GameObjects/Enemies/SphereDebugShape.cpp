#include "SphereDebugShape.h"
#include <Game\VirtualTopDefense.h>
#include <Game\GameObjects\PlayerModes\Earth.h>
#include <Actors\Components\MeshComponents\MeshRenderer.h>
#include <Actors\GameObjects\KinectPlayer.h>
GeometryInfo* SphereDebugShape::geo = nullptr;
TextureInfo SphereDebugShape::sphereTex = TextureInfo();

SphereDebugShape::SphereDebugShape(physx::PxPhysicsWorld* world,
	Renderer* renderer,
	const vec3& p)
	: m_world(world),
	r(renderer)
{
	*m_translation = p;
}

void SphereDebugShape::InitData(bool useGravity, 
	vec3& goal,
	vec3 scale,
	float density,
	bool enableARDepthTesting,
	GLchar* texPath)
{
	setTransform(scale, mat4(), *m_translation);
	
	rigidBody = new RigidBody(m_world, 0.8f, 0.8f, 0.1f);
	float radius = 0.5f;

	rigidBody->InitializeWithSphere(m_world,
		m_scale->x,
		*m_rotation,
		*m_translation,
		density);

	rigidBody->SetBodyUserData(this);
	rigidBody->SetEnableGravity(useGravity);
	m_world->SetFilterData(rigidBody->body, VirtualTopDefenseFilterGroup::eEnemy ,
		VirtualTopDefenseFilterGroup::eBody);

	AddComponent(static_cast<StrongActorComponent>(rigidBody));

	m_goal = goal;

	if (geo == nullptr)
	{
		std::string file = "Resources//Mesh//sphere.bin";

		ObjLoader loader;
		loader.loadFromFile(file.c_str());
		auto sphere = loader.getModel();
		geo = r->addGeometry(sphere.verts,
			sphere.numVerts,
			reinterpret_cast<GLuint*>(sphere.indices),
			sphere.numIndices,
			Neumont::Vertex::STRIDE,
			GL_UNSIGNED_INT,
			GL_TRIANGLES);
		sphere.cleanUp();

		sphereTex.addColorTexture(texPath,
			FIF_JPEG,
			r->shaders[0]->program_id);

		//sphereTex.addNormalTexture("Resources\\Texture\\rock_normal.jpg",
		//	FIF_JPEG,
		//	r->shaders[0]->program_id);
	}

	auto sphereRenderable = r->addRenderable(*geo,
		new mat4,
		sphereTex,
		0);

	m_enableArDepthTesting= enableARDepthTesting;
	//r->addRenderableUniformParameter(sphereRenderable,
	//	"isSkyBox",
	//	ShaderUniformParameterType::SHADER_BOOL,
	//	(char*)&m_enableArDepthTesting);

	auto meshComp = new MeshRenderer(sphereRenderable);
	AddComponent(static_cast<StrongActorComponent>(meshComp));
}

void SphereDebugShape::Hover()
{
	isHovering = true;
}


void SphereDebugShape::onContact(Actor* other)
{
	auto earth = dynamic_cast<Earth*>(other);

	if (earth != nullptr)
	{
		earth->TakeHit();
		disposable = true;
	}
	else
	{
		auto body = dynamic_cast<Limb*>(other);

		if (body != nullptr)
		{
			Die();
		}
	}


}

bool SphereDebugShape::IsDisposable()
{

	return disposable;
}


void SphereDebugShape::Update(float dt)
{
	Actor::Update(dt);
	auto pos = *m_translation;
	m_dir = glm::normalize(m_goal - pos);

	if (!isHovering)
	{
		auto force = m_dir * m_speed * dt;
		if (isAlive)
		{
			rigidBody->body->setLinearVelocity(physx::ToPxVec3(force));
		}
		else
		{
			timer += dt;

			if (timer > 3.0f)
			{
				disposable = true;
			}

			rigidBody->body->setLinearVelocity(PxVec3(.0f));

		}

		rigidBody->body->addTorque(physx::ToPxVec3(force));

	}

	if (pos.x < -4.0f || pos.x > 4.0 || pos.y < -4.0f || pos.y > 4.0f ||
		pos.z < -5.0f || pos.z > 8.0f)
	{
		disposable = true;
	}


}


void SphereDebugShape::Die()
{
	isAlive = false;
	rigidBody->SetEnableGravity(true);
}

SphereDebugShape::~SphereDebugShape()
{
}
