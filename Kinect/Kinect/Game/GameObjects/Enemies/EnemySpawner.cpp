#include "EnemySpawner.h"

EnemySpawner::EnemySpawner(
	physx::PxPhysicsWorld* world,
	Renderer* r) : 
	m_world(world),
	renderer(r)
{

}

void EnemySpawner::GenerateSphereEnemy(bool isAirEnemy, vec3& p)
{
	auto sphere = new SphereEnemy(m_world, renderer, p);
	auto scale = isAirEnemy ? vec3(.1f) : vec3(.3f);
	sphere->InitData(!isAirEnemy, enemy_goal, scale, 1.3f);
	m_container.AddActor(sphere);
}

void EnemySpawner::GenerateCubeEnemy(bool isAirEnemy, vec3& p)
{
	auto cube = new CubeEnemy(m_world, renderer, p);
	cube->InitData(!isAirEnemy, enemy_goal, vec3(.1f), 1.3f);
	m_container.AddActor(cube);
}

vec3 EnemySpawner::GenPosition()
{
	std::uniform_real_distribution<float> posDistribution(1.0f, 4.0f);
	auto pos_x = posDistribution(generator);
	pos_x *= pos_x >= 2 ? 1.0f : -1.0f;

	auto pos_y = posDistribution(generator);
	pos_y *= pos_y > 1.5 == 0 ? 1.0f : -1.0f;
	pos_y = glm::clamp(pos_y, -1.0f, 2.0f);

	auto pos_z = 0.0f;

	vec3 pos(pos_x, pos_y, pos_z);

	return pos;
}


void EnemySpawner::GenerateEnemy()
{
	std::uniform_int_distribution<int> distribution(1, 4);
	int index = distribution(generator);
	bool isAir = index % 2 == 0;
	auto pos = GenPosition();

	if (index <= 2)
	{
		GenerateCubeEnemy(true, pos);
	}
	else
	{
		GenerateSphereEnemy(true, pos);
	}
	enemyCount++;
}

void EnemySpawner::UpdateEnemies(float dt)
{
	auto m = m_container.GetContainer();
	for (std::map<std::string, Actor*>::iterator e = m.begin(); e != m.end();e++)
	{
		if (e->second->IsDisposable())
		{
			m_container.RemoveActor(e->second);
			delete e->second;
		}
		else
		{
			e->second->Update(dt);
			
			if (enableDeadZone)
			{
				auto distance = glm::distance(*e->second->m_translation, deadZone.pos);
				if (distance <= deadZone.radius)
				{
					e->second->disposable = true;
				}
			}
		}
	}
}

void EnemySpawner::SetGoal(const vec3& goal)
{
	enemy_goal = goal;
}

void EnemySpawner::EnableDeadZone(DeadZone dz)
{
	enableDeadZone = true;
	deadZone = dz;
}

void EnemySpawner::DisableDeadZone()
{
	enableDeadZone = false;
	deadZone.pos = vec3(.0f);
	deadZone.radius = .0f;
}



void EnemySpawner::Update(float dt)
{
	if (timer >= spanwerInterval)
	{
		auto m = m_container.GetContainer();
		if (m.size() < maxEnemy)
		{
			GenerateEnemy();
		}

		timer = .0f;
	}
	else
	{
		timer += dt;
	}

	UpdateEnemies(dt);
}

void EnemySpawner::Draw(Renderer* r, uint passIndex)
{
	for (auto& e : m_container.GetContainer())
	{
		e.second->Draw(r, passIndex);
	}
}

void EnemySpawner::SetSpwnerInterval(float interval)
{
	spanwerInterval = interval;
}



EnemySpawner::~EnemySpawner()
{
}
