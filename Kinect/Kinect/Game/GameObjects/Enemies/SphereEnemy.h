#pragma once
#include <Actors\Actor.h>
#include <Actors\Components\Physics\RigidBody.h>
class Renderer;
class SphereEnemy : public Actor
{
	physx::PxPhysicsWorld* m_world;
	Renderer* r;
	RigidBody* rigidBody;
	static GeometryInfo* geo;
	static TextureInfo sphereTex;

	vec3 m_dir;
	vec3 m_goal;
	float m_speed = 1.6f;
	
	float timer = 0.0f;

	float m_detectionDuration = 1.0f;
	float m_floatingHeight = 0.5f;

	bool isAlive = true;
	

public:
	explicit SphereEnemy(physx::PxPhysicsWorld* world,
		Renderer* r,
		const vec3& p);

	void InitData(bool useGravity, vec3& goal, vec3 scale, float density);

	void onContact(Actor* other) override;
	virtual bool IsDisposable() override;

	~SphereEnemy();

	bool Disposable();
	void Update(float dt) override;

	void Die();
};

