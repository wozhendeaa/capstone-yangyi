#pragma once
#include <Game\GameObjects\Enemies\SphereEnemy.h>
#include <Game\GameObjects\Enemies\CubeEnemy.h>
#include <Actors\ActorContainer.h>
#include <random>

struct DeadZone
{
	vec3 pos;
	float radius;
};

class EnemySpawner
{
	physx::PxPhysicsWorld* m_world;
	Renderer* renderer;
	ActorContainer m_container;
	vec3 enemy_goal;
	std::default_random_engine generator;

	int maxEnemy = 5;
	int enemyCount = 0;

	float timer{ .0f };
	float spanwerInterval{ .5f };

	bool enableDeadZone = false;
	DeadZone deadZone;

	void GenerateSphereEnemy(bool isAirEnemy, vec3& p);
	void GenerateCubeEnemy(bool isAirEnemy, vec3& p);
	void GenerateEnemy();
	void UpdateEnemies(float dt);


	vec3 GenPosition();
public:

	EnemySpawner(
		physx::PxPhysicsWorld* world,
		Renderer* r);
	~EnemySpawner();

	void EnableDeadZone(DeadZone deadZone);

	void DisableDeadZone();

	void SetGoal(const vec3& goal);

	void SetSpwnerInterval(float interval);

	void Update(float dt);

	void Draw(Renderer* r, uint passIndex = 0);
};

