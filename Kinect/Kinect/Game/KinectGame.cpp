#include "KinectGame.h"
#include <Actors\Components\MeshComponents\MeshRenderer.h>
#include <Actors\Components\Physics\StaticMesh.h>
#include <Actors\Components\Physics\RigidBody.h>
#include <Actors\Components\Physics\StaticMesh.h>
#include <Actors\Components\GUITexture.h>
#include <Actors\ParticleSystem\ParticleSystem.h>
#include <Actors\GameObjects\StaticMesh\GroundActor.h>
#include <QtGui\qmouseeventtransition.h>
#include <iostream>
#include <Actors\GameObjects\KinectPlayer.h>
#include <MemoryDebug.h>
#include <DebugTools\DebugShapesManager.h>
#include <glm\gtx\rotate_vector.hpp>
#include <Game\GameObjects\PlayerModes\Weapons\Lightsaber.h>
#include <Game\GameObjects\Enemies\EnemySpawner.h>
#include <Game\GameObjects\Enemies\SphereDebugShape.h>
#include <Game\GameObjects\PlayerModes\Earth.h>
#include <Actors\GameObjects\ClothActor.h>

using BatEngine::Renderer;
using Neumont::ShapeGenerator;
using namespace BatEngine;
  KinectGame::KinectGame(Renderer* r)
{
	kinectSources = new KinectSources;
	imageRenderer = new ImageRenderer(r, 
		KinectColor::colorWidth,
		KinectColor::colorHeight,
		KinectColor::colorWidth *
		sizeof(RGBQUAD));
	renderer = r;

	physicsWorld = nullptr;
	debugShapeManager = DebugShapesManager::getInstance();
	debugShapeManager->setRenderer(renderer);
}

void KinectGame::InitGame()
{
	prepPhysics();
	prepGameObjects();
	prepKinect();
}

void KinectGame::prepKinect()
{
	float x0 = 0;
	float y0 = 0;

	TextureInfo slashTex;
	slashTex.addColorTexture("Resources\\Texture\\batsymbole.jpg",
		FIF_JPEG,
		renderer->shaders[0]->program_id);

	splashScreen = renderer->GenGuiTexture(0,
		slashTex,
		x0,
		y0,
		"color_tex",
		1.0f,
		1.0f,
		true);

	TextureInfo arDepthTex;
	arDepthTex.addDynamicColorTexture(
		KinectDepth::depthWidth,
		KinectDepth::depthHeight,
		renderer->shaders[0]->program_id,
		"ar_depth_map",
		GL_TEXTURE_2D,
		GL_RGB32F,
		GL_RGB,
		GL_FLOAT);

	arDepth = renderer->GenGuiTexture(0,
		arDepthTex,
		x0,
		y0,
		"ar_depth_map",
		0.0f,
		0.0f,
		false,
		true,
		true);

	TextureInfo colorTex;
	colorTex.addDynamicColorTexture(
		//KinectColor::colorWidth,
		//KinectColor::colorHeight,
		KinectDepth::depthWidth,
		KinectDepth::depthHeight,
		renderer->shaders[1]->program_id,
		"color_tex",
		GL_TEXTURE_2D,
		GL_RGB32F,
		GL_RGB,
		GL_UNSIGNED_BYTE);
	//colorTex.addColorTexture("Resources\\Texture\\tile_color.jpg", 
	//FIF_JPEG,
	//renderer->shaders[1]->program_id);

	colorScene = renderer->GenGuiTexture(1,
		colorTex,
		x0,
		y0,
		"color_tex",
		1.0f,
		1.0f,
		false,
		false,
		true);
	colorScene->serializable = false;
	renderer->addRenderableUniformParameter(colorScene, "useScaler", ShaderUniformParameterType::SHADER_BOOL, (char*)&colorScene->serializable);

	TextureInfo depthTex;
	depthTex.addDynamicColorTexture(
		KinectDepth::depthWidth,
		KinectDepth::depthHeight,
		//KinectColor::colorWidth,
		//KinectColor::colorHeight,
		renderer->shaders[1]->program_id,
		"ar_depth_map",
		GL_TEXTURE_2D,
		GL_RGB32F,
		GL_RGB,
		GL_FLOAT);

	depthScene = renderer->GenGuiTexture(1,
		depthTex,
		x0,
		y0,
		"ar_depth_map",
		1.0f,
		1.0f,
		false,
		false,
		true);

	depthScene->vertex_color_on = false;
	renderer->addRenderableUniformParameter(depthScene, "renderingDepth", ShaderUniformParameterType::SHADER_BOOL, (char*)&depthScene->vertex_color_on);

	TextureInfo normalTex;
	normalTex.addDynamicColorTexture(
		KinectDepth::depthWidth,
		KinectDepth::depthHeight,
		renderer->shaders[1]->program_id,
		"ar_normal_map",
		GL_TEXTURE_2D,
		GL_RGB32F,
		GL_RGB,
		GL_FLOAT);

	normalMapScene = renderer->GenGuiTexture(1,
		normalTex,
		x0,
		y0,
		"ar_normal_map",
		1.0f,
		1.0f,
		false,
		false,
		true);

	TextureInfo positionTex;
	positionTex.addDynamicColorTexture(
		KinectDepth::depthWidth,
		KinectDepth::depthHeight,
		renderer->shaders[1]->program_id,
		"ar_position_tex",
		GL_TEXTURE_2D,
		GL_RGB32F,
		GL_RGB,
		GL_FLOAT);
	
	positionMapScene = renderer->GenGuiTexture(1,
		positionTex,
		x0,
		y0,
		"ar_position_tex",
		.0f,
		.0f,
		false,
		false,
		true);

	TextureInfo positionTex0;
	positionTex0.addDynamicColorTexture(
		KinectDepth::depthWidth,
		KinectDepth::depthHeight,
		renderer->shaders[0]->program_id,
		"ar_position_tex",
		GL_TEXTURE_2D,
		GL_RGB32F,
		GL_RGB,
		GL_FLOAT);

	positionMapScene1 = renderer->GenGuiTexture(0,
		positionTex0,
		x0,
		y0,
		"ar_position_tex",
		.0f,
		.0f,
		false,
		false,
		true);

	TextureInfo infaredTex;
	infaredTex.addDynamicColorTexture(
		KinectDepth::depthWidth,
		KinectDepth::depthHeight,
		renderer->shaders[1]->program_id,
		"infared_tex",
		GL_TEXTURE_2D,
		GL_RGB,
		GL_RGB,
		GL_UNSIGNED_BYTE);

	infraredScene = renderer->GenGuiTexture(1,
		infaredTex,
		x0,
		y0,
		"infared_tex",
		.0f,
		.0f,
		false,
		false,
		true);

	virtual_shadow_map.width  = 512;
	virtual_shadow_map.height = 424;
	virtual_color_map.width	  = width;
	virtual_color_map.height  = height;
	//renderer->enableShadow = true;
}


float z = 0;
Renderable* billboard = nullptr;
Renderable* r = nullptr;
Renderable* r2 = nullptr;
Renderable* r3 = nullptr;
GroundActor* visionBox;
GroundActor* a2;
ShockWaveEffect* shockWave = nullptr;
ParticleSystem* ps;
ClothActor* cape = nullptr;
vec3 goal = vec3(.0f, 0.0f, 8.0f);
LightSaber* lightSaber = nullptr;
void KinectGame::prepGameObjects()
{

	TextureInfo particle;
	particle.addColorTexture("Resources\\Texture\\tile_color.jpg",
		FIF_JPEG,
		renderer->shaders[2]->program_id);

	auto sphere = Neumont::ShapeGenerator::makeSphere(10);
	std::auto_ptr<GeometryInfo> sphereGeo(renderer->addGeometry(sphere.verts,
		sphere.numVerts,
		reinterpret_cast<GLuint*>(sphere.indices),
		sphere.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_SHORT, 
		GL_TRIANGLES));
	sphere.cleanUp();


	//auto plane = Neumont::ShapeGenerator::makePlane(3.0f);
	////auto planeGeo =( renderer->addGeometry(plane.verts,
	//std::auto_ptr<GeometryInfo> planeGeo ( renderer->addGeometry(plane.verts,
	//	plane.numVerts,
	//	reinterpret_cast<GLuint*>(plane.indices),
	//	plane.numIndices,
	//	Neumont::Vertex::STRIDE,
	//	GL_UNSIGNED_SHORT, 
	//	GL_TRIANGLES));
	//plane.cleanUp();

	//TextureInfo t;
	//t.addColorTexture("Resources\\Texture\\tile_color.jpg", 
	//FIF_JPEG,
	//renderer->shaders[0]->program_id);

	////r = renderer->addRenderable(*planeGeo, new mat4,t, 0);

	auto cube2 = Neumont::ShapeGenerator::makeCube();
	std::auto_ptr<GeometryInfo> cubeGeo2 ( renderer->addGeometry(cube2.verts,
		cube2.numVerts,
		reinterpret_cast<GLuint*>(cube2.indices),
		cube2.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_SHORT, 
		GL_TRIANGLES));
	cube2.cleanUp();

	r2 = renderer->addRenderable(*sphereGeo,
			new mat4(),
			TextureInfo(),
			0);

	r2->setTransform(vec3(.0f),
		mat4(),
		vec3(.0f));



	r3 = renderer->addRenderable(*cubeGeo2,
		new mat4(),
		TextureInfo(),
		0);

	//r3 = renderer->addRenderable(*cubeGeo2,
	//		new mat4(),
	//		particle,
	//		0);
	

	std::string file = "Resources//Mesh//perception.bin";

	ObjLoader loader;
	loader.loadFromFile(file.c_str());
	auto perception_model = loader.getModelShort();

	std::auto_ptr<GeometryInfo> verts(renderer->addGeometry(perception_model.verts,
		perception_model.numVerts,
		reinterpret_cast<GLuint*>(perception_model.indices),
		perception_model.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_SHORT,
		GL_TRIANGLES));

	perception_model.cleanUp();
	TextureInfo percepTexture;
	percepTexture.addColorTexture("Resources\\Texture\\pattern.bmp",
		FIF_BMP,
		renderer->shaders[0]->program_id);

	auto percep = renderer->addRenderable(*verts,
		new mat4(),
		percepTexture,
		0);

	renderer->addRenderableUniformParameter(percep,
		"isSkyBox",
		ShaderUniformParameterType::SHADER_BOOL,
		(char*)&percep->recievesLight);

	visionBox = new GroundActor;
	auto meshComp = new MeshRenderer(percep);
	visionBox->AddComponent(static_cast<StrongActorComponent>(meshComp));
	visionBox->setTransform(vec3(1.0f), mat4(), vec3(0.0f, 0.0f, -100.0f));
	actors.AddActor(visionBox);
	//auto a1Scale = vec3(1.0f, 1.0f, 1.0f);
	//auto staticMesh = new StaticMesh(physicsWorld, 0.9f, 0.9f);
	//staticMesh->InitializeWithBox(physicsWorld, a1Scale, quat(), vec3(0.0f, 0.0f, 28.0f));
	//a1->AddComponent(static_cast<StrongActorComponent>(staticMesh));
	//actors.AddActor(dynamic_cast<Actor*>(visionBox));

	//a2 = new GroundActor;
	//auto a1Scale = vec3(0.08f);
	//a2->setTransform(a1Scale, quat(), vec3(0.0f, 0.0f, .6f));
	//auto meshComp = new MeshRenderer(r3);
	//meshComp->EnableCastShadow();
	//a2->AddComponent(static_cast<StrongActorComponent>(meshComp));
	//actors.AddActor(dynamic_cast<Actor*>(a2));


	//a2 = new groundactor;
	//a2->settransform(vec3(.3f), quat(), vec3(-1.0f, 0.1f, 2.0f));
	//auto meshcomp2 = new meshrenderer(r2);
	//a2->addcomponent(static_cast<strongactorcomponent>(meshcomp2));

	//auto rigidBody = new RigidBody(physicsWorld, 0.0f, 5.8f, 0.0f, true);
	//float radius = 0.3f;
	//rigidBody->InitializeWithSphere(physicsWorld, radius, quat(), vec3(-3.0f, 0.0f, 3.0f), 5);
	////rigidBody->InitializeWithBox(physicsWorld, vec3(0.1f), quat(), vec3(0.0f, 1.0f, 0.0f), 5);
	//a2->AddComponent(static_cast<StrongActorComponent>(rigidBody));
	//actors.AddActor(dynamic_cast<Actor*>(a2));

	auto ground = new GroundActor;
	auto a1Scale = vec3(10.0f, .1f, 10.0f);
	ground->setTransform(a1Scale, quat(), vec3(0.0f, .0f, 0.5f));
	auto meshComp2 = new MeshRenderer(r3);

	auto staticMesh = new StaticMesh(physicsWorld, 0.9f, 0.9f);
	staticMesh->InitializeWithBox(physicsWorld, a1Scale, quat(), vec3(0.0f, -1.1f, .0f));
	//ground->AddComponent(static_cast<StrongActorComponent>(meshCom p2));
	ground->AddComponent(static_cast<StrongActorComponent>(staticMesh));
	actors.AddActor(dynamic_cast<Actor*>(ground));


	Earth* earth = new Earth(physicsWorld,renderer,vec3(.0f, .0f, 1.0f)) ;
	actors.AddActor(earth);

	enemySpawner = new EnemySpawner(physicsWorld, renderer);
	enemySpawner->SetGoal(*earth->m_translation);
	enemySpawner->SetSpwnerInterval(5.5f);

	//auto scale = vec3(0.15f);
	//vec3 pos(-1.0f, -.3, 2.0f);
	//vec3 offset(.4f, 0.0, -.1f);

	//for (size_t i = 0; i < 5; i++)
	//{
	//	auto cube1 = new GroundActor;
	//	auto cr = renderer->addRenderable(*cubeGeo2,
	//		new mat4(),
	//		t,
	//		0);
	//	auto meshComp2 = new MeshRenderer(cr);
	//	cube1->AddComponent(static_cast<StrongActorComponent>(meshComp2));
	//	cube1->setTransform(vec3(0.7f), quat(), vec3(0.0f, 1.0f, 5.5f));

	//	auto rigidBody = new RigidBody(physicsWorld, 0.0f, 2.8f, 0.0f);
	//	rigidBody->InitializeWithBox(physicsWorld, scale, quat(), pos, 3);
	//	cube1->AddComponent(static_cast<StrongActorComponent>(rigidBody));
	//	actors.AddActor(dynamic_cast<Actor*>(cube1));
	//	pos += offset;
	//}

	//pos = vec3(-1.0f, -.4, 2.0f);
	//for (size_t i = 0; i < 5; i++)
	//{
	//	auto cube1 = new GroundActor;
	//	auto cr = renderer->addRenderable(*cubeGeo2,
	//		new mat4(),
	//		t,
	//		0);
	//	auto meshComp2 = new MeshRenderer(cr);
	//	cube1->AddComponent(static_cast<StrongActorComponent>(meshComp2));
	//	cube1->setTransform(vec3(0.7f), quat(), vec3(0.0f, 1.0f, 5.5f));

	//	auto rigidBody = new RigidBody(physicsWorld, 0.0f, 2.8f, 0.0f);
	//	rigidBody->InitializeWithBox(physicsWorld, scale, quat(), pos, 3);
	//	cube1->AddComponent(static_cast<StrongActorComponent>(rigidBody));
	//	actors.AddActor(dynamic_cast<Actor*>(cube1));
	//	pos += offset;
	//}

	//pos = vec3(-1.0f, .0, 2.0f);
	//for (size_t i = 0; i < 5; i++)
	//{
	//	auto cube1 = new GroundActor;
	//	auto cr = renderer->addRenderable(*cubeGeo2,
	//		new mat4(),
	//		t,
	//		0);
	//	auto meshComp2 = new MeshRenderer(cr);
	//	cube1->AddComponent(static_cast<StrongActorComponent>(meshComp2));
	//	cube1->setTransform(vec3(0.7f), quat(), vec3(0.0f, 1.0f, 5.5f));

	//	auto rigidBody = new RigidBody(physicsWorld, 0.0f, 2.8f, 0.0f);
	//	rigidBody->InitializeWithBox(physicsWorld, scale, quat(), pos, 3);
	//	cube1->AddComponent(static_cast<StrongActorComponent>(rigidBody));
	//	actors.AddActor(dynamic_cast<Actor*>(cube1));
	//	pos += offset;
	//}

	vec3 pos(1.0f, 1.0f, 2.0f);
	lightSaber = new LightSaber(physicsWorld, renderer, pos);
	lightSaber->SetActivated(false);
	actors.AddActor(lightSaber);
	
	//SwitchToWizard();
	//SwitchToJedi();
	SwitchToBatman();


	renderer->addUniform("cameraSpace_orientation",
		renderer->shaders[1]->program_id,
		ShaderUniformParameterType::SHADER_MAT3,
		&cameraSpaceOrientation);

	renderer->addUniform("kinectHeight",
		renderer->shaders[1]->program_id,
		ShaderUniformParameterType::SHADER_FLOAT,
		&kinectHeight);

	
	//renderer->enableShadow = true;
	//renderer->enableSoftShadow = true;
	
	renderer->AttachTextureToFramebuffer(shadow_fbo_id,
		&virtual_color_map,
		1,
		&virtual_shadow_map,
		1,
		"virtual_color_map",
		"virtual_depth_map");

	float x0 = .5f;
	float y0 = .5f;

	virtualShadowScene = renderer->GenGuiTexture(0,
		virtual_shadow_map,
		x0,
		y0,
		"shadow_map",
		.0f,
		.0f,
		false,
		false,
		false);

	virtualShadowScene1 = renderer->GenGuiTexture(1,
		virtual_shadow_map,
		x0,
		y0,
		"virtual_depth_map",
		.0f,
		.0f,
		false,
		false,
		false);

	virtualGameScene = renderer->GenGuiTexture(1,
		virtual_color_map,
		x0,
		y0,
		"virtual_color_map",
		0.0f,
		0.0f,
		false,
		false,
		false);


	renderer->cams[1] = new Camera;
	renderer->cams[1]->lookAt(vec3(0.1f, -5.4f, 10.0f));
	renderer->cams[1]->position = shadowLightPosition;
	renderer->current_cams_count++;
}

void KinectGame::setGameWindowSize(int width, 
	int height)
{
	this->width = width;
	this->height = height;
}

void KinectGame::prepMeshs()
{

}

void KinectGame::prepPhysics()
{
	physicsWorld = new physx::PxPhysicsWorld;
	physicsWorld->Initialize();
}

void KinectGame::SwitchToBatman()
{
	if (player != nullptr)
	{
		player->Destroy();
		delete player;
	}
	batmanPlayer = new Batman(&kinectSources->getBodySource(),
		renderer,
		physicsWorld);
	//batmanPlayer->AttachCameraToHead(renderer->current_cam);
	player = batmanPlayer;
	lightSaber->SetActivated(false);
	playerMode = PlayerMode::BATMAN;
}

void KinectGame::SwitchToJedi()
{
	if (player != nullptr)
	{
		player->Destroy();
		delete player;
	}

	jediPlayer = new Jedi(&kinectSources->getBodySource(),
		renderer,
		physicsWorld);
	//jediPlayer->AttachCameraToHead(renderer->current_cam);
	lightSaber->SetActivated(true);
	playerMode = PlayerMode::JEDI;
	player = jediPlayer;
}

void KinectGame::SwitchToWizard()
{
	if (player != nullptr)
	{
		player->Destroy();
		delete player;
	}
	wizardPlayer = new Wizard(&kinectSources->getBodySource(),
		renderer,
		physicsWorld);

	wizardPlayer->AttachLight(&renderer->data->lightPosition);

	player = wizardPlayer;
	playerMode = PlayerMode::WIZARD;
	lightSaber->SetActivated(false);
}


void KinectGame::SwitchMode(float dt)
{
	static float timer = .0f;

	if (timer > 1.0f)
	{
		if (playerMode == PlayerMode::BATMAN)
		{
			SwitchToJedi();
		}
		else if (playerMode == PlayerMode::JEDI)
		{
			SwitchToWizard();
		}
		else if (playerMode == PlayerMode::WIZARD)
		{
			SwitchToBatman();
			enemySpawner->DisableDeadZone();
		}

		timer = .0f;
	}

	timer += dt;
}

void KinectGame::SpawnTestParticles(float dt)
{
	static float timer = 2.0f;
	if (timer > .5f && !testParticlesSpawned)
	{
		timer = .0f;

		vec3 p(-1.0f, 1.0f, .5f);

		for (size_t i = 0; i < 10; i++)
		{
			float offset = 0.2f;
			p.x = -1.0f;
			for (size_t i = 0; i < 10; i++)
			{
				auto ball = new SphereDebugShape(physicsWorld, renderer, p);
				ball->InitData(true, p, vec3(.05f), 20.0f, true, 
					"Resources\\Texture\\rock_normal.jpg");
				ball->Hover();
				testParticles.AddActor(ball);
				p.x += offset;
			}
		
			p.z += offset;
		}

		testParticlesSpawned = true;
	}

	timer += dt;
}


void KinectGame::UpdateTestParticles(float dt)
{
	static float timer = .0f;
	if (testParticlesSpawned)
	{
		timer += dt;
	}

	if (timer > 5.0f)
	{
		testParticles.Destroy();
		testParticlesSpawned = false;
		timer = .0f;
	}

	testParticles.Update(dt);
}


void KinectGame::updateInput(float dt)
{
	auto VK_F = 0x46;
	auto VK_G = 0x47;
	auto VK_I = 0x49;
	auto VK_J = 0x4A;
	auto VK_K = 0x4B;
	auto VK_C = 0x43;
	auto VK_L = 0x4C;

	static float x_scaler = 1.0f;
	static float y_scaler = 1.0f;
	static float x_offset = .0f;
	static float y_offset = .0f;

	if (GetAsyncKeyState(VK_G))
	{
		SpawnTestParticles(dt);
	}

	if (GetAsyncKeyState(VK_C))
	{
		SwitchMode(dt);
	}

	if (GetAsyncKeyState(VK_UP))
	{
		//y_offset += 0.001f;
		//kinectSources->getDepthSource().deltaThreshold += 1;
		shadowLightPosition.z += 0.1f;
	}

	if (GetAsyncKeyState(VK_DOWN))
	{
		//y_offset -= 0.001f;
		//kinectSources->getDepthSource().deltaThreshold -= 1;
		shadowLightPosition.z -= 0.1f;

	}


	if (GetAsyncKeyState(VK_LEFT))
	{
		//x_offset -= 0.001f;
		//kinectSources->getColorSource().x_trunctuation -= 1;
		//shadowLightPosition.x -= 0.1f;
		lightSaber->DetectedByForce(0.0f, 3.0f);
		
	}

	if (GetAsyncKeyState(VK_RIGHT))
	{
		//x_offset += 0.001f;
		//kinectSources->getColorSource().x_trunctuation += 1;
		shadowLightPosition.x += 0.1f;

	}

	if(GetAsyncKeyState(VK_I))
	{
		//y_scaler += 0.001f;
		shadowLightPosition.y += 0.1f;
		runPlayer = true;
	}
	
	if(GetAsyncKeyState(VK_J))
	{
		//x_scaler -= 0.001f;
		runGame = true;
	}
	
	if(GetAsyncKeyState(VK_K))
	{
		shadowLightPosition.y -= 0.1f;
		//y_scaler -= 0.001f;
	}
	
	if(GetAsyncKeyState(VK_L))
	{
		kinectSources->UpdateRoomScanning();
	}
	
	auto x = glGetUniformLocation(renderer->shaders[1]->program_id, "x_scaler");
	auto y = glGetUniformLocation(renderer->shaders[1]->program_id, "y_scaler");
	glUniform1f(x, x_scaler);
	glUniform1f(y, y_scaler);

	auto x_off = glGetUniformLocation(renderer->shaders[1]->program_id, "x_offset");
	auto y_off = glGetUniformLocation(renderer->shaders[1]->program_id, "y_offset");
	glUniform1f(x_off, x_offset);
	glUniform1f(y_off, y_offset);
}

void KinectGame::updateVisionMode(float dt)
{
	if (player->IsInVisionMode())
	{
		visionBox->setTransform(vec3(1.0f), mat4(), vec3(.0f));
	}
	else
	{
		visionBox->setTransform(vec3(1.0f), mat4(), vec3(-100.0f));
	}
}

void KinectGame::Update(float dt)
{
	updateInput(dt);
	physicsWorld->Update(dt);
	if (runGame)
	{
		enemySpawner->Update(dt);
	}

	actors.Update(dt);
	UpdateTestParticles(dt);

	//player->AttachCameraToHead(renderer->current_cam);
	if (runPlayer)
	{
		player->Update(dt);
	}

	if(kinectSources != nullptr)
	{
		kinectSources->updateColor();
		kinectSources->updateDepth();
		kinectSources->updateBody();
		kinectSources->getInfaredSource().Update();

		imageRenderer->updateARDeapthMap(arDepth->textureInfo,
			kinectSources->getDepthSource().getTexDepthData());
	}

	updateVisionMode(dt);

	if (playerMode == PlayerMode::WIZARD)
	{
		DeadZone dz;
		dz.pos = dynamic_cast<Wizard*>(player)->GetLightPosition();
		dz.radius = .5f;
		enemySpawner->EnableDeadZone(dz);
	}

	//vec3 p(0.0f, 0.0, 5.0f);
	//debugShapeManager->AddCube(p, vec4(1.0f), 1.0f, new mat4(), 0.2f);

	//update light source
	auto joint = kinectSources->getBodySource().getJointsData()[JointType_HandLeft];
	auto leftHandCamSpace = joint.Position;

	if (playerMode != PlayerMode::WIZARD)
	{
		renderer->data->lightPosition = vec3(0.5f, 0.3f, 0.7f);
	}
	//renderer->data->lightPosition = vec3(-leftHandCamSpace.X, leftHandCamSpace.Y, leftHandCamSpace.Z);
	//r2->setTransform(vec3(.04f), mat4(), renderer->data->lightPosition);
	auto up = kinectSources->GetFloorUpVector();
	auto normal = glm::normalize(vec3(up));
	cameraSpaceOrientation = mat3(glm::orientation(normal, vec3(.0f, 1.0f, .0f)));
	kinectHeight = up.w == 0 ? -1.12f : up.w;
	renderer->cams[1]->position = shadowLightPosition;


	if(shockWave != nullptr)
	{
		if(shockWave->Disposable())
		{
			delete shockWave;
			shockWave = nullptr;
		}
		else
		{
			shockWave->Update(dt);
		}
	}

	loadingCounter += dt;
}

void KinectGame::Draw()
{

	glBindFramebuffer(GL_FRAMEBUFFER, shadow_fbo_id);
	glViewport(0, 0, 512, 424);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	renderer->current_cam = renderer->cams[1];
	actors.Draw(renderer, 1);
	enemySpawner->Draw(renderer, 1);
	if (runPlayer)
	{
		player->Draw(renderer, 1);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, width, height);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	renderer->current_cam = renderer->cams[0];
	//imageRenderer->DrawImage(colorScene,
	//	kinectSources->getColorSource().getColorData());
	
	//imageRenderer->DrawImage(colorScene,
	//	kinectSources->getColorSource().getLowResolutionColorData(),
	//	GL_RGB);

	isRoomDark = kinectSources->IsScreenDark(0.05f);
	
	imageRenderer->DrawImage(infraredScene,
		kinectSources->getInfaredSource().GetInfaredTexBuffer(),
		GL_RGB,
		GL_UNSIGNED_BYTE);

	imageRenderer->DrawImage(colorScene,
		kinectSources->GetDepthColorData(),
		GL_RGB);

	imageRenderer->DrawImage(normalMapScene, 
		kinectSources->GetNormalData(),
		GL_RGB,
		GL_FLOAT);

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	imageRenderer->DrawImage(depthScene,
		kinectSources->getDepthSource().getTexDepthData(),
		GL_RGB,
		GL_FLOAT);
	//glDisable(GL_BLEND);
	imageRenderer->DrawImage(positionMapScene,
	kinectSources->getDepthSource().getRaysOfDepths(),
		GL_RGB,
		GL_FLOAT);

	imageRenderer->DrawImage(arDepth,
		kinectSources->getDepthSource().getTexDepthData(),
		GL_RGB,
		GL_FLOAT);

	//renderer->Draw(billboard);
	renderer->Draw(virtualShadowScene);
	renderer->Draw(virtualGameScene);
	renderer->Draw(virtualShadowScene1);


	enemySpawner->Draw(renderer);
	actors.Draw(renderer);
	if (runPlayer)
	{
		player->Draw(renderer);
	}
	testParticles.Draw(renderer);
	//kinectSources->getRoomeScanner().DeugDraw(renderer);
	renderer->Draw(r2);
	//kinectSources->DrawBody(renderer, width, height);

	//auto up = kinectSources->GetFloorUpVector();

	//std::cout << up.x << "," <<
	//	up.y << "," <<
	//	up.z << "," <<
	//	std::endl;

	if (!kinectSources->IsRoomScannerReadt())
	{
		kinectSources->EnableRoomScanning(physicsWorld);
	}
}

void KinectGame::mousePressEvent(const float& x,
								 const float& y)
{ 
	if(shockWave == nullptr)
	{
		shockWave = new ShockWaveEffect(
			vec2(x, y),
			renderer->shaders[1]->program_id
			);
		shockWave->Activate();
	}
}


KinectGame::~KinectGame(void)
{
	if(kinectSources != nullptr) 
	{
		delete kinectSources;
		kinectSources = nullptr;
	}

	if(imageRenderer != nullptr) 
	{		
		delete imageRenderer;
		imageRenderer = nullptr;
	}

	delete enemySpawner;
	delete colorScene;
	delete depthScene;
	delete normalMapScene;
	delete arDepth;
	delete positionMapScene;
	//depthScene->shutDown();
	
	actors.Destroy();
	testParticles.Destroy();
	delete r ;//->shutDown();
	//delete r2;//->shutDown();
	//delete r3;//->shutDown();
	delete shockWave;
	delete physicsWorld;
	delete billboard;
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );

}
