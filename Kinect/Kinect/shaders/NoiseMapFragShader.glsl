#version 420

out vec4 _color;


in vec2 TexCoord;
in vec3 _vertexPosition;
in vec3 _normal;
in vec4 _shadow_coord;
in vec4 _vertex_color;
in mat4 _biased_mat;
in mat3 _tangentTransform;
in vec4 noise_color;
in float _burnThreshold;
in float _burn;


void burn()
{
	if(_burn > 0.0f)
	{
		if(_burnThreshold - noise_color.r < 0.03f)
		{
			discard;
		}
		else if(_burnThreshold - noise_color.r < 0.08f)
		{
			_color *= pow(noise_color.r, 5)  * vec4(255.0f, 40.2f, 0.0f, 1.0f);
		}
	}
}

void main()
{	
	_color = noise_color;
	//_color = vec4(1);
	burn();
}