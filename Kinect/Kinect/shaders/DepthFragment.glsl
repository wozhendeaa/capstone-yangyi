#version 420

in vec2 TexCoord;
in vec4 _VertexColor;
uniform sampler2D shadow_map;

out vec4 _color;

void main()
{
	vec4 color = texture2D(shadow_map, TexCoord);
	float depth = pow(color.r , 100);
	_color = vec4(depth, depth, depth, 1.0f);
};