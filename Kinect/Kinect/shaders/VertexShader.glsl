#version 420

in layout (location=0) vec3 VertexPosition;
in layout (location=1) vec2 VertexTexCoord;
in layout (location=2) vec3 VertexNormal;
in layout (location=3) vec4 VertexColor;
in layout (location=4) vec3 Tangent;

out vec2 TexCoord;
out vec3 _vertexPosition;
out vec3 _normal;
out vec4 _shadow_coord;
out vec4 _virual_obj_coord;
out vec4 _vertex_color;
out mat3 _tangentTransform;
out mat4 _biased_mat;
out vec4 _noise_tex;
out vec4 noise_color;
out float _burnThreshold;
out float _burn;
out vec3 _cube_map_vertexPos;
out vec3 _cube_map_normal;
out float NDC_Z;
out mat4 _light_proj;
out mat4 _light_view;

uniform mat4 projection;
uniform mat4 world_view;
uniform mat4 world_model;
uniform mat4 light_proj;
uniform mat4 light_view;
uniform mat4 biased_mat;
uniform mat4 lightRotation;
uniform sampler2D noise_tex;
uniform float octave;
uniform float uvTransformationMat;
uniform bool useNoiseAsHeightMap;
uniform bool useNoiseAsExplosion;
uniform bool useNoiseAsMelting;
uniform float burnThreshold;
uniform bool transformUV;

void calculateLocaTransformMatrix()
{
    vec3 t = mat3(lightRotation) * Tangent;
    vec3 n = mat3(lightRotation) * VertexNormal;
    vec3 biTangent = cross(n,t);
    _tangentTransform = mat3(
							normalize(t),
							normalize(biTangent),
							normalize(n));
}

void renderScene()
{
	TexCoord = VertexTexCoord;
	_burn = 0;
	_burnThreshold = burnThreshold;
	_vertex_color = max(vec4(0.0f), VertexColor);
    _vertexPosition = (world_model * vec4( VertexPosition, 1)).xyz;
    _normal =  mat3((lightRotation)) * VertexNormal;
    _shadow_coord = biased_mat 
	* light_proj 
	* light_view 
	* world_model
	* vec4(VertexPosition, 1);
	_light_proj = light_proj;
	_light_view = light_view;

	_cube_map_normal = VertexNormal;
	_cube_map_vertexPos =  (inverse(world_model) * inverse(lightRotation) * vec4(_vertexPosition,1)).xyz;
    gl_Position = projection * world_view *  world_model * vec4(VertexPosition, 1);
	NDC_Z = gl_Position.z;
	_virual_obj_coord = biased_mat * gl_Position;
}


void generateHeight()
{
    vec4 y = texture2D(noise_tex, TexCoord);
    int index = int(octave);
    noise_color = vec4(y[index]);

    if(useNoiseAsHeightMap)
	{
        gl_Position.y += gl_Position.y * y[index] * 100;
    }

}

void generateExplosion()
{
	if(useNoiseAsExplosion)
	{
		float explosionSpeed = 1 / pow(abs(noise_color.r), 3.0f)
		*  pow(-(burnThreshold - 1.0f), 1.2f) * 5.0f;
		_burn = 1.0f;
		vec3 dir = normalize(VertexPosition);
		gl_Position += explosionSpeed * vec4(dir, 1.0f);
	}
}

void generateMelting()
{
	if(useNoiseAsMelting)
	{
		float explosionSpeed = (noise_color.r * 100.0f) * -(burnThreshold - 1.0f);
		_burn = 1;
		gl_Position.y -= abs(explosionSpeed * vec4(VertexNormal, 1.0f).y);
	}
}


void main()
{
	renderScene();
   
    calculateLocaTransformMatrix();
}
;
