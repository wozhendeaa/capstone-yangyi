#version 420

in layout(location = 0) vec3 VertexPosition;

uniform sampler2D positionTex;
uniform mat4 world_model;
uniform mat4 light_proj;
uniform mat4 light_view;
uniform bool usingPositionTex = false;

void renderScene()
{
	gl_Position = light_proj *
		light_view *
		world_model * 
		vec4(VertexPosition, 1);
}


void main()
{
	renderScene();

};
