#version 420

in vec2 TexCoord;
in vec4 _VertexColor;
in vec3 _position;
in float distanceFactor;
in float distanceMoved;
out vec4 _color;

uniform sampler2D color_tex;
uniform vec3 particlePosition;

uniform bool fadeOverDistance = false;
uniform float m_startOpacity = 1.0f;
uniform float m_endOpacity = .1f;

void main()
{
	vec4 daColor = texture2D(color_tex, TexCoord);
	
	{
		_color = daColor;
	}

	if (fadeOverDistance)
	{
		_color.a =  distanceFactor ;
	}

	if (daColor.a == 0.0f)
	{
		discard;
	}

};