#version 420

in layout(location = 0) vec3 VertexPosition;
in layout(location = 1) vec2 VertexTexCoord;

uniform sampler2D positionTex;
uniform mat4 world_model;
uniform mat4 light_proj;
uniform mat4 light_view;

void renderScene()
{
	gl_Position = light_proj * light_view * world_model *
		vec4(texture2D(positionTex, VertexTexCoord).xyz, 1);
}


void main()
{
	renderScene();

};
