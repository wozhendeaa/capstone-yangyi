#version 420

in vec2 TexCoord;
in vec4 _VertexColor;
in vec3 _position;


uniform sampler2D color_tex;
uniform sampler2D infared_tex;
uniform sampler2D ar_depth_map;
uniform sampler2D virtual_depth_map;
uniform sampler2D virtual_color_map;
uniform sampler2D ar_normal_map;
uniform sampler2D ar_position_tex;

uniform mat4 light_proj;
uniform mat4 light_view;
uniform mat4 biased_mat;

uniform vec2 shockWaves[10];

uniform vec2 screenLocation;

uniform vec3 lightPosition;
uniform float kinectHeight;
uniform mat3 cameraSpace_orientation;

uniform bool enableLighting;
uniform bool renderingDepth;
uniform bool renderingColor;
uniform bool renderingPosition;
uniform bool isRoomDark = false;

vec2 poissonDisk[5] = {
	vec2(-0.613392, 0.617481),
	vec2(0.170019, -0.040254),
	vec2(-0.299417, 0.791925),
	vec2(0.645680, 0.493210),
	vec2(-0.651784, 0.717887)

};

// x: max range of shock wave
// y: currentTime
// z: delta length  
// w: pixel offset distance
struct ShockWave
{
	float waveRange  ;
	float time       ;
	float deltaLength;
	float amplitude;
	float timeLimit;
};

//uniform struct PointLight
//{
	float Attenuation = 1.0f;
	float Linear = 0.7f;
	float exp = 2.0f;
	float range = .5f;
//};

uniform ShockWave shockWaveData;
uniform bool hasShockWave;

out vec4 _color;

vec4 CalcShockWaveColor() 
{
    vec4 color = texture2D(color_tex, TexCoord);
	vec2 dir = TexCoord - screenLocation;
	dir.x /= 0.75;// fix apspect ratio

    float dist = length(dir);
	vec2 current = normalize(dir);// * (shockWaveData.time / shockWaveData.timeLimit); 
	
	float waveBeginDistance = length(current);
	float waveEndDistance = waveBeginDistance + shockWaveData.deltaLength;

	bool inValidRange = dist < shockWaveData.waveRange;// < waveEndDistance && waveEndDistance < shockWaveData.waveRange;
	
	bool inBetweenDeltaWave = dist >= waveBeginDistance
	&& dist <= waveEndDistance;

	if(inValidRange)
	{
		float distanceOffset =  1 - (dist / shockWaveData.waveRange) ;//: (shockWaveData.waveRange - dist);

		float factor = -shockWaveData.amplitude * pow(distanceOffset, 2) * shockWaveData.time;

		vec2 newTexCoord =  TexCoord + dir * factor;
		
		color = texture2D(color_tex, newTexCoord);

		if(newTexCoord.x > 1.0f || newTexCoord.x < 0.0f)
		{
			color = vec4(0);
		}
		
		if(newTexCoord.y > 1.0f || newTexCoord.y < 0.0f)
		{
			//newTexCoord.y  = 0.0f;
			color = vec4(0);
		}
	}
	else
	{
		color = vec4(0);
	}

    return color;
}

void renderScene(vec4 tex_color, vec3 _vertexPosition)
{
	vec2 newCoord = TexCoord;
	newCoord.x += 0.1f;
	vec4 normalColor = texture2D(ar_normal_map, TexCoord);
	vec3 newNormal = normalColor.xyz;// (2 * vec3(normalColor)) - 1;
	newNormal.x *= -1;

	vec3 lightPositionXReverted = _vertexPosition;
	lightPositionXReverted.x *= -1;

	vec3 transformedPosition = _vertexPosition;
	transformedPosition.x *= -1;

	//transformedPosition.y -= kinectHeight;
	vec4 diffuseColor = isRoomDark ? texture2D(infared_tex, TexCoord) : tex_color;

	float distance = distance(lightPosition, transformedPosition);
	if (distance < range)
	{
		//light
		vec3 lightVector = normalize(lightPosition - transformedPosition);
		//vec3 toEye = normalize(camera_position - _vertexPosition);

		//diffuse light
		float diffuseFactor = 0;
		float specFactor = 0;
		vec4 lightColor = vec4(.3f);
		if (true)
		{
			float newRange = range - distance;;
			//I = E ��(D2 / (D2 + Q �� r2))
			float diffuseFactor = max(0, dot(lightVector, newNormal));
			float intensity = 30.1f * (newRange * newRange / (newRange * newRange + 0.9f * range * range));
			float diffuse = (diffuseFactor * intensity) + diffuseColor;

			////specular light 
			vec4 spec = vec4(1.0f);
			if(diffuseFactor > 0.0f)
			{			
				vec3 reflected_light = normalize(reflect(-lightVector, newNormal));
				float specFactor = max(dot(reflected_light, vec3(.0f)), 0.0f);
				specFactor = pow(specFactor, 40.0f);
			
				spec = vec4(specFactor * diffuseColor.xyz, 1.0f);
			} 
			else
			{
				diffuse = diffuseColor;
			}

			//vec4 ambient = vec4(0.1f,0.1f,0.1f,0.1f);

			//vec4 cube_map_color = GetCubeMapColor(newNormal);
			//vec4 finalColor = GetTexColor() * cube_map_color;

			//if(recievesLight)
			//{
			//	finalColor *=  (diffuse) ;
			//}

			//if(useShadowMap)
			//{
			//	//sample shadow map
			//	vec4 biasedShadowCoord = _shadow_coord;
			//	biasedShadowCoord.z -= 0.03;
			//	float shadow = textureProj(shadow_map, biasedShadowCoord);

			//	
			//	if(enableSoftShadow)
			//	{
			//		shadow = shadeSoftShadow(shadow, biasedShadowCoord);
			//	}

			//	finalColor *= shadow + 0.5;

			//}

			_color = ((diffuse * spec)) + tex_color;
		}
	}
	else

	{
		_color = tex_color;
	}
}

float calcShadowFactor(vec3 VertexPosition)
{
	VertexPosition.x *= -1;
	vec4 _shadowCoord = biased_mat * light_proj * light_view * vec4(VertexPosition, 1);

	vec2 newCoord = _shadowCoord.xy;
	newCoord /= _shadowCoord.w;
	//newCoord.y *= -1f;
	
	vec4 virtualDepth = texture2D(virtual_depth_map, newCoord);

	if (_shadowCoord.z / _shadowCoord.w < virtualDepth.r)
	{
		return 1.0f;
	}
	else
	{
		return 0.3f;
	}
}

void main()
{

	vec4 color = texture2D(color_tex, TexCoord);
	vec4 positionColor = texture2D(ar_position_tex, TexCoord);
	vec4 depthColor = texture2D(ar_depth_map, TexCoord);
	
	vec2 newCoord = TexCoord;
	newCoord.y *= -1;
	vec4 virtualDepth = texture2D(virtual_depth_map, newCoord);
	vec4 virtualColor = texture2D(virtual_color_map, newCoord);

	vec4 finalColor = vec4(1.0f);
	vec4 shockWaveColor = vec4(0.0f);
	if(hasShockWave)
	{
		shockWaveColor = CalcShockWaveColor();
	}
	
	if (false)
	{
		_color = vec4(pow(depthColor.r , 1));
    }

	else if (false)
	{
		vec4 normalColor = texture2D(ar_normal_map, TexCoord);
		_color = normalColor;
	}
	else if (false)
	{
		_color = texture2D(infared_tex, TexCoord);

	}
	else 
	{
		renderScene(color, positionColor.xyz);
	}

	//calcShadowFactor(positionColor.xyz);
	_color *= finalColor * calcShadowFactor(positionColor.xyz);

	if (shockWaveColor.r != 0 && shockWaveColor.g != 0 &&
		shockWaveColor.b != 0)
	{
		_color = shockWaveColor;
	}
	//_color = vec4(pow(virtualDepth.r, 80));
	//_color = texture2D(infared_tex, TexCoord);
};