#version 420

in layout(location = 0) vec3 VertexPosition;
in layout(location = 1) vec2 VertexTexCoord;
in layout(location = 2) vec3 VertexNormal;
in layout(location = 3) vec4 VertexColor;
in layout(location = 4) vec3 Tangent;

out vec2 TexCoord;
out vec4 _VertexColor;
out vec3 _position;
out float distanceFactor;
out float distanceMoved;

uniform vec3 particleSpawnPos;
uniform mat4 projection;
uniform mat4 world_view;
uniform mat4 world_model;
uniform bool isBillboard = true;

uniform bool scaleOverDistance = false;
uniform float startScale = 1.0f;
uniform float endScale = .1f;
uniform float maxDistanceRange = 3.0f;

vec3 offset = vec3(.0f, .0f, 1.0f);

void renderBillboard()
{

	float distanceMoved = length(VertexPosition - particleSpawnPos);
	distanceFactor = 1 / distanceMoved;// pow(1 / distanceMoved, 3);


	vec3 camRight = vec3(world_view[0][0], world_view[1][0], world_view[2][0]);
	vec3 camUp = vec3(world_view[0][1], world_view[1][1], world_view[2][1]);

	vec3 vert = (camRight * VertexPosition.x + camUp * VertexPosition.y) * 1.5f;

	gl_Position = projection * world_view * world_model * vec4(vert, 1);

}

void renderMesh()
{

}

void main()
{
	TexCoord = VertexTexCoord;
	_VertexColor = VertexColor;
	_position = VertexPosition;

	if (isBillboard)
	{
		renderBillboard();
	}
	else
	{
		renderMesh();
	}
};
