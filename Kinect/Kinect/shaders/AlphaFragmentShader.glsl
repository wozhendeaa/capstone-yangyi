#version 420

out vec4 _color;


in vec2 TexCoord;
in vec3 _vertexPosition;
in vec3 _normal;
in vec4 _shadow_coord;
in vec4 _vertex_color;

uniform sampler2D color_tex;
uniform sampler2D alpha_tex;
uniform sampler2D normal_tex;
uniform sampler2D spec_tex;
uniform sampler2D color2_tex;
uniform float alpha;


void BlendTexturesAlpha()
{
	vec4 textColor = texture2D(color_tex, TexCoord);
	textColor.a = alpha;
	_color = textColor;
}

void main()
{		
	BlendTexturesAlpha();
}