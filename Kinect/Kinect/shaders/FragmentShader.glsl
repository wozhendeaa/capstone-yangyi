#version 420

out vec4 _color;

in vec2 TexCoord;
in vec3 _vertexPosition;
in vec3 _normal;
in vec4 _shadow_coord;
in vec4 _virual_obj_coord;
in vec4 _vertex_color;
in mat4 _biased_mat;
in mat3 _tangentTransform;
in vec4 _noise_color;
in vec3 _cube_map_reflection;
in vec3 _cube_map_vertexPos;
in vec3 _cube_map_normal;
in float NDC_Z;
in mat4 _light_proj;
in mat4 _light_view;

uniform sampler2D color_tex;
uniform sampler2D alpha_tex;
uniform sampler2D normal_tex;
uniform sampler2D spec_tex;
uniform sampler2D ao_tex;
uniform sampler2D blur_tex;
uniform sampler2D render_tex;
uniform sampler2D ar_position_tex;
uniform sampler2D ar_depth_map;
uniform sampler2D ar_color_map;
uniform sampler2DShadow shadow_map;
uniform samplerCube cube_map;



uniform float radius;
uniform float reflectiveFactor;
uniform float refractiveFactor;
uniform vec4 Obj_Color;
uniform vec4 color;
uniform vec3 ambient;
uniform vec3 lightPosition;
uniform vec3 diffuseColor;
uniform vec3 specular;
uniform int specular_power;
uniform vec3 camera_position;
uniform bool enable_lighting;
uniform bool enable_vertex_color;
uniform bool enable_click_color;
uniform vec3 directional_light;
uniform vec4 click_color;
uniform float alpha;
uniform bool userNormalMapLighting;
uniform bool blend;
uniform bool useDiffuseMap;
uniform bool useSpecMap;
uniform bool useAlpha;
uniform bool useAOMap;
uniform bool useNoiseMap;
uniform bool useShadowMap = false;
uniform bool enablePCF;
uniform bool enableARDepthTesting = true;
uniform bool disableARDepthTesting = false;
uniform bool enableSoftShadow = false;
uniform bool recievesLight;
uniform bool isSkyBox;
uniform bool reflectionNotRefraction;
uniform bool tex_color_only = false;

vec2 poissonDisk[5] = {
vec2(-0.613392, 0.617481),
vec2(0.170019, -0.040254),
vec2(-0.299417, 0.791925),
vec2(0.645680, 0.493210),
vec2(-0.651784, 0.717887)

};

uniform float offset[5] = float[](
 0.0, 1.0, 2.0, 3.0, 4.0 
 );

uniform float weight[5] = float[]( 
0.2270270270, 0.1945945946, 0.1216216216,
 0.0540540541, 0.0162162162 );

vec4 blur()
{
	vec4 color = texture2D(blur_tex, TexCoord);

	if(color.r == 0 && color.g == 0 && color.b ==0)
	{
		color = vec4(1.0f);
	}
	else
	{
		vec4 render = texture2D(render_tex, TexCoord);
		float dx = 1.0 / float(1024.0f);
		vec4 sum = texture2D(blur_tex, vec2(gl_FragCoord) * dx ) * weight[0];
		for( int i = 1; i < 5; i++ )
		{
			sum += texture2D( blur_tex, (vec2(gl_FragCoord) + vec2(offset[i], 0.0))* dx ) * weight[i];
			sum += texture2D( blur_tex, (vec2(gl_FragCoord) - vec2(offset[i], 0.0))* dx ) * weight[i];
		}

		color = render + sum;
	}

	return color;
}

float shadeSoftShadow(float shadow, vec4 sc)
{
	int numSamples = 5;
	float sum = 0.0f;

	vec4 temp = sc;

	for(int i = 0; i < numSamples; i++)
	{
		temp.xy = sc.xy + poissonDisk[i] * radius;
		sum += textureProj(shadow_map, temp) ;		
	}

	shadow = sum / float(numSamples * 1.0f);

	return shadow;
}

vec4 GetCubeMapColor(vec3 normal)
{
	vec3 reflection = -_cube_map_vertexPos;
	vec3 refraction  = _cube_map_vertexPos;
	float reflectFactor = 1;
	if(!isSkyBox)
	{
		vec3 toEye = normalize(camera_position - _vertexPosition);
		reflection = reflect(toEye, normal);	
		reflectFactor =  max(0, 0.1f + 0.4 * pow(1.0 + dot(-toEye, normal), 0.5));
	}	

	vec4 reflecColor = texture(cube_map, reflection);

	vec4 color = reflectFactor * reflecColor;


	if(!isSkyBox && !reflectionNotRefraction)
	{
		vec3 toEye = normalize(camera_position - _vertexPosition);
		refraction = refract(-toEye, normal, refractiveFactor) ;	
		vec4 refracColor = texture(cube_map, -refraction);
		
		vec3 dispersion = vec3(1.10, 1.12, 1.14);
		vec3 redRefrac   = refract(toEye, normal,  dispersion.x  );
		vec3 greenRefrac = refract(toEye, normal,  dispersion.y  );
		vec3 blueRefrac  = refract(toEye, normal,   dispersion.z  );

		normal.y *= -1; 
		normal.y *= -1; 
		normal.y *= -1; 
		redRefrac  	.y *= -1;
		greenRefrac	.y *= -1;
		blueRefrac 	.y *= -1;

		vec4 dispersionColor;
		dispersionColor.r = texture(cube_map, redRefrac).r;
		dispersionColor.g = texture(cube_map, greenRefrac).g;
		dispersionColor.b = texture(cube_map, blueRefrac).b;
		
		color *= dispersionColor;
		refracColor = (1 - reflectFactor) * refracColor;

		color += refracColor;
	}

	
	if(color.r == 0 && color.g == 0 && color.b == 0)
	{
		color = vec4(1.0f);
	}



	return color ;
}

vec4 GetTexColor()
{
	vec4 textColor = texture2D(color_tex, TexCoord);
	textColor.a = alpha;

	bool useColor = (textColor.r == 0 && textColor.g == 0 && textColor.b == 0)
	|| !useDiffuseMap;
	
	if(useColor)
	{
		textColor = vec4(1.0f);
	}

	//if(useNoiseMap)
	//{
	//	textColor = _noise_color;
	//}

	if(useAOMap)
	{
		vec4 texAO = texture2D(ao_tex, TexCoord);
		textColor *= texAO;
	}

	if(useAlpha)
	{
		vec4 alpha = texture2D(alpha_tex, TexCoord);
		bool shouldDiscard = alpha.r <= 0.002 && alpha.g <= 0.002 && alpha.b <= 0.002;
		bool outOfBound = TexCoord.x < 0.0f || TexCoord.x > 0.99f ||
		TexCoord.y < 0.0f || TexCoord.y > 0.99f;
		if(shouldDiscard || outOfBound)
		{
			discard;
		}
	}

	return textColor;
}

vec4 GetSpecTexColor()
{
	vec4 textColor = texture2D(spec_tex, TexCoord);

	if(textColor.r == 0 && textColor.g == 0 && textColor.b == 0)
	{
		textColor = vec4(0.5f);
	}

	return textColor;
}

bool IsFragInfontOfRealScene()
{
	bool isInfront = true;
	if(enableARDepthTesting)
	{
		vec4 coord = _virual_obj_coord;
		coord /= coord.w;
		coord.y *= -1f;

		float depth = texture2D(ar_depth_map, coord.xy);
		isInfront = _virual_obj_coord.z / _virual_obj_coord.w < depth;
	}

	return isInfront;
}

void renderScene()
{
	vec4 tex_color = GetTexColor();
	vec4  normalColor = texture2D(normal_tex, TexCoord);

	vec3 newNormal =  userNormalMapLighting ? 
	( 2 * vec3(normalColor) ) - 1 : _normal;

	if(userNormalMapLighting)
	{
		newNormal = normalize(_tangentTransform * newNormal);
	}

	float direct_factor = max(0, dot(normalize(directional_light), _normal.xyz));
	vec4 direct_color =  direct_factor * vec4(1, 1, 1, 1);

	//light
	vec3 lightVector = normalize(lightPosition - _vertexPosition);
	vec3 toEye = normalize(camera_position - _vertexPosition);

	//diffuse light
	float diffuseFactor = 0;
	float specFactor = 0;
	
	if(enable_lighting && !tex_color_only)
	{
		diffuseFactor = max(0, dot(lightVector, newNormal));
		vec4 diffuse = vec4(diffuseFactor * diffuseColor, 1);
		
		//specular light
		vec4 spec = vec4(0.0f, 0.0f, 0.0f, 1.0f);
		if(diffuseFactor > 0)
		{			
			vec3 reflected_light = normalize(reflect(-lightVector, newNormal));
			specFactor = max(dot(reflected_light, toEye), 0.0f);
			specFactor = pow(specFactor, specular_power);
		
			spec = vec4(specFactor * specular, 1.0f);
		} 

		vec4 ambient = vec4(0.1f,0.1f,0.1f,0.1f);

		if(!blend)
		{
			ambient = vec4(0.1f);
		}

		//vec4 cube_map_color = GetCubeMapColor(newNormal);
		vec4 finalColor = GetTexColor() ;
		
		if(recievesLight)
		{
			finalColor *=  (ambient + diffuse  + spec) ;
		}


		if(useShadowMap)
		{
			//sample shadow map
			vec4 biasedShadowCoord = _shadow_coord;
			biasedShadowCoord.z -= 0.03;
			float shadow = textureProj(shadow_map, biasedShadowCoord);

			vec4 clippedCoord = _shadow_coord / _shadow_coord.w;
			vec3 ar_position = texture2D(ar_position_tex, clippedCoord.xy).xyz;
			vec4 ar_lightview_pos = _biased_mat * _light_proj * _light_view * vec4(ar_position, 1);

			if (ar_lightview_pos.z / ar_lightview_pos.w > gl_FragCoord.z / gl_FragCoord.w)
			{
				shadow = 0.0f;
			}

			//pcf
			if(enablePCF)
			{
				float sum = 0.0f;
				float offset = 1.7f;
				sum += textureProjOffset(shadow_map, biasedShadowCoord, ivec2(offset,offset));
				sum += textureProjOffset(shadow_map, biasedShadowCoord, ivec2(-offset,offset));
				sum += textureProjOffset(shadow_map, biasedShadowCoord, ivec2(-offset,-offset));
				sum += textureProjOffset(shadow_map, biasedShadowCoord, ivec2(offset,-offset));

				sum *= 0.25;
				shadow = sum;
			}

			if(enableSoftShadow)
			{
				shadow = shadeSoftShadow(shadow, biasedShadowCoord);
			}
 
			
			finalColor *= shadow + 0.5;

		}

		_color = finalColor * GetTexColor();
	} 
	else
	{
		_color =  GetTexColor();
	}	
	
	if(enable_vertex_color)
	{
		_color = _vertex_color;
	}

	if(tex_color_only)
	{
		_color =  GetTexColor();
	}
	
}


void main()
{	
	bool noDepthTesting = IsFragInfontOfRealScene();
	
	//noDepthTesting = true;
	if (noDepthTesting || isSkyBox)
	{
		renderScene();
	}
	else
	{
		discard;
	}

};