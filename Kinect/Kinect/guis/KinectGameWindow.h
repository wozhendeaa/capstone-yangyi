#pragma once
#include <GUI\GLWindow.h>
#include <Qt\qtimer.h>
#include <Qt\qelapsedtimer.h>
#include <Game\KinectGame.h>
#include <CStopWatch.h>
#include <KinectModule\KinectSensor.h>
using BatEngine::Renderer;

typedef void GameLoop(float);
static GameLoop* gameLoop;
class DebugMenu;
class KinectGameWindow : public GLWindow
{
	Q_OBJECT
	QTimer timer;
	CStopWatch watch;
	KinectGame* game;
	DebugMenu* debug_menu;
	float deltaTime{ .0f };
	void addShapes();
	void initdebugInfo();

	public slots:
		void Loop();

protected:
	void initializeGL() override;
	void paintGL() override;
public:
	void SetDebugMenu(DebugMenu* debugMenu);
	//matrix debug info
	void updateDebugInfo();
	float fovY;
	float aspectRatio;
	float nearPlane; 
	float farPlane ;

	void mousePressEvent(QMouseEvent* e) override;


	bool isRunning;
	void setGameLoop(GameLoop* loop);

	KinectGameWindow(void);
	~KinectGameWindow(void);
};

