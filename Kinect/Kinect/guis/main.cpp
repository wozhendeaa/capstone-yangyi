#include <Qt\qapplication.h>
#include "KinectWindow.h"
#include <MemoryDebug.h>

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	KinectWindow window;

	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );

	return  app.exec();

	//return result;
}  