#include "KinectWindow.h"
#include "KinectGameWindow.h"
#include <MemoryDebug.h>


KinectWindow::KinectWindow(void)
{
	glWindow = new KinectGameWindow;
	addGLWindow(glWindow);	
	auto kinectGameWindow = dynamic_cast<KinectGameWindow*>(glWindow);

	debug_menu->setVisible(false);
	//debug_menu->addFloatsSliderLayout();
	//debug_menu->watchFloatSlider("fovY",60.0f, 40.0f, &kinectGameWindow->fovY);
	//debug_menu->watchFloatSlider("aspect",2, 1.3f, &kinectGameWindow->aspectRatio);
	//debug_menu->watchFloatSlider("near", 2.0f, 0.1f, &kinectGameWindow->nearPlane);
	//debug_menu->watchFloatSlider("far", 300.0f, 180.0f, &kinectGameWindow->farPlane);
	kinectGameWindow->SetDebugMenu(debug_menu);
}

void KinectWindow::startGameLoop()
{
	auto kinectGameWindow = dynamic_cast<KinectGameWindow*>(glWindow);
	kinectGameWindow->Loop();
}

KinectWindow::~KinectWindow(void)
{	
	delete glWindow;
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );

}
