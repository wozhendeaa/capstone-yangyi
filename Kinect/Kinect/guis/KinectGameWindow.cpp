#include "KinectGameWindow.h"
#include <ShapeGenerator.h>
#include <glm\gtc\matrix_transform.hpp>
#include <MemoryDebug.h>
#include <DebugTools\DebugMenu.h>
#include <ctime>
#include <chrono>

using Neumont::ShapeData;
using Neumont::ShapeGenerator;
using Neumont::Vertex;

KinectGameWindow::KinectGameWindow(void)
{
	connect(&timer, SIGNAL(timeout()), this, SLOT(Loop()));
	timer.start(0);	
	game = new KinectGame(renderer);
	isRunning = true;
	initdebugInfo();
		watch.startTime();
		elapsedTimer.start();

	auto w = 512.0f;
	auto h = 424.0f;
	renderer->Matrices.projection =
		glm::perspective(fovY, w / h, 0.01f, 1000.0f);

}

void KinectGameWindow::initdebugInfo()
{
	fovY = 60.0f;
	aspectRatio = 1920 / 1080.0f;
	nearPlane = 0.01f;
	farPlane = 1000.0f;
}

void KinectGameWindow::setGameLoop(GameLoop* gl)
{
	gameLoop = gl;
}

void KinectGameWindow::addShapes()
{
	auto kinectSensor = KinectSensor::GetSensor();
	auto w = width();
	auto h = height();
	game->setGameWindowSize(w, h);
	game->InitGame();
}

void KinectGameWindow::mousePressEvent(QMouseEvent* e) 
{
	float x = e->x() / (float)width();
	float y = e->y() / (float)height();
	game->mousePressEvent( x, y);
}

void KinectGameWindow::updateDebugInfo()
{
	auto w = 512.0f;
	auto h = 424.0f;
	
	renderer->Matrices.projection = 
		glm::perspective(fovY, w / h , nearPlane, farPlane);

	//float height = 3.0f;
	//float width = 4.0f;
	//vec3 pc(-width, height, .0f);
	////vec3 tr(width, height, .0f);
	//vec3 pa(-width, -height, .0f);
	//vec3 pb(width, -height, .0f);

	//vec3 pe = renderer->current_cam->position;

	//vec3 vr = glm::normalize(pb - pa);
	//vec3 vu = glm::normalize(pc - pa);
	//vec3 vn = glm::normalize(glm::cross(vr, vu));

	//vec3 va = pa - pe;
	//vec3 vb = pb - pe;
	//vec3 vc = pc - pe;

	//auto d = -glm::dot(va, vn);

	//float n = 0.01f;
	//float f = 100.0f;
	//float l = glm::dot(vr, va) * n / d;
	//float r = glm::dot(vr, vb) * n / d;
	//float b = glm::dot(vu, va) * n / d;
	//float t = glm::dot(vu, vc) * n / d;

	//auto perspective = glm::frustum(l, r, b, t, n, f);
	//auto rot = glm::mat4(
	//	vec4(vr, .0f),
	//	vec4(vu, .0f),
	//	vec4(vn, .0f),
	//	vec4(.0f, .0f, .0f, 1.0f));

	//pe.x *= -1.0f;
	////auto translate = glm::translate(mat4(), -pe);

	//auto mat = perspective;
	//renderer->Matrices.projection = mat;
}

void KinectGameWindow::SetDebugMenu(DebugMenu* debugMenu)
{
	debug_menu = debugMenu;
}


void KinectGameWindow::Loop()
{	
	if (GLWindow::IsReady() && isRunning)
	{
		updateDebugInfo();
		elapsedTimer.restart();
		debug_menu->update(deltaTime);
		game->Update(deltaTime);
		repaint();
		deltaTime = elapsedTimer.elapsed() * 0.001f;

	}
}

void KinectGameWindow::paintGL()
{
	DefaultDraw();
	game->Draw();
}

void KinectGameWindow::initializeGL() 
{
	GLWindow::initializeGL();
	renderer->createShaderInfo("shaders//vertexShader.glsl", 
		"shaders//fragmentShader.glsl");

	renderer->createShaderInfo("shaders//FramebufferVertex.glsl",
		"shaders//FramebufferFrag.glsl");

	renderer->createShaderInfo("shaders//ParticleVertexShader.glsl",
		"shaders//ParticleFragmentShader.glsl");

	renderer->createShaderInfo("shaders//VertexShader_Shadow.glsl",
		"shaders//FragmentShader_Shadow.glsl");

	renderer->createShaderInfo("shaders//VertexShader_ARShadow.glsl",
		"shaders//FragmentShader_Shadow.glsl");

	renderer->addUniform("lightPosition",
		renderer->shaders[1]->program_id,
		ShaderUniformParameterType::SHADER_VEC3,
		&renderer->data->lightPosition);

	renderer->addUniform("isRoomDark",
		renderer->shaders[1]->program_id,
		ShaderUniformParameterType::SHADER_BOOL,
		&game->isRoomDark);

	addShapes();
}

KinectGameWindow::~KinectGameWindow(void)
{
	if(game != nullptr)
	{
		delete game;
		game = nullptr;
	}
	KinectSensor::GetSensor()->shutDown();

	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );
	
}
