#include "ParticleEffect.h"
#include "Random.h"

ParticleEffect::ParticleEffect()
{

}

ParticleEffect::ParticleEffect(int num, glm::vec2 origin, Type t, Core::RGB color)
{
	type = t;
	disposable = false;
	initParticles(num, origin, color);
	if(t == STAY)
		initStationaryParticles( color);
	else if(t == RING)
		initRingParticles(num, origin);
}

void ParticleEffect::initRingParticles(int num, glm::vec2 origin)
{
	for(int i = 0; i < particles.size(); i++)
	{
		Particle& p = particles[i];
		p.position = position + Random::RandomUnitVector() * 10.0f;
		int r1 = Random::RandomInRange(255, 0);
		int g1 = Random::RandomInRange(255, 0);
		int b1 = Random::RandomInRange(255, 0);
		p.color = RGB(r1, g1, b1);	

	}
}

void ParticleEffect::initParticles(int num, glm::vec2 origin, Core::RGB color)
{
	
	for(int i = 0; i < num; i++)
	{
		Particle p;
		position = origin;
		p.particleSize = 5;
		p.position = position;
		p.velocity = generateVelocity();
		p.color = Colorness().VaryColor(color, 30);
		if(type == THRUST)
		{
			p.lifetime = Random::RandomInRange(2,1);
		} 		
		else
		{
			p.lifetime = Random::RandomInRange(10,1);
		}
	
		particles.push_back(p);
	}
}

void ParticleEffect::initStationaryParticles(Core::RGB color)
{
	for(unsigned int i = 0; i < particles.size(); i++)
	{
		Particle& p = particles[i];
		p.position.x = Random::RandomInRange(1440, 0);
		p.position.y = Random::RandomInRange(900, 0);
		p.particleSize = 2;
		float randomBrightness = Random::RandomFloat();
		p.color = Colorness().Brightness(color, randomBrightness);
	}
}

void ParticleEffect::setVelocity(glm::vec2 vel)
{
	for(unsigned i = 0; i < particles.size(); i++)
	{
		vel.y = Random::RandomInRange(30, 10);
		vel.x = Random::RandomInRange(30, 10);
		particles[i].velocity = vel;
	}
}

glm::vec2 ParticleEffect::generateVelocity()
{
	glm::vec2 vel;
	
	if(type == EXPLOSION)
		vel = glm::vec2(Random::RandomFloat() * Random::RandomInRange(120, -120), Random::RandomFloat() * Random::RandomInRange(120, -120));
	else if(type == THRUST)
		vel = glm::vec2();
	else if(type == STAY)
		vel = glm::vec2();


	return vel;
}

void ParticleEffect::DrawRing(Core::Graphics& g)
{
	for(unsigned i = 0; i < particles.size(); i++)
	{
		Particle& p = particles[i];	
		g.SetColor(p.color);		
		g.DrawLine(p.position.x, p.position.y, p.position.x + 5, p.position.y + 5);
	}
}

void ParticleEffect::UpdateRing(float radius)
{

	for(unsigned i = 0; i < particles.size(); i++)
	{
		Particle& p = particles[i];				
		p.position = position + Random::RandomUnitVector() * Random::RandomInRange(radius + 30, radius);		
	}

	
}

void ParticleEffect::Update(float dt)
{
	if(type == EXPLOSION)
		UpdateExplosion(dt);
	else if(type == THRUST)
		UpdateThrust(dt);
	else if(type == STAY)
		UpdateStay(dt);
	else if(type == RING)
		UpdateRing(dt);
}

void ParticleEffect::UpdateStay(float dt)
{
	for(unsigned i = 0; i < particles.size();i++)
	{
		Particle& p = particles[i];	
		p.position = p.position + velocity * dt;

		if(p.position.x < 0 || p.position.x > 1440 || p.position.y < 0 || p.position.y > 900)
		{
			p.position.x = Random::RandomInRange(1440, 0);
			p.position.y = Random::RandomInRange(900, 0);
		}
	}
}

void ParticleEffect::DrawStay(Core::Graphics& g)
{
	for(unsigned i = 0;i < particles.size();i++)
	{
		Particle& p = particles[i];
		RGB temp = Colorness().Brightness(p.color, abs(sin(p.brightness)));
		p.brightness += 0.02;
		g.SetColor(temp);

		for(int j = 0; j < p.particleSize; j++)
		{
			glm::vec2 start(0, j + 2);
			glm::vec2 end(2, j);

			start = start + p.position;
			end = end + p.position;			
			g.DrawLine(start.x, start.y, end.x, end.y);
		}
	}
}

void ParticleEffect::UpdateExplosion(float dt)
{
	for(unsigned i = 0; i < particles.size(); i++)
	{	
		Particle& p = particles[i];	
		p.position = p.position + p.velocity * dt;		
		p.lifetime -= 0.1;	
	}

	for(unsigned i = 0; i < particles.size() && !disposable;i++)
	{
		Particle p = particles[i];	
		disposable = !(p.lifetime > 0);
	}


}

void ParticleEffect::UpdateThrust(float dt)
{
	for(unsigned i = 0; i < particles.size(); i++)
	{
		Particle& p = particles[i];		
		p.position = p.position + p.velocity * dt;				
		p.position = p.position + glm::vec2(Random::RandomInRange(2,-2), Random::RandomInRange(2,-2));
		p.lifetime -= 0.1;

		if(p.lifetime <= 0)
		{
			p.position = position;
			p.lifetime = Random::RandomInRange(3,0);
		}
		
	}
}

 void ParticleEffect::Draw(Core::Graphics& g)
{
	if(type == EXPLOSION)
		DrawSquare(g);
	else if(type == THRUST)
		DrawParticle(g);
	else if(type == BRUSH){}
		//DrawBrush(g);
	else if(type == STAY)
		DrawStay(g);
	else if(type == RING)
		DrawRing(g);
}

void ParticleEffect::DrawSquare(Core::Graphics& g)
{	 
	for(unsigned i = 0; i < particles.size(); i++)
	{
		Particle p = particles[i];
		
		if(p.lifetime > 0)
		{
			for(int j = 0; j < p.particleSize; j++)
			{	
				glm::vec2 bottom(0, j);
				glm::vec2 left(-j, 0);
				glm::vec2 right(j, 0);
				glm::vec2 top(0, -j);			

				bottom = bottom + p.position;
				left = left + p.position;
				right = right + p.position;
				top = top + p.position;

				RGB color = Colorness().Brightness(p.color, 1 - (float)j / p.particleSize);		
				g.SetColor(color * p.lifetime);				
				g.DrawLine(bottom.x, bottom.y, left.x, left.y);
				g.DrawLine(left.x, left.y, top.x, top.y);
				g.DrawLine(top.x, top.y, right.x, right.y);
				g.DrawLine(right.x, right.y, bottom.x, bottom.y);
			}
		}	
	}
}	 

void ParticleEffect::DrawParticle(Core::Graphics& g)
{
	for(unsigned i = 0; i < particles.size(); i++)
	{
		Particle p = particles[i];														
		g.SetColor(Colorness().VaryColor(p.color, 2));
		g.DrawLine(p.position.x, p.position.y, p.position.x + 2, p.position.y);	
	}
}

void ParticleEffect::setThrustParticles(glm::vec2 origin, Core::RGB color, glm::vec2 vel)
{
	position = origin;
	velocity = vel;
	for(unsigned i = 0; i < particles.size();i++)
	{
		Particle& p = particles[i];
		p.velocity.x = -velocity.x;
		p.velocity.y = -velocity.y;
		p.color = color;
	}
}
