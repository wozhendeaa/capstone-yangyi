#pragma once
#include <vector>
#include "Colorness.h"
#include "Particle.h"
#include "Core\Engine.h"
#include "ExportHeader.h"
using BatEngine::Particle;

class ENGINE_SHARED ParticleEffect
{
private:
	void UpdateExplosion(float dt);
	void UpdateThrust(float dt);
	void UpdateRing(float radius);
	void UpdateStay(float dt);
	void initParticles(int num, glm::vec2 origin, Core::RGB color);
	void initStationaryParticles(Core::RGB color);
	void initRingParticles(int num, glm::vec2 origin);
	void DrawSquare(Core::Graphics& g);
	void DrawParticle(Core::Graphics& g);
	void DrawStay(Core::Graphics& g);
	glm::vec2 generateVelocity();

public:
	std::vector<Particle> particles;
	glm::vec2 position;
	glm::vec2 velocity;
	bool disposable;
	enum Type
	{
		EXPLOSION,
		THRUST,
		BRUSH,
		STAY,
		RING
	};

	Type type;
	ParticleEffect();
	ParticleEffect(int num, glm::vec2 origin, Type type, Core::RGB color);
	
	void DrawRing(Core::Graphics& g);
	void setThrustParticles(glm::vec2 origin, Core::RGB color,glm::vec2 vel);
	void setVelocity(glm::vec2 vel);
	void setLifeTime(float lt);
	virtual void Update(float dt);
	virtual void Draw(Core::Graphics& g);
};