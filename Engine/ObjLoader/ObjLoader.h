#pragma once
#include <ShapeGenerator.h>
#include <ObjLoader\ObjModel.h>
#include <iostream>
#include <fstream>
#include <ExportHeader.h>
class ENGINE_SHARED ObjLoader
{
	char* data;
	glm::vec3 scale;
	ObjData shape;	
	ObjDataShort shape_short;

	void read_num_verts(
		std::ifstream& fs);

	void read_num_indices(
		std::ifstream& fs);

	template<typename T>
	void read_value(
		std::ifstream& fs, T& num)
	{
		T* data = new T;
		fs.read((char*)data, sizeof(T));

		num = *data;
		delete data;
	}

	template<typename T>
	void read_scaled_value(
		std::ifstream& fs, T& num, float _scale)
	{
		T* data = new T;
		fs.read((char*)data, sizeof(T));

		num = *data * _scale;
		delete data;
	}

	void read_verts(
		std::ifstream& fs);
		
	void read_indices(
		std::ifstream& fs);

	static ObjDataShortV2 getModelWithTangentFunc(ObjDataShort& model);	

public:
	ObjLoader(glm::vec3 _scale = glm::vec3(1.0f, 1.0f, 1.0f));

	ObjData getModel();
	ObjDataShort getModelShort();	

	
	void loadFromFile(
		const char* path);
	
	char* loadLevelFromBinary(
		char* data);

	static ObjDataShortV2 getModelWithTangent(Neumont::ShapeData& model);
	static ObjDataShortV2 getModelWithTangent(ObjDataShort& model);

	static glm::vec3 computeTanget(const Vertex& vert0, 
		const Vertex& vert1,
		const Vertex& vert2);
	
	~ObjLoader(void);
};

