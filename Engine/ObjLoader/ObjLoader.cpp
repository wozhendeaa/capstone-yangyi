#include "ObjLoader.h"
uint VertexV2::POSITION_OFFSET = 0;
uint VertexV2::COLOR_OFFSET = 12;
uint VertexV2::NORMAL_OFFSET = 28;
uint VertexV2::UV_OFFSET = 40;
uint VertexV2::TANGENT_OFFSET = 48;
uint VertexV2::STRIDE = 60;

ObjLoader::ObjLoader(glm::vec3 _scale) : scale(_scale)
{

}

void ObjLoader::read_num_verts(
	std::ifstream& fs)
{
	read_value(fs, shape.numVerts);
	//std::cout << shape.numVerts << std::endl;

}

void ObjLoader::read_num_indices(
	std::ifstream& fs)
{
	read_value(fs, shape.numIndices);
	//std::cout << shape.numIndices << std::endl;
}

void ObjLoader::read_verts(
	std::ifstream& fs)
{
//	auto pos_size = sizeof(shape.verts->position);
//	auto uv_size = sizeof(shape.verts->uv);
//	auto normal_size = sizeof(shape.verts->normal);
//	auto total_size = pos_size + uv_size + normal_size;

	Neumont::Vertex* verts = new Neumont::Vertex[sizeof(Neumont::Vertex) * shape.numVerts];

	for (uint i = 0; i < shape.numVerts; i++)
	{
		read_scaled_value(fs, verts[i].position.x, scale.x);
		read_scaled_value(fs, verts[i].position.y, scale.y);
		read_scaled_value(fs, verts[i].position.z, scale.z);
		read_value(fs, verts[i].uv.x);
		read_value(fs, verts[i].uv.y);
		read_value(fs, verts[i].normal.x);
		read_value(fs, verts[i].normal.y);
		read_value(fs, verts[i].normal.z);
	}

	shape.verts = verts;
}


void ObjLoader::read_indices(
	std::ifstream& fs)
{
	uint* indices = new uint[sizeof(uint) * shape.numIndices];
	for (uint i = 0; i < shape.numIndices; i++)
	{
		read_value(fs, indices[i]);
	}

	shape.indices = indices;
}


ObjData ObjLoader::getModel()
{
	return shape;
}

ObjDataShort ObjLoader::getModelShort()
{
	shape_short.verts = shape.verts;
	shape_short.numVerts = shape.numVerts;
	shape_short.numIndices = shape.numIndices;
	shape_short.indices = new ushort[shape.numIndices];
	
	for (uint i = 0; i < shape.numIndices; i++)
	{
		shape_short.indices[i] = static_cast<ushort>(shape.indices[i]);
	}

	delete shape.indices;
	
	return shape_short;
}
	
void ObjLoader::loadFromFile(
	const char* path)
{
	std::ifstream file(path, std::ios::in|std::ios::binary);
	if(file.is_open())
	{
		file.seekg(0, std::ios::end);
		int size = static_cast<int>(file.tellg());
		data = new char[size];

		//read data from begining
		file.seekg(0, std::ios::beg);
		file.read(data, size);
		file.close();
		
		/*read_num_verts(file);
		read_num_indices(file);
		read_verts(file);
		read_indices(file);
*/
		auto pos = data;
		shape.numVerts =*reinterpret_cast<uint*>(pos);
		pos += sizeof(uint);
		shape.numIndices =*reinterpret_cast<uint*>(pos);
		pos += sizeof(uint);

		auto verts = new Neumont::Vertex[shape.numVerts];

		for (uint i = 0; i < shape.numVerts; i++)
		{
			verts[i].position = *reinterpret_cast<glm::vec3*>(pos);
			pos += sizeof(glm::vec3);
			verts[i].uv = *reinterpret_cast<glm::vec2*>(pos);
			pos += sizeof(glm::vec2);
			verts[i].normal = *reinterpret_cast<glm::vec3*>(pos);
			pos += sizeof(glm::vec3);
			verts[i].color = glm::vec4(1.0f);
		}

		shape.verts = verts;

		auto indices = new uint[shape.numIndices];

		for (uint i = 0; i < shape.numIndices; i++)
		{
			indices[i] = *reinterpret_cast<uint*>(pos);
			pos += sizeof(uint);
		}

		shape.indices = indices;

		delete[] data;
	}

}


char* ObjLoader::loadLevelFromBinary(
	char* data)
{
	auto pos = data;
	shape.numVerts =*reinterpret_cast<uint*>(pos);
	pos += sizeof(uint);
	shape.numIndices =*reinterpret_cast<uint*>(pos);
	pos += sizeof(uint);
	static int i = 0;
	i ++;

	auto verts = new Neumont::Vertex[shape.numVerts];

	for (uint i = 0; i < shape.numVerts; i++)
	{
		verts[i].position = *reinterpret_cast<glm::vec3*>(pos);
		pos += sizeof(glm::vec3);
		verts[i].uv = *reinterpret_cast<glm::vec2*>(pos);
		pos += sizeof(glm::vec2);
		verts[i].normal = *reinterpret_cast<glm::vec3*>(pos);
		pos += sizeof(glm::vec3);
		verts[i].color = glm::vec4(1.0f);
		pos += sizeof(glm::vec4);
	}

	shape.verts = verts;

	auto indices = new uint[shape.numIndices];

	for (uint i = 0; i < shape.numIndices; i++)
	{
		indices[i] = *reinterpret_cast<uint*>(pos);
		pos += sizeof(uint);
	}

	shape.indices = indices;

	return pos;	
}



ObjDataShortV2 ObjLoader::getModelWithTangentFunc(ObjDataShort& model)
{
	ObjDataShortV2 newModel;
	newModel.verts = new VertexV2[model.numVerts];
	newModel.numVerts = model.numVerts;
	newModel.indices =new ushort[model.numIndices];
	newModel.numIndices = model.numIndices;

	for (uint i = 0; i < model.numVerts; i++)
	{
		newModel.verts[i].position = model.verts[i].position;
		newModel.verts[i].uv = model.verts[i].uv;
		newModel.verts[i].normal = model.verts[i].normal;
		newModel.verts[i].color = model.verts[i].color;
	}

	for (uint i = 0; i < model.numIndices; i++)
	{
		auto index = model.indices[i];
		newModel.indices[i] = index;
	}

	for (uint i = 0; i < model.numIndices; i += 3)
	{
		auto index = model.indices[i];
		auto index2 = model.indices[i + 1];
		auto index3 = model.indices[i + 2];


		newModel.indices[i] = index;
		auto vert0 = model.verts[index];
		auto vert1 = model.verts[index2];
		auto vert2 = model.verts[index3];

		auto tangent = computeTanget(
									vert0,
									vert1,
									vert2);

		newModel.verts[index].tangent += tangent;
		newModel.verts[index2].tangent +=tangent;
		newModel.verts[index3].tangent += tangent;
	}

	for (uint i = 0; i < model.numVerts ; i ++ )
	{
		auto& t = newModel.verts[i].tangent;
		t = glm::normalize(t);
	}

	return newModel;
}


ObjDataShortV2 ObjLoader::getModelWithTangent(ObjDataShort& model)
{
	return getModelWithTangentFunc(model);
}

ObjDataShortV2 ObjLoader::getModelWithTangent(Neumont::ShapeData& model)
{
	ObjDataShort converted;
	converted.verts = model.verts;
	converted.indices = model.indices;
	converted.numVerts = model.numVerts;
	converted.numIndices = model.numIndices;

	auto m = ObjLoader::getModelWithTangent(converted);

	return m;
}


glm::vec3 ObjLoader::computeTanget(const Vertex& vert0, 
		const Vertex& vert1,
		const Vertex& vert2)
{
	auto pos0 = vert0.position;
	auto pos1 = vert1.position;
	auto pos2 = vert2.position;

	auto uv0 = vert0.uv;
	auto uv1 = vert1.uv;
	auto uv2 = vert2.uv;

	auto deltaPos1 = pos1 - pos0;
	auto deltaPos2 = pos2 - pos0;

	auto deltaUV1 = uv1 - uv0;
	auto deltaUV2 = uv2 - uv0;

	//auto pos0 = vec3(0.0f);
	//auto pos1 = vec3(0.0f, 0.0f, -1.0f);
	//auto pos2 = vec3(1.0f, 0.0f, 0.0f);

	//auto uv0 = vec2(0.0f, 0.0f);
	//auto uv1 = vec2(0.0f, 0.3f);
	//auto uv2 = vec2(0.3f, 0.0f);

	//auto deltaPos1 = pos1 - pos0;
	//auto deltaPos2 = pos2 - pos0;

	//auto deltaUV1 = uv1 - uv0;
	//auto deltaUV2 = uv2 - uv0;

	auto uvMat = glm::mat2x2(deltaUV1, deltaUV2);

	auto posMat = glm::mat2x3(deltaPos1, deltaPos2);

	auto inverse = glm::inverse(uvMat);

	auto tangentMat = glm::mat3(glm::inverse(uvMat)) * posMat;

	auto tangent = glm::vec3(tangentMat[0].x, tangentMat[0].y, tangentMat[0].z);

	return tangent;
}

ObjLoader::~ObjLoader(void)
{
}
