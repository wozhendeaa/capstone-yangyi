#pragma once
#include <Vertex.h>
#include <ExportHeader.h>
using Neumont::Vertex;
using Neumont::VertexV2;
struct ENGINE_SHARED ObjData
{
	Vertex* verts;
	uint numVerts;
	uint* indices;
	uint numIndices;
	static uint POSITION_OFFSET;
	static uint COLOR_OFFSET;
	static uint NORMAL_OFFSET;
	static uint UV_OFFSET;
	static uint TANGENT_OFFSET;
	static uint STRIDE;
	ObjData() : verts(0), numVerts(0), indices(0), numIndices(0) {}
	uint vertexBufferSize() const { return numVerts * sizeof(Vertex); }
	uint indexBufferSize() const { return numIndices * sizeof(uint); }
	void cleanUp()
	{
		delete[] verts;
		verts = 0;
		delete[] indices;
		indices = 0;
		numVerts = numIndices = 0;
	}
};

struct ENGINE_SHARED ObjDataShort
{
	Vertex* verts;
	uint numVerts;
	ushort* indices;
	uint numIndices;
	static uint POSITION_OFFSET;
	static uint COLOR_OFFSET;
	static uint NORMAL_OFFSET;
	static uint UV_OFFSET;
	static uint STRIDE;
	ObjDataShort() : verts(0), numVerts(0), indices(0), numIndices(0) {}
	uint vertexBufferSize() const { return numVerts * sizeof(Vertex); }
	uint indexBufferSize() const { return numIndices * sizeof(ushort); }
	void cleanUp()
	{
		delete[] verts;
		verts = 0;
		delete[] indices;
		indices = 0;
		numVerts = numIndices = 0;
	}
};
struct ENGINE_SHARED ObjDataShortV2 
{
	VertexV2* verts;
	uint numVerts;
	ushort* indices;
	uint numIndices;

	ObjDataShortV2() : verts(0), numVerts(0), indices(0), numIndices(0) {}
	uint vertexBufferSize() const { return numVerts * sizeof(VertexV2); }
	uint indexBufferSize() const { return numIndices * sizeof(ushort); }
	void cleanUp()
	{
		delete[] verts;
		verts = 0;
		delete[] indices;
		indices = 0;
		numVerts = numIndices = 0;
	}
};

