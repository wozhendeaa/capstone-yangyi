#include "Random.h"

const float TWO_PI = 2.0f * 3.1415926f;


float Random::RandomFloat()
{
	return (float)rand() / RAND_MAX;
}

glm::vec2 Random::RandomUnitVector()
{
	float angle = TWO_PI * RandomFloat();
	return glm::vec2(std::cos(angle), std::sin(angle));
}

float Random::RandomInRange(float max, float min)
{
	return RandomFloat() * (max - min + 1) + min;	
}

