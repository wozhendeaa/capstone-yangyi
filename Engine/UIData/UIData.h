#pragma once
#include <glm\glm.hpp>

using glm::vec3;
using glm::vec4;

struct UIData
{
	vec3 AmbientColor;
	vec3 AmbientLightColor;
	vec3 lightPosition;
	vec4 directional;
	vec3 SpecColor;
	int specExpo;
	vec3 Diffuse_color;
	bool enable_lighting;
	bool useNoiseAsHeightMap;
	float octave;
	UIData()
	{
		AmbientColor = vec3(0.0f);
	    AmbientLightColor = vec3(0.7f);
		lightPosition = vec3(0.0f, 2.0f, 10.0f);
		SpecColor = vec3(1.0f);
		specExpo = 40.0f;
	    Diffuse_color = vec3(0.5f, 0.5f, 0.5f);
		enable_lighting = true;
		useNoiseAsHeightMap = false;
		octave = 1.0f;
	}
};