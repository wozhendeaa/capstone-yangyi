#pragma once
#include <ExportHeader.h>
#include <glm\glm.hpp>

using glm::vec3;
using glm::mat3;
using glm::mat4;


class ENGINE_SHARED IGamePhysics
{
public:
	virtual void Initialize() =0;
	virtual void Update(float dt) =0;
	virtual void Destroy() =0;
	virtual void SetGravity(const vec3& gravity) =0;
						  
};

