#pragma once
#include <ExportHeader.h>
#include <glm\glm.hpp>

using glm::vec3;

template <class T>
class ENGINE_SHARED Octree
{
	vec3 origin;
	float halfSize;
	Octree* children[8];

	T* data;
public:

	Octree(const vec3& pos,
		   const float& half) :
		   origin(pos),
		   halfSize(half),
		   data(nullptr)
	{
		for (size_t i = 0; i < 8; i++)
		{
			children[i] = nullptr;
		}
	}

	void Subdivide(const float& unitLength)
	{
		if (halfSize > unitLength)
		{
			for (size_t i = 0; i < 8; i++)
			{
				vec3 newPos = origin;

				if (i == 0)
				{
					newPos.x += halfSize *-.5f;
					newPos.y += halfSize * .5f;
				}
				else if (i == 1)
				{
					newPos.x += halfSize * .5f;
					newPos.y += halfSize * .5f;
					newPos.z += .0f;
				}
				else if (i == 2)
				{
					newPos.x += halfSize * -.5f;
					newPos.y += halfSize * -.5f;
					newPos.z += .0f;
				}
				else if (i == 3)
				{
					newPos.x += halfSize * .5f;
					newPos.y += halfSize * -.5f;
					newPos.z += .0f;
				}
				else if (i == 4)
				{
					newPos.x += halfSize * -.5f;
					newPos.y += halfSize * .5f;
					newPos.z +=  halfSize;
				}
				else if (i == 5)
				{
					newPos.x += halfSize * .5f;
					newPos.y += halfSize * .5f;
					newPos.z +=  halfSize;
				}
				else if (i == 6)
				{
					newPos.x += halfSize * -.5f;
					newPos.y += halfSize * -.5f;
					newPos.z += halfSize;
				}
				else if (i == 7)
				{
					newPos.x += halfSize * .5f;
					newPos.y += halfSize * -.5f;
					newPos.z += halfSize;
				}

				children[i] = new Octree(newPos, halfSize * .5f);
				children[i]->Subdivide(unitLength);
			}
		}
	}

	int GetChildIndexByPosition(const vec3& pos)
	{
		int oct = 0;
		bool isNearTopLeft = pos.x < origin.x &&
			pos.y > origin.y &&
			pos.z < origin.z + halfSize  + FLT_EPSILON ;

		bool isNearTopRight = pos.x > origin.x &&
			pos.y > origin.y &&
			pos.z < origin.z + halfSize  + FLT_EPSILON;

		bool isNearBottomLeft = pos.x < origin.x &&
			pos.y < origin.y &&
			pos.z < origin.z + halfSize  + FLT_EPSILON;

		bool isNearBottomRight = pos.x > origin.x &&
			pos.y < origin.y &&
			pos.z < origin.z + halfSize  + FLT_EPSILON;

		bool isFarTopLeft = pos.x < origin.x &&
			pos.y > origin.y &&
			pos.z > origin.z + halfSize  + FLT_EPSILON;

		bool isFarTopRight = pos.x > origin.x &&
			pos.y > origin.y &&
			pos.z > (origin.z  + FLT_EPSILON);

		bool isFarBottomLeft = pos.x < origin.x &&
			pos.y < origin.y &&
			pos.z > (origin.z  + FLT_EPSILON);

		bool isFarBottomRight = pos.x > origin.x &&
			pos.y < origin.y &&
			pos.z > (origin.z  + FLT_EPSILON);

		if (isNearTopLeft)
		{
			oct = 0;
		}
		else if (isNearTopRight)
		{
			oct = 1;
		}
		else if (isNearBottomLeft)
		{
			oct = 2;
		}
		else if (isNearBottomRight)
		{
			oct = 3;
		}
		else if (isFarTopLeft)
		{
			oct = 4;
		}
		else if (isFarTopRight)
		{
			oct = 5;
		}
		else if (isFarBottomLeft)
		{
			oct = 6;
		}
		else if (isFarBottomRight)
		{
			oct = 7;
		}

		return oct;
	}

	bool InsertCollisionAt(T* node, const vec3& pos, vec3* newPos)
	{

		if (IsLeaf())
		{
			if (data == nullptr)
			{
				data = node;
				*newPos = origin;
				return true;
			}
			else
			{
				return false;
			}
		}
		else 
		{
			int index = GetChildIndexByPosition(pos);
			return children[index]->InsertCollisionAt(node, pos, newPos);
		}		
	}

	bool IsLeaf()
	{
		return children == nullptr || children[0] == nullptr;
	}

	void clearData()
	{
		
		data = nullptr;
		for (size_t i = 0; i < 8; i++)
		{
			if (children[i] != nullptr)
			{
				children[i]->clearData();
			}
		}
	}

	~Octree()
	{
		/*if (children[0] != nullptr)
		{
			for (size_t i = 0; i < 8; i++)
			{
				if (children[0] != nullptr)
					delete children[i];
			}
		}*/

	}
};

