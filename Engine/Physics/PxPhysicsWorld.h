#pragma once
#pragma warning (disable:4099)
#include <ExportHeader.h>
#include <..\PhysX\include\PxPhysicsAPI.h>
#include <Physics\IGamePhysics.h>
#include <glm\gtx\quaternion.hpp>
#include "BatEngineDefaultSimulationEvent.h"

using glm::quat;
using physx::PxMaterial;
using physx::PxTransform;
using physx::PxVec3;
using physx::PxMat44;
namespace physx
{
	static PxSimulationEventCallback* batEngineDefaultCallback;
	static PxDefaultErrorCallback gDefaultErrorCallback;
	static PxDefaultAllocator gDefaultAllocatorCallback;
	static PxFoundation* gFoundation = nullptr;
	static PxProfileZoneManager* pProfile = nullptr;
	static PxScene* pScene = nullptr;
	static PxPhysics* pPhysicsSDK = nullptr;
	static PxCudaContextManager* mCudaContextManager = nullptr;
	static PxVisualDebuggerConnection* theConnection = nullptr;
	static PxCpuDispatcher* cpuDispatcher = nullptr;
	static PxGpuDispatcher* gpuDispatcher = nullptr;
	static PxControllerManager* controllerManager = nullptr;

	inline PxVec3 ToPxVec3(const vec3& vec)
	{
		return PxVec3(vec.x, vec.y, vec.z);
	}


	inline vec3 ToVec3(const PxExtendedVec3& vec)
	{
		return vec3(vec.x, vec.y, vec.z);
	}

	inline PxQuat ToPxQuat(const quat& q)
	{		
		return PxQuat(q.w, q.x, q.y, q.z);
	}

	inline quat ToQuat(const PxQuat& q)
	{
		return quat( q.w, q.x, q.y, q.z);

	}

	inline PxTransform ToPxTransform(vec3& pos, quat& quat)
	{
		return PxTransform(ToPxVec3(pos), ToPxQuat(quat));
	}

	inline PxTransform ToPxTransform(vec3& pos)
	{
		return PxTransform(ToPxVec3(pos));
	}

	inline PxTransform ToPxTransform(quat& quat)
	{
		return PxTransform(ToPxQuat(quat));
	}

	inline PxMat44 ToPxMat4(mat4& mat)
	{
		PxMat44 pMat;
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				pMat[i][j] = mat[i][j];
			}
		}
		return pMat;
	}


	inline PxTransform ToPxTransform(mat4& mat)
	{
		return PxTransform(ToPxMat4(mat));
	}

	
	inline vec3 ToVec3(const PxVec3& vec)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
	{
		return vec3(vec.x, vec.y, vec.z);
	}



	class ENGINE_SHARED PxPhysicsWorld 
		: public IGamePhysics
	{   
		
		public:
			PxPhysicsWorld(void);
			~PxPhysicsWorld(void);

			void Initialize() override;
			void Update(float dt) override;
			void Destroy() override;
			void SetGravity(const vec3& gravity) override;

			void SetFilterData(PxRigidActor* actor, PxU32 filterGroup, PxU32 filterMask);

			PxScene* GetPhysicsWorld();

			PxPhysics* GetSDK();

			PxController* GenCapsuleController(const vec3& pos,
				float radius = .5f,
				float height = 1.0f,
				const vec3& up = vec3(.0f, 1.0f, .0f),
				float contactOffset = .05f,
				float stepOffset = .01f,
				float slopeLimit = .01f);
	
	
	};

}