
#include "PxPhysicsWorld.h"
#include <MemoryDebug.h>
#include <iostream>


namespace physx
{
	PxPhysicsWorld::PxPhysicsWorld(void)
	{
	}

	PxFilterFlags BatEngineDefaultFilterShader(
		PxFilterObjectAttributes attributes0, PxFilterData filterData0,
		PxFilterObjectAttributes attributes1, PxFilterData filterData1,
		PxPairFlags& pairFlags, const void* constantBlock, PxU32 constantBlockSize)
	{
		if (PxFilterObjectIsTrigger(attributes0) || PxFilterObjectIsTrigger(attributes1))
		{
			pairFlags = PxPairFlag::eTRIGGER_DEFAULT;
			return PxFilterFlag::eDEFAULT;
		}
		pairFlags = PxPairFlag::eSOLVE_CONTACT | PxPairFlag::eCONTACT_DEFAULT;

		if ((filterData0.word0 & filterData1.word1) && (filterData1.word0 & filterData0.word1))
			pairFlags |= PxPairFlag::eNOTIFY_TOUCH_FOUND;

		return PxFilterFlag::eDEFAULT;
	}

	void PxPhysicsWorld::Initialize()
	{
		gFoundation = PxCreateFoundation(PX_PHYSICS_VERSION,
			gDefaultAllocatorCallback,
			gDefaultErrorCallback);

		//pProfile = &PxProfileZoneManager::createProfileZoneManager(gFoundation);
		//
		//assert(pProfile);
		//if(pProfile == nullptr)
		//{
		//	//std::cerr<<"Error creating PhysX3 device, Exiting..."<< std::endl;
		//	exit(1);
		//}

		auto scale = PxTolerancesScale();

		pPhysicsSDK = PxCreatePhysics(PX_PHYSICS_VERSION,
			*gFoundation,
			scale,
			false);

		if(pPhysicsSDK == nullptr)
		{
			//std::cerr<<"Error creating PhysX3 device, Exiting..."<< std::endl;
			exit(1);
		}

		if(pPhysicsSDK->getPvdConnectionManager() == NULL)
			return;

		// setup connection parameters
		const char*     pvd_host_ip = "127.0.0.1";  // IP of the PC which is running PVD
		int             port        = 5425;         // TCP port to connect to, where PVD is listening
		unsigned int    timeout     = 100;          // timeout in milliseconds to wait for PVD to respond,

		PxVisualDebuggerConnectionFlags connectionFlags = PxVisualDebuggerExt::getAllConnectionFlags();
		// and now try to connect
		 theConnection = PxVisualDebuggerExt::createConnection(pPhysicsSDK
			->getPvdConnectionManager(), pvd_host_ip, port, timeout, connectionFlags);
		if(theConnection)
			std::cout<<"PVD Connection Successful!\n" << std::endl;

		PxSceneDesc sceneDesc(pPhysicsSDK->getTolerancesScale());
		sceneDesc.gravity = PxVec3(0.0f, -9.8f, 0.0f);
		sceneDesc.cpuDispatcher = cpuDispatcher = PxDefaultCpuDispatcherCreate(1);
		sceneDesc.filterShader = PxDefaultSimulationFilterShader;
		sceneDesc.flags = PxSceneFlag::eENABLE_KINEMATIC_STATIC_PAIRS;
		sceneDesc.filterShader = BatEngineDefaultFilterShader;

		PxCudaContextManagerDesc cudaContextManagerDesc;
		mCudaContextManager = PxCreateCudaContextManager(*gFoundation,
			cudaContextManagerDesc,
			pProfile);

		if(!sceneDesc.gpuDispatcher && mCudaContextManager)
		{
			sceneDesc.gpuDispatcher = mCudaContextManager->getGpuDispatcher();
		}

		pScene = pPhysicsSDK->createScene(sceneDesc);
		batEngineDefaultCallback = new BatEngineDefaultSimulationEvent;
		pScene->setSimulationEventCallback(batEngineDefaultCallback);
		

		if (controllerManager == nullptr)
		{
			controllerManager = PxCreateControllerManager(*pScene);
		}
	}


	void PxPhysicsWorld::Destroy()
	{
		if(theConnection != nullptr)
			theConnection->release();

		if (controllerManager != nullptr)
			controllerManager->release();

		if(pScene != nullptr) 
			pScene->release();

		if(pPhysicsSDK != nullptr)
			pPhysicsSDK->release();

		if(cpuDispatcher != nullptr)
			delete cpuDispatcher;

		if(mCudaContextManager != nullptr)
			mCudaContextManager->release();
		
		if(gFoundation )
			gFoundation->release();

		if (batEngineDefaultCallback != nullptr)
			delete batEngineDefaultCallback;
	}

	void PxPhysicsWorld::Update(float dt)
	{
		static float stepSize = 1.0f/30.0f;
		static float accum = 0.0f;
		accum += dt;
		
		//if(accum >= stepSize)
		{
			pScene->simulate(stepSize);
			pScene->fetchResults(true);
			accum = 0.0f;
		}
	}

	void PxPhysicsWorld::SetGravity(const vec3& gravity) 
	{
		pScene->setGravity(ToPxVec3(gravity));
	}


	PxScene* PxPhysicsWorld::GetPhysicsWorld()
	{
		return pScene;
	}

	PxPhysics* PxPhysicsWorld::GetSDK()
	{
		return pPhysicsSDK;
	}


	void PxPhysicsWorld::SetFilterData(PxRigidActor* actor, 
		PxU32 filterGroup, 
		PxU32 filterMask)
	{
		PxFilterData filter;
		filter.word0 = filterGroup;
		filter.word1 = filterMask;

		auto numShapes = actor->getNbShapes();
		auto shapes = new PxShape*[sizeof(PxShape*) * numShapes];
		actor->getShapes(shapes, numShapes);

		for (size_t i = 0; i < numShapes; i++)
		{
			auto s = shapes[i];
			s->setSimulationFilterData(filter);
		}

		delete[] shapes;
	}



	PxController* PxPhysicsWorld::GenCapsuleController(const vec3& pos,
		float radius,
		float height,
		const vec3& up,
		float contactOffset,
		float stepOffset,
		float slopeLimit)
	{
		PxCapsuleControllerDesc desc;
		desc.position = PxExtendedVec3(pos.x, pos.y, pos.z);
		desc.contactOffset = contactOffset;
		desc.stepOffset = stepOffset;
		desc.slopeLimit = slopeLimit;
		desc.radius = radius;
		desc.height = height;
		radius;
		height;
		desc.upDirection = ToPxVec3(up);		

		desc.material = GetSDK()->createMaterial(.2f, .2f, 0.0f);

		auto controller = controllerManager->createController(desc);
		return controller;
	}


	PxPhysicsWorld::~PxPhysicsWorld(void)
	{
		Destroy();
		_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
		_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );

	}
}