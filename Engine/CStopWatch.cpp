#include "CStopWatch.h"
#include <cstdio>

CStopWatch::CStopWatch()
{
	timer.start.QuadPart = 0;
	timer.end.QuadPart = 0;
	QueryPerformanceFrequency(&frequency);
}

double CStopWatch::LTToSecs(LARGE_INTEGER& l)
{
	auto secs = (double) l.QuadPart / (double) frequency.QuadPart;
	secs = secs > 1.0 ? 0.000001f : secs;
	return secs;
}

void CStopWatch::startTime()
{
	QueryPerformanceCounter(&timer.start);
}

void CStopWatch::stopTime()
{
	QueryPerformanceCounter(&timer.end);
}

double CStopWatch::interval()
{
	LARGE_INTEGER time;
	time.QuadPart = timer.end.QuadPart - timer.start.QuadPart;
	return LTToSecs(time);
}

