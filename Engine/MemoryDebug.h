#pragma once
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#define _DEBUG_NEW new (_CLIENT_BLOCK, __FILE__, __LINE__)
#ifdef _DEBUG
#define new _DEBUG_NEW
#endif