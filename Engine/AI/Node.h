#pragma once
#include <glm\glm.hpp>
#include <vector>
#include <DebugTools\DebugShapesManager.h>

using glm::mat4;
using glm::vec3;
using glm::vec4;

#define NODE_RADIUS 1

class EditorNode;
struct Renderable;


class ENGINE_SHARED Connection
{
public:
	float cost;
	EditorNode* target;
	DebugShapesManager::VectorGraphic* vect;
};

class ENGINE_SHARED EditorNode
{

public:
	vec3 position;
	vec4 current_color;
	bool enable_click_color;
	static vec4 selected_color;
	static vec4 connected_color;
	static vec4 no_color;
	std::vector<Connection*> connections;
	Renderable* r;

	void deleteConnections(Renderer* r = nullptr);
	EditorNode(void);
	~EditorNode(void);
};

struct GameNode;

struct ENGINE_SHARED GameConnection
{
	float cost;
	GameNode* target;
};


struct ENGINE_SHARED GameNode
{
	unsigned int num_connections;
	vec3 position;
	GameConnection* connections;
};

struct ENGINE_SHARED PathNode
{
	float f;
	float g;
	float h;
	GameNode* this_node;
	PathNode* p_node;
};

struct ENGINE_SHARED PathNodeDebug
{
	float f;
	float g;
	float h;
	EditorNode* this_node;
	PathNodeDebug* p_node;
};




