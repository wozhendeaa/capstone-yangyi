#pragma once
#include <Qt\qlist.h>
#include <AI\Node.h>
class ENGINE_SHARED AStarManager
{
protected:
	GameNode* nodes;
	GameNode* from;
	GameNode* to;
	QList<PathNode*> open;
	QList<PathNode*> close;
	PathNode* curr_node;
	void processNode(PathNode* current);
	bool isNodeInClose(PathNode* node);
	PathNode* getLowFromOpen();
	PathNode* getLowFromClose();
	void removeCurrentFromOpen();
	void moveCurrentToClose();
	bool isEndNode(PathNode* node); 
	void gameNodesToPathNodes(uint& num);
	friend class DebugEditor;
	PathNode* toPathNode(GameNode* n);
	void setupNodes(uint& num);

public:
	QList<GameNode*> path;
	QList<PathNode*> path_nodes;
	PathNode* getPathNodeByNode(GameNode* n);
	void gen_path(PathNode* node);
	bool initializeAstars(PathNode* from, PathNode* to);
	
	//clear all node lists
	void clear();
	AStarManager(GameNode* gn, uint num);
	~AStarManager(void);
};

