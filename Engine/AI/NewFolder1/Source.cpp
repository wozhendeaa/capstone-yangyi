#include <iostream>
#include "DeleFastDelegate.h"
using namespace std;
class GameObject;

class State
{
public:
	virtual void update(GameObject* target) = 0;
};


class GameObject
{
	DecisionTreeNode* meDecisionTree;
	State* meState;
	float amountOfMoney;
	unsigned int numHomeworkAssignments;

	static State* states[3];
public:
	GameObject() : amountOfMoney(25), numHomeworkAssignments(0) {}
	void initialize(); // Defined lower
	void shutdown()
	{
		delete states[0];
		delete states[1];
		delete states[2];
		delete meDecisionTree;
	}
	float getYeMoolah() const { return amountOfMoney; }
	void setYeMoolah(float moolah) { amountOfMoney = moolah; }
	unsigned int getNumHomeworkAssignments() const { return numHomeworkAssignments; }
	void doHomework() { numHomeworkAssignments--; }
	void update()
	{ 
		if(rand() % 2 == 0)
			numHomeworkAssignments++;
		meState = meDecisionTree->evaluate(this);
		meState->update(this); 
	}
};

State* GameObject::states[3];

const float COST_FOR_GOOD_MEAL = 12.00;

class EatingAwesomeFoodState : public State
{
public:
	void update(GameObject* target)
	{
		target->setYeMoolah(target->getYeMoolah() - COST_FOR_GOOD_MEAL);
		cout << "Oh so good! $" << target->getYeMoolah() << " left." << endl;
	}
};

class EatingRamenState : public State
{
public:
	void update(GameObject* target)
	{
		cout << "Mere sustenance..." << endl;
	}
};

class DoingHomeworkState : public State
{
public:
	void update(GameObject* target)
	{
		cout << "Fine, I'll do my homework!" << endl;
		target->doHomework();
	}
};

class Questions
{
public:
	static bool feelingLazy(GameObject* target)
	{
		cout << "Feeling lazy?" << endl;
		return rand() % 4 == 0;
	}
	static bool haveEnoughMoneyToGoOut(GameObject* target)
	{
		cout << "Have enough money to go out?" << endl;
		return target->getYeMoolah() > COST_FOR_GOOD_MEAL;
	}
	static bool shouldDoHomework(GameObject* target)
	{
		cout << "Should do homework?" << endl;
		return target->getNumHomeworkAssignments() > 2;
	}
};

void GameObject::initialize()
{
	// Build states:
	states[0] = new EatingAwesomeFoodState();
	states[1] = new EatingRamenState();
	states[2] = new DoingHomeworkState();

	// Build the decision tree
	auto eatingAwesomeFoodStateNode = new DecisionTreeNode(states[0]);
	auto eatingRamenStateNode = new DecisionTreeNode(states[1]);
	auto doingHomeworkStateNode = new DecisionTreeNode(states[2]);

	auto enoughMoney = new DecisionTreeNode(Questions::haveEnoughMoneyToGoOut, eatingAwesomeFoodStateNode, eatingRamenStateNode);
	auto lazyNoHomework = new DecisionTreeNode(Questions::feelingLazy, eatingRamenStateNode, enoughMoney);

	auto lazyWithHomework = new DecisionTreeNode(Questions::feelingLazy, lazyNoHomework, doingHomeworkStateNode);
	meDecisionTree = new DecisionTreeNode(Questions::shouldDoHomework, lazyWithHomework, lazyNoHomework);
}

void main()
{
	GameObject layne;
	layne.initialize();

	bool gameLoop = true;
	while(gameLoop)
	{
		layne.update();
		cout  << endl << endl;
	}
	layne.shutdown();
}