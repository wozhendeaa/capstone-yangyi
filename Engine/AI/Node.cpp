#include "Node.h"
#include <MemoryDebug.h>
vec4 EditorNode::connected_color = vec4(0.0f, 1.0f, 0.0f, 1.0f);
vec4 EditorNode::selected_color = vec4(1.0f, 0.0f, 0.0f, 1.0f);
vec4 EditorNode::no_color = vec4(1.0f);
EditorNode::EditorNode(void)
{
	enable_click_color = true;
	current_color = no_color;
}

void EditorNode::deleteConnections(Renderer* r)
{
	if(r != nullptr)
	{
		for (uint i = 0; i < connections.size();)
		{
			auto temp = connections[i];	
			temp->vect->dispose();
			temp->target = nullptr;
			connections.erase(connections.begin());
		}
	}
}


EditorNode::~EditorNode(void)
{
}
