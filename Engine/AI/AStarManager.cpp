#include "AStarManager.h"
#include <MemoryDebug.h>


AStarManager::AStarManager(GameNode* _nodes, uint num)
{
	nodes = _nodes;
	setupNodes(num);
}

PathNode* AStarManager::toPathNode(GameNode* en)
{
	assert(en != nullptr);
	auto path_node = new PathNode; 
	path_node->this_node = en;
	path_node->p_node = nullptr;
	return path_node;
}


void AStarManager::setupNodes(uint& num)
{
	gameNodesToPathNodes(num);
}

void AStarManager::gameNodesToPathNodes(uint& node_num)
{
	for (uint i = 0; i < node_num; i++)
	{
		path_nodes.push_back(toPathNode(&nodes[i]));
	}
}


bool AStarManager::initializeAstars(PathNode* _from, PathNode* _to)
{
	from = _from->this_node;
	to = _to->this_node;

	curr_node = _from;
	open.push_back(curr_node);
	bool path_generated = false;

	while(open.size() > 0)
	{		
		//auto count = open.size();
		auto con_num = curr_node->this_node->num_connections;
		removeCurrentFromOpen();
	
		//process children nodes
		processNode(curr_node);

		if(con_num > 0)
		{
			if(!isNodeInClose(curr_node))
				close.push_back(curr_node);

			//check if we have the final node
			if(isEndNode(curr_node))
			{
				gen_path(curr_node);
				path_generated = true;
				break;
			}

		}
		else if(isEndNode(curr_node))
		{
			removeCurrentFromOpen();
			close.push_back(curr_node);
			gen_path(curr_node);
			path_generated = true;
		}
		else
		{
			removeCurrentFromOpen();
		}

		if(open.size() > 0)
		{
			curr_node = getLowFromOpen();
		}
	}

	return path_generated;
}

void AStarManager::removeCurrentFromOpen()
{
	//put current node into close list
	for (int i = 0; i < open.size(); i++)
	{
		if(open[i] == curr_node)
		{
			open.erase(open.begin() + i);
		}
	}
}

void AStarManager::moveCurrentToClose()
{
	close.push_back(curr_node);
	curr_node = nullptr;
}


void AStarManager::gen_path(PathNode* node)
{
	clear();
	auto temp = node;
	static int count = 0;
	while(temp != nullptr)
	{
		path.insert(0, temp->this_node);
		temp = temp->p_node;
		count ++;
	}

	count = 0;

}

bool AStarManager::isEndNode(PathNode* node)
{
	auto isEnd = node->this_node == to;
	return isEnd;
}

bool AStarManager::isNodeInClose(PathNode* node)
{
	bool found = false;
	for (int i = 0; i < close.size() && !found; i++)
	{
		found = node->this_node == close[i]->this_node;
	}

	return found;
}
				

void AStarManager::	processNode(PathNode* current)
{
	current->h = glm::length(to->position - current->this_node->position);

	if(current->p_node != nullptr)
		current->g = glm::length(current->p_node->this_node->position - current->this_node->position);
	else 
		current->g = 0;
	current->f = current->g + current->h;

	for (unsigned i = 0; i < current->this_node->num_connections; i++)
	{
		auto con = current->this_node->connections[i];
		auto node = toPathNode(con.target);

		if(!isNodeInClose(node))
		{
			node->h = glm::length(to->position - path_nodes[i]->this_node->position);
			node->g = con.cost;
			node->f = node->g + node->h;

			node->p_node = current;
			
			open.push_back(node);
		}
	}
}



PathNode* AStarManager::getLowFromOpen()
{
	for (int i = 0; i < open.size(); i++)
	{
        for (int j =  i + 1; j < open.size(); j++)
        {
            if (open[i]->g < open[j]->g)
            {
                auto temp = open[j];
                open[j] = open[i];
                open[i] = temp;
            }
        }
        
	}
	return open[0];
}

PathNode* AStarManager::getLowFromClose()
{
	for (int i = 0; i < close.size(); i++)
	{
        for (int j =  i + 1; j < close.size(); j++)
        {
            if (close[i]->g > close[j]->g)
            {
                auto temp = close[j];
                close[j] = close[i];
                close[i] = temp;
            }
        }
	}
	
	return close[0];
}


void AStarManager::clear()
{
	open.clear();
	close.clear();
	path.clear();
}

AStarManager::~AStarManager(void)
{
	
}
