#pragma once
#include <Qt\qlist.h>
#include <AI\Node.h>
class ENGINE_SHARED AStarManagerDebug
{
	EditorNode* from;
	EditorNode* to;
	QList<PathNodeDebug*> open;
	QList<PathNodeDebug*> close;
	PathNodeDebug* curr_node;
	void processNode(PathNodeDebug* current);
	bool isNodeInClose(PathNodeDebug* node);
	PathNodeDebug* getLowFromOpen();
	PathNodeDebug* getLowFromClose();
	void removeCurrentFromOpen();
	void moveCurrentToClose();
	bool isEndNode(PathNodeDebug* node);
	friend class DebugEditor;
	PathNodeDebug* toPathNodeDebug(EditorNode* gn);
public:
	int current_goal_index;
	GeometryInfo* node_geo;
	QList<EditorNode*> path;
	QList<PathNodeDebug*> path_nodes;
	QList<EditorNode*> game_nodes;
	void setupNodes();
	void gen_path(PathNodeDebug* node);
	bool initializeAstars(PathNodeDebug* from, PathNodeDebug* to);
	void clear();
	//reset the debug nodes' color  to white
	void clearColor();
	PathNodeDebug* getNearestNode(const vec3& position);
	AStarManagerDebug(void);
	~AStarManagerDebug(void);
};