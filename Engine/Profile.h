#pragma once
#include "CStopWatch.h"
#include "Typedefh.h"
class ENGINE_SHARED Profiler
{
private:
	const char *fileName;
	static const uint MAX_FRAME_NUM = 500;
	static const uint MAX_CATEGORY_NUM = 20;
    uint frameIndex;
	uint categoryIndex;
	uint used_categories_num;

	struct ProfilerCategory
	{
		const char *categoryName;
		float data[MAX_FRAME_NUM]; 

	} categories[MAX_CATEGORY_NUM];

public:
	bool isWrapped() const;
	void writeFrame(uint startIndex) const;
	void addEntry(const char *name, float time);
	void newFrame();
	void shutDown();
	void initialize(const char *fileName);
	/*void DrawBar(Core::Graphics& g, int x, int y, float height);
	void DrawGraph(Core::Graphics& g);*/
};