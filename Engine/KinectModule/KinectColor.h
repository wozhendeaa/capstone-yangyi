#pragma once
#include <ExportHeader.h>
#include <KinectModule\KinectSensor.h>
#include <NuiKinectFusionApi.h>
struct uchar1;
struct uchar4;
struct float2;

class ENGINE_SHARED KinectColor
{
	RGBQUAD* colorPool;
	byte* colorDepth = nullptr;
	ColorSpacePoint* depthPoints = nullptr;
	ICoordinateMapper* coordinateMapper = nullptr;

	//cuda data
	uchar4* d_color = nullptr;
	uchar1* d_depth_color = nullptr;
	uchar1* d_brightness_color = nullptr;
	float2* d_depthColorSpacePoints = nullptr;
	float* d_gama_sum = nullptr;

	bool colorSampleMask[1920 * 1080];
	bool isReady = false;

public:
	static const int        colorWidth  = 1920;
    static const int        colorHeight = 1080;
	
	int x_trunctuation = 0;
	int y_trunctuation = 0;

	KinectColor(void);
	~KinectColor(void);
	void updateColor();

	bool IsScreenDark(float brightnessThreshold = 3.0f,
		int darkPixelCountThreshold = 72362);

	byte* getColorData() const;

	byte* GetDepthColorData(UINT16* depth);

	void shutDown();
};

