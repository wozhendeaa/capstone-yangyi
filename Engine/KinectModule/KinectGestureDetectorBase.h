#pragma once
#include <ExportHeader.h>
#include <vector>
#include <Kinect.h>
#include <Delegation\FastDelegate.h>
#include <MemoryDebug.h>

struct ENGINE_SHARED Entry
{
	Entry(const CameraSpacePoint& pos,
		  const double ptime) :
		position(pos),
		time(ptime)
	{

	}

	CameraSpacePoint position;
	double time;
};

class ENGINE_SHARED KinectGestureDetectorBase
{
protected:
	std::vector<Entry> joints;
	double timeStampSinceLastEvent;
	float minDeltaTimeBetweenGestures;
	unsigned maxJointNum;

public:
	double totalTime;

	fastdelegate::FastDelegate0<> swipeRightDetected;
	fastdelegate::FastDelegate0<> swipeLeftDetected;
	fastdelegate::FastDelegate0<> swipeUpDetected;
	fastdelegate::FastDelegate0<> swipeDowntDetected;

	explicit KinectGestureDetectorBase(unsigned int maxNumJoints = 100) : maxJointNum(maxNumJoints)
	{
		swipeRightDetected= nullptr;
		swipeLeftDetected= nullptr;
		swipeUpDetected= nullptr;
		swipeDowntDetected= nullptr;
		totalTime = 0.0;
	}
	
	virtual ~KinectGestureDetectorBase(void)
	{
	}

	virtual void Add(Joint j)
	{
		/*bool isValid = j.TrackingState == TrackingState::TrackingState_Tracked ||
			j.TrackingState == TrackingState::TrackingState_Inferred;
		if (isValid)
		{
			if (joints.size() >= maxJointNum)
			{
				joints.erase(joints.begin());
			}

			Entry entry(j.Position, totalTime);
			joints.push_back(entry);
		}*/
	}

	virtual void DetectGestures(float dt) =0;

protected:

	void GestureDetected(fastdelegate::FastDelegate0<> callback)
	{
		if (totalTime - timeStampSinceLastEvent > minDeltaTimeBetweenGestures)
		{
			timeStampSinceLastEvent = totalTime;

			if(callback != nullptr)
			{
				callback();
			}

			//joints.clear();
		}
	}
};

