#include <cstdlib>
#include <cmath>
#include <device_launch_parameters.h>
#include <curand_kernel.h>
#include <helper_cuda.h>
#include <helper_math.h>

__device__ const float r_y = 0.299f;
__device__ const float g_y = 0.587f;
__device__ const float b_y = 0.114f; 
__device__ const float InfraredSourceValueMaximum = static_cast<float>(USHRT_MAX);
__device__ const float InfraredSceneValueAverage = 0.08f;
__device__ const float InfraredSceneStandardDeviations = 3.0f;
__device__ const float InfraredOutputValueMinimum = 0.01f;
__device__ const float InfraredOutputValueMaximum = 1.0f;


__global__ void InfaredBrightnessProcessing(uchar1* color,
	float* gamathreshold,
	float threshold )
{
	auto bId = max(blockIdx.x, 1);
	auto i = max(blockDim.x, 1) * bId + threadIdx.x;
	
	if (i < 512 * 424)
	{
		float y = r_y * color[i * 3 + 0].x + 
			g_y * color[i * 3 + 1].x +
			b_y * color[i * 3 + 2].x;

		*gamathreshold += y;
		
	}
}

__global__ void UpdateInfaredTexOnDevice(uchar1* infaredTexBuffer,
	ushort1* infaredBuffer)
{
	auto bId = max(blockIdx.x, 1);
	auto i = max(blockDim.x, 1) * bId + threadIdx.x;

	if (i < 512 * 424)
	{
		auto intensityRatio = (float)(infaredBuffer[i].x) / InfraredSourceValueMaximum;
		intensityRatio /= InfraredSceneValueAverage * InfraredSceneStandardDeviations;

		intensityRatio = min(InfraredOutputValueMaximum, intensityRatio);

		intensityRatio = max(InfraredOutputValueMinimum, intensityRatio);

		infaredTexBuffer[i * 3 + 0].x = (unsigned char)(intensityRatio * 255.0f);
		infaredTexBuffer[i * 3 + 1].x = (unsigned char)(intensityRatio * 255.0f);
		infaredTexBuffer[i * 3 + 2].x = (unsigned char)(intensityRatio * 255.0f);
	}
}

__global__ void UpdateDepthColorOnDevice(uchar1* destDepthColor, 
	uchar4* color,
	float2* depthColorSpacePoint,
	float colorWidth,
	float colorHeight)
{
	auto bId = max(blockIdx.x, 1);
	auto i = max(blockDim.x, 1) * bId + threadIdx.x;

	if (i < 512 * 424)
	{
		auto p = depthColorSpacePoint[i];
		auto x = std::floorf(p.x + .5f);
		auto y = std::floorf(p.y + .5f);

		if (x >= 0 && x < colorWidth && y >= 0 && y < colorHeight)
		{
			auto colorIndex = static_cast<int>(x + y * colorWidth);

			destDepthColor[i * 3 + 0].x = color[colorIndex].z;
			destDepthColor[i * 3 + 1].x = color[colorIndex].y;
			destDepthColor[i * 3 + 2].x = color[colorIndex].x;
		}
		else
		{
			destDepthColor[i * 3 + 0].x = 0;
			destDepthColor[i * 3 + 1].x = 0;
			destDepthColor[i * 3 + 2].x = 0;
		}
	}
	
}

void UpdateDepthColor(uchar1* destDepthColor, 
	uchar4* color,
	float2* depthColorSpacePoint,
	int colorWidth,
	int colorHeight)
{
	UpdateDepthColorOnDevice << <512, 928 >> >(destDepthColor,
		color,
		depthColorSpacePoint,
		colorWidth,
		colorHeight);
	cudaThreadSynchronize();
}

void UpdateInfaredTex(uchar1* texSource,
	ushort1* infaredBuffer)
{
	UpdateInfaredTexOnDevice << <512, 928 >> >(texSource, infaredBuffer);
	cudaThreadSynchronize();
}


void UpdateBrightnessDetection(uchar1* color,
	float* gamathreshold,
	float threshold = 3.0f)
{
	InfaredBrightnessProcessing << <512, 928 >> >(color, gamathreshold, threshold);
	cudaThreadSynchronize();
}