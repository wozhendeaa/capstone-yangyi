#pragma once
#include <KinectModule\KinectSensor.h>
#include <Kinect.Face.h>
#include <ExportHeader.h>
class KinectPlayer;
class ENGINE_SHARED KinectBodyFrame
{	
	
	Joint joints[JointType_Count];
	vec4 jointVirtualPosition[JointType_Count];
	HandState leftHandState;
	HandState rightHandState;
	IBody* bodies[BODY_COUNT];
	IFaceFrameSource* faceSources[BODY_COUNT];
	IFaceFrameReader* faceReaders[BODY_COUNT];
	vec4 floorClip;
	vec4 faceOrientation;
	bool isBodyTracked;
	bool enableFaceTracking = false;

	ICoordinateMapper* coordinateMapper;	

	vec4 BodyToDepthScreen(const CameraSpacePoint& bodyPoint,
						   const int& width, 
						   const int& height);

	vec4 BodyToColorScreen(const CameraSpacePoint& bodyPoint,
						   const int& width, 
						   const int& height);


	vec2 DepthToColor(const DepthSpacePoint& bodyPoint,
		UINT16 depth);


	vec2 getUVInRawDepthData(const CameraSpacePoint& bodyPoint);

	vec3 castRay(const float& x, 
				 const float& y,
				 const mat4& projMat,
				 const mat4& viewMat);	

friend class KinectPlayer;
	int windowWidth;
	int windowHeight;
public:
	explicit KinectBodyFrame(const float screenWidth = 1880,
							 const float screenHeight = 1019);
	~KinectBodyFrame(void);

	void SetScreenSize(const float& width,
					   const float& height);

	void SetFaceTrackingEnabled(bool enable);

	vec4 getFloorUpVector() const;
	vec4 GetFaceOrientation() const;

	void updateBody(ushort* rawDepthData);

	bool IsBodyTracked();

	IBody* getBodyData() const;

	Joint* getJointsData() const;

	JointOrientation* orientations = nullptr;

	vec4* getVirtualJointsPostitons() const;


	HandState getRightHandState();

	HandState getLeftHandState();

	///get the game world position 
	///of the specified joint 	
	vec3 getJointVirtualWorldPos(const JointType& jType,
								 const mat4& projMat,
								 const mat4& viewMat,
								 const vec3 offset = vec3(0.0f));

	bool HitDepthAt(const vec3& ray,
		uint tolerance = 1);

	void shutDown();
};

