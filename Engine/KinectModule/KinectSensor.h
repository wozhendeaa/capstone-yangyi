#pragma once
#include <ExportHeader.h>
#include <Kinect.h>
#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <Renderer.h>
#include <MemoryDebug.h>

using glm::vec3;
using glm::vec4;
using glm::vec2;
using glm::mat3;
using glm::mat4;

class ENGINE_SHARED KinectSensor
{
	static KinectSensor* sensorClass;
	static bool instanceFlag;
	KinectSensor(void);
	~KinectSensor(void);
	HRESULT initializeSensor();

	IKinectSensor* kinectSensor = nullptr;

	IColorFrameReader* colorFrameReader = nullptr;

	IDepthFrameReader* depthFrameReader = nullptr;

	IInfraredFrameReader* infraredFrameReader = nullptr;

	IBodyFrameReader* bodyFrameReader = nullptr;

	IBodyIndexFrameReader* bodyIndexReader = nullptr;

public:
	static KinectSensor* GetSensor();
	
	HRESULT initColorReader();
	HRESULT initDepthReader();
	HRESULT initBodyFrameReader();
	HRESULT initBodyIndexReader();
	HRESULT initInfaredReader();
	
	IColorFrameReader*     getColorFrameReader();
	IDepthFrameReader*     getDepthFrameReader();
	IBodyFrameReader*      getBodyFrameReader();
	IBodyIndexFrameReader* getBodyIndexReader();
	IInfraredFrameReader*  getInfaredReader();

	IKinectSensor* GetKinectDevice();

	void shutDown();
};

// Safe release for interfaces
template<class Interface>
inline void SafeRelease(Interface *& pInterfaceToRelease)
{
    if (pInterfaceToRelease != nullptr)
    {
        pInterfaceToRelease->Release();
        pInterfaceToRelease = nullptr;
    }
}
