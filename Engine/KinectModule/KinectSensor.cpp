#include "KinectSensor.h"
#include <MemoryDebug.h>
KinectSensor* KinectSensor::sensorClass = nullptr;
bool KinectSensor::instanceFlag = false;

KinectSensor::KinectSensor(void)
{
	kinectSensor        = nullptr;
	colorFrameReader    = nullptr;
	depthFrameReader    = nullptr;
	infraredFrameReader = nullptr;
	bodyFrameReader     = nullptr;

}

HRESULT KinectSensor::initializeSensor() {
	HRESULT hr = S_FALSE;

	if(kinectSensor == nullptr)
	{
		hr = GetDefaultKinectSensor(&kinectSensor);
		
		if(SUCCEEDED(hr))
		{
			hr = kinectSensor->Open();
		}

		if(SUCCEEDED(hr))
		{
			std::cout << "kinect connected" 
				<< std::endl;
		} 
		else 
		{
			std::cout << "device not found" 
				<< std::endl;
		}
	}
	else 
	{
		hr = S_OK;
	}

	return hr;
}

HRESULT KinectSensor::initColorReader() {
	auto hr = initializeSensor();

	if(SUCCEEDED(hr))
	{
		IColorFrameSource* colorFrame = nullptr;
		
		if(SUCCEEDED(hr)) 
		{
			hr = kinectSensor->get_ColorFrameSource(&colorFrame);
		}

		if(SUCCEEDED(hr)) 
		{
			hr = colorFrame->OpenReader(&colorFrameReader);
		}
	
		SafeRelease(colorFrame);
		std::cout << "color initialized" 
			<< std::endl;
	
	} 
	else
	{
		std::cout << "color initialization failed" 
			<< std::endl;
	}
	return hr;
}

HRESULT KinectSensor::initDepthReader() {
	auto hr = initializeSensor();

	if(SUCCEEDED(hr))
	{
		IDepthFrameSource* depthFrame = nullptr;
		
		if(SUCCEEDED(hr)) 
		{
			hr = kinectSensor->get_DepthFrameSource(&depthFrame);
		}

		if(SUCCEEDED(hr)) 
		{
			hr = depthFrame->OpenReader(&depthFrameReader);
		}

		SafeRelease(depthFrame);
		std::cout << "depth initialized" 
			<< std::endl;
	
	} 
	else
	{
		std::cout << "depth initialization failed" 
			<< std::endl;
	}
	return hr;
}

HRESULT KinectSensor::initInfaredReader() {
	auto hr = initializeSensor();

	if(SUCCEEDED(hr))
	{
		IInfraredFrameSource* infaredFrame = nullptr;
		
		if(SUCCEEDED(hr)) 
		{
			hr = kinectSensor->get_InfraredFrameSource(&infaredFrame);
		}

		if(SUCCEEDED(hr)) 
		{
			hr = infaredFrame->OpenReader(&infraredFrameReader);
		}

		SafeRelease(infaredFrame);
		std::cout << "infared initialized" 
			<< std::endl;
	
	} 
	else
	{
		std::cout << "infared initialization failed" 
			<< std::endl;
	}
	return hr;
}

HRESULT KinectSensor::initBodyFrameReader() {
	auto hr = initializeSensor();

	if(SUCCEEDED(hr))
	{
		IBodyFrameSource* bodyFrame = nullptr;
		
		if(SUCCEEDED(hr)) 
		{
			hr = kinectSensor->get_BodyFrameSource(&bodyFrame);
		}

		if(SUCCEEDED(hr)) 
		{
			hr = bodyFrame->OpenReader(&bodyFrameReader);
		}

		SafeRelease(bodyFrame);
		std::cout << "body initialized" 
			<< std::endl;
	
	} 
	else
	{
		std::cout << "body initialization failed" 
			<< std::endl;
	}
	return hr;
}

HRESULT KinectSensor::initBodyIndexReader()
{
	auto hr = initializeSensor();

	if (SUCCEEDED(hr))
	{
		IBodyIndexFrameSource* bodyIndex = nullptr;

		if (SUCCEEDED(hr))
		{
			hr = kinectSensor->get_BodyIndexFrameSource(&bodyIndex);
		}

		if (SUCCEEDED(hr))
		{
			hr = bodyIndex->OpenReader(&bodyIndexReader);
		}

		SafeRelease(bodyIndex);
		std::cout << "body index initialized"
			<< std::endl;

	}
	else
	{
		std::cout << "body index initialization failed"
			<< std::endl;
	}
	return hr;

}



KinectSensor* KinectSensor::GetSensor()
{

	if(!instanceFlag)
	{
		sensorClass = new KinectSensor;
		instanceFlag = true;
	}

	return sensorClass;
}

IColorFrameReader* KinectSensor::getColorFrameReader()
{
	if(colorFrameReader == nullptr) initColorReader();

	return colorFrameReader;
}

IDepthFrameReader* KinectSensor::getDepthFrameReader()
{
	if(depthFrameReader == nullptr) initDepthReader();

	return depthFrameReader;
}

IBodyFrameReader* KinectSensor::getBodyFrameReader()
{
	if(bodyFrameReader == nullptr) initBodyFrameReader();

	return bodyFrameReader;
}

IBodyIndexFrameReader* KinectSensor::getBodyIndexReader()
{
	if (bodyIndexReader == nullptr) initBodyIndexReader();

	return bodyIndexReader;
}

IInfraredFrameReader* KinectSensor::getInfaredReader()
{
	if (infraredFrameReader == nullptr) initInfaredReader();

	return infraredFrameReader;
}


IKinectSensor* KinectSensor::GetKinectDevice()
{
	return kinectSensor;
}

void KinectSensor::shutDown() {

	if(instanceFlag)
	{
		instanceFlag = false;
		SafeRelease(sensorClass->kinectSensor);
		SafeRelease(sensorClass->colorFrameReader);
		SafeRelease(sensorClass->depthFrameReader);
		SafeRelease(sensorClass->infraredFrameReader);
		SafeRelease(sensorClass->bodyFrameReader);

		delete sensorClass;
		sensorClass = nullptr;
	}

}


KinectSensor::~KinectSensor(void)
{
	shutDown();
}
