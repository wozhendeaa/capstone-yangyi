#include "KinectBodyFrame.h"
#include <KinectModule\KinectColor.h>
#include <KinectModule\KinectDepth.h>
#include <MemoryDebug.h>
//#include <Kinect.Face.h>

static const DWORD c_FaceFrameFeatures = 
FaceFrameFeatures::FaceFrameFeatures_RotationOrientation;


KinectBodyFrame::KinectBodyFrame(const float screenWidth,
								 const float screenHeight) 
								 : windowWidth(screenWidth), 
								   windowHeight(screenHeight)
{
	coordinateMapper = nullptr;

	for (int i = 0; i < BODY_COUNT; i++)
	{
		bodies[i] = nullptr;
		faceSources[i] = nullptr;
	}


	isBodyTracked = false;
}

void KinectBodyFrame::updateBody(ushort* rawDepthData)
{
	rawDepthData;
	auto bodyFrameReader = KinectSensor::GetSensor()->getBodyFrameReader();

	if(bodyFrameReader != nullptr)
	{
		IBodyFrame* bf = nullptr;
		auto hr = bodyFrameReader->AcquireLatestFrame(&bf);
		Vector4 floorPlane;

		if(SUCCEEDED(hr))
		{
			hr = bf->GetAndRefreshBodyData(_countof(bodies), bodies);
			bf->get_FloorClipPlane(&floorPlane);

			floorClip.x = floorPlane.x;
			floorClip.y = floorPlane.y;
			floorClip.z = floorPlane.z;
			floorClip.w = floorPlane.w;
		}

		SafeRelease(bf);		
	}


	//for (size_t i = 0; i < BODY_COUNT; i++)
	//{
	//	auto kinectSensor = KinectSensor::GetSensor()->GetKinectDevice();
	//	auto hr = CreateFaceFrameSource(kinectSensor,
	//		i,
	//		c_FaceFrameFeatures,
	//		&faceSources[i]);

	//	if (SUCCEEDED(hr))
	//	{
	//		hr = faceSources[i]->OpenReader(&faceReaders[i]);
	//	}
	//}


	//update body data
	for (int i = 0; i < _countof(bodies); i++)
	{
		auto body = bodies[i];

		if(body != nullptr)
		{
			BOOLEAN isTracked = false;
			auto hr = body->get_IsTracked(&isTracked);
			isBodyTracked = true;
			if(SUCCEEDED(hr) && isTracked)
			{
				leftHandState = HandState_Unknown;
				rightHandState = HandState_Unknown;

				body->get_HandLeftState(&leftHandState);
				body->get_HandRightState(&rightHandState);

				hr = body->GetJoints(_countof(joints), joints);
				if(SUCCEEDED(hr))
				{
					for (int j = 0; j < _countof(joints); j++)
					{
						jointVirtualPosition[j] = BodyToDepthScreen(joints[j].Position, 
							windowWidth, windowHeight);
						
					}
				}				
				//if (SUCCEEDED(hr))
				//{
				//	//update face
				//	IFaceFrame* faceFrame = nullptr;
				//	hr = faceReaders[i]->AcquireLatestFrame(&faceFrame);

				//	BOOLEAN bFaceTracked = false;
				//	if (SUCCEEDED(hr) && nullptr != faceFrame)
				//	{
				//		hr = faceFrame->get_IsTrackingIdValid(&bFaceTracked);
				//	}

				//	IFaceFrameResult* faceFrameResult = nullptr;
				//	if (SUCCEEDED(hr) && bFaceTracked)
				//	{
				//		hr = faceFrame->get_FaceFrameResult(&faceFrameResult);
				//	}

				//	if (SUCCEEDED(hr) && faceFrameResult != nullptr)
				//	{
				//		Vector4 tempFaceOrientation;
				//		faceFrameResult->get_FaceRotationQuaternion(&tempFaceOrientation);
				//		faceOrientation.x = tempFaceOrientation.x;
				//		faceOrientation.y = tempFaceOrientation.y;
				//		faceOrientation.z = tempFaceOrientation.z;
				//		faceOrientation.w = tempFaceOrientation.w;
				//	}
				//}
			}
		}
	}
		
	for (int i = 0; i < _countof(bodies); i++)
	{
		SafeRelease(bodies[i]);
	}

}

bool KinectBodyFrame::IsBodyTracked()
{
	return isBodyTracked;
}

vec4 KinectBodyFrame::getFloorUpVector() const
{
	return floorClip;
}

vec4 KinectBodyFrame::GetFaceOrientation() const
{
	return faceOrientation;
}


vec2 KinectBodyFrame::DepthToColor(const DepthSpacePoint& bodyPoint,
								   UINT16 depth)
{
	ColorSpacePoint colorPoint;
	
	if(coordinateMapper == nullptr) 
	{
		KinectSensor::GetSensor()->
			    GetKinectDevice()->
			    get_CoordinateMapper(&coordinateMapper);
	}

	coordinateMapper->MapDepthPointToColorSpace(bodyPoint, depth ,&colorPoint);
	return vec2(colorPoint.X, colorPoint.Y);
}


IBody* KinectBodyFrame::getBodyData() const
{
	return bodies[0];
}


Joint* KinectBodyFrame::getJointsData() const
{
	return const_cast<Joint*>(joints);
}

vec4* KinectBodyFrame::getVirtualJointsPostitons() const
{
	return const_cast<vec4*>(jointVirtualPosition);
}



vec4 KinectBodyFrame::BodyToDepthScreen(const CameraSpacePoint& bodyPoint,
								   const int& width, 
								   const int& height)
{
	DepthSpacePoint depthPoint;
	if(coordinateMapper == nullptr) 
	{
		KinectSensor::GetSensor()->
			    GetKinectDevice()->
			    get_CoordinateMapper(&coordinateMapper);
	}

	coordinateMapper->MapCameraPointToDepthSpace(bodyPoint, &depthPoint);
	auto x = static_cast<float>(depthPoint.X * width) / KinectDepth::depthWidth;
	auto y = static_cast<float>(depthPoint.Y * height) / KinectDepth::depthHeight;
	
	return vec4(x, y, 5.0, 1.0f);
}



vec2 KinectBodyFrame::getUVInRawDepthData(const CameraSpacePoint& bodyPoint)
{
	DepthSpacePoint depthPoint = {0};
	if(coordinateMapper == nullptr) 
	{
		KinectSensor::GetSensor()->
			    GetKinectDevice()->
			    get_CoordinateMapper(&coordinateMapper);
	}

	auto hr = coordinateMapper->MapCameraPointToDepthSpace(bodyPoint, &depthPoint);

	if(SUCCEEDED(hr))
	{

	}
	auto x = glm::clamp(static_cast<int>(depthPoint.X), 0, KinectDepth::depthWidth);
	auto y = glm::clamp(static_cast<int>(depthPoint.Y), 0, KinectDepth::depthHeight);

	return vec2(x, y);
}

vec4 KinectBodyFrame::BodyToColorScreen(const CameraSpacePoint& bodyPoint,
								   const int& width, 
								   const int& height)
{
	ColorSpacePoint colorPoint;
	if(coordinateMapper == nullptr) 
	{
		KinectSensor::GetSensor()->
			    GetKinectDevice()->
			    get_CoordinateMapper(&coordinateMapper);
	}

	coordinateMapper->MapCameraPointToColorSpace(bodyPoint, &colorPoint);
	auto x = static_cast<float>(colorPoint.X * width) / KinectColor::colorWidth;
	auto y = static_cast<float>(colorPoint.Y * height) / KinectColor::colorHeight;
	
	return vec4(x, y, 0, 1);
}

vec3 KinectBodyFrame::castRay(const float& x, 
							  const float& y,
							  const mat4& projMat,
							  const mat4& viewMat)
{
	vec2 screen_center(windowWidth / 2.0f, windowHeight / 2.0f);
	vec2 screen_clicked(x, y);

	vec4 result = vec4(screen_clicked - screen_center, -1.0f, 1.0f);
	result.x = result.x * 2 / windowWidth;
	result.y = -result.y * 2 / windowHeight;
	
	auto ray_eye = glm::inverse(projMat) * result;
	ray_eye = vec4(ray_eye.x, ray_eye.y, -1.0f, 0.0f);

	auto l = (vec3(glm::inverse(viewMat) * ray_eye));

	return l;
}

HandState KinectBodyFrame::getRightHandState()
{
	return leftHandState;
}

HandState KinectBodyFrame::getLeftHandState()
{
	return rightHandState;
}

///get the game world position 
///of the specified joint 	
vec3 KinectBodyFrame::getJointVirtualWorldPos(const JointType& jType,
											  const mat4& projMat,
											  const mat4& viewMat,
											  const vec3 offset)
{
	auto jointPos = joints[jType].Position;
	jointPos.Y += offset.y;
	auto colorPos = BodyToColorScreen(jointPos, windowWidth, windowHeight);
	auto worldPos = castRay(colorPos.x, 
							colorPos.y,
							projMat,
							viewMat);

	return worldPos;
}

void KinectBodyFrame::SetScreenSize(const float& width,
									const float& height)
{
	windowWidth = width;
	windowHeight = height;
}

void KinectBodyFrame::SetFaceTrackingEnabled(bool enable)
{
	enableFaceTracking = enable;
}


bool KinectBodyFrame::HitDepthAt(const vec3& ray,
							     uint tolerance)
{
	ray;
	tolerance;



	return false;
}


void KinectBodyFrame::shutDown()
{	
	for (int i = 0; i < BODY_COUNT; i++)
	{
		SafeRelease(bodies[i]);
	}	

	//if(jointGeometry != nullptr)
	//{
	//	delete jointGeometry;
	//	jointGeometry = nullptr;
	//}
	SafeRelease(coordinateMapper);

}

KinectBodyFrame::~KinectBodyFrame(void)
{
	shutDown();
}
