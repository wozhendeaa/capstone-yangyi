#pragma once
#include <ExportHeader.h>
#include <KinectModule\KinectGestureDetectorBase.h>
enum ENGINE_SHARED SwipeDirection
{
	UP, 
	DOWN,
	LEFT,
	RIGHT,
};

class ENGINE_SHARED KinectSwipeDetector
	: public KinectGestureDetectorBase
{
	void DetectRightSwipe();
	void DetectLeftSwipe();
	void DetectUpSwipe();
	void DetectDownSwipe();

	float timeConstraint;
	float verticalDifferenceTolerance = 0.03f;
	float horizontalDifferenceTolerance = 0.03f;
	float depthDifferenceTolerance = 0.03f;	
public:

	KinectSwipeDetector(float time = 1.0f);
	~KinectSwipeDetector(void);

	void Destroy();

	void DetectGestures(float dt) override;

};

