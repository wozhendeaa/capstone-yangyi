#pragma once
#include <ExportHeader.h>
#include <KinectModule\KinectSensor.h>

struct uchar1;
struct ushort1;

class ENGINE_SHARED KinectInfared
{
	UINT16* infaredBuffer = nullptr;
	byte* infaredTexBuffer = nullptr;
	UINT bufferSize = 0;
	INT width = 0;
	INT height = 0;
	ICoordinateMapper* coodinateMapper = nullptr;	

	ushort1* d_infaredBuffer = nullptr;
	uchar1* d_infaredTexBuffer = nullptr;
public:
	KinectInfared();
	~KinectInfared();

	UINT16* GetInfaredBuffer() const;
	byte* GetInfaredTexBuffer();
	
	void Update();
};

