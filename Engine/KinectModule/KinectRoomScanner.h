#pragma once
#include <ExportHeader.h>
#include <Physics\PxPhysicsWorld.h>
#include <GL\glew.h>
#include <Renderer.h>

class KinectBodyIndex;
class KinectRoomCollision;
class KinectDepth;
class KinectBodyFrame;

class ENGINE_SHARED KinectRoomScanner
{
	typedef unsigned int uint;

	// unit is in meters
	float m_subdivision;

	float m_updateInterval;

	float m_roomHalfSize;

	KinectRoomCollision* roomCollision;

	friend class KinectSources;
	void InitVoxelization(physx::PxPhysicsWorld* world);

public:

	explicit KinectRoomScanner(float subdivision = .2f,
		float roomHalfSize = 5.0f);

	~KinectRoomScanner(void);

	void shutDown();

	void UpdateRoomScanning(KinectBodyFrame* head,
							KinectDepth* depth,
							KinectBodyIndex* bodyIndex,
						    bool treatPlayerAsCollision);

	void DeugDraw(BatEngine::Renderer* r, 
		GLenum drawMode = GL_LINES);

};

