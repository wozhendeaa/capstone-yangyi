#pragma once
#include <RenderComponents\Renderer.h>
using BatEngine::Renderer;
using BatEngine::Renderable;
class ENGINE_SHARED ImageRenderer
{
	Renderer* renderer;


public:
	uint sourceWidth;
	uint sourceHeight;
	uint sourceStride;

	ImageRenderer(Renderer* r, 
				  uint width, 
				  uint height, 
				  uint stride);

	void updateTexture(const uint& tex_unit,
		const uint& tex_id,
		const uint& width,
		const uint& height,
		void* data,
		GLenum format = GL_BGRA,
		GLenum type = GL_UNSIGNED_BYTE);

	void updateARDeapthMap(const TextureInfo* t,
		void* data,
		GLuint program_id = 3);
	//
	//r: the game object to be drawn on
	// type: image type, it can be in bytes, floats
	void DrawImage(Renderable* r, 
				   void* data,
				   GLenum format = GL_BGRA,
				   GLenum type = GL_UNSIGNED_BYTE);


	~ImageRenderer(void);
};

