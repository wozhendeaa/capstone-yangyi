#include "KinectDepth.h"
#include <MemoryDebug.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <helper_cuda.h>

KinectDepth::KinectDepth(void)
{
	texBuffer = nullptr;
	dBuffer = new UINT16[depthWidth * depthHeight];
	isReady = false;
	depthPoints = new DepthSpacePoint[KinectColor::colorWidth * KinectColor::colorHeight];
	colorDepthBuffer = new float[KinectColor::colorWidth * KinectColor::colorHeight * 3];
	colorPoints = new ColorSpacePoint[depthWidth * depthHeight];
	smoothedData = new ushort[depthWidth * depthHeight];
	newFrameDepth = new ushort[depthWidth * depthHeight];
	flatDepth = new ushort[depthWidth * depthHeight];
	texBuffer = new float[depthWidth * depthHeight * 3];
	normalBuffer = new float[depthWidth * depthHeight * 3];
	cameraSpacePoints = new CameraSpacePoint[depthWidth * depthHeight];
	auto size = sizeof(float3) * depthHeight * depthWidth;
	
	cudaMalloc((void**)&d_depthBuffer, size);
	cudaMalloc((void**)&d_normalBuffer, size);

	auto depthBufferSize = sizeof(ushort1) * depthHeight * depthWidth;
	cudaMalloc((void**)&d_prevDepth, depthBufferSize);
	cudaMalloc((void**)&d_currentDepth, depthBufferSize);
	cudaMalloc((void**)&d_smoothDepth, depthBufferSize);
	cudaMalloc((void**)&d_depthTex, size);

	coordinateMapper = nullptr;
}

void callCudaUpdateDepth(ushort1* destDepth,
	ushort1* prevDepth,
	int depthWidth, 
	int depthHeight,
	ushort1 threshold);

void KinectDepth::updateDepth()
{
	auto depthFrameReader = KinectSensor::GetSensor()->getDepthFrameReader();
	isDepthDataFiltered = false;
	if(depthFrameReader != nullptr )
	{
		IDepthFrame* depthFrame = nullptr;
		long long dt = 0;
		auto hr = depthFrameReader->AcquireLatestFrame(&depthFrame);

		if (coordinateMapper == nullptr)
		{
			KinectSensor::GetSensor()->
				GetKinectDevice()->
				get_CoordinateMapper(&coordinateMapper);
		}

		if(SUCCEEDED(hr))
		{
			IFrameDescription* description = nullptr;
			int width = 0;
			int height = 0;

			hr = depthFrame->get_RelativeTime(&dt);

			if(SUCCEEDED(hr))
			{
				hr = depthFrame->get_FrameDescription(&description);
			}
			
			if(SUCCEEDED(hr))
			{
				hr = description->get_Width(&width);
			}
			
			if(SUCCEEDED(hr))
			{
				hr = description->get_Height(&height);
			}

			if(SUCCEEDED(hr))
			{
				hr = depthFrame->get_DepthMinReliableDistance(&minDistance);
			}

			if(SUCCEEDED(hr))
			{
				hr = depthFrame->get_DepthMaxReliableDistance(&maxDistance);
			}

			if(SUCCEEDED(hr))
			{
				hr = depthFrame->get_DepthMinReliableDistance(&minDistance);
			}

			//if(SUCCEEDED(hr))
			//{
			//	hr = depthFrame->AccessUnderlyingBuffer(&bufferSize, &newFrameDepth);
			//}

			if(SUCCEEDED(hr))
			{
				if (coordinateMapper == nullptr)
				{
					KinectSensor::GetSensor()->
						GetKinectDevice()->
						get_CoordinateMapper(&coordinateMapper);
				}

				depthFrame->CopyFrameDataToArray(depthWidth * depthHeight, newFrameDepth);
				auto depthBufferSize = sizeof(ushort1) * depthHeight * depthWidth;
				cudaMemcpy(d_currentDepth, newFrameDepth, depthBufferSize, cudaMemcpyHostToDevice);
				cudaMemcpy(d_prevDepth, dBuffer, depthBufferSize, cudaMemcpyHostToDevice);

				ushort1 threshold;
				threshold.x = 20;
				callCudaUpdateDepth(d_prevDepth, d_currentDepth, depthWidth, depthHeight, threshold);
				cudaMemcpy(dBuffer, d_prevDepth, depthBufferSize, cudaMemcpyDeviceToHost);
				isReady = true;
			}

			SafeRelease(description);

		}

		SafeRelease(depthFrame);
	}
}

 

extern void UpdateDepthTex(float3* destDepthTex,
	ushort1* depth,
	float neg_infinity,
	int depthWidth,
	int depthHeight);

float* KinectDepth::getTexDepthData()
{
	if (dBuffer != nullptr)
	{
		UpdateDepthTex(d_depthTex, d_currentDepth, neg_infinity, KinectDepth::depthWidth, KinectDepth::depthHeight);
		cudaMemcpy(texBuffer, d_depthTex, sizeof(float3) * depthWidth * depthHeight, cudaMemcpyDeviceToHost);
	}

	return (texBuffer);
}

extern void callSmoothDepth(ushort1* destDepth, int depthWidth, int depthHeight);

ushort* KinectDepth::getFilteredData()
{

	//for (size_t i = 0; i < depthWidth * depthHeight && dBuffer != nullptr; i++)
	//{
	//	auto depth = dBuffer[i];
	//	depth = depth >= minDistance && depth <= maxDistance ? depth : 0.0f;
	//
	//	if (depth == .0f)
	//	{
	//		ushort frequncyMap[24][2];

	//		auto x = i % depthWidth;
	//		auto y = (i - x) / depthWidth;

	//		int innerNonZeroDepthCount = 0;
	//		int outterNonZeroDepthCount = 0;

	//		// check the filter matrix
	//		for (int yOffset = -2; yOffset < 3; ++yOffset)
	//		{
	//			for (int xOffset = -2; xOffset < 3; ++xOffset)
	//			{
	//				if (xOffset != 0 && yOffset != 0)
	//				{
	//					auto x2 = x + xOffset;
	//					auto y2 = y + yOffset;
	//					auto index = static_cast<int>(x2 + y2 * depthWidth);

	//					bool isInBound = x2 >= 0 && x2 < depthWidth;
	//					y2 >= 0 && y2 < depthHeight;
	//				
	//					if (isInBound)
	//					{
	//						auto isValid = dBuffer[index] >= minDistance &&
	//							dBuffer[index] <= maxDistance;

	//						if (isValid)
	//						{
	//							for (int i = 0; i < 24; i++)
	//							{
	//								auto mapValue = frequncyMap[i][0];
	//								auto mapDepth = dBuffer[index];
	//								if (mapValue == mapDepth)
	//								{
	//									frequncyMap[i][1]++;
	//									break;
	//								}
	//								else if (mapValue == 52428)
	//								{
	//									frequncyMap[i][0] = dBuffer[index];
	//									frequncyMap[i][1]++;
	//									break;
	//								}

	//							}
	//						}

	//						if (y2 != 2 && y2 != -2 && x2 != 2 && x2 != -2)
	//							innerNonZeroDepthCount++;
	//						else
	//							outterNonZeroDepthCount++;

	//					}
	//				}
	//			}
	//		}
	//	if (innerNonZeroDepthCount >= 0 || outterNonZeroDepthCount >= 0)
	//	{
	//		short frequency = 0;
	//		short depth = 0;
	//		for (int j = 0; j < 24 && frequncyMap[j][0] != 0; j++)
	//		{
	//			if (frequncyMap[j][1] > frequency)
	//			{
	//				depth = frequncyMap[j][0];
	//				frequency = frequncyMap[j][1];
	//			}
	//		}

	//		smoothedData[i] = depth;
	//	}
	//	else
	//	{
	//		smoothedData[i] = dBuffer[i];
	//	}

	//	}
	//}
	//if (depthFrames.size() > maxWeight)
	//{
	//	for (size_t i = 0; i < maxWeight; i++)
	//	{
	//		auto oldestFrame = depthFrames.front();
	//		for (size_t i = 0; i < depthWidth * depthHeight; i++)
	//		{
	//			smoothedData[i] += oldestFrame[i] ;
	//		}
	//		depthFrames.pop();
//	//		delete[] oldestFrame;
//	//	}
//	//}
//memcpy(prevDepth, dBuffer, sizeof(UINT16) * depthHeight * depthWidth);
//
//
//	for (size_t i = 0; i < depthWidth * depthHeight; i++)
//	{
//		smoothedData[i] += prevDepth[i] ;
//		smoothedData[i] /= 2;
//	}
//
//
	auto depthBufferSize = sizeof(ushort1) * depthHeight * depthWidth;
	cudaMemcpy(d_smoothDepth, smoothedData, depthBufferSize, cudaMemcpyHostToDevice);
	callSmoothDepth(d_smoothDepth, depthWidth, depthHeight);
	cudaThreadSynchronize();
	cudaMemcpy(smoothedData, d_smoothDepth, depthBufferSize, cudaMemcpyDeviceToHost);

	return smoothedData;
}

//kernel function
extern void callCalcNormal(float3* depth, float3* normal, int depthWidth, int depthHeight);

float* KinectDepth::GetNormalData()
{
	if (dBuffer != nullptr && coordinateMapper != nullptr)
	{
		coordinateMapper->MapDepthFrameToCameraSpace(
			depthWidth * depthHeight,
			(UINT16*)newFrameDepth,
			depthWidth * depthHeight,
			cameraSpacePoints);

		auto size = sizeof(float3) * depthHeight * depthWidth;
		cudaMemcpy(d_depthBuffer, (void**)&cameraSpacePoints[0], size, cudaMemcpyHostToDevice);
		
		callCalcNormal(d_depthBuffer,
			d_normalBuffer,
			depthWidth, depthHeight);
	
		//cudaThreadSynchronize();

		cudaMemcpy(normalBuffer, d_normalBuffer, size, cudaMemcpyDeviceToHost);
		auto error = cudaGetLastError();

		if (error != cudaSuccess)
		{
			fprintf(stderr, "GPUassert: %s\n", cudaGetErrorString(error));
		}
	}

	return normalBuffer;
}

float* KinectDepth::getColorDepthData()
{
	if (coordinateMapper != nullptr)
	{
		coordinateMapper->MapColorFrameToDepthSpace(
			depthWidth * depthHeight,
			(UINT16*)dBuffer,
			KinectColor::colorWidth * KinectColor::colorHeight,
			depthPoints);
	}

	if (dBuffer != nullptr)
	{
		for (size_t i = 0; i < KinectColor::colorWidth * KinectColor::colorHeight; i++)
		{
			auto p = depthPoints[i];
			auto x = std::floorf(p.X );
			auto y = std::floorf(p.Y );

			auto depthIndex = static_cast<int>(x + y * depthWidth);
			//if (x >= 0 && x < depthWidth && y >= 0 && y < depthHeight)
			{ 
				colorDepthBuffer[i * 3 + 0] = static_cast<float>(newFrameDepth[depthIndex]);
				colorDepthBuffer[i * 3 + 1] =  static_cast<float>(newFrameDepth[depthIndex]);
				colorDepthBuffer[i * 3 + 2] =  static_cast<float>(newFrameDepth[depthIndex]);
			}
		}
	}

	return colorDepthBuffer;
}


ushort* KinectDepth::getRawDepthData()
{
	//auto depthBufferSize = sizeof(ushort1) * depthHeight * depthWidth;
	//cudaMemcpy(d_smoothDepth, dBuffer, depthBufferSize, cudaMemcpyHostToDevice);
	//callSmoothDepth(d_smoothDepth, depthWidth, depthHeight);
	//cudaThreadSynchronize();
	//cudaMemcpy(dBuffer, d_smoothDepth, depthBufferSize, cudaMemcpyDeviceToHost);
	return (dBuffer);
}


	
CameraSpacePoint* KinectDepth::getRaysOfDepths() 
{

	if (coordinateMapper != nullptr)
	{
		coordinateMapper->MapDepthFrameToCameraSpace(
			depthWidth * depthHeight,
			(UINT16*)dBuffer,
			depthWidth * depthHeight,
			cameraSpacePoints);
	}

	return cameraSpacePoints;
}



ushort* KinectDepth::getFlatDepthData()
{
	static bool initialized = false;

	if (!initialized)
	{
		for (size_t i = 0; i < depthWidth * depthHeight; i++)
		{
			flatDepth[i] = 4500;
		}

		initialized = !initialized;
	}


	return flatDepth;
}

void KinectDepth::shutDown()
{
	if(texBuffer != nullptr) 
	{
		delete [] texBuffer;
		texBuffer = nullptr;
	}

	if(dBuffer != nullptr) 
	{
		delete [] dBuffer;
		dBuffer = nullptr;
	}

	if(coordinateMapper != nullptr) 
	{
		 coordinateMapper->Release();
	}

	if(depthPoints != nullptr)
	{
		delete [] depthPoints;
		depthPoints = nullptr;
	}
	
	if (cameraSpacePoints != nullptr)
	{
		delete[] cameraSpacePoints;
		cameraSpacePoints = nullptr;
	}

	if (normalBuffer != nullptr)
	{
		delete[] normalBuffer;
		normalBuffer = nullptr;
	}

	if (newFrameDepth != nullptr)
	{
		delete[] newFrameDepth;
		newFrameDepth = nullptr;
	}

	if (colorPoints != nullptr)
	{
		delete[] colorPoints;
		colorPoints = nullptr;
	}

	if (smoothedData != nullptr)
	{
		delete[] smoothedData;
		smoothedData = nullptr;
	}

	if (flatDepth != nullptr)
	{
		delete[] flatDepth;
		flatDepth = nullptr;
	}

	if (colorDepthBuffer != nullptr)
	{
		delete[] colorDepthBuffer;
		colorDepthBuffer = nullptr;
	}


	cudaFree(d_depthBuffer);
	cudaFree(d_normalBuffer);
	cudaFree(d_currentDepth);
	cudaFree(d_prevDepth);
	cudaFree(d_smoothDepth);
	cudaFree(d_depthTex);

	cudaDeviceReset();
}

KinectDepth::~KinectDepth(void)
{
	shutDown();


}
