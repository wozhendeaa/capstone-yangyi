#pragma once
#include <KinectModule\KinectSensor.h>
#include <KinectModule\KinectColor.h>
#include <queue>
#include <ExportHeader.h>
#include <NuiKinectFusionApi.h>

class float3;
class ushort1;
class ENGINE_SHARED KinectDepth
{
private:

	bool isReady;
	bool isDepthDataFiltered = false;
	float infinity = std::numeric_limits<float>::infinity();
	float neg_infinity = -std::numeric_limits<float>::infinity();
	std::queue<uint*> depthFrames;
	uint maxWeight = 5;

	//device pointer, free with cudaFree
	float3* d_depthBuffer = nullptr;
	float3* d_depthTex = nullptr;
	float3* d_normalBuffer = nullptr;
	ushort1* d_currentDepth = nullptr;
	ushort1* d_prevDepth = nullptr;
	ushort1* d_smoothDepth = nullptr;
public:
	static const int        depthWidth = 512;
    static const int        depthHeight = 424;
	ushort deltaThreshold = 105;
	int innerBandThreshold = 20;
	int outerBandThreshold = 20;
	
	USHORT	minDistance	= 0	;
	USHORT	maxDistance	= 0;
	KinectDepth(void);
	~KinectDepth(void);
	void updateDepth();
	
	float* getTexDepthData();
	float* getColorDepthData();
	ushort* getRawDepthData();
	ushort* getFlatDepthData();
	CameraSpacePoint* getRaysOfDepths();

	void shutDown();

	bool IsReady() const {return isReady;}

	ushort* getFilteredData();


	float* GetNormalData();

private:
	DepthSpacePoint* depthPoints;
	ColorSpacePoint* colorPoints;
	CameraSpacePoint* cameraSpacePoints;
	UINT16* dBuffer;
	ushort* smoothedData;
	ushort* newFrameDepth;//
	ushort* flatDepth;//
	float* texBuffer;
	float* normalBuffer;
	float* colorDepthBuffer;
	ICoordinateMapper* coordinateMapper;
};

