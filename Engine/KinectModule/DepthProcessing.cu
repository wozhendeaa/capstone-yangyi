#pragma once 
#include <cstdlib>
#include <device_launch_parameters.h>
#include <curand_kernel.h>
#include <cmath>
#include <helper_cuda.h>
#include <helper_math.h>

__global__ void CalcNormal(float3* depth, float3* normal, int depthWidth, int depthHeight)
{
	auto bId = max(blockIdx.x, 1);
	auto i = max(blockDim.x, 1) * bId + threadIdx.x;
	int x = i % depthWidth;
	int y = (i - x) / depthWidth;

	if (x >= 1 && (x < depthWidth - 1) && y >= 1 && y < depthHeight - 2)
	{
		float3 center(depth[i]);
		float3 below(depth[i + depthWidth]);
		float3 right(depth[i + 1]);

		float3 belowUnit = below - center;
		float3 rightUnit = right - center;

		float3 crossProduct = cross(belowUnit, rightUnit);
		float3 n = normalize(crossProduct);
		normal[i].x = n.x;
		normal[i].y = n.y;
		normal[i].z = n.z;
	}
}


__global__ void UpdateDepth(ushort1* destDepth,
	ushort1* newDepth,
	int depthWidth,
	int depthHeight,
	ushort1 threshold)
{
	auto bId = max(blockIdx.x, 1);
	auto i = max(blockDim.x, 1) * bId + threadIdx.x;

	if (i < depthHeight * depthWidth)
	{
		auto filteredDepth = destDepth[i].x == 52685 ? 0 : destDepth[i].x;
		auto delta = std::fabsf(newDepth[i].x - filteredDepth);
		if (filteredDepth == 0 || destDepth[i].x == 0)
		{
			int sum = 0;
			for (int row = -3; row < 5; row++)
			{
				for (int col = -3; col < 5; col++)
				{
					if (row != 0 && col != 0)
					{
						int index = i + row * depthWidth + col;
						if (index < depthHeight * depthWidth)
						{
							sum += newDepth[index].x;
						}
					}
				}
			}		

			destDepth[i].x = sum / 49 ;
		}
		else if (delta > 65)
		{ 
			destDepth[i].x = newDepth[i].x;
		}

		if (destDepth[i].x == 0)
		{
			destDepth[i].x = 4500;
		}
	}
}


__global__ void SmoothDepth(ushort1* destDepth,
	int depthWidth,
	int depthHeight)
{
	auto bId = max(blockIdx.x, 1);
	auto i = max(blockDim.x, 1) * bId + threadIdx.x;

	if (i < depthHeight * depthWidth - depthWidth)
	{
		destDepth[i].x = 4500;
	}
	
}

__global__ void UpdateDepthTexOnDevice(float3* destDepthTex,
	ushort1* dBuffer,
	float neg_infinity,
	int depthWidth, 
	int depthHeight)
{
	auto bId = max(blockIdx.x, 1);
	auto i = max(blockDim.x, 1) * bId + threadIdx.x;

	if (i < depthHeight * depthWidth)
	{
		auto depth = dBuffer[i].x;
		depth = depth >= 400 && depth <= 4500 ? depth : 0;
		ushort intensity = static_cast<ushort>(depth);
		auto ndc = (0.01f + 1000.0f) / (1000.0f - 0.01f) +
			(-2 * 0.01f * 1000.0f) / ((1000.0f - 0.01f) * (intensity * 0.001f));

		ndc = ndc == neg_infinity ? 1.0f : ndc;

		destDepthTex[i].x = ndc;// red
		destDepthTex[i].y = ndc;// green 
		destDepthTex[i].z = ndc;// blue
	}
}

void UpdateDepthTex(float3* destDepthTex,
	ushort1* depth,
	float neg_infinity,
	int depthWidth, 
	int depthHeight)
{
	UpdateDepthTexOnDevice << <depthWidth, 928 >> >
		(destDepthTex, depth, neg_infinity,depthWidth, depthHeight);
}

void callSmoothDepth(ushort1* destDepth, int depthWidth, int depthHeight)
{
	SmoothDepth << <depthWidth, 928 >> >(destDepth, depthWidth, depthHeight);
}

void callCudaUpdateDepth(ushort1* destDepth, ushort1* newDepth, int depthWidth, int depthHeight, ushort1 threshold)
{
	UpdateDepth << <depthWidth, 928 >> >(destDepth, newDepth, depthWidth, depthHeight, threshold);
}

void callCalcNormal(float3* depth, float3* normal, int depthWidth, int depthHeight)
{
	CalcNormal << <depthWidth, 928 >> >(depth, normal, depthWidth, depthHeight);

}
