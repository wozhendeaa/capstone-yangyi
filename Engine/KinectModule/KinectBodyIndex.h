#pragma once
#include <KinectModule\KinectSensor.h>
class ENGINE_SHARED KinectBodyIndex
{
	byte* bodyIndexBuffer = nullptr;
	ICoordinateMapper* coodinateMapper = nullptr;
public:

	KinectBodyIndex(void);
	~KinectBodyIndex(void);

	void Update();

	bool IsColorPixelOnBody(const vec2& uv);
	bool IsDepthPixelOnBody(const vec2& uv);

};

