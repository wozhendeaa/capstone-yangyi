#pragma once
#include <ExportHeader.h>
#include <Actors\Components\Physics\RigidBody.h>
#include <RenderComponents\Renderer.h>
#include "Physics\Octree.h"

class PxPhysicsWorld;
using physx::PxRigidDynamic;

class ENGINE_SHARED KinectRoomCollision
{
	const static int numCollision = 800;
	RigidBody voxelCollision[numCollision];
	int usedCollisionCount;
	physx::PxPhysicsWorld* world;
	Octree<RigidBody>* voxelSpace = nullptr;
	void InitStoregeForCollision(physx::PxPhysicsWorld* world);
	float m_subdivision ;
	float m_roomHalfSize;
	bool isReady;
	float floorHeight;

public:

	explicit KinectRoomCollision(
		physx::PxPhysicsWorld * world,
		const float& roomHalfSize,
		const float& subdivision);

	bool IsReady() const {return isReady;}

	void DebugDraw(BatEngine::Renderer* r, 
		GLenum drawMode = GL_LINES);

	void Destroy();

	void ActivateCollisionAt(const vec3& pos);

	void GenerateImplictFloor(const mat4& rot,
		float height);

	//void DeactivateCollisionAt(const vec3& pos);

	void clearAllCollision();


	~KinectRoomCollision(void);
};

