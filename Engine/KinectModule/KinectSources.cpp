#include "KinectSources.h"
#include "KinectSensor.h"
#include <MemoryDebug.h>

KinectSources::KinectSources(void)
{
	start.QuadPart = 0;
	end.QuadPart = 0;
	enableRoomScanning = false;
}


double KinectSources::GetFPS() 
{
	double fps = 0.0;

	LARGE_INTEGER current = {0};

	if (QueryPerformanceCounter(&current))
	{
		if (lastCounter)
		{
			frameSinceUpdate++;
			fps = freq * frameSinceUpdate / double(current.QuadPart - lastCounter);
		}
	}

	frameSinceUpdate = 0;
	lastCounter = current.QuadPart;

	return fps;
}

KinectColor& KinectSources::getColorSource() 
{
	return kinectColor;
}

KinectDepth& KinectSources::getDepthSource() 
{
	return kinectDepth;
}

KinectBodyFrame& KinectSources::getBodySource()
{
	return kinectBodyFrame;
}

KinectRoomScanner& KinectSources::getRoomeScanner()
{
	return kinectRoomScanner;
}

KinectInfared& KinectSources::getInfaredSource()
{
	return kinectInfared;
}


vec4 KinectSources::GetFloorUpVector() const
{
	return kinectBodyFrame.getFloorUpVector();
}

bool KinectSources::IsRoomScannerReadt() const
{
	return enableRoomScanning;
}


void KinectSources::RoomScannerDebugDraw(Renderer* r, 
	GLenum drawMode)
{
	kinectRoomScanner.DeugDraw(r, drawMode);
}


void KinectSources::updateColor() 
{
	kinectColor.updateColor();
}

void KinectSources::updateDepth() 
{
	kinectDepth.updateDepth();
}

void KinectSources::updateBody()
{
	auto data = kinectDepth.getRawDepthData();
	kinectBodyFrame.updateBody(data);
}

bool KinectSources::IsScreenDark(float brightnessThreshold,
	int darkPixelCountThreshold)
{
	return kinectColor.IsScreenDark(brightnessThreshold, darkPixelCountThreshold);
}

void KinectSources::UpdateRoomScanning(bool treatPlayerAsCollision)
{
	if(enableRoomScanning)
	{
		kinectRoomScanner.UpdateRoomScanning(
			&kinectBodyFrame,
			&kinectDepth,
			&kinectBodyIndex,
			treatPlayerAsCollision);
	}
}

byte* KinectSources::GetDepthColorData()
{
	return kinectColor.GetDepthColorData(kinectDepth.getRawDepthData());
}

float* KinectSources::GetNormalData()
{
	return kinectDepth.GetNormalData();
}

void KinectSources::EnableRoomScanning(physx::PxPhysicsWorld* world)
{
	enableRoomScanning = true;
	kinectRoomScanner.InitVoxelization(world);
}

void KinectSources::DisableRoomScanning()
{
	enableRoomScanning = false;
}

KinectSources::~KinectSources(void)
{

	kinectColor.shutDown();
	kinectDepth.shutDown();
//	kinectRoomScanner.shutDown();

}
