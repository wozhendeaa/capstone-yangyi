#include "KinectSwipeDetector.h"
#include <iostream>
#include <MemoryDebug.h>

KinectSwipeDetector::KinectSwipeDetector(float time) :
								timeConstraint(time)
{
}

void KinectSwipeDetector::DetectRightSwipe()
{
	bool isRightSwipe = false;

	for (size_t i = 1;
		i < joints.size();
		i++)
	{
		auto prevPos = joints[i - 1].position;
		auto currPos = joints[i].position;

		auto isMovingToRight = prevPos.X > currPos.X;
		auto isOnTheHorizontalLine = prevPos.Y - currPos.Y < verticalDifferenceTolerance;
		auto isOnDepthLine = prevPos.Z - currPos.Z < depthDifferenceTolerance;

		isRightSwipe = isMovingToRight;  isOnTheHorizontalLine && isOnDepthLine;

		if (!isRightSwipe)
		{
			break;
		}
	}

	if (isRightSwipe )
	{
		GestureDetected(swipeRightDetected);
	}
}

void KinectSwipeDetector::DetectLeftSwipe()
{
	/*bool isLeftSwipe = true;

	for (size_t i = 1; 
		i < joints.size() && isLeftSwipe;
		i++)
	{
		auto prevPos = joints[i - 1].position;
		auto currPos = joints[i].position;

		auto isMovingToLeft = prevPos.X < currPos.X;
		auto isOnTheHorizontalLine = prevPos.Y - currPos.Y < verticalDifferenceTolerance;
		auto isOnDepthLine = prevPos.Z - currPos.Z < depthDifferenceTolerance;

		isLeftSwipe = isMovingToLeft && isOnTheHorizontalLine && isOnDepthLine;		
	}

	if (isLeftSwipe && joints[joints.size() - 1].time - joints[0].time < timeConstraint)
	{
		GestureDetected(swipeLeftDetected);
	}*/
}

void KinectSwipeDetector::DetectUpSwipe()
{

}

void KinectSwipeDetector::DetectDownSwipe()
{

}

void KinectSwipeDetector::Destroy()
{
	joints.clear();
}


void KinectSwipeDetector::DetectGestures(float dt)
{
	DetectRightSwipe();
	//DetectLeftSwipe();
	//DetectUpSwipe();
	//DetectDownSwipe();
	totalTime += dt;
}

KinectSwipeDetector::~KinectSwipeDetector(void)
{
	
}
