#include "KinectColor.h"
#include <KinectModule\KinectDepth.h>
#include <device_launch_parameters.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <MemoryDebug.h>

KinectColor::KinectColor(void)
{
	colorPool = new RGBQUAD[colorWidth * colorHeight];	
	colorDepth = new byte[KinectDepth::depthWidth * KinectDepth::depthHeight * 3];
	depthPoints = new ColorSpacePoint[KinectDepth::depthWidth * KinectDepth::depthHeight];
	
	auto size = sizeof(byte) * KinectDepth::depthWidth * KinectDepth::depthHeight * 3;
	cudaMalloc((void**)&d_color, sizeof(RGBQUAD) * colorWidth * colorHeight);
	cudaMalloc((void**)&d_depthColorSpacePoints, sizeof(float2) * KinectDepth::depthWidth * KinectDepth::depthHeight);
	cudaMalloc((void**)&d_gama_sum, sizeof(float));
	cudaMalloc((void**)&d_brightness_color, size);
	cudaMalloc((void**)&d_depth_color, sizeof(unsigned char) * KinectDepth::depthWidth * KinectDepth::depthHeight * 3);

}

void KinectColor::updateColor()
{
	auto colorFrameReader = KinectSensor::GetSensor()->getColorFrameReader();

	if(colorFrameReader != nullptr )
	{
		IColorFrame* colorFrame = nullptr;
		long long dt = 0;
		auto hr = colorFrameReader->AcquireLatestFrame(&colorFrame);

		if(SUCCEEDED(hr))
		{
			IFrameDescription* description = nullptr;
			ColorImageFormat imageFormat = ColorImageFormat_None;
			unsigned bufferSize = 0;
			RGBQUAD* cBuffer = nullptr;
			int width = 0;
			int height = 0;

			hr = colorFrame->get_RelativeTime(&dt);

			if (coordinateMapper == nullptr)
			{
				KinectSensor::GetSensor()->
					GetKinectDevice()->
					get_CoordinateMapper(&coordinateMapper);
			}

			if(SUCCEEDED(hr))
			{
				hr = colorFrame->get_FrameDescription(&description);
			}

			if(SUCCEEDED(hr))
			{
				hr = description->get_Width(&width);
			}

			if(SUCCEEDED(hr))
			{
				hr = description->get_Height(&height);
			}

			if(SUCCEEDED(hr))
			{
				hr = colorFrame->get_RawColorImageFormat(&imageFormat);
			}

			if(SUCCEEDED(hr))
			{
				if(imageFormat == ColorImageFormat_Bgra)
				{
					hr = colorFrame->AccessRawUnderlyingBuffer(&bufferSize, 
						reinterpret_cast<byte**>(cBuffer));
				}
				else if ( colorPool != nullptr)
				{
					cBuffer = colorPool;
					bufferSize = width * height * sizeof(RGBQUAD);
					hr = colorFrame->CopyConvertedFrameDataToArray(bufferSize, 
						reinterpret_cast<byte*>(cBuffer), ColorImageFormat_Bgra);
					isReady = true;
				}
			}

			SafeRelease(description);

		}

		SafeRelease(colorFrame);
	}
}

extern void UpdateBrightnessDetection(uchar1* color, float* darkPixelCount, float threshold);

byte* KinectColor::getColorData() const
{ 
	return reinterpret_cast<byte*>(colorPool);
}

bool KinectColor::IsScreenDark(float brightnessThreshold, 
	int darkPixelCountThreshold)
{
	auto size = sizeof(byte) * KinectDepth::depthWidth * KinectDepth::depthHeight * 3;
	float gama = 0;
	cudaMemcpy(d_brightness_color, colorDepth, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_gama_sum, &gama, sizeof(float), cudaMemcpyHostToDevice);

	UpdateBrightnessDetection(d_brightness_color, d_gama_sum, brightnessThreshold);

	cudaMemcpy(&gama, d_gama_sum, sizeof(int), cudaMemcpyDeviceToHost);

	gama /= KinectDepth::depthWidth * KinectDepth::depthHeight;
	darkPixelCountThreshold;
	return gama <= brightnessThreshold;
}

extern void UpdateDepthColor(uchar1* destDepthColor,
uchar4* color,
float2* depthColorSpacePoint,
int colorWidth, int colorHeight);

byte* KinectColor::GetDepthColorData(UINT16* depth)
{

	if (depth != nullptr && coordinateMapper != nullptr)
	{
		coordinateMapper->MapDepthFrameToColorSpace(KinectDepth::depthWidth * KinectDepth::depthHeight,
			depth,
			KinectDepth::depthWidth * KinectDepth::depthHeight,
			depthPoints);
		cudaMemcpy(d_color, colorPool, sizeof(RGBQUAD) * colorWidth * colorHeight, cudaMemcpyHostToDevice);
		cudaMemcpy(d_depthColorSpacePoints, 
			depthPoints, sizeof(float2) * KinectDepth::depthWidth * KinectDepth::depthHeight, 
			cudaMemcpyHostToDevice);
	
		UpdateDepthColor(d_depth_color,
			d_color, 
			d_depthColorSpacePoints,
			colorWidth,
			colorHeight);

		auto size = sizeof(uchar1) * KinectDepth::depthWidth * KinectDepth::depthHeight * 3;
		cudaMemcpy(colorDepth, d_depth_color, size, cudaMemcpyDeviceToHost);
	}

	return colorDepth;
}




void KinectColor::shutDown()
{
	if (colorPool != nullptr)
	{
		delete[] colorPool;
		colorPool = nullptr;
	}


	if (colorDepth != nullptr)
	{
		delete[] colorDepth;
		colorDepth = nullptr;
	}

	if (depthPoints != nullptr)
	{
		delete[] depthPoints;
		depthPoints = nullptr;
	}

	cudaFree(d_depth_color);
	cudaFree(d_gama_sum);
	cudaFree(d_color);
	cudaFree(d_brightness_color);
	cudaFree(d_depthColorSpacePoints);
}


KinectColor::~KinectColor(void)
{
	shutDown();
}
