#include "KinectRoomCollision.h"
#include <Physics\PxPhysicsWorld.h>
#include <Actors\Components\Physics\StaticMesh.h>
#include <memory>

vec3 default_collision_position = vec3(0.0f, 0.0f, 10.0f);

KinectRoomCollision::KinectRoomCollision(physx::PxPhysicsWorld* world,
	const float& roomHalfSize,
	const float& f_subdivision) :
											m_subdivision(f_subdivision),
											usedCollisionCount(0),
											m_roomHalfSize(roomHalfSize),
											floorHeight(FLT_MAX),
											isReady(false)
{
	InitStoregeForCollision(world);

}



void KinectRoomCollision::clearAllCollision()
{
	voxelSpace->clearData();
	for (size_t i = 0; i < numCollision; i++)
	{
		auto body = &voxelCollision[usedCollisionCount];
		body->body->setKinematicTarget(PxTransform(PxVec3(physx::ToPxVec3(default_collision_position))));
	}

	usedCollisionCount = 0;
	floorHeight = FLT_MAX;
}

void KinectRoomCollision::InitStoregeForCollision(physx::PxPhysicsWorld* _world)
{

	m_subdivision = .1f;
	world = _world;
	for (size_t i = 0; i < numCollision; i++)
	{
		voxelCollision[i] = RigidBody(world, 0.8f, 0.79f, 0.6f, true);

		auto halfSize = vec3(m_subdivision);
		auto q = quat();				
		
		voxelCollision[i].InitializeWithBox(world,
			halfSize,
			q,
			default_collision_position);

		voxelCollision[i].SetCollisionEnabled(false);
		voxelCollision[i].body->setKinematicTarget(PxTransform(PxVec3(physx::ToPxVec3(default_collision_position))));
	}

	voxelSpace = new Octree<RigidBody>(vec3(.0f, .0f, .5f),	5.0f);
	voxelSpace->Subdivide(m_subdivision);
	isReady = true;
}

Renderable* debugCube = nullptr;
void KinectRoomCollision::DebugDraw(BatEngine::Renderer* r, 
	GLenum drawMode)
{
	if (debugCube == nullptr)
	{
		auto cube2 = Neumont::ShapeGenerator::makeCube();
		std::auto_ptr<GeometryInfo> cubeGeo2(r->addGeometry(cube2.verts,
			cube2.numVerts,
			reinterpret_cast<GLuint*>(cube2.indices),
			cube2.numIndices,
			Neumont::Vertex::STRIDE,
			GL_UNSIGNED_SHORT,
			GL_TRIANGLES));
		cube2.cleanUp();

		debugCube = r->addRenderable(*cubeGeo2,
			new mat4(),
			TextureInfo(),
			0);
	}

	for (int i = 0; i < usedCollisionCount && isReady; i++)
	{
		auto pos = physx::ToVec3(voxelCollision[i].body->getGlobalPose().p);
		debugCube->setTransform(vec3(m_subdivision), mat4(), pos);
		r->Draw(debugCube);
	}
}

void KinectRoomCollision::Destroy()
{
	isReady = false;
}


void KinectRoomCollision::ActivateCollisionAt(const vec3& pos)
{
	
	if (usedCollisionCount < numCollision)
	{
		vec3 newPos;
		auto body = &voxelCollision[usedCollisionCount];
		auto isValid = voxelSpace->InsertCollisionAt(body, pos, &newPos);

		if (isValid)
		{
			newPos.x *= -1.0f;
			//newPos /= m_roomScale * m_subdivision;
			body->body->setKinematicTarget(PxTransform(physx::ToPxVec3(newPos)));
			body->SetCollisionEnabled(true);
			usedCollisionCount++;

			//check for lowest height for floor generation
			if (newPos.y < floorHeight)
			{
				floorHeight = newPos.y;
			}
		}
	}
}

void KinectRoomCollision::GenerateImplictFloor(const mat4& rot,
	float height)
{
	auto a1Scale = vec3(10.0f, 0.1f, 10.0f);
	auto floor = new StaticMesh(world, 0.9f, 0.9f);
	auto q = glm::toQuat(rot);
	auto p = vec3(.0f, height, .0f);
	floor->InitializeWithBox(world, a1Scale, q, p);
}


//void KinectRoomCollision::DeactivateCollisionAt(const vec3& pos)
//{
//	auto body = voxelCollision[usedCollisionCount];
//	auto newPos = voxelSpace->InsertAt(body, pos);
//
//	body->body->setKinematicTarget(PxTransform(physx::ToPxVec3(default_collision_position)));
//	body->SetCollisionEnabled(true);
//}




KinectRoomCollision::~KinectRoomCollision(void)
{
	Destroy();
}
