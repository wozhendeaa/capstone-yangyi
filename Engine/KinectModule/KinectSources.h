#pragma once

#include <KinectModule\ImageRenderer.h>
#include <ExportHeader.h>
#include <KinectModule\KinectColor.h>
#include <KinectModule\KinectDepth.h>
#include <KinectModule\KinectBodyFrame.h>
#include <KinectModule\KinectRoomScanner.h>
#include <KinectModule\KinectRoomCollision.h>
#include <KinectModule\KinectBodyIndex.h>
#include <KinectModule\KinectInfared.h>
class ENGINE_SHARED KinectSources
{
	double freq;
	LARGE_INTEGER start;
	LARGE_INTEGER end;
	long long lastCounter;
	long long frameSinceUpdate;

	KinectColor kinectColor;
	KinectDepth kinectDepth;
	KinectInfared kinectInfared;
	KinectBodyFrame kinectBodyFrame;
	KinectBodyIndex kinectBodyIndex;
	KinectRoomScanner kinectRoomScanner;

	bool enableRoomScanning;
public:

	KinectColor& getColorSource();
	KinectDepth& getDepthSource();
	KinectBodyFrame& getBodySource();
	KinectRoomScanner& getRoomeScanner();
	KinectInfared& getInfaredSource();

	vec4 GetFloorUpVector() const;

	bool IsRoomScannerReadt() const;

	KinectSources(void);	
	~KinectSources(void);

	void updateColor();
	void updateDepth();
	void updateBody();

	bool IsScreenDark(float brightnessThreshold = 3.0f,
		int darkPixelCountThreshold = 1036800);

	void RoomScannerDebugDraw(Renderer* r, 
		GLenum drawMode = GL_LINES);

	byte* GetDepthColorData();

	float* GetNormalData();

	double GetFPS();

	void UpdateRoomScanning(
		bool treatPlayerAsCollision = false);

	void EnableRoomScanning(physx::PxPhysicsWorld* world);
	void DisableRoomScanning();
};

