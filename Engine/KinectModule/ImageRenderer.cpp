#include "ImageRenderer.h"
#include <MemoryDebug.h>

ImageRenderer::ImageRenderer(Renderer* r, 
							 uint width, 
							 uint height,
							 uint stride)
{
	renderer = r;
	sourceWidth = width;
	sourceHeight = height;
	sourceStride = stride;
	std::cout << "image renderer initialized" << std::endl;

}

void ImageRenderer::updateTexture(const uint& tex_unit,
									const uint& tex_id,
									const uint& width,
									const uint& height,
									void* data,
								    GLenum format ,
									GLenum type) 
{
	
	if(data != nullptr)
	{
		glActiveTexture(GL_TEXTURE0 + tex_unit);
		glBindTexture(GL_TEXTURE_2D, tex_id); 
		glTexSubImage2D(GL_TEXTURE_2D, 
			0, 
			0,
			0,
			width, 
			height, 
			format,
			type,
			data);
		glBindTexture(GL_TEXTURE_2D, 0); 

	}
}

void ImageRenderer::updateARDeapthMap(
	const TextureInfo* t,
	void* data,
	GLuint program_id)
{
	if (data != nullptr)
	{
		auto enableArDepthLocation = glGetUniformLocation(program_id, "enableARDepthTesting");
		glUniform1i(enableArDepthLocation, true);

		glActiveTexture(GL_TEXTURE0 + t->getColorTexUnit());
		glBindTexture(GL_TEXTURE_2D, t->color_tex_id);
		glTexSubImage2D(GL_TEXTURE_2D,
			0,
			0,
			0,
			t->width,
			t->height,
			GL_RGB,
			GL_FLOAT,
			data);

		glBindTexture(GL_TEXTURE_2D, 0);

	}
}


void ImageRenderer::DrawImage(Renderable* r, 
							  void* data,
							  GLenum format ,
							  GLenum type) 
{
	assert(r->textureInfo != nullptr);
	updateTexture(r->textureInfo->getColorTexUnit(),
					r->textureInfo->color_tex_id,
					r->textureInfo->width,
					r->textureInfo->height,					
					data,
					format,
					type);

	renderer->Draw(r);
}




ImageRenderer::~ImageRenderer(void)
{
}
