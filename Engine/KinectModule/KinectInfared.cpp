#include "KinectInfared.h"
#include <device_launch_parameters.h>
#include <cuda.h>
#include <cuda_runtime_api.h>

KinectInfared::KinectInfared()
{
	infaredTexBuffer = new byte[512 * 424 * 3];
	infaredBuffer = new UINT16[512 * 424];
	
	auto bufferSize = sizeof(UINT16) * 512 * 424;
	cudaMalloc((void**)&d_infaredBuffer, bufferSize);

	auto texSize = sizeof(byte) * 512 * 424 * 3;
	cudaMalloc((void**)&d_infaredTexBuffer, texSize);
}

UINT16* KinectInfared::GetInfaredBuffer() const
{
	return infaredBuffer;
}


extern void UpdateInfaredTex(uchar1* texSource,
	ushort1* infaredBuffer);
const float InfraredSourceValueMaximum = static_cast<float>(USHRT_MAX);
const float InfraredSceneValueAverage = 0.08f;
const float InfraredSceneStandardDeviations = 3.0f;
const float InfraredOutputValueMinimum = 0.01f;
const float InfraredOutputValueMaximum = 1.0f;


byte* KinectInfared::GetInfaredTexBuffer() 
{
	if (infaredBuffer != nullptr)
	{
		auto bufferSize = sizeof(UINT16) * 512 * 424;
		auto texSize = sizeof(byte) * 512 * 424 * 3;
		cudaMemcpy(d_infaredBuffer, infaredBuffer, bufferSize, cudaMemcpyHostToDevice);
		UpdateInfaredTex(d_infaredTexBuffer, d_infaredBuffer);
		cudaMemcpy(infaredTexBuffer, d_infaredTexBuffer, texSize, cudaMemcpyDeviceToHost);
	/*	for (size_t i = 0; i < 512 * 424 && infaredBuffer != nullptr; ++i)
		{
			auto intensityRatio = static_cast<float>(infaredBuffer[i]) / InfraredSourceValueMaximum;
			intensityRatio /= InfraredSceneValueAverage * InfraredSceneStandardDeviations;

			intensityRatio = glm::min(InfraredOutputValueMaximum, intensityRatio);

			intensityRatio = glm::max(InfraredOutputValueMinimum, intensityRatio);

			infaredTexBuffer[i * 3 + 0] = static_cast<byte>(intensityRatio * 255.0f);
			infaredTexBuffer[i * 3 + 1] = static_cast<byte>(intensityRatio * 255.0f);
			infaredTexBuffer[i * 3 + 2] = static_cast<byte>(intensityRatio * 255.0f);
		}*/

	}

	return infaredTexBuffer;
}

void KinectInfared::Update()
{
	auto reader = KinectSensor::GetSensor()->getInfaredReader();

	IInfraredFrame* pInfraredFrame = nullptr;
	auto hr = reader->AcquireLatestFrame(&pInfraredFrame);
	if (SUCCEEDED(hr))
	{
		IFrameDescription* pFrameDescription = nullptr;
		hr = pInfraredFrame->get_FrameDescription(&pFrameDescription);

		if (SUCCEEDED(hr))
		{
			hr = pFrameDescription->get_Width(&width);
		}

		if (SUCCEEDED(hr))
		{
			hr = pFrameDescription->get_Height(&height);
		}

		if (SUCCEEDED(hr))
		{
			//hr = pInfraredFrame->AccessUnderlyingBuffer(&bufferSize, &temp);
		}

		if (SUCCEEDED(hr))
		{
			hr = pInfraredFrame->CopyFrameDataToArray(512 * 424, infaredBuffer);
		}


		SafeRelease(pFrameDescription);
	}

	SafeRelease(pInfraredFrame);
}

KinectInfared::~KinectInfared()
{
	if (infaredTexBuffer != nullptr)
	{
		delete[] infaredTexBuffer;
		infaredTexBuffer = nullptr;
	}

	if (infaredBuffer != nullptr)
	{
		delete[] infaredBuffer;
		infaredBuffer = nullptr;
	}

	cudaFree(d_infaredBuffer);
	cudaFree(d_infaredTexBuffer);
}
