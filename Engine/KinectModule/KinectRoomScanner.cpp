#include "KinectRoomScanner.h"
#include "KinectBodyIndex.h"
#include "KinectRoomCollision.h"
#include "KinectDepth.h"
#include "kinectBodyFrame.h"
#include <assert.h>
#include <MemoryDebug.h>
#include <glm\gtx\rotate_vector.hpp>

KinectRoomScanner::KinectRoomScanner(float subdivision,
	float roomHalfSize) :
	m_roomHalfSize(roomHalfSize),
	m_subdivision(subdivision),
	m_updateInterval(0.5f)
{
	roomCollision = nullptr;
}

void KinectRoomScanner::InitVoxelization(physx::PxPhysicsWorld* world)
{

	roomCollision = new KinectRoomCollision(world, 
		m_roomHalfSize,
		m_subdivision);
}


void KinectRoomScanner::UpdateRoomScanning(
	KinectBodyFrame* player,
	KinectDepth* depth,
	KinectBodyIndex* bodyIndex,

	bool treatPlayerAsCollision)
{
	static auto scanned = false;
	
	if (roomCollision->IsReady() && depth->IsReady())
	{
		auto depthRays = depth->getRaysOfDepths();
		auto depthData = depth->getRawDepthData();

		roomCollision->clearAllCollision();

		for (uint x = 0; x < KinectDepth::depthWidth * KinectDepth::depthHeight; x += 50)
		{
			if (depthData[x] > 500 && depthData[x] < 4500)
			{
				auto point = depthRays[x];
				bool isDepthOnPlayer = false;

				if (treatPlayerAsCollision)
				{
					isDepthOnPlayer =
						bodyIndex->IsDepthPixelOnBody(vec2(point.X, point.Y));
				}

				auto pos = vec3(point.X, point.Y, point.Z);

				player;
				if (pos.z > .5f)
				{
					roomCollision->ActivateCollisionAt(pos);
				}
			}
		}

		//auto up = player->getFloorUpVector();
		//auto floorRot = glm::orientation(vec3(up), vec3(.0f, 1.0f, .0f));
		//auto height = up.w == .0f ? -1.4f : up.w;
		//roomCollision->GenersateImplictFloor(floorRot, height);
		scanned = true;
	}
}


void KinectRoomScanner::DeugDraw(BatEngine::Renderer* r,
	GLenum drawMode)
{
	if (roomCollision != nullptr)
	{
		roomCollision->DebugDraw(r, drawMode);
	}
}

void KinectRoomScanner::shutDown()
{

	delete roomCollision;
}

KinectRoomScanner::~KinectRoomScanner(void)
{
	shutDown();
}
