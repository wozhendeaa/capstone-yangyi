#include "ClothActor.h"
#include <Actors\Components\MeshComponents\MeshRenderer.h>


ClothActor::ClothActor(BatEngine::Renderer* r,
		physx::PxPhysicsWorld* world) 
{
	LoadMesh(r);
	InitComponents(world);
}

void ClothActor::InitComponents(physx::PxPhysicsWorld* world)
{
	auto meshRenderer = new MeshRenderer(renderable);
	AddComponent(static_cast<StrongActorComponent>(meshRenderer));

	clothComponent = new Cloth(world, renderable->geometry);
	AddComponent(static_cast<StrongActorComponent>(clothComponent));
}

void ClothActor::Update(float dt)
{
	Actor::Update(dt);
}

void ClothActor::Draw(Renderer* r, GLuint passindex)
{
	Actor::Draw(r, passindex);
	renderable->setTransform(vec3(0.7f, 0.7f, 1.0f), 
		glm::toMat4(*m_rotation), 
		*m_translation);
}

void ClothActor::LoadMesh(BatEngine::Renderer* r)
{
	std::string file = "Resources//Mesh//Cloth.bin";

	ObjLoader loader;
	loader.loadFromFile(file.c_str());
	model = loader.getModel();
	
	std::auto_ptr<GeometryInfo> verts( r->addGeometry(model.verts,
		model.numVerts,
		reinterpret_cast<GLuint*>(model.indices),
		model.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_INT, 
		GL_TRIANGLES));

	TextureInfo t;
	t.addColorTexture("Resources\\Texture\\cape_color.png", 
	FIF_PNG,
	r->shaders[0]->program_id);

	t.addAlphaTexture("Resources\\Texture\\cape_alpha.png",
		FIF_PNG,
		r->shaders[0]->program_id);

	renderable = r->addRenderable(*verts,
			new mat4(),
			t,
			0);

	
}


void ClothActor::SetClothPose(vec3 pos, quat rot)
{
	auto trans = physx::ToPxTransform(pos);
	clothComponent->clothObject->setTargetPose(trans);
	pos.z -= 0.2f;
	*m_translation = pos;
	*m_rotation = rot;
	//clothComponent->offsetPos = pos;
}


void ClothActor::SetFixPoints(vec4* points, 
		const unsigned num,
		bool useWAsOffset)
{
	for (unsigned i = 0; i < num; i++)
	{
		//clothComponent->clothObject->setp
		points;
		useWAsOffset;
		//(
	}
}

void ClothActor::SetVerticalStretch(const float val,
									const float stiffness , 
									const float compressionLimit ,
									const float stretchLimit )
{
	auto clothObj = clothComponent->clothObject;
	clothObj->setStretchConfig(physx::PxClothFabricPhaseType::eVERTICAL,
		physx::PxClothStretchConfig(val,stiffness, compressionLimit, stretchLimit));

}

void ClothActor::SetHorizontalStretch(const float val,
									const float stiffness , 
									const float compressionLimit ,
									const float stretchLimit )
{
	auto clothObj = clothComponent->clothObject;
	clothObj->setStretchConfig(physx::PxClothFabricPhaseType::eHORIZONTAL,
		physx::PxClothStretchConfig(val,stiffness, compressionLimit, stretchLimit));

}

void ClothActor::SetBending(const float val,
									const float stiffness , 
									const float compressionLimit ,
									const float stretchLimit )
{
	auto clothObj = clothComponent->clothObject;
	clothObj->setStretchConfig(physx::PxClothFabricPhaseType::eBENDING,
		physx::PxClothStretchConfig(val,stiffness, compressionLimit, stretchLimit));

}

void ClothActor::SetSHEARING(const float val,
									const float stiffness , 
									const float compressionLimit ,
									const float stretchLimit )
{
	auto clothObj = clothComponent->clothObject;
	clothObj->setStretchConfig(physx::PxClothFabricPhaseType::eSHEARING,
		physx::PxClothStretchConfig(val,stiffness, compressionLimit, stretchLimit));

}


void ClothActor::Destroy() 
{
	Actor::Destroy();
	model.cleanUp();
}

ClothActor::~ClothActor(void)
{

}
