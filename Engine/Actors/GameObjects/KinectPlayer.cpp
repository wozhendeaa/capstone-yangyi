#include "KinectPlayer.h"
#include <glm\gtx\rotate_vector.hpp>
#include <MemoryDebug.h>
#include <Delegation\FastDelegate.h>
KinectPlayer::KinectPlayer(KinectBodyFrame* kBody, 
						   Renderer* r,
						   physx::PxPhysicsWorld* world) 
						   : m_world(world),
						   postureTimeTolerance(0.8f),
						   isTrackingHead(false),
						   isVisionMode(false)
{
	bodyFrame = kBody;
	renderer = r;
	Init();
	InitMeshes(r);
}

void KinectPlayer::Init()
{
	 head = nullptr;
	 body = nullptr;
	 leftShoulder = nullptr;
	 leftUpperArm = nullptr;
	 leftLowerArm = nullptr;
	 leftHand = nullptr;
	 rightShoulder = nullptr;
	 rightUpperArm = nullptr;
	 rightLowerArm = nullptr;
	 rightHand = nullptr;
	 leftThigh = nullptr;
	 leftHive = nullptr;
	 rightThigh = nullptr;
	 rightHive = nullptr;
	 aspect_ratio = 16.0f / 9.0f; 
	 InitGestureDetection();
}


void KinectPlayer::InitMeshes(Renderer* r)
{
	auto cube = Neumont::ShapeGenerator::makeCube();
	std::auto_ptr<GeometryInfo> cubeGeo (r->addGeometry(cube.verts,
		cube.numVerts,
		reinterpret_cast<GLuint*>(cube.indices),
		cube.numIndices,
		Neumont::Vertex::STRIDE,
		GL_UNSIGNED_SHORT, 
		GL_TRIANGLES));
	cube.cleanUp();
	bone = r->addRenderable(*cubeGeo, 
		new mat4,
		TextureInfo(),
		0);;


	auto headRenderable = r->addRenderable(*cubeGeo, 
		new mat4,
		TextureInfo(),
		0);

	head = new Actor();
	auto meshComp = new MeshRenderer(headRenderable, false);
	head->AddComponent(static_cast<StrongActorComponent>(meshComp));

	body = new Actor();
	auto bodyRen = r->addRenderable(*cubeGeo, 
	new mat4,
	TextureInfo(),
	0);
	auto m2 = new MeshRenderer(bodyRen, false);
	body->AddComponent(static_cast<StrongActorComponent>(m2));

	leftUpperArm = new Limb();
	auto leftArmRen = r->addRenderable(*cubeGeo, 
	new mat4,
	TextureInfo(),
	0);
	auto m3 = new MeshRenderer(leftArmRen, false);
	leftUpperArm->AddComponent(static_cast<StrongActorComponent>(m3));

	leftLowerArm = new Limb();
	auto leftLowerArmRen = r->addRenderable(*cubeGeo, 
	new mat4,
	TextureInfo(),
	0);
	auto m4 = new MeshRenderer(leftLowerArmRen, false);
	leftLowerArm->AddComponent(static_cast<StrongActorComponent>(m4));

	rightUpperArm = new Limb();
	auto rightUpperArmRen = r->addRenderable(*cubeGeo, 
	new mat4,
	TextureInfo(),
	0);
	auto m5 = new MeshRenderer(rightUpperArmRen, false);
	rightUpperArm->AddComponent(static_cast<StrongActorComponent>(m5));

	rightLowerArm = new Limb();
	auto rightLowerArmRen = r->addRenderable(*cubeGeo, 
	new mat4,
	TextureInfo(),
	0);
	auto m6 = new MeshRenderer(rightLowerArmRen, false);
	rightLowerArm->AddComponent(static_cast<StrongActorComponent>(m6));

	leftThigh = new Limb();
	auto leftThighRen = r->addRenderable(*cubeGeo,
		new mat4,
		TextureInfo(),
		0);
	auto m7 = new MeshRenderer(leftThighRen, false);
	leftThigh->AddComponent(static_cast<StrongActorComponent>(m7));

	leftHive = new Limb();
	auto leftHiveRen = r->addRenderable(*cubeGeo,
		new mat4,
		TextureInfo(),
		0);
	m7 = new MeshRenderer(leftHiveRen, false);
	leftHive->AddComponent(static_cast<StrongActorComponent>(m7));

	rightThigh = new Limb();
	auto rightThighRen = r->addRenderable(*cubeGeo,
		new mat4,
		TextureInfo(),
		0);
	m7 = new MeshRenderer(rightThighRen, false);
	rightThigh->AddComponent(static_cast<StrongActorComponent>(m7));

	rightHive = new Limb();
	auto rightHiveRen = r->addRenderable(*cubeGeo,
		new mat4,
		TextureInfo(),
		0);
	m7 = new MeshRenderer(rightHiveRen, false);
	rightHive->AddComponent(static_cast<StrongActorComponent>(m7));

	originalPerspective = r->Matrices.projection;
}

void KinectPlayer::InitGestureDetection()
{
	auto rightSwipe = fastdelegate::MakeDelegate(this, &KinectPlayer::RightCut);
	swipeDetector.swipeRightDetected = rightSwipe;

	//auto leftSwipe = fastdelegate::MakeDelegate(this, &KinectPlayer::LeftCut);
	//swipeDetector.swipeLeftDetected = leftSwipe;

}

bool KinectPlayer::IsInVisionMode() const
{
	return isVisionMode;
}

vec3 KinectPlayer::GetLeftHandKinematicEnergy() const
{
	return m_leftHandKinematicEnergy.totalMomentum;
}

vec3 KinectPlayer::GetRightHandKinematicEnergy() const
{
	return m_rightHandKinematicEnergy.totalMomentum;
}

vec3 KinectPlayer::GetLeftFootKinematicEnergy() const
{
	return m_leftFootKinematicEnergy.totalMomentum;
}

vec3 KinectPlayer::GetRightFootKinematicEnergy() const
{
	return m_rightFootKinematicEnergy.totalMomentum;
}
void KinectPlayer::CalcJointMomentum(JointMomentumData& data,
	Joint* joint,
	float dt)
{
	if (joint->TrackingState == TrackingState_Tracked)
	{
		auto jointPos = &joint->Position;
		vec3 glmVec3(-jointPos->X, jointPos->Y, jointPos->Z);
		auto deltaV = (glmVec3 - data.prevPos);
		auto mass = .5f;
		data.changeInMomentum = deltaV * mass;
		data.totalMomentum += data.changeInMomentum;
		data.momentumMagnitude = glm::length(data.totalMomentum);

		bool reachedMaxEnergy = data.momentumMagnitude >= m_maxEnergyCap;
		data.totalMomentum = data.changeInMomentum + (reachedMaxEnergy ? vec3(0.0f) : data.changeInMomentum);
		data.totalMomentum *= m_energyDamping;
		data.prevPos = glmVec3;
	}
}


PxVec3 KinectPlayer::CastRightHandRay()
{
	auto joints = bodyFrame->getJointsData();
	auto rightHandJoint = joints[JointType_HandRight].Position;
	auto rightElbowJoint = joints[JointType_ElbowRight].Position;

	auto rightHandVec3 = vec3(-rightHandJoint.X, rightHandJoint.Y, rightHandJoint.Z);
	auto rightElbowVec3 = vec3(-rightElbowJoint.X, rightElbowJoint.Y, rightElbowJoint.Z);
	auto dir = rightHandVec3 - rightElbowVec3;

	auto origin = physx::ToPxVec3(rightHandVec3);
	auto unitDir = physx::ToPxVec3(glm::normalize(dir));
	auto maxDistance = 4.0f;

	if (!isnan(unitDir.x))
	{
		physx::PxRaycastBuffer hitInfo;
		auto isHit = m_world->GetPhysicsWorld()->raycast(origin, unitDir, maxDistance, hitInfo);

		if (isHit)
		{
			auto hitObject = hitInfo.block.actor->userData;
			auto castObject = static_cast<Actor*>(hitObject);

			RightHandRayHitCallback(castObject);
		}
	}

	return unitDir;
}

PxVec3 KinectPlayer::CastLeftHandRay()
{
	auto joints = bodyFrame->getJointsData();
	auto leftHandJoint = joints[JointType_HandLeft].Position;
	auto leftElbowJoint = joints[JointType_ElbowLeft].Position;

	auto leftHandVec3 = vec3(-leftHandJoint.X, leftHandJoint.Y, leftHandJoint.Z);
	auto leftElbowVec3 = vec3(-leftElbowJoint.X, leftElbowJoint.Y, leftElbowJoint.Z);
	auto dir = leftHandVec3 - leftElbowVec3;

	auto origin = physx::ToPxVec3(leftHandVec3);
	auto unitDir = physx::ToPxVec3(glm::normalize(dir));
	auto maxDistance = 4.0f;

	if (!isnan(unitDir.x))
	{
		physx::PxRaycastBuffer hitInfo;
		auto isHit = m_world->GetPhysicsWorld()->raycast(origin, unitDir, maxDistance, hitInfo);

		if (isHit)
		{
			auto hitObject = hitInfo.block.actor->userData;
			auto castObject = static_cast<Actor*>(hitObject);

			LeftHandRayHitCallback(castObject);
		}
	}

	return unitDir;
}

mat3 KinectPlayer::GetBodyOrientation()
{
	static mat3 prevOrientation;
	auto joints = bodyFrame->getJointsData();

	//auto useLeftShoulder = joints[JointType_ShoulderLeft].TrackingState == TrackingState::TrackingState_Tracked;
	//auto useRightShoulder = joints[JointType_ShoulderRight].TrackingState == TrackingState::TrackingState_Tracked;

	auto shoulderJoint = joints[JointType_ShoulderRight].Position;
	//if (useLeftShoulder)
	//{
	//	shoulderJoint = joints[JointType_ShoulderLeft].Position;
	//}
	//else if (useRightShoulder)
	//{
	//	shoulderJoint = joints[JointType_ShoulderRight].Position;
	//	shoulderJoint.X *= -1;
	//}

	auto shoulderVec3 = vec3(-shoulderJoint.X,
		shoulderJoint.Y,
		shoulderJoint.Z);

	auto baseJoint = joints[JointType_ShoulderLeft].Position;
	auto baseJointVec3 = vec3(-baseJoint.X,
		baseJoint.Y,
		baseJoint.Z);

	auto xBasis = glm::normalize(shoulderVec3 - baseJointVec3);

	auto bodyOrientation = glm::orientation(xBasis, vec3(0.0f, 1.0f, .0f));
	bodyOrientation = glm::rotate(bodyOrientation, -90.0f, vec3(0.0f, .0f, 1.0f));

	return mat3(bodyOrientation);
}


void KinectPlayer::UpdateJointsKinematicEnergy(float dt)
{
	auto handLeftJoint = bodyFrame->getJointsData()[JointType_HandLeft];
	CalcJointMomentum(m_leftHandKinematicEnergy, &handLeftJoint, dt);

	static float max = 0.0f;
	if (m_leftHandKinematicEnergy.momentumMagnitude > max)
	{
		max = m_leftHandKinematicEnergy.momentumMagnitude;
	}

	if (m_leftHandKinematicEnergy.momentumMagnitude > m_maxEnergyCap)
	{
		LeftHandEnergyHitMax();
		m_leftHandKinematicEnergy.momentumMagnitude = 0;
		m_leftHandKinematicEnergy.totalMomentum = vec3(.0f);
	}

	auto handRightJoint = bodyFrame->getJointsData()[JointType_HandRight];
	CalcJointMomentum(m_rightHandKinematicEnergy, &handRightJoint, dt);
	if (m_rightHandKinematicEnergy.momentumMagnitude > m_maxEnergyCap)
	{
		RightHandEnergyHitMax();
		m_rightHandKinematicEnergy.momentumMagnitude = 0;
		m_rightHandKinematicEnergy.totalMomentum = vec3(.0f);
	}

	auto ankleLeftJoint = bodyFrame->getJointsData()[JointType_AnkleLeft];
	CalcJointMomentum(m_leftFootKinematicEnergy, &ankleLeftJoint, dt);

	auto ankleRightJoint = bodyFrame->getJointsData()[JointType_AnkleRight];
	CalcJointMomentum(m_rightFootKinematicEnergy, &ankleRightJoint, dt);
}



float SCREEN_WIDTH ;
float SCREEN_HEIGHT; 
void KinectPlayer::Draw(Renderer* r, GLuint passIndex )
{
	//head->Draw(r);
	//body->Draw(r);
	//leftUpperArm->Draw(r);
	//leftLowerArm->Draw(r);
	//rightUpperArm->Draw(r);
	//rightLowerArm->Draw(r);

	////rightHand->Draw(r);
	//leftThigh->Draw(r);
	//leftHive->Draw(r);
	//leftHive->Draw(r);
	//rightThigh->Draw(r);
	//rightHive->Draw(r);

	 //auto joints = bodyFrame->getJointsData();
  //  
  //  //draw body joints
  //  for (int i = 0; i < JointType_Count && i < JointType_KneeRight; i++)
  //  {
  //      if(joints[i].TrackingState == TrackingState_Inferred
  //          || joints[i].TrackingState == TrackingState_Tracked)

  //      {
  //          auto ren = bone;
  //          auto scale = vec3(0.03f);
  //          auto worldPos = joints[i].Position;

  //          ren->setTransform(scale, mat4(), vec3(-worldPos.X - 0.08f, worldPos.Y, worldPos.Z)); 
  //          r->Draw(ren);
  //      }

  //  }

	UpdatePlayerCameraTracking(r);


}

void KinectPlayer::AttachCameraToHead(Camera* cam)
{
	isTrackingHead = true;
	playerCamera = cam;
}


float KinectPlayer::CalculateBodyWidth()
{
	auto joints = bodyFrame->getJointsData();
	auto j1Pos = joints[JointType_ShoulderLeft].Position;
	auto j2Pos = joints[JointType_ShoulderRight].Position;
		
	auto j1PosVec3 = vec3(j1Pos.X, j1Pos.Y, j1Pos.Z);
	auto j2PosVec3 = vec3(j2Pos.X, j2Pos.Y, j2Pos.Z);

	return glm::length(j1PosVec3 - j2PosVec3) * 0.5f;
}


void KinectPlayer::UpdateBones()
{
	auto virtualJointPos = bodyFrame->getVirtualJointsPostitons();
	//draw bones
	UpdateBone(head, virtualJointPos, JointType_Head, JointType_SpineBase, 0.1f, vec3(0.0f, 0.5f, 0.0f));
    UpdateBone(body, virtualJointPos, JointType_SpineShoulder, JointType_SpineBase, CalculateBodyWidth());
    //UpdateBone(r, virtualJointPos, JointType_Neck, JointType_SpineShoulder);
    //UpdateBone(r, virtualJointPos, JointType_SpineMid, JointType_SpineBase);
    //UpdateBone(r, virtualJointPos, JointType_SpineShoulder, JointType_ShoulderRight);
    //UpdateBone(r, virtualJointPos, JointType_SpineShoulder, JointType_ShoulderLeft);
    //UpdateBone(r, virtualJointPos, JointType_SpineBase, JointType_HipRight);
    //UpdateBone(r, virtualJointPos, JointType_SpineBase, JointType_HipLeft);
    //
    // Right Arm    
    UpdateBone(rightUpperArm, virtualJointPos, JointType_ShoulderRight, JointType_ElbowRight);
    UpdateBone(rightLowerArm, virtualJointPos, JointType_ElbowRight, JointType_HandTipRight);
    //UpdateBone(&rightHand, virtualJointPos, JointType_HandRight, JointType_HandTipRight);
    //UpdateBone(r, virtualJointPos, JointType_WristRight, JointType_ThumbRight);

     //Left Arm
    UpdateBone(leftUpperArm, virtualJointPos, JointType_ShoulderLeft, JointType_ElbowLeft);
    UpdateBone(leftLowerArm, virtualJointPos, JointType_ElbowLeft, JointType_HandTipLeft);
    //UpdateBone(r, virtualJointPos, JointType_WristLeft, JointType_ThumbLeft);


	//Left hand
	//UpdateBone(r, virtualJointPos, JointType_ElbowLeft, JointType_WristLeft);
    //UpdateBone(r, virtualJointPos, JointType_WristLeft, JointType_HandLeft);
	//UpdateBone(r, virtualJointPos, JointType_ElbowRight, JointType_WristRight);
	//UpdateBone(r, virtualJointPos, JointType_WristRight, JointType_HandRight);

    // Right Leg
    UpdateBone(rightThigh, virtualJointPos, JointType_HipRight, JointType_KneeRight);
	UpdateBone(rightHive, virtualJointPos, JointType_KneeRight, JointType_FootRight);
    //UpdateBone(r, virtualJointPos, JointType_AnkleRight, JointType_FootRight);

    //// Left Leg
    UpdateBone(leftThigh, virtualJointPos, JointType_HipLeft, JointType_KneeLeft);
	UpdateBone(leftHive, virtualJointPos, JointType_KneeLeft, JointType_FootLeft);
    //UpdateBone(r, virtualJointPos, JointType_AnkleLeft, JointType_FootLeft);
	//
}

void KinectPlayer::DrawLeftHand(
								   const vec4* bJointPoints)
{
	bJointPoints;

}

void KinectPlayer::DrawRightHand(
									const vec4* bJointPoints)
{
	bJointPoints;
	
}


void KinectPlayer::UpdateVision(float dt)
{
	if (isTrackingHead)
	{
		static float postureTime = .0f;
		float isInDistance = 0.2f;
		auto headPos = bodyFrame->getJointsData()[JointType_Head].Position;

		auto leftHandJoint = bodyFrame->getJointsData()[JointType_HandLeft];
		auto rightHandJoint = bodyFrame->getJointsData()[JointType_HandRight];

		float LeftHandHeadDist = glm::distance(vec3(headPos.X, headPos.Y, headPos.Z),
			vec3(leftHandJoint.Position.X, leftHandJoint.Position.Y, leftHandJoint.Position.Z));

		float rightHandHeadDist = glm::distance(vec3(headPos.X, headPos.Y, headPos.Z),
			vec3(rightHandJoint.Position.X, rightHandJoint.Position.Y, rightHandJoint.Position.Z));

		bool isRightHandOnHead = rightHandHeadDist < isInDistance
			&& rightHandHeadDist > .0f &&
			(leftHandJoint.TrackingState == TrackingState_Tracked ||
			leftHandJoint.TrackingState == TrackingState_Inferred);

		bool isLeftHandOnHead = LeftHandHeadDist < isInDistance
			&& LeftHandHeadDist > .0f &&
			(rightHandJoint.TrackingState == TrackingState_Tracked ||
			rightHandJoint.TrackingState == TrackingState_Inferred);

		if (isRightHandOnHead || isLeftHandOnHead)
		{
			postureTime += dt;

			if (postureTime >= postureTimeTolerance)
				isVisionMode = true;
		}
		else
		{
			postureTime = 0.0f;
			auto shoulderLeft = bodyFrame->getJointsData()[JointType_ShoulderLeft].Position;
			auto shoulderRight = bodyFrame->getJointsData()[JointType_ShoulderRight].Position;

			if ((shoulderLeft.Y > leftHandJoint.Position.Y &&
				leftHandJoint.TrackingState == TrackingState_Tracked)
				&& (rightHandJoint.Position.Y < shoulderRight.Y  &&
				rightHandJoint.TrackingState == TrackingState_Tracked))
			{
				isVisionMode = false;
			}
		}

	}
}

ICoordinateMapper* KinectPlayer::GetCoordinateMapper()
{
	if (coordinateMapper == nullptr)
	{
		KinectSensor::GetSensor()->
			GetKinectDevice()->
			get_CoordinateMapper(&coordinateMapper);
	}

	return coordinateMapper;
}

void KinectPlayer::UpdateBone( Actor* bodyPart,
							   const vec4* bPointJoints,
							   JointType j1,
							   JointType j2,
							   const float weight,
							   vec3 offset)
{
	bPointJoints;
	offset;
	auto joints = bodyFrame->getJointsData();

	auto j1State = joints[j1].TrackingState;
	auto j2State = joints[j2].TrackingState;

	bool jointsNotFound = j1State == TrackingState_NotTracked &&
						  j2State == TrackingState_NotTracked;

	bool oneInferred = j1State == TrackingState_Inferred ||
						j2State == TrackingState_Inferred;

	bool bothTracked = j1State == TrackingState_Tracked &&
					   j2State == TrackingState_Tracked;	

	auto collision = bodyPart->GetComponent<RigidBody>();

	if(!jointsNotFound && (oneInferred || bothTracked))
	{
		auto j1Pos = joints[j1].Position;
		auto j2Pos = joints[j2].Position;
		
		auto j1PosVec3 = vec3(-j1Pos.X, j1Pos.Y, j1Pos.Z);
		auto j2PosVec3 = vec3(-j2Pos.X, j2Pos.Y, j2Pos.Z);

		auto xBasis = vec3(j2PosVec3 - j1PosVec3); 
		auto distance = glm::length(xBasis);
		auto mat = glm::orientation(glm::normalize(xBasis), vec3(1.0f, 0.0f, 0.0f));
		auto scale = vec3(distance * 0.5f, weight, weight);
		

		bodyPart->setTOffsetransform(scale, mat, vec3(j1PosVec3),	vec3(distance * 0.5f, 0.0f, 0.0f));
		bodyPart->GetComponent<MeshRenderer>().lock()->SetVisibility(true);

		if(collision.lock() == nullptr && m_world != nullptr)
		{
				auto col = new RigidBody(m_world, 0.7f, 0.7f, 0.9f, true);
				col->InitializeWithBox(m_world,
					*bodyPart->m_scale, 
					*bodyPart->m_rotation, 
					*bodyPart->m_translation,
					2.0f);

			bodyPart->AddComponent(static_cast<StrongActorComponent>(col));


			if (j1 == JointType_ElbowRight && j2 == JointType_HandTipRight)
			{
				LowerRightArmCollisionBuilt();
			}
			else if (j1 == JointType_ElbowLeft && j2 == JointType_HandTipLeft)
			{
				LowerRightArmCollisionBuilt();
			}
			else if (j1 == JointType_KneeLeft && j2 == JointType_AnkleLeft)
			{
				LowerLeftLegCollisionBuilt();
			}
			else if (j1 == JointType_KneeRight && j2 == JointType_AnkleRight)
			{
				LowerRightLegCollisionBuilt();
			}
		}
	}
	else 
	{
		bodyPart->GetComponent<MeshRenderer>().lock()->SetVisibility(false);

	}

}

void KinectPlayer::SetAspectRatioForMeshPosition(const float aspectRatio)
{
	aspect_ratio = aspectRatio;
}


void KinectPlayer::Update(float dt)
{
	Actor::Update(dt);
	UpdateBones();
	head->Update(dt);
	body->Update(dt);
	//leftShoulder->Update(dt);
	leftUpperArm->Update(dt);
	leftLowerArm->Update(dt);
	//leftHand->Update(dt);
	//rightShoulder->Update(dt);
	rightUpperArm->Update(dt);
	rightLowerArm->Update(dt);
	//rightHand->Update(dt);
	leftThigh->Update(dt);
	leftHive->Update(dt);
	//leftHive->Update(dt);
	rightThigh->Update(dt);
	rightHive->Update(dt);

	GestureDetection(dt);
	UpdateJointsKinematicEnergy(dt);
	UpdateVision(dt);
}


void KinectPlayer::GestureDetection(float dt)
{

	if (bodyFrame->IsBodyTracked())
	{
		auto rightHandJoint = bodyFrame->getJointsData()[JointType_HandRight];
		swipeDetector.Add(rightHandJoint);

		//auto leftHandJoint = bodyFrame->getJointsData()[JointType_HandLeft];
		//swipeDetector.Add(leftHandJoint);

		swipeDetector.DetectGestures(dt);
	}	
}

void KinectPlayer::UpdatePlayerCameraTracking(Renderer* r)
{
	if (isVisionMode)
	{
		r->Matrices.projection = AxisOffsetEyeFrustrum();
	}
	else
	{
		r->Matrices.projection = originalPerspective;
	}
}

mat4 KinectPlayer::AxisOffsetEyeFrustrum()
{
	float height = 3.0f;
	float width = 4.0f;
	float distance = -5.0f;
	vec3 pc(-width, height, distance);
	vec3 pa(-width, -height, distance);
	vec3 pb(width, -height, distance);

	vec3 pe = *head->m_translation;
	float offsetAmplifier = 10.0f;
	pe.x *= offsetAmplifier;
	pe.y *= offsetAmplifier;
	
	vec3 vr = glm::normalize(pb - pa);
	vec3 vu = glm::normalize(pc - pa);
	vec3 vn = glm::normalize(glm::cross(vr, vu));
	
	vec3 va = pa - pe;
	vec3 vb = pb - pe;
	vec3 vc = pc - pe;

	auto d = -glm::dot(va, vn);

	float n = 0.01f;
	float l = glm::dot(vr, va) * n / d;
	float r = glm::dot(vr, vb) * n / d;
	float b = glm::dot(vu, va) * n / d;
	float t = glm::dot(vu, vc) * n / d;
	float f = 100.0f;

	auto perspective = glm::frustum(l, r, b, t, n, f);
	auto rot = glm::mat4(
		vec4(vr.x, vu.x, vn.x, .0f),
		vec4(vr.y, vu.y, vn.y,.0f),
		vec4(vr.z, vu.z, vn.z,.0f),
		vec4(.0f, .0f, .0f, 1.0f));
	
	//pe.z *= -1.0f;
	auto translate = glm::translate(mat4(), -pe);
	auto mat =  perspective * rot * translate;

	return mat;
}



void KinectPlayer::RightCut()
{
	//std::cout << "right cut!!!" << std::endl;
}

void KinectPlayer::LeftCut()
{
	//std::cout << "left cut!!!" << std::endl;
}


void KinectPlayer::Destroy()
{
	Actor::Destroy();
	head->Destroy();
	body->Destroy();
	//leftShoulder->Destroy();
	leftUpperArm->Destroy();
	leftLowerArm->Destroy();
	//leftHand->Destroy();
	//rightShoulder->Destroy();
	rightUpperArm->Destroy();
	rightLowerArm->Destroy();
	//rightHand->Destroy();
	//leftThigh->Destroy();
	//leftHive->Destroy();
	//rightThigh->Destroy();
	//rightHive->Destroy();

	delete head;
	delete body;
	delete leftShoulder;
	delete leftUpperArm;
	delete leftLowerArm;
	delete leftHand;
	delete rightShoulder;
	delete rightUpperArm;
	delete rightLowerArm;
	delete rightHand;
	delete leftThigh;
	delete leftHive;
	delete rightThigh;
	delete rightHive;
	delete bone;

	swipeDetector.Destroy();
}

KinectPlayer::~KinectPlayer(void)
{
	Destroy();

}
