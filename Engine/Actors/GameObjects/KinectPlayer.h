#pragma once
#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <Actors\Components\MeshComponents\MeshRenderer.h>
#include <Actors\Components\physics\RigidBody.h>
#include <KinectModule\KinectBodyFrame.h>
#include <KinectModule\KinectSwipeDetector.h>
#include <Game\GameObjects\Enemies\SphereEnemy.h>
#include <Game\GameObjects\Enemies\CubeEnemy.h>
class PxPhysicsWorld;
class ClothActor;
struct ENGINE_SHARED JointMomentumData
{
	vec3 prevPos{ .0f };
	vec3 changeInMomentum{ .0f };
	vec3 totalMomentum{ .0f };
	float momentumMagnitude{ .0f };
};

class ENGINE_SHARED Limb :
	public Actor
{
	void onContact(Actor* other) override
	{
		//auto obj = dynamic_cast<Actor*>(other);
		if (other != nullptr)
		{
			for (size_t i = 0; i < 100; i++)
			{
				i = i + 1;
			}
		}
	}

};

class ENGINE_SHARED KinectPlayer
	: public Actor
{	
protected:
	KinectBodyFrame* bodyFrame;
	KinectSwipeDetector swipeDetector;
	physx::PxPhysicsWorld* m_world;
	Actor* head;
	Actor* body;
	Actor* leftShoulder;
	Limb* leftUpperArm;
	Limb* leftLowerArm;
	Actor* leftHand;
	Actor* rightShoulder;
	Limb* rightUpperArm;
	Limb* rightLowerArm;
	Actor* rightHand;
	Limb* leftThigh;
	Limb* leftHive;
	Limb* rightThigh;
	Limb* rightHive;
	vec4 faceOrientation;
	Camera* playerCamera;
	Renderer* renderer;
	ICoordinateMapper* coordinateMapper = nullptr;

	virtual PxVec3 CastRightHandRay();
	virtual PxVec3 CastLeftHandRay();

	virtual void RightHandRayHitCallback(Actor* obj) {}
	virtual void LeftHandRayHitCallback(Actor* obj) {}
	virtual void RightHandEnergyHitMax() {}
	virtual void LeftHandEnergyHitMax() {}

	virtual void LowerLeftArmCollisionBuilt() {}
	virtual void LowerRightArmCollisionBuilt() {}
	virtual void LowerLeftLegCollisionBuilt() {}
	virtual void LowerRightLegCollisionBuilt() {}

private:
	mat4 originalPerspective;

	float aspect_ratio;
	float postureTimeTolerance;
	float m_energyDamping = 0.35f;
	float m_maxEnergyCap= 0.2f;

	JointMomentumData m_leftHandKinematicEnergy ;
	JointMomentumData m_rightHandKinematicEnergy;
	JointMomentumData m_rightFootKinematicEnergy;
	JointMomentumData m_leftFootKinematicEnergy ;

	Renderable* bone = nullptr;

	bool isTrackingHead = false;

	bool isVisionMode;

	void Init() override;
	void InitMeshes(Renderer* r);
	void InitGestureDetection();
	
	void UpdateBone(Actor* bodyPart,
		const vec4* bPointJoints,
		JointType j1,
		JointType j2,
		const float weight = 0.06f,
		vec3 offset = vec3(0.0f));

	void UpdateJointsKinematicEnergy(float dt);

	float CalculateBodyWidth();

	void CalcJointMomentum(JointMomentumData& enegyData,
		Joint* jointPos,
		float dt);

	void UpdateVision(float dt);

	ICoordinateMapper* GetCoordinateMapper();
public:	


	explicit KinectPlayer(KinectBodyFrame* kBody,
						  Renderer* r,
						  physx::PxPhysicsWorld* m_world = nullptr);
	virtual ~KinectPlayer(void);

	vec3 GetLeftHandKinematicEnergy() const;
	vec3 GetRightHandKinematicEnergy() const;
	vec3 GetLeftFootKinematicEnergy() const;
	vec3 GetRightFootKinematicEnergy() const;

	bool IsInVisionMode() const;

	void SetAspectRatioForMeshPosition(const float aspectRatio);

	virtual void Destroy() override;
	virtual void Update(float dt) override;
	virtual void Draw(Renderer* r, GLuint passIndex = 0) override;
	void AttachCameraToHead(Camera* cam);

	void GestureDetection(float dt);

	void UpdatePlayerCameraTracking(Renderer* r);
	
	void DrawLeftHand(
		const vec4* bJointPoints);

	void DrawRightHand(
		const vec4* bJointPoints);

	void UpdateBones();

	void RightCut();
	void LeftCut();

	mat4 AxisOffsetEyeFrustrum();

	mat3 GetBodyOrientation();
};

