#pragma once
#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <RenderComponents\Renderer.h>


class ENGINE_SHARED MeshRenderer
	: public ActorComponent
{
	Renderable* mesh;
	vec3 offset;
	bool m_applyParentScale = true;
public:
	explicit MeshRenderer(Renderable* mesh = nullptr,
		bool isVisible = true);

	~MeshRenderer(void);

	void EnableCastShadow();

	void SetMesh(Renderable* mesh,
				vec3 offset = vec3(0.0f),
				bool disposeOldMesh = true);

	void setTOffsetransform(vec3 pscale = vec3(1.0f), 
		mat4 rot = mat4(),
		vec3 pos = vec3(),
		vec3 offset = vec3());

	void SetOffset(const vec3& offset);
	void SetApplyParentScale(bool parentScale);
	void Update(float dt) override;
	void Draw(Renderer* r, GLuint passIndex = 0) override;
	void Destroy() override;

	void SetVisibility(bool visible);

	Component_id GetComponentId() const override; 

};

