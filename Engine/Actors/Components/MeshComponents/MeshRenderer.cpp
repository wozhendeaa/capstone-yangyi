#include "MeshRenderer.h"
#include <MemoryDebug.h>

MeshRenderer::MeshRenderer(Renderable* m ,
						   bool isVisible)
{
	mesh = m;
	mesh->visible = isVisible;
}

void MeshRenderer::EnableCastShadow()
{
	mesh->setShaderForPass(1, 3);
}

Component_id MeshRenderer::GetComponentId() const
{
	static Component_id id = 
		typeid(MeshRenderer).name();
	return id;
}

void MeshRenderer::SetMesh(Renderable* m,
						   vec3 off,
							  bool disposeOldMesh)
{
	if(mesh != nullptr && disposeOldMesh)
	{
		mesh->shutDown();
		delete mesh;
	}
	
	mesh = m;

	offset = off;
}



void MeshRenderer::SetVisibility(bool visible)
{
	mesh->visible = visible;
}


void MeshRenderer::SetOffset(const vec3& off)
{
	offset = off;
}

void MeshRenderer::setTOffsetransform(vec3 pscale, 
		mat4 rot ,
		vec3 pos ,
		vec3 offset)
{
	mesh->setTOffsetransform(pscale, rot, pos, offset);
		
}


void MeshRenderer::Update(float dt)
{
	dt;
	*mesh->scale = glm::scale(*parent->m_scale);
	*mesh->rotation = glm::mat4_cast(*parent->m_rotation);

	auto offsetMat = glm::translate(mat4(), *parent->m_translation);
	*mesh->translation =  glm::translate(offsetMat, offset);
	*mesh->where = *parent->m_transform;
	
}

void MeshRenderer::SetApplyParentScale(bool parentScale)
{
	m_applyParentScale = parentScale;
}


void MeshRenderer::Draw(Renderer* r, GLuint passIndex)
{
	r->Draw(mesh, passIndex);
}


void MeshRenderer::Destroy() 
{
	if(mesh != nullptr)
	{
		mesh->shutDown();
		delete mesh;
		mesh = nullptr;
	}
}

MeshRenderer::~MeshRenderer(void)
{
	Destroy();

}
