#pragma once
#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <renderer.h>

class ENGINE_SHARED GUITexture :
	public ActorComponent
{
	BatEngine::Renderable* guiTexture;
public:
	explicit GUITexture(BatEngine::Renderer* r,
					   unsigned shaderIndex,
					   const FREE_IMAGE_FORMAT& fmt,
					   const char* filePath, 
					   float x,
					   float y,
					   float width = 0.15f,
					   float height = 0.25f,
					   bool isNearPlane = true,
					   const char* uniformName = "color_tex",
					   bool flipHorizontal = false,
					   bool flipVertical = false   );

	void Draw(BatEngine::Renderer* r, GLuint passIndex = 0) override;

	void Destroy() override;

	Component_id GetComponentId() const override; 



	~GUITexture(void);


};

