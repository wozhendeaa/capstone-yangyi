#pragma once
#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <Physics\PxPhysicsWorld.h>

using physx::PxClothFabric;
using physx::PxCloth;

class ENGINE_SHARED Cloth
	: public ActorComponent
{
	void Init(physx::PxPhysicsWorld* world);
	GeometryInfo* verts;
public:
	PxCloth* clothObject;
	vec3 offsetPos;
	explicit Cloth(physx::PxPhysicsWorld* world, 
		GeometryInfo* clothVerts);
	~Cloth(void);

	void Update(float dt) override;
	void Destroy() override;
	Component_id GetComponentId() const override; 
};

