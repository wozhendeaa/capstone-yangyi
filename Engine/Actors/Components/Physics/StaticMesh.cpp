#include "StaticMesh.h"
#include <MemoryDebug.h>
#include <Physics\PxPhysicsWorld.h>

using physx::PxPhysicsWorld;

using physx::PxCreateStatic;


StaticMesh::StaticMesh(PxPhysicsWorld* world,
					   float staticFric ,
					   float dynamicFric,
					   float rest)
{
	material = world->GetSDK()->createMaterial(staticFric, dynamicFric, rest);
	isInitialized = false;
}

void StaticMesh::InitializeWithPlane(PxPhysicsWorld* world, quat q)
{
	if(!isInitialized)
	{
		world;
		auto plane = physx::PxPlane();
		m_scale = vec3(1.0f);
		quaternion = q;
		position = vec3(0.0f);
		//auto pxTrans = physx::ToPxTransform(pos, quat);
		body = physx::PxCreatePlane(*world->GetSDK(), plane, *material);
		world->GetPhysicsWorld()->addActor(*body);
		isInitialized = false;
	}

}

void StaticMesh::InitializeWithBox(PxPhysicsWorld* world,
								   vec3& scale,
								   quat& quat,
								   vec3& pos)
{
	if(!isInitialized)
	{
		m_scale = scale;
		quaternion = quat;
		position = pos;
		auto pxTrans = physx::ToPxTransform(pos, quat);
		auto geo = physx::PxBoxGeometry(scale.x, scale.y, scale.z );

		body = PxCreateStatic(*world->GetSDK(), 
			pxTrans,
			geo, 
			*material);	

		world->GetPhysicsWorld()->addActor(*body);
		isInitialized = false;
	}
}


Component_id StaticMesh::GetComponentId() const
{
	static Component_id id = 
		(typeid(StaticMesh).name());
	return id;
}

void StaticMesh::Update(float dt)
{
	dt;
	parent->setTransform(m_scale, quaternion ,position);

}

void StaticMesh::Destroy() 
{
	body->release();
	material->release();

}

StaticMesh::~StaticMesh(void)
{
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );

}
