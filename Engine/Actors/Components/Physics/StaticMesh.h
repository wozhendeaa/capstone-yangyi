#pragma once
#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <Physics\PxPhysicsWorld.h>

using physx::PxRigidStatic;


class ENGINE_SHARED StaticMesh
	: public ActorComponent
{
	bool isInitialized;
	vec3 m_scale;
	quat quaternion;
	vec3 position;
public:
	PxRigidStatic* body;
	PxMaterial* material;
	
	
	explicit StaticMesh(physx::PxPhysicsWorld* world, 
		float staticFric = 0.5f,
		float dynamicFric = 0.45f,
		float rest = 0.8f);

	~StaticMesh(void);

	void Update(float dt) override;
	void Destroy() override;
	Component_id GetComponentId() const override; 

	void InitializeWithPlane(physx::PxPhysicsWorld* sdk, 
		quat q = quat());

	void InitializeWithBox(physx::PxPhysicsWorld* sdk,
						   vec3& scale,
						   quat& quat,
						   vec3& pos);
};

