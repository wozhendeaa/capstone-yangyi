#include "RigidBody.h"
#include <MemoryDebug.h>

using physx::PxCreateDynamic;
using physx::PxPhysicsWorld;

class MeshRenderer;
RigidBody::RigidBody(PxPhysicsWorld* world,
					 float staticFric,
					 float dynamicFric,
					 float rest,
					 bool isKinema)
{
	material = world->GetSDK()->createMaterial(staticFric, dynamicFric, rest);
	isInitialized = false;
	isKinematic = isKinema;
	body = nullptr;
}


void RigidBody::InitializeWithBox(PxPhysicsWorld* world,
								  vec3& scale,
								  quat& quat,
								  vec3& pos,
								  float density,
								  vec3 offset)
{
	if(!isInitialized)
	{
		m_scale = scale;
		quaternion = quat;
		position = pos;
		auto rot = glm::mat4_cast(quaternion);
		auto transform = glm::translate(mat4(), pos);
		auto pxTrans = PxTransform(physx::ToPxMat4(transform));
		auto geo = physx::PxBoxGeometry(scale.x, scale.y , scale.z );
		//auto geo = physx::PxBoxGeometry	(scale.x * 0.5f, scale.y* 0.5f, scale.z* 0.5f );
		auto pxOffset = physx::ToPxTransform(offset);

		body = PxCreateDynamic(*world->GetSDK(), 
			pxTrans,
			geo, 
			*material,
			density,
			pxOffset);	

		MakeBody();

		world->GetPhysicsWorld()->addActor(*body);
		if(!isKinematic)
		{
			body->setGlobalPose(pxTrans);
			physx::PxRigidBodyExt::updateMassAndInertia(*body,
				density);
		}
		else 
		{
			body->setKinematicTarget(pxTrans);
		}

		isInitialized = false;
	}
}

void RigidBody::InitializeWithSphere(PxPhysicsWorld* world,
									 float& radius,
									 quat& quat,
									 vec3& pos,
									 float density )
{
	if(!isInitialized)
	{
		m_scale = vec3(radius);
		quaternion = quat;
		position = pos;
		auto pxTrans = physx::ToPxTransform(pos, quat);
		auto geo = physx::PxSphereGeometry(radius);
		body = PxCreateDynamic(*world->GetSDK(), 
			pxTrans,
			geo, 
			*material,
			density);	

		MakeBody();
		world->GetPhysicsWorld()->addActor(*body);
		if(!isKinematic)
		physx::PxRigidBodyExt::updateMassAndInertia(*body,
			density);
		isInitialized = false;

	}
}

void RigidBody::SetBodyUserData(void* data)
{
	body->userData = data;
}

void RigidBody::InitializeWithCapsule(physx::PxPhysicsWorld* sdk,
	vec3& pos,
	quat quat,
	float radius,
	float height,
	float density)
{
	if (!isInitialized)
	{
		m_scale = vec3(radius);
		quaternion = quat;
		position = pos;
		auto pxTrans = physx::ToPxTransform(pos, quat);
		auto geo = physx::PxCapsuleGeometry(radius, height) ;
		body = PxCreateDynamic(*sdk->GetSDK(),
			pxTrans,
			geo,
			*material,
			density);

		MakeBody();
		sdk->GetPhysicsWorld()->addActor(*body);
		if (!isKinematic)
			physx::PxRigidBodyExt::updateMassAndInertia(*body,
			density);
		isInitialized = false;

	}
}

void RigidBody::SetEnableGravity(bool enabled)
{
	body->setActorFlag(physx::PxActorFlag::eDISABLE_GRAVITY, !enabled);
}

void RigidBody::MakeBody()
{
	if(isKinematic)
	{
		MakeBodyKinematic();
	}
	else
	{
		MakeBodyDynamic();
	}
}


void RigidBody::MakeBodyKinematic()
{
	body->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
	isKinematic = true;
}

void RigidBody::MakeBodyDynamic()
{

	body->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, false);
	isKinematic = false;

}

void RigidBody::SetCollisionEnabled(bool enabled)
{
	if(body->getNbShapes() > 0)
	{
		physx::PxShape* shape;
		body->getShapes(&shape, 1);

		if(enabled)
		{
			shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, false);
			shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, true);
		}
		else
		{
			shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
			shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
		}
	}
}

bool RigidBody::IsTrigger()
{
	
	physx::PxShape* shape;
	body->getShapes(&shape, 1);
	auto flags = shape->getFlags();
	auto isBody = flags.isSet(physx::PxShapeFlag::eTRIGGER_SHAPE);
	auto isSimulated = flags.isSet(physx::PxShapeFlag::eSIMULATION_SHAPE);
	return isBody && !isSimulated;
}

bool RigidBody::IsKinematic()
{
	return isKinematic;
}


Component_id RigidBody::GetComponentId() const
{
	static Component_id id = typeid(RigidBody).name();

	return id;
}

void RigidBody::Update(float dt)
{
	dt;
	if(body != nullptr)
	{
		auto bodyFlags = body->getRigidBodyFlags();

		if(isKinematic)
		{
			auto offset = glm::translate(mat4(), parent->m_offset);
			auto transform = glm::translate(mat4(), *parent->m_translation);
			auto rot = glm::mat4_cast(*parent->m_rotation);
			transform = transform * rot * offset;
			auto mat = physx::ToPxTransform(transform);
			body->setKinematicTarget(mat);
		}
		else 
		{
			auto trans = body->getGlobalPose();
			parent->setTransform(m_scale * 2.0f, physx::ToQuat(trans.q), 
				physx::ToVec3(trans.p));
		}
	}
}

void RigidBody::Destroy() 
{
	material->release();
	auto shapeNum = body->getNbShapes();
	physx::PxShape* shape;
	body->getShapes(&shape, shapeNum);
	for (size_t i = 0; i < shapeNum; i++)
	{
		body->detachShape(shape[i]);
	}
	body->release();
}

RigidBody::~RigidBody(void)
{
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );

}
