#include "Cloth.h"
#include <ObjLoader\ObjLoader.h>
#include <MemoryDebug.h>

using physx::PxClothParticle;
using physx::PxVec3;
using physx::PxU32;
using physx::PxClothMeshDesc;
using physx::PxClothFlags;
using physx::PxPhysicsWorld;
const float offset = 0.5f;

Cloth::Cloth(PxPhysicsWorld* world,
			 		GeometryInfo* clothVerts)
{
	verts = clothVerts;
	Init(world);
}

void Cloth::Init(PxPhysicsWorld* world)
{
	const auto numVerts = verts->num_vertex;
	vec4* vertices = new vec4[numVerts];

	auto geoVerts = reinterpret_cast<Vertex*>(verts->verts);
	for (unsigned i = 0; i < numVerts; i++)
	{
		auto mass = (i == 0 || i == 6 || i == 8 || i == 10 || i ==12)? 0.0f : 1.0f;
		vertices[i] = vec4(geoVerts[i].position, mass);
	}

	PxU32* indices = new PxU32[verts->num_index];
	for (unsigned i = 0; i < verts->num_index; i++)
	{
		auto index = static_cast<byte>(verts->indices[i]);
		indices[i] = static_cast<PxU32>(index);
	}

	PxClothMeshDesc meshDesc;
	meshDesc.points.data = &vertices[0].x;
	meshDesc.points.count = numVerts;
	meshDesc.points.stride = sizeof(vec4);

	meshDesc.invMasses.data = &vertices[0].w;
	meshDesc.invMasses.count = numVerts;
	meshDesc.invMasses.stride = sizeof(vec4);

	meshDesc.triangles.data = indices;
	meshDesc.triangles.count = 50;
	meshDesc.triangles.stride = sizeof(PxU32) * 3;
	
	auto sdk = world->GetSDK();
	auto clothFabric = PxClothFabricCreate(*sdk,
	meshDesc, 
	PxVec3(0,-1,0));

	PxTransform pose = PxTransform(PxVec3(0.0f, .0f, -5.0f));
	clothObject = sdk->createCloth(pose, *clothFabric, (const PxClothParticle*)meshDesc.points.data, 
	PxClothFlags());
	world->GetPhysicsWorld()->addActor(*clothObject); 
	clothObject->setClothFlag(physx::PxClothFlag::eGPU, true);
	clothObject->setDampingCoefficient(PxVec3(0.1f));
	clothObject->setSolverFrequency(240);

	clothObject->setStiffnessFrequency(10.0f);

	clothObject->setDampingCoefficient(PxVec3(0.2f)); // damp local particle velocity
	clothObject->setLinearDragCoefficient(PxVec3(0.2f)); // transfer frame velocity
	clothObject->setAngularDragCoefficient(PxVec3(0.2f)); // transfer frame rotation

	clothObject->setLinearInertiaScale(PxVec3(0.8f, 0.6f, 0.8f));

	clothObject->setAngularInertiaScale(PxVec3(1.0f));

	clothObject->setCentrifugalInertiaScale(PxVec3(0.3f));


	delete [] vertices;
	delete [] indices;
}


Component_id Cloth::GetComponentId() const
{
	static Component_id id = 
		typeid(this).name();

	return id;
}

void Cloth::Update(float dt)
{
	dt;

	auto geoVerts = reinterpret_cast<Vertex*>(verts->verts);
	auto particles = clothObject->lockParticleData()->particles;
	auto p = clothObject->getGlobalPose().p;
	auto numParticle = clothObject->getNbParticles();

	for (unsigned i = 0; i < numParticle; i++)
	{
		auto pos = physx::ToVec3(particles[i].pos);;
		geoVerts[i].position = pos;
	}

	glBindBuffer(GL_ARRAY_BUFFER, verts->buffer_id);
	glBufferSubData(GL_ARRAY_BUFFER, verts->offset, (Vertex::STRIDE) * verts->num_vertex, (void*)geoVerts);
}

void Cloth::Destroy() 
{
	clothObject->release();
}

Cloth::~Cloth(void)
{
}
