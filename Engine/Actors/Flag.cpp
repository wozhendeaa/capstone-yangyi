#include "Flag.h"
#include <glm\gtc\matrix_transform.hpp>
ObjData Flag::geo_data = ObjData();

Flag::Flag()
{
	initModel();
}

void Flag::initModel()
{

	ObjLoader loader;
	loader.loadFromFile("model/obj/flag.bin");
	geo_data = loader.getModel();	
	r = new Renderable;
}

void Flag::Update()
{
	if(r->where != nullptr)
		*r->where = glm::translate(glm::mat4(), position);
}


Flag::~Flag()
{
//	delete r;
}