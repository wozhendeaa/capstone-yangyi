#pragma once
#include <Actors\Actor.h>
#include <Physics\PxPhysicsWorld.h>
#include <Random.h>

enum ENGINE_SHARED ParticleType
{
	BILLBOARD,
	CUBE,
	SHPERE,
};

using physx::PxVec3;
class ENGINE_SHARED ParticleEmitter
{
public:
	std::vector<PxVec3> forceBuffer;
	PxVec3 m_pos;
	PxVec3 m_velocity;
	PxVec3 m_force;	
	float rate {10.0f};
	bool useGravity = false;

	explicit ParticleEmitter(
		PxVec3 pos,
		PxVec3 vel, 
		PxVec3 force);

	void ComputeVelocity(PxVec3& vel, float offsetRange);
	void ComputePosition(PxVec3& pos, float offsetRange);
	void ComputeDuration(physx::PxReal& duration, float lt, float offsetRange);

	void InitPositions(std::vector<PxVec3>& positions, float offsetRange = 0.1f);
	void InitVelocities(std::vector<PxVec3>& vels, float offsetRange = 0.1f);
	void InitLifeTime(std::vector<physx::PxReal>& lifeTimes, 
		float duration = 0.4f,
		float offsetRange = 0.3f);


	~ParticleEmitter();
};

