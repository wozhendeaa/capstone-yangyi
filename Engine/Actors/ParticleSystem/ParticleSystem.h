#pragma once
#include <ExportHeader.h>
#include <Actors\Actor.h>
#include <Physics\PxPhysicsWorld.h>
#include "ParticleEmitter.h"
using physx::PxParticleSystem;

class ENGINE_SHARED ParticleSystem :
	public Actor
{

	std::vector<PxVec3> m_positions;
	std::vector<PxVec3> m_velocities;
	std::vector<PxU32>  m_indicies;
	std::vector<physx::PxMat33> m_orientations;
	physx::PxParticleExt::IndexPool* m_indexPool;
	Renderable* particleRenderer{ nullptr };
	Renderer* renderer{ nullptr };

	std::vector<float> m_lifes;
	PxU32 m_numParticles { 0 };
	PxU32 m_maxParticles { 0 };
	float uniformLife = .0f;
	float lifeOffset = .0f;
	float positionOffset = .0f;
	float velocityOffset = .0f;

	float m_startOpacity = .0f;
	float m_endOpacity = .0f;
	float m_startScale = .0f;
	float m_endScale = .0f;
	float m_maxMotionDistance = .0f;

	bool useLifeTime{ false };
	bool loop{ true };
	bool fadeOverDistance{ false };
	bool scaleOverDistance{ false };
	
	ParticleType rendererType;
	/*The maximum distance a particle can travel during one
	simulation step. High values may hurt performance,
	while low values may restrict the particle velocity too much.*/
	void InitMaxMotionDistance(float val);
	/* Defines the minimum distance between particles and the surface of
	rigid actors that is maintained by the collision system.*/
	void InitRestOffset(float val);
	/*Defines the distance at which contacts between particles and
	rigid actors are created. The contacts are internally used to avoid jitter and sticking.
	It needs to be larger than restOffset*/
	void InitContactOffset(float val);
	/*A hint for the PhysX SDK to choose the particle grouping granularity
	for proximity tests and parallelization*/
	void InitGridSize(float val);

	void InitEnableGPUAccleration(bool enabled);

	void InitEnableTwoWayCollision(float val);

	physx::PxPhysicsWorld *world = nullptr;

	void InitRendererWithBillBoard(vec3 scale, TextureInfo texture);
	void InitRendererWithCube(vec3 scale, TextureInfo texture);
	void InitRendererWithShpere(vec3 scale, TextureInfo texture);

public:
	PxParticleSystem* ps = nullptr;
	ParticleEmitter* emitter = nullptr;
	PxMaterial* material = nullptr;

	explicit ParticleSystem(physx::PxPhysicsWorld *world,
							Renderer* r,
							int maxParticles,
							bool perParticleOffset = false);
	~ParticleSystem();

	void SetRenderable(TextureInfo texture = TextureInfo(),
		vec3 scale = vec3(1.0f),
		ParticleType type = ParticleType::BILLBOARD);

	void EnableFadeOverDistance(float startOpacity = 1.0f, float endOpacity = .0f);

	void EnableScaleOverDistance(float startScale = 1.0f, float endScale = .1f);

	void DisableFadeOverDistance();

	void DisableScaleOverDistance();

	void SetDamping(float damping = .0f);

	void SetEmissionPosition(vec3 pos);

	void Destroy() override;

	void InitParticleSystem(
		PxVec3 pos = PxVec3(.0f, .0f, 1.0f),
		float positionOffset = 0.01f,
		PxVec3 vel = PxVec3(.0f, .0f, 1.0f),
		float velocityOffset = 0.1f,
		float maxMotionDistance = .33f,
		float restOffset = .0143f,
		float contactOffset = .0143f * 2,
		bool enableTwoWayCollision = true,
		bool enableGPU = true,
		float gridSize = 3.0f);

	void CreateParticles(
		bool useGravity = false, 
		physx::PxReal mass = 1.0f);

	void SetProperties(float resitution = 0.5f,
		float staticFriction = 0.6f,
		float dynamicFriction = 0.5f,
		float damping = 0.3f);

	void SetEnableGravity(bool enabled);

	void SetEnableParticleSystem(bool enabled);

	void setUseLifetime(bool use);
	bool useLifetime();
	void setLifetime(float lt, float offsetRange = .0f);
	void SetLoop(bool loops);
	bool GetIfLoop() const;

	void SetPositions(physx::PxVec3& pos, float offsetRange = .0f);
	void SetVelocities(physx::PxVec3& vel, float offsetRange = .0f);

	void SetParticletPosition(physx::PxVec3& pos, uint index, float offsetRange = .0f);
	void SetParticletVelocity(physx::PxVec3& vel, uint index, float offsetRange = .0f);

	const std::vector<PxVec3>& getPositions();
	const std::vector<float>& getLifetimes();
	const std::vector<PxVec3>& getVelocities();
	const std::vector<physx::PxMat33>& getOrientations();

	PxU32 CreateParticles(const physx::PxParticleCreationData& particles, 
		physx::PxStrideIterator<PxU32>* particleIndices = nullptr, 
		physx::PxReal lifetime = 0.0f);


	void Update(float dt) override;

	void Draw(Renderer* r, GLuint passIndex = 0) override;
};

