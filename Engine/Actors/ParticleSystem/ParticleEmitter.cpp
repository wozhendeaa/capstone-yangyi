#include "ParticleEmitter.h"
#include <MemoryDebug.h>
#include <Random.h>


ParticleEmitter::ParticleEmitter(
	PxVec3 pos,
	PxVec3 vel,
	PxVec3 force) : 
	m_pos(pos),
	m_velocity(vel),
	m_force(force)
{

}

void ParticleEmitter::ComputeVelocity(PxVec3& vel, float offsetRange)
{
	if (offsetRange > .0f)
	{
		auto offsetX = Random::RandomInRange(offsetRange, .0f);
		auto offsetY = Random::RandomInRange(offsetRange, .0f);
		auto offsetZ = Random::RandomInRange(offsetRange, .0f);
		vel = m_velocity + PxVec3(offsetX, offsetY, offsetZ);
	}
	else
	{
		vel = m_velocity;
	}

}

void ParticleEmitter::ComputePosition(PxVec3& pos, float offsetRange)
{
	if (offsetRange > .0f)
	{
		auto offsetX = Random::RandomInRange(offsetRange, .0f);
		auto offsetY = Random::RandomInRange(offsetRange, .0f);
		auto offsetZ = Random::RandomInRange(offsetRange, .0f);
		pos = m_pos + PxVec3(offsetX, offsetY, offsetZ);
	}
	else
	{
		pos = m_pos;
	}
}

void ParticleEmitter::ComputeDuration(physx::PxReal& ref, float duration, float offsetRange)
{
	ref = duration + Random::RandomInRange(offsetRange, .0f);
}


void ParticleEmitter::InitPositions(std::vector<PxVec3>& positions,
	float offsetRange)
{
	for (auto& p : positions)
	{
		ComputePosition(p, offsetRange);
	}
}

void ParticleEmitter::InitVelocities(std::vector<PxVec3>& vels,
	float offsetRange)
{
	for (auto& v : vels)
	{
		ComputeVelocity(v, offsetRange);
	}
}




void ParticleEmitter::InitLifeTime(std::vector<physx::PxReal>& lifeTimes,
	float duration,
	float offsetRange)
{
	for (auto& d : lifeTimes)
	{
		ComputeDuration(d, duration, offsetRange);
	}

}


ParticleEmitter::~ParticleEmitter()
{
}
