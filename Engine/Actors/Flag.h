#pragma once 
#include <glm\glm.hpp>
#include <ObjLoader\ObjLoader.h>
#include <Qt\qstring.h>
#include <RenderComponents\Renderable.h>
using BatEngine::Renderable;
class ENGINE_SHARED Flag 
{
	void initModel();
public:
	Flag(void);
	~Flag(void);

	glm::vec3 position;
	static ObjData geo_data;
	Renderable* r;
	void Update();

};

