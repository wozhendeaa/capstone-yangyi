#include "DebugButton.h"
#include <MemoryDebug.h>

void DebugButton::buttonClicked()
{
	delegation();
}

DebugButton::DebugButton(const char* text,
						 fastdelegate::FastDelegate0<> delegation)
{
	button = new QPushButton(text);
	this->delegation = delegation; 
	connect(button, SIGNAL(clicked(bool)), this, SLOT(buttonClicked()));
}

DebugButton::~DebugButton()
{

}