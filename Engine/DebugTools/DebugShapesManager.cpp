#include "DebugShapesManager.h"
#include <MemoryDebug.h>
#include <glm\gtx\rotate_vector.hpp>

#ifdef DEBUG_SHAPE_ON
bool DebugShapesManager::instanceFlag = false;
DebugShapesManager* DebugShapesManager::instance = nullptr;
DebugShapesManager::DebugShapesManager() 
{
	renderer = nullptr;
	coneGeometry  = nullptr;
	stemGeometry  = nullptr;
	lineGeo= nullptr;
	pointGeo= nullptr;
	cubeGeo= nullptr;
	sphereGeo= nullptr;
	vectorGeo= nullptr;
	unitVectorGeo= nullptr;
}

void DebugShapesManager::setRenderer(Renderer* r)
{
	renderer = r;
}


DebugShapesManager* DebugShapesManager::getInstance()
{
	if(!instanceFlag)
	{
		instance = new DebugShapesManager;
		instanceFlag = !instanceFlag;
	}

	return instance;
}

void  DebugShapesManager::AddLine( 
		const vec3& fromPosition, 
		const  vec3& toPosition,
		glm::vec4  color,
		float  duration,
		bool  depthEnabled)
{	
	DebugShape shape;
	duration;
	depthEnabled;
	//construct line vert data
	Neumont::Vertex v;
	v.position = fromPosition;
	v.color = color;

	Neumont::Vertex v2;
	v2.position = toPosition;
	v2.color = color;

	Neumont::Vertex verts[2] = {v, v2};
	ushort indicies[2] = {0, 1};

	shape.shape_data.verts = verts;
	shape.shape_data.indices = indicies;

	if(lineGeo == nullptr)
	{
		lineGeo = renderer->addGeometry(shape.shape_data.verts, 2, reinterpret_cast<uint*>(shape.shape_data.indices), 2, sizeof(vec3), GL_UNSIGNED_SHORT, GL_LINES);	
	}
	//auto r = renderer->addRenderable(*lineGeo, mat4(), TextureInfo(), renderer->shaders[0]->shaderInfoIndex, depthEnabled, true);
	//if (r != nullptr)
	//{
	//	renderer->addRenderableUniformParameter(r, "world_model", ShaderUniformParameterType::SHADER_MAT4, (char*)r->where);

	//	//renderer->addRenderableUniformParameter(r, "enable_vertex_color", ShaderUniformParameterType::SHADER_BOOL, (char*)r->vertex_color_on);
	//	shape.howShaderIndex = r->howShaderIndex;
	//	shape.geometry = lineGeo;
	//	*shape.where = *r->where;
	//	shape.duration = duration;

	//	shapes.push_back(shape); 
	//}
}


void  DebugShapesManager::AddPoint(
		const vec3& position,
		float  size,
		glm::mat4* transform,
		float  duration,
		bool  depthEnabled )
{
	DebugShape shape;
	duration;
	depthEnabled;
	//construct line vert data
	Neumont::Vertex v1;
	v1.position = vec3(1.0f, 0.0f, 0.0f) * size;
	v1.color = vec4(1, 0 , 0 , 0);

	Neumont::Vertex v1_2;
	v1_2.position = vec3(-1.0f, 0.0f, 0.0f) * size;
	v1_2.color = vec4(0, 0 , 0 , 0);

	Neumont::Vertex v2;
	v2.position = vec3(0.0f, 1.0f, 0.0f) * size;
	v2.color = vec4(0, 1 , 0 , 0);

	Neumont::Vertex v2_2;
	v2_2.position = vec3(0.0f, -1.0f, 0.0f) * size;
	v2_2.color = vec4(0, 0 , 0 , 0);

	Neumont::Vertex v3;
	v3.position = vec3(0.0f, 0.0f, 1.0f) * size;
	v3.color = vec4(0, 0 , 1 , 0);

	Neumont::Vertex v3_2;
	v3_2.position = vec3(0.0f, 0.0f, -1.0f) * size;
	v3_2.color = vec4(0, 0 , 0 , 0);

	Neumont::Vertex verts[6] = {v1, v1_2, v2, v2_2, v3, v3_2};
	ushort indicies[6] = {0, 1, 2, 3, 4, 5};

	shape.shape_data.verts = verts;
	shape.shape_data.indices = indicies;

	*transform = glm::translate(*transform, position);
	if(pointGeo == nullptr)
	{
		pointGeo = renderer->addGeometry(shape.shape_data.verts, 6, reinterpret_cast<GLuint*>(shape.shape_data.indices), 6, sizeof(vec3), GL_UNSIGNED_SHORT, GL_LINES);	
	}
	//auto r = renderer->addRenderable(*pointGeo, *transform, TextureInfo(), renderer->shaders[0]->shaderInfoIndex, depthEnabled, true);

	//if(r != nullptr)
	//{
	//	//renderer->addRenderableUniformParameter(r, "enable_vertex_color", ShaderUniformParameterType::SHADER_BOOL, (char*)&r->vertex_color_on);
	//	renderer->addRenderableUniformParameter(r, "world_model", ShaderUniformParameterType::SHADER_MAT4, (char*)r->where);
	//
	//	shape.howShaderIndex = r->howShaderIndex;
	//	
	//	shape.geometry= pointGeo;
	//	shape.where = r->where;
	//	shape.duration = duration;
	//	shapes.push_back(shape);
	//}
}

void  DebugShapesManager::AddCube( const vec3& position,
		glm::vec4 color,
		float  size,
		glm::mat4* transform,
		float  duration ,
		bool  depthEnabled)
{
	depthEnabled;
	duration;
	DebugShape* shape = new DebugShape;
	shape->shape_data = Neumont::ShapeGenerator::makeCube();
	for (uint i = 0; i < shape->shape_data.numVerts; i++)
	{
		shape->shape_data.verts[i].color = color;
		shape->shape_data.verts[i].position *= size;
	}
	*transform = glm::translate(*transform, position);
	
	if(cubeGeo == nullptr)
	{
		cubeGeo = renderer->addGeometry(shape->shape_data.verts,
			shape->shape_data.numVerts,
			reinterpret_cast<GLuint*>(shape->shape_data.indices),
			shape->shape_data.numIndices, sizeof(vec3),
			GL_UNSIGNED_SHORT, 
			GL_LINES);	
	}

	static auto r = renderer->addRenderable(*cubeGeo, transform, TextureInfo(), renderer->shaders[0]->shaderInfoIndex, depthEnabled, true);
	
	if (r != nullptr)
	{
		renderer->addRenderableUniformParameter(r, "world_model", ShaderUniformParameterType::SHADER_MAT4, (char*)r->where);

		shape->howShaderIndex = r->howShaderIndex;
		
		shape->geometry = cubeGeo;
		shape->where = r->where;
		shape->duration = duration;

		shapes.push_back(shape); 
	}

}

void  DebugShapesManager::AddSphere( 
		const vec3& position,
		float  radius,
		glm::vec4  color,
		glm::mat4*  transform,
		float  duration,
		bool  depthEnabled)
{
	duration;
	depthEnabled;
	DebugShape shape;
	shape.shape_data = Neumont::ShapeGenerator::makeSphere(10);
	for (uint i = 0; i < shape.shape_data.numVerts; i++)
	{
		shape.shape_data.verts[i].color = color;
		shape.shape_data.verts[i].position *= radius;
	}
	
	*transform = glm::translate(*transform, position);

	if(sphereGeo == nullptr)
	{
		sphereGeo = renderer->addGeometry(shape.shape_data.verts,shape.shape_data.numVerts , reinterpret_cast<GLuint*>(shape.shape_data.indices), shape.shape_data.numIndices, sizeof(vec3), GL_UNSIGNED_SHORT, GL_LINES);	
	}
	//auto r = renderer->addRenderable(*sphereGeo, *transform, TextureInfo(), renderer->shaders[0]->shaderInfoIndex,depthEnabled, true);
	//if (r != nullptr)
	//{
	//	shape.howShaderIndex = r->howShaderIndex;
	//	
	//	shape.geometry = sphereGeo;
	//	shape.where = r->where;
	//	shape.duration = duration;

	//	shapes.push_back(shape); 
	//}
}
		
void DebugShapesManager::addUnitVector( 
	vec3& vec,
	glm::vec4  color,
	glm::mat4* transform,
	float  duration,
	bool  depthEnabled)
{
	depthEnabled;
	duration;
	DebugShape shape;
	//construct line vert data
	Neumont::Vertex v1;
	v1.position = vec3(0.0f) ;
	v1.color = vec4(0.0f);

	Neumont::Vertex v1_2;
	v1_2.position = glm::normalize(vec * mat3(*transform));
	v1_2.color = color;

	Neumont::Vertex verts[2] = {v1, v1_2};
	ushort indicies[2] = {0, 1};

	shape.shape_data.verts = verts;
	shape.shape_data.indices = indicies;

	if(unitVectorGeo == nullptr)
	{
		unitVectorGeo = renderer->addGeometry(shape.shape_data.verts, 2, reinterpret_cast<GLuint*>(shape.shape_data.indices), 2, sizeof(vec3), GL_UNSIGNED_SHORT, GL_LINES);	
	}
	//auto r = renderer->addRenderable(*unitVectorGeo, *transform, TextureInfo(), renderer->shaders[0]->shaderInfoIndex,depthEnabled, true);

 //   
	//if (r != nullptr)
	//{
	//	renderer->addRenderableUniformParameter(r, "world_model", ShaderUniformParameterType::SHADER_MAT4, (char*)r->where);

	//	shape.howShaderIndex = r->howShaderIndex;
	//	
	//	shape.geometry= unitVectorGeo;
	//	shape.where = r->where;
	//	shape.duration = duration;

	//	shapes.push_back(shape); 
	//}
}

void DebugShapesManager::update(float dt)
{
	for (uint i = 0; i < shapes.size(); i++)
	{
		shapes[i]->past_time += dt;

		if (shapes[i]->past_time >= shapes[i]->duration || shapes[i]->alive_forever)
		{
			//auto r_id = shapes[i];
			//shapes[i]->shutDown();
			//shapes.erase(shapes.begin() + i);
		}
		else
		{
			renderer->Draw(dynamic_cast<Renderable*>(shapes[i]));
		}
	}
}

DebugShapesManager::VectorGraphic* DebugShapesManager::addVector(
	const glm::vec3& from, const glm::vec3& to, float lengthDelta)
{
	if(coneGeometry == nullptr)
	{
		auto cone_geo = ShapeGenerator::makeCone(10);
		auto cyn_geo = ShapeGenerator::makeCylinder(10);

		coneGeometry = renderer->addGeometry(cone_geo.verts, cone_geo.numVerts, (GLuint*)cone_geo.indices, cone_geo.numIndices, Vertex::STRIDE, GL_UNSIGNED_SHORT, GL_TRIANGLES);
		stemGeometry = renderer->addGeometry(cyn_geo.verts, cyn_geo.numVerts, (GLuint*)cyn_geo.indices, cyn_geo.numIndices, Vertex::STRIDE, GL_UNSIGNED_SHORT, GL_TRIANGLES);
	}

	VectorGraphic* vect = new VectorGraphic;
	//auto mat = new mat4();
	vect->coneRenderable = renderer->addRenderable(*coneGeometry, new mat4(),  TextureInfo("Texture/gold.bmp", FIF_BMP), 0,  true, false, false);
	//vect->coneRenderable = renderer->addRenderable(*coneGeometry, *mat,  TextureInfo("Texture/gold.bmp", FIF_BMP), 0,  true, false, false);

	//mat = new mat4();
	vect->stemRenderable = renderer->addRenderable(*stemGeometry, new mat4() , TextureInfo("Texture/gold.bmp", FIF_BMP), 0,true, false, false);
	//vect->stemRenderable = renderer->addRenderable(*stemGeometry, *mat , TextureInfo("Texture/gold.bmp", FIF_BMP), 0,true, false, false);
	

	delete coneGeometry;
	coneGeometry = nullptr;
	delete stemGeometry;
	stemGeometry = nullptr;


	vect->setEndPoints(from, to, lengthDelta);
	return vect; 
}

void DebugShapesManager::clear()
{	
	coneGeometry = nullptr;
	stemGeometry = nullptr;
	
}

void DebugShapesManager::VectorGraphic::setVisible(bool visible)
{
	if(coneRenderable != nullptr)
		coneRenderable->visible = visible;
	if(stemRenderable != nullptr)
		stemRenderable->visible = visible;
}

void DebugShapesManager::VectorGraphic::dispose()
{
	if(coneRenderable != nullptr)
	{
		coneRenderable->shutDown();
		delete coneRenderable;
		coneRenderable = nullptr;
	}

	if(stemRenderable != nullptr)
	{
		stemRenderable->shutDown();
		delete stemRenderable;
		stemRenderable = nullptr;
	}
}


 //CreateBasisFromOneVector() is an excellent mathematical exercise
const float CONE_HEIGHT = 0.35f;

void DebugShapesManager::VectorGraphic::setEndPoints(
	const glm::vec3& from, const glm::vec3& to, float lengthDelta)
{
	glm::vec3 theLine = to - from;
	glm::vec3 xBasis = glm::normalize(theLine);
	glm::mat4 theRotation(glm::orientation(xBasis, vec3(1.0f, 0.0f, 0.0f)));

	const float cylinderWeightLoss = 0.02f;
	glm::vec3 backOff = (lengthDelta / 2) * xBasis;
	const float CONE_SCALE = 5.0f;
	glm::vec3 coneHeight = CONE_HEIGHT * CONE_SCALE * xBasis;
	
	*stemRenderable->where = 
		(glm::translate(from) *
		theRotation * 
		glm::scale(vec3(glm::length(theLine) - CONE_HEIGHT - 1, 
		cylinderWeightLoss, cylinderWeightLoss)));

	*coneRenderable->where = 
		(glm::translate(to - coneHeight) * 
		theRotation *
		glm::scale(vec3(CONE_SCALE, CONE_HEIGHT, 1.0f)));
}

void DebugShapesManager::shutDown()
{
	shapes.clear();
	delete coneGeometry;
	delete stemGeometry;
	delete instance;
	//if(lineGeo != nullptr)       { delete lineGeo;		  }
	//if(pointGeo != nullptr)      { delete pointGeo;		  }
	//if(cubeGeo != nullptr)       { delete cubeGeo;		  }
	//if(sphereGeo != nullptr)     { delete sphereGeo;	  }
	//if(vectorGeo != nullptr)     { delete vectorGeo;	  }
	//if(unitVectorGeo != nullptr) { delete unitVectorGeo;  }
	//delete lineGeo;		
	//delete pointGeo;		
	//delete cubeGeo;		
	//delete sphereGeo;	
	//delete vectorGeo;	
	//delete unitVectorGeo;

	instanceFlag = false;
}

DebugShapesManager::~DebugShapesManager()
{
	instanceFlag = false;
	
}
#endif