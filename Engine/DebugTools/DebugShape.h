#pragma once
#include <glm\glm.hpp>
#include "ShapeData.h"
#include <RenderComponents\Renderable.h>

using glm::vec3;
using glm::vec4;
using Neumont::ShapeData;
using BatEngine::Renderable;
struct DebugShape : public Renderable
{
public:
	DebugShape(bool alive_forever = false)
	{
		duration = 0.0f;
		past_time = 0.0f;
		alive_forever = false;
	}

	ShapeData shape_data;
	bool alive_forever;
	float duration;
	float past_time;
};

