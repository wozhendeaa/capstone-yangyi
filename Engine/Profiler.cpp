#include "Profile.h"
#include <cassert>
#include <fstream>

void Profiler::addEntry(const char *name, float time)
{
	ProfilerCategory &pc = categories[categoryIndex];
	
	if(frameIndex == 0)
	{
		pc.categoryName = name;
		used_categories_num++;
	} 
	else
	{
		assert(name != NULL);
	}

	categoryIndex++;
	pc.data[frameIndex % MAX_FRAME_NUM] = time;
}

void Profiler::newFrame()
{
	if(frameIndex != -1)
	{
		categoryIndex = 0;
	} 

	frameIndex++;

}

//void Profiler::DrawGraph(Core::Graphics& g)
//{
//	int x = 900;
//	int y = 250;
//
//	g.DrawString(x, y + 50, "Profile Bar Graph");
//
//	for(uint i = 0; i < used_categories_num; i++)
//	{
//		float height = categories[i].data[frameIndex % MAX_FRAME_NUM] * 30000;
//		g.DrawString(x - 30, y + 20, categories[i].categoryName);
//		DrawBar(g, x, y, height);
//		x += 50;
//	}
//}
//
//void Profiler::DrawBar(Core::Graphics& g,int x, int y, float height)
//{
//	g.DrawLine(x, y, x - 10, y);
//	g.DrawLine(x - 10, y, x - 10, y - height);
//	g.DrawLine(x, y, x, y - height);
//	g.DrawLine(x, y - height, x - 10, y - height);
//
//}

void Profiler::shutDown()
{
	std::ofstream os(fileName, std::ios::trunc);

	os << "Frame,";
	for(uint i = 0; i < used_categories_num;i++)
	{
		os << categories[i].categoryName;
		os << ((i + 1) < used_categories_num ?  "," : "\n");
	}

	uint actualFrameNum = frameIndex;
	if(categoryIndex == used_categories_num)
		actualFrameNum++;

	uint endIndex ;  
	uint startIndex; 

	if(isWrapped())
	{
		endIndex = frameIndex % MAX_FRAME_NUM;
		startIndex = (endIndex + 1) % MAX_FRAME_NUM;

		while(startIndex != endIndex)
		{	
			os << startIndex << ",";

			for(uint c = 0; c < used_categories_num ; c++)
			{
 				os << categories[c].data[startIndex];				

				os << ((c + 1) < used_categories_num ? "," : "\n");
			}

			startIndex = (startIndex + 1) % MAX_FRAME_NUM;
		}	
	} 
	else
	{

		endIndex = actualFrameNum;
		startIndex = 0;

		while(startIndex < endIndex)
		{
			os << startIndex <<",";
			for(uint c = 0; c < used_categories_num; c++)
			{
				os << categories[c].data[startIndex];				

				os << ((c + 1) < used_categories_num ? "," : "\n");
			}

			startIndex++;
		}
	}	
}

void Profiler::writeFrame(uint startIndex) const
{
	startIndex;
}

bool Profiler::isWrapped() const
{
	return frameIndex > MAX_FRAME_NUM && frameIndex != -1;
}

void Profiler::initialize(const char *fileName)
{
	this -> fileName = fileName;
	frameIndex = 0;
	categoryIndex = 0;
	//categories[0].categoryName = "Frame";
	used_categories_num = 0;
}
