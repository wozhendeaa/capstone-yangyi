#pragma once
#include <GL\glew.h>
#include <QtOpenGL\qglwidget>
#include <cstring>
#include <Qt\qtimer.h>
#include <Qt\qelapsedtimer.h>
#include <QtGui\QKeyEvent>
#include <QtGui\qmouseeventtransition>
#include <ShapeGenerator.h>
#include <ObjLoader\ObjLoader.h>
#include <GUI\Data.h>
#include <RenderComponents\Renderer.h>
#include <CStopWatch.h>
#include <AI\AstarManager.h>
#include <DebugTools\DebugShapesManager.h>
#include <fstream>
#include <Player\Player.h>


using std::string;

class ENGINE_SHARED GLWindow : public QGLWidget
{
	bool isReady;
protected:

	Renderer* renderer;
	Data* data;
	GameNode* game_nodes;
	uint num_game_nodes;
	QTimer timer;
	QElapsedTimer elapsedTimer;
	void mousePressEvent(QMouseEvent* e );
	void mouseMoveEvent(QMouseEvent* e) override;
	bool IsReady();
	//core window funcions
	float RayIntersectPlaneDistance(
									const vec3& n, 
									const vec3& l, 
									const vec3& p0, 
									vec3& l0);
	bool rayIntersectSphere(const vec3& ray,
							const vec3& target_position,
							const float& radius);

	vec3 castRay(const float& x, 
				 const float& y);

	void readAstars(const char* file_address,
					const uint& num_node);

	virtual void loadLevel(const char* path);
	void prepareForNewLevel();

	virtual void PressKey();
	
	void paintGL() override;
	void DefaultDraw();
	void initializeGL() override;

public:
	GLWindow();
	~GLWindow();
};

