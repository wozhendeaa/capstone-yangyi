#pragma once

#include <gl\glew.h>
#include <QtGui\qwidget.h>
#include <MemoryDebug.h>
#include <GUI\Data.h>
#include <DebugTools\DebugMenu.h>
#include <GUI\GLWindow.h>


class ENGINE_SHARED AppWindow : public QWidget
{
protected:
	GLWindow *glWindow;
	QVBoxLayout *windowLayout;
	Data *data;
	void setupGUI();
	
public:
	AppWindow();
	void addGLWindow(GLWindow* window);
	void keyPressEvent(QKeyEvent* e) override;

	DebugMenu* debug_menu;
	void mouseMoveEvent(QMouseEvent* e) override;
	virtual ~AppWindow();
};

