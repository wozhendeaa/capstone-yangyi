#include "ShockWaveEffect.h"
#include <MemoryDebug.h>

ShockWaveEffect::ShockWaveEffect(glm::vec2 screenCoord, 
	const GLuint program_id,
	const float timeLimit  ,
	const float waveRange  , 
	const float deltaLength,
	const float amplitude) 
	: m_program_id(program_id),
	timeAccum(0.0f),
	m_screenCoord(screenCoord),
	m_disposable(false),
	active(false),
	activated(false),
	hitHalf(false)
{
	Shockwave.currentTime = 0.0f;
	Shockwave.deltaLength = deltaLength;
	Shockwave.amplitude = amplitude;
	Shockwave.waveRange = waveRange;
	Shockwave.timeLimit = timeLimit;

}


void ShockWaveEffect::Activate() {
	active = true; 
	activated = true; 
} 

bool ShockWaveEffect::Disposable()
{
	return activated && m_disposable; 
}

void ShockWaveEffect::Update(float dt)
{

	if(!m_disposable && active)
	{
		dt = 1 / 30.0f;
		if(hitHalf)
		{
			Shockwave.currentTime -= dt;
		}
		else
		{
			Shockwave.currentTime += dt;
		}

		timeAccum += dt;

		m_disposable = timeAccum >= Shockwave.timeLimit;
		hitHalf = timeAccum >= Shockwave.timeLimit * 0.5f;
		
				
		glUseProgram(m_program_id);
		auto waveRangeId = glGetUniformLocation(m_program_id, "shockWaveData.waveRange");
		glUniform1f(waveRangeId, Shockwave.waveRange);

		auto timeId = glGetUniformLocation(m_program_id, "shockWaveData.time");
		glUniform1f(timeId, Shockwave.currentTime);

		auto deltaLength = glGetUniformLocation(m_program_id, "shockWaveData.deltaLength");
		glUniform1f(deltaLength, Shockwave.deltaLength);

		auto ampId = glGetUniformLocation(m_program_id, "shockWaveData.amplitude");
		glUniform1f(ampId, Shockwave.amplitude);

		auto timeLimitId = glGetUniformLocation(m_program_id, "shockWaveData.timeLimit");
		glUniform1f(timeLimitId, Shockwave.timeLimit);


		auto screenCoordId = glGetUniformLocation(m_program_id, "screenLocation");
		glUniform2fv(screenCoordId, 1, &m_screenCoord.x);

		auto disposableId = glGetUniformLocation(m_program_id, "hasShockWave");
		glUniform1i(disposableId, active);

		if(m_disposable)
		{
			glUniform1i(disposableId, !m_disposable);

		}
	}

}

ShockWaveEffect::~ShockWaveEffect()
{

}