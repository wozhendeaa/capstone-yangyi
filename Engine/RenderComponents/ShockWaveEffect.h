#pragma once
#include <ExportHeader.h>
#include <GL\glew.h>
#include <glm\glm.hpp>

class ENGINE_SHARED ShockWaveEffect
{
private:
	glm::vec2 m_screenCoord;
	float timeAccum;
	bool m_disposable;
	bool active;
	bool activated;
	bool hitHalf;
	GLuint m_program_id;
	glm::vec4 data;

	struct _shockWave
	{
		float waveRange;	   
		float currentTime;	
		float deltaLength;	
		float amplitude;	
		float timeLimit;
	} Shockwave;

public:
	ShockWaveEffect(glm::vec2 screenCoord, 
		const GLuint program_id,
		const float timeLimit   = 0.5f, 
		const float waveRange   = 0.3f, 
		const float deltaLength = 0.1f,
		const float amplitude = -7.0f);

	GLuint GetProgramId() const {return m_program_id; }

	float* GetLocation() {return &m_screenCoord.x;}

	void Activate();

	bool Disposable();

	void Update(float dt);
	
	~ShockWaveEffect();

};