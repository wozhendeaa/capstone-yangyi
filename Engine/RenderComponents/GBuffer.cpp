#include "GBuffer.h"
#include <iostream>

GBuffer::GBuffer(uint colorWidth,
	uint colorheight,
	uint depthWidth,
	uint depthHeight) :
	m_colorWidth(colorWidth),
	m_colorheight(colorheight),
	m_depthWidth(depthWidth),
	m_depthHeight(depthHeight)

{
	
}

void GBuffer::init()
{
	glGenFramebuffers(1, &fbo_id);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo_id);

	glGenTextures(GBUFFER_NUM_TEX, m_textures);
	glGenTextures(1, &depthTexture);

	for (size_t i = 0; i < GBUFFER_NUM_TEX; i++)
	{
		m_texture_units[i] = TextureInfo::textureCount++;
		glActiveTexture(GL_TEXTURE0 + m_texture_units[i]);
		glBindTexture(GL_TEXTURE_2D, m_textures[i]);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D,
			0,
			GL_RGB32F,
			m_colorWidth,
			m_colorheight,
			0,
			GL_RGB,
			GL_FLOAT,
			0);

		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER,
			GL_COLOR_ATTACHMENT0 + i,
			GL_TEXTURE_2D,
			m_textures[i],
			0);
	}
	
	depthTextureUnit = TextureInfo::textureCount++;
	glActiveTexture(GL_TEXTURE0 + depthTextureUnit);
	glBindTexture(GL_TEXTURE_2D, depthTexture);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D,	
		0,
		GL_DEPTH_COMPONENT32F,
		m_depthWidth,
		m_depthHeight,
		0,
		GL_DEPTH_COMPONENT,
		GL_FLOAT,
		0);

	glFramebufferTexture2D(
		GL_DRAW_FRAMEBUFFER,
		GL_DEPTH_ATTACHMENT,
		GL_TEXTURE_2D,
		depthTexture,
		0);

	GLenum drawBuffers[GBUFFER_NUM_TEX];
	for (size_t i = 0; i < GBUFFER_NUM_TEX; i++)
	{
		drawBuffers[i] = GL_COLOR_ATTACHMENT0 + i;
	}

	glDrawBuffers(GBUFFER_NUM_TEX, drawBuffers);
	GLenum Status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	if (Status != GL_FRAMEBUFFER_COMPLETE) {
		std::cout << "FB error, status: 0x%x\n"
			<< std::endl;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}


void GBuffer::BindForReading()
{
	glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo_id);
}

void GBuffer::BindForWriting()
{
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_id);
}

void GBuffer::SetCurrentReadingBuffer(GBUFFER_TEXTURE_TYPE type)
{
	glReadBuffer(GL_COLOR_ATTACHMENT0 + type);
}


GBuffer::~GBuffer()
{
}
