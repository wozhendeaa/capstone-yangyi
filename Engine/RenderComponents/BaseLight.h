#pragma once
#include <ExportHeader.h>
#include <glm\glm.hpp>

struct BaseLight
{
	glm::vec3 color;
	float AmbientIntensity;
	float DiffuseIntensity;
};

