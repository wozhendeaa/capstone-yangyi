#pragma once
#pragma warning( disable : 4251)

#include <gl\glew.h>
#include <glm\glm.hpp>
#include <ExportHeader.h>
#include <vector>
class ENGINE_SHARED GeometryInfo
{
public:
	GeometryInfo(glm::vec3&& scale = glm::vec3(1.0f));
	~GeometryInfo(void);
	void addVaryingPositionBuffer(void* data, 
		GLuint bufferSize, 
		GLuint location);
	void shutDown();
	glm::vec3 scale;
	GLuint buffer_id;
	GLuint vertex_array_id;
	GLuint offset;
	GLuint index_offset;
	GLuint total_size;
	GLuint num_vertex;
	GLuint num_index;
	GLuint geometry_index;
	GLuint index_mode;
	GLuint drawing_mode;
	GLuint bufferIndex;
	void* verts;
	GLuint* indices;
	bool serializeable;
	bool useInstanceDraw = false;
};

