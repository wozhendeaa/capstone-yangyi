#pragma once
#include <ExportHeader.h>
#include <TextureInfo.h>
#include <GL\glew.h>

class ENGINE_SHARED GBuffer
{
public:
	enum GBUFFER_TEXTURE_TYPE
	{
		GBUFFER_POSITION,
		GBUFFER_DIFFUSE,
		GBUFFER_TEXCOORD,
		GBUFFER_NORMAL,
		GBUFFER_NUM_TEX
	};

	explicit GBuffer(uint colorWidth = 1920, 
		uint colorheight = 1080,
		uint depthWidth = 512,
		uint depthHeight = 424);

	void BindForReading();
	void BindForWriting();
	void SetCurrentReadingBuffer(GBUFFER_TEXTURE_TYPE type);

	~GBuffer();

private:
	uint m_colorWidth;
	uint m_colorheight;
	uint m_depthWidth;
	uint m_depthHeight;

	void init();
			  

	GLuint fbo_id;
	GLuint m_textures[GBUFFER_NUM_TEX];
	GLuint m_texture_units[GBUFFER_NUM_TEX];
	GLuint depthTexture;
	GLuint depthTextureUnit;
};

