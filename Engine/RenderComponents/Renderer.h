#pragma once
#include <GL\glew.h>
#include <QtOpenGL\qglwidget>
#include <ExportHeader.h>
#include <glm\glm.hpp>
#include <vector>
#include "GeometryInfo.h"
#include "ShaderInfo.h"
#include "Renderable.h"
#include "BufferInfo.h"
#include "TextureInfo.h"
#include "Camera.h"
#include "UIData\UIData.h"
#include <string>
#include <ObjLoader\ObjLoader.h>
#include <memory>
#include <MemoryDebug.h>
#include <GBuffer.h>

using std::tr1::shared_ptr;
using glm::mat4;
#define MAX_LIGHT_NUM 20
#define MAX_BUFFER_NUM 20
#define MAX_SHADER_NUM 5
#define MAX_UNIFORM_NUM 1000
#define MAX_CAMERA_NUM 20
#define DEPTH_TEXTURE_WIDTH 2048 
#define DEPTH_TEXTURE_HEIGHT 2048
#define MAX_SHADOW_MAP_NUM 5
#define MAX_GLOBAL_TEX_NUM 5
#define MAX_SHOCKWAVE_NUM 5

#define TO_BYTES(n) reinterpret_cast<char*>(&n)
#define TO_TYPE(t, n) reinterpret_cast<t>(n)
typedef unsigned int uint;

namespace BatEngine
{
	class ENGINE_SHARED Renderer : public QGLWidget
	{
		vec3 softShadowTexSize;
		void initialize();
		bool can_update;
		
		void resetUniformByProgramid(GLuint program_id,
			ShaderUniformParameter* uniform);
		
	public:
		GLuint currentShaderIndex;
		Renderer();

		~Renderer();
		bool canUpdate();
		void shutdown();
		//LightComponent *lights_components[MAX_LIGHT_NUM];
		char* currentSubroutineName = "renderScene";
		GBuffer* m_gBuffer = nullptr;
		BufferInfo *buffers[MAX_BUFFER_NUM];
		ShaderInfo *shaders[MAX_SHADER_NUM];
		ShaderUniformParameter *uniforms[MAX_UNIFORM_NUM];
		TextureInfo* shadowMaps[MAX_SHADOW_MAP_NUM];
		TextureInfo* global_textures[MAX_GLOBAL_TEX_NUM];
		Camera *cams[MAX_CAMERA_NUM];
		UIData* data;
		Camera* current_cam;
		GLuint current_buffers_count;
		GLuint current_global_texture_count;
		GLuint current_geometry_count;
		GLuint current_shader_count;
		GLuint current_cams_count;
		GLuint current_light_count;
		GLuint current_uniform_count;
		GLuint current_shadow_map_count;
		GLuint depth_framebuffer_id;
		GLuint depth_texture_id;
		bool enableShadow;
		bool enableSoftShadow;
		enum ParameterType
		{
			// These values matter:
			PT_FLOAT= sizeof(float) * 1,
			PT_VEC2 = sizeof(float) * 2,
			PT_VEC3 = sizeof(float) * 3,
			PT_VEC4 = sizeof(float) * 4,
			PT_MAT3 = sizeof(float) * 9,
			PT_MAT4 = sizeof(float) * 16,
		};

		struct 
		{
			GLuint projection;
			GLuint world_view;
			GLuint world_model;
			GLuint biased_matrix;
			GLuint light_projc;
			GLuint light_view;
			GLuint shadow_map;
			GLuint ambient;
			GLuint diffuse;
			GLuint specular;
			GLuint specular_power;
			GLuint directional_light;
			GLuint light_position;
			GLuint eye;
			GLuint texture;
			GLuint texture2;
			GLuint texture_alpha;
			GLuint texture_normal;
			GLuint texture_ao;
			GLuint texture_noise;
			GLuint texture_useNormalLighting;
			GLuint texture_useDiffuseMap;
			GLuint texture_useSpecMap;
			GLuint texture_useAlpha;
			GLuint texture_useAOMap;
			GLuint texture_useNoiseMap;
			GLuint texture_noise_octave;
			GLuint texture_useNoiseAsHeightMap;
			GLuint texture_spec;
			GLuint enable_lighting;
			GLuint global_color;
			GLuint  enable_vertex_color;
			GLuint temp;
			GLuint mvp;
			GLuint alpha;
			GLuint softShadowTexId;
			GLuint softShadowTexUnit;
		} GlobalUniformLocations;

		struct 
		{
			mat4 projection;
			mat4 world_view;
			mat4 world_model;
			mat4 light_proj;
			mat4 light_view;
			mat4 biased_matrix;
		} Matrices;


		void switchShader(GLuint shaderIndex, GLuint passIndex = 0);

		GeometryInfo* addGeometry(	
			const void* verts, GLuint numVerts, 
			GLuint* indices, GLuint numIndices,
			GLuint stride,GLuint indexingMode,
			GLuint drawingMode, bool withTangent = false, 
			bool serializable = true,
			GLenum usage = GL_STATIC_DRAW,
			bool instanceDraw = false);
	
		//GeometryInfo* Renderer::addGeometry(
		//const void* verts, GLuint numVerts, 
		//GLushort* indices, GLuint numIndices,
		//GLuint stride, GLuint indexingMode, GLuint drawingMode, bool serializable = true);


		ShaderInfo* updateShaderInfo(
			GLuint shaderInfoIndex,
			const GLchar* vertexShaderCode,
			const GLchar* fragmentShaderCode);

		Renderable* addRenderable(
			GeometryInfo geometry,
			glm::mat4* whereMatrix,
			TextureInfo texture,
			GLuint howShaders,
			bool depth_test_on = true,
			bool vertex_color_on = false,
			bool serializable = true,
			bool enable_blending = false,
			bool recievesLight = true,
			bool isSkybox = false,
			bool textureColorOnly = false
			);

		void addShaderStreamedParameter(
			GLuint layoutLocation, 
			GLuint attrib_offset,
			ParameterType psize,
			GeometryInfo* geo_info,
			GLuint bufferStride,
			bool useInstanceDraw = false);
	
		GLuint addRenderableUniformParameter(
			Renderable* renderable,
			const GLchar* name,
			ShaderUniformParameterType parameterType, 
			char* value);

		void addUniform(ShaderUniformParameter* uniform);

		void setUniform(
			ShaderUniformParameter* para);


		void addUniform(
			GLchar* name,
			const GLuint& shaderID,
			const ShaderUniformParameterType& type,
			void* value);

		void addGlobalTexture(TextureInfo* tex);

		void plusAllUniforms();

		void Draw(Renderable* r, 
			GLuint passIndex = 0);

		void GeometryPassDraw();
		void LightingPassDraw();


		Renderable* GenGuiTexture(GLuint shaderIndex,
			TextureInfo texture,
			float& x,
			float& y,
			const char* uniformName = "color_tex",
			float width = 0.15f,
			float height = 0.25f,
			bool isNearPlane = true,
			bool flipHorizanontal = false,
			bool flipVertical     = false );

		Renderable* GenBillboard(GLuint shaderIndex,
			TextureInfo texture,
			vec3& pos, 
			float width = 0.15f,
			float height = 0.25f,
			bool instanceDraw = false,
			bool flipHorizanontal = false,
			bool flipVertical = false);

		void AttachTextureToFramebuffer(GLuint& fbo_id, 
			TextureInfo* colorTexture,
			const GLuint& colorTexture_shaderIndex,
			TextureInfo* depthTexture,
			const GLuint& depthTexture_shaderIndex,
			const char* colorUniformName = "color_tex",
			const char* depthUniformName = "shadow_map");


		void AttachTextureToHDRFramebuffer(GLuint& fbo_id, 
			TextureInfo* colorTexture,
			const GLuint& colorTexture_shaderIndex);

	
		void setShadowUniform(
			GLuint program_id);
	
		void setNormalRenderUniform(
			GLuint program_id);

		void setLevelEditorUniform(
			GLuint program_id);
	
		void useShaderUniform(
			GLuint program_id);

		void LoadUniform(
			GLuint program_id);

		ShaderInfo* createShaderInfo(
			const char* vertex_shader,
			const char* fragment_shader);

		void selectSubroutines(
			const char* vertex_sub, 
			const char* frag_sub, ShaderInfo* shader);

		bool checkStatus(GLint fd, 
						PFNGLGETSHADERIVPROC object_prop_get_func, 
						PFNGLGETSHADERINFOLOGPROC get_info_log_func, 
						GLenum status_type);



		std::string ReadShaderCode(const char* file_name);

		// serialize the geometry data and return the total geometry size
		void serializeRenderer(std::ofstream& out_stream,
			GeometryInfo** geometries,
			const GLuint& geoCount);

		//deserialize the geometry data and write geometry size data in the header
		char* deserializeRenderer(char* data, uint geo_num, uint geo_size);

		int serializableGeoCount(
			GeometryInfo** geometries,
			const GLuint& geoCount);

		void reset();

		void Update(float dt);


	};
}