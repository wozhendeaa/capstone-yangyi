#pragma once
#include <GL\glew.h>
struct ENGINE_SHARED BufferInfo
{
	GLuint buffer_id;
	GLuint remaining_size;
};