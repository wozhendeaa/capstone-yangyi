#include "TextureInfo.h"
#include <glm\gtc\noise.hpp>
#include <MemoryDebug.h>
#include <iostream>
GLuint TextureInfo::textureCount = 1;

void TextureInfo::addTexture(const GLchar* _file_name, const FREE_IMAGE_FORMAT& fmt, GLuint& tex_id, GLuint& unit)
	{
		auto tex = FreeImage_Load(fmt, _file_name);
		if(tex)
		{
			auto width = FreeImage_GetWidth(tex);
			auto height = FreeImage_GetHeight(tex);
			tex = FreeImage_ConvertTo32Bits(tex);
			auto tempPixles = new GLbyte[4 * width * height];
			auto pixels = (GLbyte*)(FreeImage_GetBits(tex));

			for (unsigned i = 0; i < width * height; i++)
			{
				tempPixles[i * 4 + 0] = pixels[i * 4 + 2];
				tempPixles[i * 4 + 1] = pixels[i * 4 + 1];
				tempPixles[i * 4 + 2] = pixels[i * 4 + 0];
				tempPixles[i * 4 + 3] = pixels[i * 4 + 3];
			}

			unit = textureCount++;
			glActiveTexture(GL_TEXTURE0 + unit);
			glGenTextures(1, &tex_id);
			glBindTexture(GL_TEXTURE_2D, tex_id); 
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 
				0, GL_RGBA, GL_UNSIGNED_BYTE, tempPixles);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glBindTexture(GL_TEXTURE_2D, 0); 

			delete [] tempPixles;
			FreeImage_Unload(tex);

		}

	}

void TextureInfo::addColorTexture(const GLchar* _file_name, 
								  const FREE_IMAGE_FORMAT& fmt,
								  GLuint program_id,
								  const char* uniformName,
								  GLenum tagert )
{
	if(_file_name != nullptr)
	{
		addTexture(_file_name, fmt, color_tex_id, color_tex_unit);
	}
	else
	{
		color_tex_unit = textureCount++;
	}

	colorUniformLocation = glGetUniformLocation(program_id,uniformName);
	color_target = tagert;
	name = const_cast<char*>(uniformName);
	useDiffuseMap = true;

}

void TextureInfo::addDynamicColorTexture(uint width,
										 uint height,
										 GLuint program_id, 
										 const char* uniformName,
										 GLenum tagert,
										 GLenum internalFormat ,
										 GLenum format,
										 GLenum type)
{
	
	color_tex_unit = textureCount++;
	colorUniformLocation = glGetUniformLocation(program_id,uniformName);
	color_target = tagert;
	name = const_cast<char*>(uniformName);
	useDiffuseMap = true;
	this->width = width;
	this->height = height;

	glActiveTexture(GL_TEXTURE0 + color_tex_unit);
	glGenTextures(1, &color_tex_id);
	glBindTexture(GL_TEXTURE_2D, color_tex_id); 
	glTexImage2D(GL_TEXTURE_2D, 
		0,
		internalFormat, 
		width,
		height, 
		0,
		format, 
		type, 
		0);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);


}

void TextureInfo::addDynamicDepthMap(uint width,
									 uint height,
									 GLuint program_id,
									 const char* uniformName)
{
	spec_tex_unit = textureCount++;
	name = const_cast<char*>(uniformName);
	specUniformLocation = glGetUniformLocation(program_id, uniformName);

	auto enableArDepthLocation = glGetUniformLocation(program_id, "enableARDepthTesting");
	glUniform1i(enableArDepthLocation, true);

	spec_target = GL_TEXTURE_2D;

	this->width = width;
	this->height = height;

	glActiveTexture(GL_TEXTURE0 + spec_tex_unit);
	glGenTextures(1, &spec_tex_id);
	glBindTexture(GL_TEXTURE_2D, spec_tex_id);
	glTexImage2D(GL_TEXTURE_2D,
			     0,
			     GL_RGB,
			     width,
			     height,
			     0,
				 GL_RGB,
			     GL_FLOAT,
			     0);

	//GLfloat border[] = { 1.0f, 0.0f, 0.0f, 0.0f };
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LESS);
	//glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);
	glBindTexture(GL_TEXTURE_2D, 0);

}


void TextureInfo::addAlphaTexture(const GLchar* _file_name, 
								  const FREE_IMAGE_FORMAT& fmt,
								  GLuint program_id,
								  const char* uniformName,
								  GLenum target )
{
	if(_file_name != nullptr)
	{
		addTexture(_file_name, fmt, alpha_tex_id, alpha_tex_unit);
	}
	else
	{
		alpha_tex_unit = textureCount++;
	}

	alphaUniformLocation = glGetUniformLocation(program_id, uniformName);
	alpha_target = target;
	useAlphaMap = true;
}

void TextureInfo::addSpecTexture(const GLchar* _file_name, 
								 const FREE_IMAGE_FORMAT& fmt,
								 GLuint program_id,
								 const char* uniformName,
								 GLenum target )
{
	if(_file_name != nullptr)
	{
		addTexture(_file_name, fmt, spec_tex_id, spec_tex_unit);
	}
	else
	{
		spec_tex_unit = textureCount++;
	}
	specUniformLocation = glGetUniformLocation(program_id,uniformName);
	spec_target = target;
	useSpecMap = true;
}

void TextureInfo::addNormalTexture(const GLchar* _file_name,
								   const FREE_IMAGE_FORMAT& fmt,
								   GLuint program_id,
								   const char* uniformName,
								   GLenum target )
{
	if(_file_name != nullptr)
	{
		addTexture(_file_name, fmt, normal_tex_id, normal_tex_unit);
	}
	else
	{
		normal_tex_unit = textureCount++;
	}
	normal_target = target;
	normalUniformLocation = glGetUniformLocation(program_id,uniformName);
	userNormalMapLighting = true;
}

void TextureInfo::addAOTexture(const GLchar* _file_name, 
							   const FREE_IMAGE_FORMAT& fmt,
							   GLuint program_id,
							   const char* uniformName,
							   GLenum target )
{
	if(_file_name != nullptr)
	{
		addTexture(_file_name, fmt, ao_tex_id, ao_tex_unit);
	}
	else
	{
		ao_tex_unit = textureCount++;
	}

	aoUniformLocation = glGetUniformLocation(program_id,uniformName);
	ao_target = target;
	useAOMap = true;
}

void TextureInfo::bindTexture(GLuint& tex_id, GLuint uniformLocation, GLuint unit, GLenum target)
{
	if(tex_id < 3000)
	{
		glActiveTexture(GL_TEXTURE0  + unit);
		glBindTexture(target, tex_id);
		glUniform1i(uniformLocation, tex_id);
	}
	//else
	//{
	//	glActiveTexture(GL_TEXTURE0);
	//	glBindTexture(target, 0);
	//	glUniform1i(uniformLocation, 0);
	//}

}

void TextureInfo::bindColorTexture(GLuint uniformLocation)
{
	bindTexture(color_tex_id, uniformLocation, color_tex_unit, color_target);

}

void TextureInfo::bindAlphaTexture(GLuint uniformLocation)
{
	bindTexture(alpha_tex_id, uniformLocation, alpha_tex_unit, alpha_target);
}
void TextureInfo::bindSpecTexture(GLuint uniformLocation)
{
	bindTexture(spec_tex_id, uniformLocation, spec_tex_unit, spec_target);
}
void TextureInfo::bindNormalTexture(GLuint uniformLocation)
{
	bindTexture(normal_tex_id, uniformLocation, normal_tex_unit, normal_target);
}

void TextureInfo::bindAOTexture(GLuint uniformLocation)
{
	bindTexture(ao_tex_id, uniformLocation, ao_tex_unit, ao_target);
}

void TextureInfo::bindNoiseTexture(GLuint uniformLocation)
{
	bindTexture(noise_tex_id, uniformLocation, noise_tex_unit, noise_target);
}

void TextureInfo::addCubeMap(const char* pos_x, 
	const char* neg_x,
	const char* pos_y,
	const char* neg_y,
	const char* pos_z,
	const char* neg_z,
	const FREE_IMAGE_FORMAT& fmt,
	GLuint program_id)
{
	const char* files[] = {
		pos_x,
		neg_x,
		pos_y,
		neg_y,
		pos_z,
		neg_z
	};

	GLuint targets[] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};

	colorUniformLocation = glGetUniformLocation(program_id,"cube_map");
	color_target = GL_TEXTURE_CUBE_MAP;
	if(colorUniformLocation < 3000)
	{
		color_tex_unit = textureCount++;
		glActiveTexture(GL_TEXTURE0 + color_tex_unit);
		glGenTextures(1, &color_tex_id);
		glBindTexture(color_target, color_tex_id);
	
		for (int i = 0; i < 6; i++)
		{	
			auto tex = FreeImage_Load(fmt, files[i]);
			if(tex)
			{
				auto width = FreeImage_GetWidth(tex);
				auto height = FreeImage_GetHeight(tex);
				tex = FreeImage_ConvertTo32Bits(tex);
				auto tempPixles = new GLbyte[4 * width * height];
				auto pixels = (GLbyte*)(FreeImage_GetBits(tex));

				for (unsigned j = 0; j < width * height; j++)
				{
					tempPixles[j * 4 + 0] = pixels[j * 4 + 2];
					tempPixles[j * 4 + 1] = pixels[j * 4 + 1];
					tempPixles[j * 4 + 2] = pixels[j * 4 + 0];
					tempPixles[j * 4 + 3] = pixels[j * 4 + 3];
				}

				glTexImage2D(targets[i], 0,
					GL_RGBA,
					width,
					height,
					0,
					GL_RGBA,
					GL_UNSIGNED_BYTE,
					tempPixles);

				delete [] tempPixles;
				FreeImage_Unload(tex);
			}
		}
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);		
		glUniform1i(colorUniformLocation, color_tex_id);
		
	}


}

void TextureInfo::addNoiseTexture(const int& width,
									const int& height,
									const float& frequency,
									const float& scale)
{
	GLubyte* data = new GLubyte[width * height * 4];
	float xFactor = 1.0f / width;
	float yFactor = 1.0f / height;

	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			float f = frequency;
			float s = scale;
			float sum = 0.0f;
			 
			for (int octave = 0; octave < 4; octave++)
			{
				auto x = xFactor * j;
				auto y = yFactor * i;

				auto vecVal = glm::vec2(x, y);
				
				auto val = glm::perlin(vecVal * f) / s; 

				sum += val;

				val = (sum + 1.0f) * 0.5f;

				f *= 2.0f;
				s *= 2.0f;

				data[(width * j + i) * 4 + octave] = static_cast<GLubyte>(val * 255.0f);
			}
		}
	}

	noise_tex_unit = textureCount++;
	glActiveTexture(GL_TEXTURE0 + noise_tex_unit);
	glGenTextures(1, &noise_tex_id);
	glBindTexture(GL_TEXTURE_2D, noise_tex_id);
	glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, width, height);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height,
		GL_RGBA, GL_UNSIGNED_BYTE, data);
	glBindTexture(GL_TEXTURE_2D, 0); 

	delete [] data;


	useNoiseMap = true;
}


