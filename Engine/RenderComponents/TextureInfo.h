#pragma once
#include <GL\glew.h>
#include "..\Middleware\FreeImage\include\FreeImage.h"
#include <ExportHeader.h>
#include <RenderComponents\GeometryInfo.h>
#include <ShapeGenerator.h>

struct ENGINE_SHARED TextureInfo
{
private:

	GLuint color_tex_unit ;
	GLuint alpha_tex_unit ;
	GLuint spec_tex_unit  ;
	GLuint normal_tex_unit;
	GLuint ao_tex_unit;	
	GLuint noise_tex_unit;
	GLenum color_target;
	GLenum spec_target;
	GLenum normal_target;
	GLenum alpha_target;
	GLenum noise_target;
	GLenum ao_target;


	void addTexture(const GLchar* _file_name, 
		const FREE_IMAGE_FORMAT& fmt, 
		GLuint& tex_id,
		GLuint& unit);

	void bindTexture(GLuint& tex_id, 
		GLuint uniformLocation,
		GLuint unit,
		GLenum target);

public:
	static GLuint textureCount;
	GLuint color_tex_id;
	GLuint alpha_tex_id;
	GLuint normal_tex_id;
	GLuint spec_tex_id;
	GLuint ao_tex_id;
	GLuint noise_tex_id;
	GLuint currentUnit;
	GLuint width;
	GLuint height;
	GLuint colorUniformLocation;
	GLuint alphaUniformLocation;
	GLuint normalUniformLocation;
	GLuint specUniformLocation;
	GLuint aoUniformLocation;
	GLuint noiseUniformLocation;
	GLchar* name;
	bool userNormalMapLighting;
	bool useDiffuseMap;
	bool useSpecMap;
	bool useAOMap;
	bool useAlphaMap;
	bool useNoiseMap;

	TextureInfo()
	{
		color_tex_id = 
		alpha_tex_id = 
		normal_tex_id = 
		spec_tex_id = 
		ao_tex_id = INT_MAX;
		color_tex_unit =
		alpha_tex_unit =
		spec_tex_unit  =
		ao_tex_unit  =
		normal_tex_unit = 0;
		width = height = 512;
		userNormalMapLighting = false;
		useDiffuseMap= false;
		useSpecMap= false;
		useAOMap= false;
		useAlphaMap= false;
		useNoiseMap = false;
		name = nullptr;

		colorUniformLocation =
		alphaUniformLocation =
		normalUniformLocation =
		specUniformLocation = 
		aoUniformLocation = 
		noiseUniformLocation = 0;

		color_target = GL_TEXTURE_2D; 
		spec_target = GL_TEXTURE_2D; 
		normal_target = GL_TEXTURE_2D; 
		alpha_target = GL_TEXTURE_2D; 
		noise_target = GL_TEXTURE_2D; 
		ao_target = GL_TEXTURE_2D; 
	}

	TextureInfo(const GLchar* _file_name, GLuint program_id, const FREE_IMAGE_FORMAT& fmt = FIF_JPEG) 
	{ 
		color_tex_id = 
		alpha_tex_id = 
		normal_tex_id = 
		spec_tex_id = 
		ao_tex_id = INT_MAX;
		color_tex_unit =
		alpha_tex_unit =
		spec_tex_unit  =
		ao_tex_unit  =
		normal_tex_unit = 0;
		width = height = 512;
		userNormalMapLighting = false;
		useDiffuseMap= false;
		useNoiseMap = false;
		useSpecMap= false;
		useAOMap= false;
		useAlphaMap= false;
		name = nullptr;
		
		colorUniformLocation = 
		alphaUniformLocation = 
		normalUniformLocation = 
		specUniformLocation = 
		aoUniformLocation = 
		noiseUniformLocation = 0;
		
		color_target = GL_TEXTURE_2D; 
		spec_target = GL_TEXTURE_2D; 
		normal_target = GL_TEXTURE_2D; 
		alpha_target = GL_TEXTURE_2D; 
		noise_target = GL_TEXTURE_2D; 
		ao_target = GL_TEXTURE_2D; 
		
		addColorTexture(_file_name, fmt, program_id);
	}

	void addColorTexture(const GLchar* _file_name, const FREE_IMAGE_FORMAT& fmt, GLuint program_id, const char* uniformName = "color_tex" ,GLenum tagert = GL_TEXTURE_2D);
	void addAlphaTexture(const GLchar* _file_name, const FREE_IMAGE_FORMAT& fmt,GLuint program_id, const char* uniformName = "alpha_tex"  ,GLenum tagert = GL_TEXTURE_2D);
	void addSpecTexture(const GLchar* _file_name, const FREE_IMAGE_FORMAT& fmt,GLuint program_id, const char* uniformName = "spec_tex"    ,GLenum tagert = GL_TEXTURE_2D);
	void addNormalTexture(const GLchar* _file_name, const FREE_IMAGE_FORMAT& fmt,GLuint program_id, const char* uniformName = "normal_tex",GLenum tagert = GL_TEXTURE_2D);
	void addAOTexture(const GLchar* _file_name, const FREE_IMAGE_FORMAT& fmt,GLuint program_id, const char* uniformName = "ao_tex"        ,GLenum tagert = GL_TEXTURE_2D);

	void addDynamicColorTexture(uint width, 
		uint height,
		GLuint program_id, 
		const char* uniformName = "color_tex" ,
		GLenum tagert = GL_TEXTURE_2D,
		GLenum internalFormat = GL_RGBA,
		GLenum format = GL_BGRA,
		GLenum type = GL_UNSIGNED_BYTE
		);

	void addDynamicDepthMap(uint width,
		uint height,
		GLuint program_id,
		const char* uniformName = "ar_depth_map");


	void bindColorTexture(GLuint uniformLocation);
	void bindAlphaTexture(GLuint uniformLocation);
	void bindSpecTexture(GLuint uniformLocation);
	void bindNormalTexture(GLuint uniformLocation);
	void bindAOTexture(GLuint uniformLocation);
	void bindNoiseTexture(GLuint uniformLocation);

	//pass in the picture file name in
	//the order specified
	void addCubeMap(const char* pos_x, 
		const char* neg_x,
		const char* pos_y,
		const char* neg_y,
		const char* pos_z,
		const char* neg_z,
		const FREE_IMAGE_FORMAT& fmt,
		GLuint program_id);

	GLuint getColorTexUnit() const 
	{
		return color_tex_unit;
	}

	GLuint getAlphaTexUnit() const 
	{
		return alpha_tex_unit;
	}

	GLuint getNormalTexUnit() const 
	{
		return normal_tex_unit;
	}

	GLuint getAoTexUnit() const 
	{
		return ao_tex_unit;
	}

	GLuint getSpecTexUnit() const 
	{
		return color_tex_unit;
	}


	void addNoiseTexture(const int& width = 128, 
		const int& height = 128, 
		const float& frequency = 1.0f, 
		const float& scale = 2.0f);

	static void genTerrian(const GLuint& noise_map_id,
		Neumont::ShapeData* data,
		const int& width = 128,
		const int& height = 128);

	void shutDown()
	{
		if(textureCount > 0)
		{
			textureCount --;
		}
	}

	static void reset()
	{
		textureCount = 1;
	}
};


