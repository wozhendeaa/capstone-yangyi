#pragma once
#include <glm\glm.hpp>
#include <cstdlib>
#include "ExportHeader.h"

class Random
{
public:
	ENGINE_SHARED static float RandomFloat();
	ENGINE_SHARED static glm::vec2 RandomUnitVector();
	ENGINE_SHARED static float RandomInRange(float max, float min);
};
