﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjParser
{
    public class ObjManager
    {
        private List<Vec3> positions = new List<Vec3>();
        private List<Vec3> normals = new List<Vec3>();
        private List<Vec2> uvs = new List<Vec2>();

        //private List<int> vertIndices = new List<int>();
        //private List<int> uvIndices = new List<int>();
        //private List<int> normIndices = new List<int>();

        private List<Vertex> vertices = new List<Vertex>();
        private List<uint> indices = new List<uint>();

        bool isTriangle = true;

        public void ReadFile(string path, bool isTriangle = true)
        {
            StreamReader reader = new StreamReader(path);
            this.isTriangle = isTriangle;
 
            string line;
            while ((line = reader.ReadLine()) != null)
            {
                string[] lineParts = line.Split();
                
                AddPosition(lineParts);
                AddNormal(lineParts);
                AddUv(lineParts);
                AddIndex(lineParts);
            }

            reader.Close();
        }

        private void AddPosition(string[] parts)
        {
            if (parts[0].Equals("v"))
            {
                Vec3 position = new Vec3()
                {
                    X = float.Parse(parts[1]),
                    Y = float.Parse(parts[2]),
                    Z = float.Parse(parts[3]),
                };

                positions.Add(position);
            }
        }

        private void AddNormal(string[] parts)
        {
            if (parts[0].Equals("vn"))
            {
                Vec3 normal = new Vec3()
                {
                    X = float.Parse(parts[1]),
                    Y = float.Parse(parts[2]),
                    Z = float.Parse(parts[3]),
                };

                normals.Add(normal);
            }
        }

        private void AddUv(string[] parts)
        {
            if (parts[0].Equals("vt"))
            {
                Vec3 uv = new Vec3()
                {
                    X = float.Parse(parts[1]),
                    Y = float.Parse(parts[2]),
                };

                uvs.Add(uv);
            }
        }

        private void AddIndex(string[] parts)
        {
            if (parts[0].Equals("f"))
            {
                for (int i = 1; i < 4; i++)
			    {
                    var part = parts[i].Split('/');
                    var posIndex = int.Parse(part[0]);
                    var texIndex = int.Parse(part[1]);
                    var normIndex = int.Parse(part[2]);

                    var position = positions[posIndex - 1];
                    var uv = uvs[texIndex - 1];
                    var normal = normals[normIndex - 1];
                    var vert = new Vertex() { Position = position, UV = uv, Normal = normal };
                    AddVertAndIndex(vert);
			    }
            }
        }

        private void AddVertAndIndex(Vertex vert)
        {
            var index = vertices.IndexOf(vert);

            if (index == -1)
            {
                vertices.Add(vert);
                indices.Add((uint)(vertices.Count - 1));
            }
            else
            {
                indices.Add((uint)index);
            }
        }

        
        public void WriteToBinary(string path)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(path, FileMode.OpenOrCreate)))
            {
                //write headers 
                WriteNumToBinary(writer);

                //write verts
                foreach (var vert in vertices)
			    {
                    WriteVertToBinary(writer, vert);
			    }

                //write indices
                foreach (var i in indices)
                {
                    WriteIndexToBinary(writer, i);
                }                
            }
        }

        private void WriteVertToBinary(BinaryWriter bw, Vertex vert)
        {
            bw.Write(vert.Position.X);
            bw.Write(vert.Position.Y);
            bw.Write(vert.Position.Z);

            bw.Write(vert.UV.X);
            bw.Write(vert.UV.Y);

            bw.Write(vert.Normal.X);
            bw.Write(vert.Normal.Y);
            bw.Write(vert.Normal.Z);

        }

        private void WriteIndexToBinary(BinaryWriter bw, uint index)
        {
            bw.Write(index);
        }

        private void WriteNumToBinary(BinaryWriter bw)
        {
            bw.Write(vertices.Count);
            bw.Write(indices.Count);          
        }

        public void WriteToText(string path)
        {
            using (StreamWriter writer = new StreamWriter(File.Open(path, FileMode.OpenOrCreate)))
            {
                //write headers 
                WriteNumToBinary(writer);

                //write verts
                foreach (var vert in vertices)
                {
                    WriteVertToBinary(writer, vert);
                    writer.WriteLine();

                }

                //write indices
                foreach (var i in indices)
                {
                    WriteIndexToBinary(writer, i);
                    writer.WriteLine();
                }
            }
        }

        private void WriteVertToBinary(StreamWriter bw, Vertex vert)
        {
            bw.Write(vert.Position.X);
            bw.Write(vert.Position.Y);
            bw.Write(vert.Position.Z);

            bw.Write(vert.Normal.X);
            bw.Write(vert.Normal.Y);
            bw.Write(vert.Normal.Z);

            bw.Write(vert.UV.X);
            bw.Write(vert.UV.Y);

        }

        private void WriteIndexToBinary(StreamWriter bw, uint index)
        {
            bw.Write(index);
        }

        private void WriteNumToBinary(StreamWriter bw)
        {
            bw.Write(vertices.Count);
            bw.WriteLine();

            bw.Write(indices.Count);
            bw.WriteLine();
        }



    }
}
