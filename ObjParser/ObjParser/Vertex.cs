﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjParser
{
    public class Vertex
    {
        public Vec3 Position { get; set; }
        public Vec3 Normal { get; set; }
        public Vec2 UV { get; set; }

        public override bool Equals(object obj)
        {
            var v = obj as Vertex;
            return Position.Equals(v.Position) &&
                    Normal.Equals(v.Normal) &&
                    UV.Equals(v.UV);
        }
    }

    public class Vec2
    {
       public float X, Y;

        public override bool Equals(object obj)
        {
            var v = obj as Vec2;
            return Math.Abs(X - v.X) < float.Epsilon && Math.Abs(Y - v.Y) < float.Epsilon;
        }
    }

    public class Vec3 : Vec2
    {
        public float Z;

        public override bool Equals(object obj)
        {
            var v = obj as Vec3;
            return base.Equals(obj) && Math.Abs(Z - v.Z) < float.Epsilon;
        }
    }

    public class Vec4 : Vec3
    {
        public float W;

        public override bool Equals(object obj)
        {
            var v = obj as Vec4;
            return base.Equals(obj) && Math.Abs(W - v.W) < float.Epsilon;
        }
    }
}
