#ifndef NEUMONT_VERTEX_H
#define NEUMONT_VERTEX_H
#include "TypeDefs.h"
#include "ExportImportHeader.h"
#include "glm\glm.hpp"
#include "ExportHeader.h"
#define BUFFER_OFFSET(i) ((char *)NULL + (i))

using glm::vec2;
using glm::vec3;
using glm::vec4;

namespace Neumont
{
	struct DLL_SHARED Vertex
	{
		vec3 position;
		vec4 color;
		vec3 normal;
		vec2 uv;
		static uint POSITION_OFFSET;
		static uint COLOR_OFFSET;
		static uint NORMAL_OFFSET;
		static uint UV_OFFSET;
		static uint STRIDE;
	};

	struct ENGINE_SHARED VertexV2
	{
		vec3 position;
		vec4 color;
		vec3 normal;
		vec2 uv;
		vec3 tangent;

		static uint POSITION_OFFSET;
		static uint COLOR_OFFSET;
		static uint NORMAL_OFFSET;
		static uint UV_OFFSET;
		static uint STRIDE;
		static uint TANGENT_OFFSET;
	};

};

	

#endif